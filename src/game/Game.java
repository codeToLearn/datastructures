package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import game.board.Cell;
import game.board.Empty;
import game.board.Hotel;
import game.board.Jail;
import game.board.Lottery;
import game.entities.Bank;
import game.entities.Dice;
import game.entities.Owner;
import game.entities.Player;

public class Game {

	Queue<Player> players=new LinkedList<Player>();
	List<Cell> grid=new ArrayList<Cell>();
	List<Owner> ownerDetails=new ArrayList<Owner>();
	Dice dice;
	Bank bank=new Bank();

	public Game(List<Player> players,Dice dice) {
		// TODO Auto-generated constructor stub
		this.players.addAll(players);
		addGrid(Arrays.asList("J","H","L","H","E","L","H","L","H","J"));
		this.dice=dice;
	}

	public boolean takeTurn() {
		Integer number=dice.getRandom();
		if(number==null)
			return false;
		Player p=this.players.poll();
		this.players.add(p);

		int index=(p.getCurrentLocation()+number);
		index=index%this.grid.size();
		this.grid.get(index).doAction(p, bank,ownerDetails.get(index));
		p.setCurrentLocation(index);
		p.incrementTurns();
		return true;
	}
	
	public void addGrid(List<String> cells) {
		for (int i = 0; i < cells.size(); i++) {
			ownerDetails.add(new Owner());
			switch (cells.get(i)) {
			case "L":
				grid.add(new Lottery(""));
				break;
			case "H":
				grid.add(new Hotel(""));
				break;
			case "E":
				grid.add(new Empty(""));
				break;
			case "J":
				grid.add(new Jail(""));
				break;
			}
		}
	}

	public void displayResults(Game game) {
		Map<String, Double> wealthEvaluation=new HashMap<String, Double>();
		for(int i=0;i<game.ownerDetails.size();i++) {
			Owner g=game.ownerDetails.get(i);
			if(g.owner==null)
				continue;
			if(wealthEvaluation.containsKey(g.owner.getName()))
				wealthEvaluation.put(g.owner.getName(), game.grid.get(i).getCost()+wealthEvaluation.get(g.owner.getName()));
			else wealthEvaluation.put(g.owner.getName(), game.grid.get(i).getCost());
		}
		System.out.println("Bank Balance:"+game.bank.getCashAvailable());
		for(Player p:game.players) 
			System.out.println(p.getName()+" "+p.getAmount()+" assets"+(wealthEvaluation.get(p.getName())==null?"0":wealthEvaluation.get(p.getName())));
	}
}
