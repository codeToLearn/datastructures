package game.enums;

public enum HotelType {
	SILVER(200,50),GOLD(300,150),PLATINUM(500,300);

	private double cost;
	private double rent;
	
	private HotelType(double cost, double rent) {
		this.cost = cost;
		this.rent = rent;
	}

	public double getCost() {
		return cost;
	}

	public double getRent() {
		return rent;
	}
	
	

}
