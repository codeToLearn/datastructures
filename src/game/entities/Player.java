package game.entities;

public class Player {

	private String name;
	private double amount=1000;
	private int turns;
	private int currentLocation;
	
	public Player(String name) {
		super();
		this.name = name;
		turns=0;
		this.currentLocation=-1;
	}
	
	public boolean deductAmount(double amount) {
		if(this.amount<amount)
			return false;
		this.amount-=amount;
		return true;
	}
	
	public void addAmount(double amount) {
		this.amount+=amount;
	}

	public double getAmount() {
		return amount;
	}
	
	public void incrementTurns() {
		turns++;
	}
	
	public String getName() {
		return name;
	}

	public int getTurns() {
		return turns;
	}

	public int getCurrentLocation() {
		return currentLocation;
	}
	
	

	public void setCurrentLocation(int currentLocation) {
		this.currentLocation = currentLocation;
	}

	public boolean equals(Player p) {
		// TODO Auto-generated method stub
		return name.equals(p.name);
	}
	
}
