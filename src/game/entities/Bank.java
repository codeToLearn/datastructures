package game.entities;

import java.util.List;

public class Bank {
	private double cashAvailable=5000;
	
	public boolean deductAmount(double amount) {
		if(cashAvailable<amount)
			return false;
		cashAvailable-=amount;
		return true;
	}
	
	public void addAmount(double amount) {
		cashAvailable+=amount;
	}

	public double getCashAvailable() {
		return cashAvailable;
	}
	
	
}
