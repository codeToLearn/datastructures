package game.entities;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

public class Dice {
	private final int maxLimit=12;
	Queue<Integer> rolls=new LinkedList<Integer>();
	
	public Dice(List<Integer> rolls) {
		// TODO Auto-generated constructor stub
		this.rolls.addAll(rolls);
	}
	
	public Integer getRandom() {
		return rolls.poll();
	}
}
