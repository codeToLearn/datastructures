package game.entities;

public class Owner {
	public boolean isOwned;
	public Player owner;
	
	public Owner() {
		super();
		this.isOwned = false;
		this.owner = null;
	}
	
	public boolean isOwned() {
		return isOwned;
	}
	public void setOwned(boolean isOwned) {
		this.isOwned = isOwned;
	}
	public Player getOwner() {
		return owner;
	}
	public void setOwner(Player owner) {
		this.owner = owner;
	}
	
	
}
