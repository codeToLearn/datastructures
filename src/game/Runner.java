package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import game.entities.Dice;
import game.entities.Player;

public class Runner {
	public static void main(String[] args) {
		List<Player> players=new ArrayList<Player>();
		Player p1=new Player("player 1");
		Player p2=new Player("player 2");
		Player p3=new Player("player 3");
		players.add(p1);players.add(p2);players.add(p3);
		
		Dice dice=new Dice(Arrays.asList(2,2,1,4,4,2,4,4,2,2,2,1,4,4,2,4,4,2,2,2,1));
		Game game=new Game(players,dice);

		while(game.takeTurn()) {
		}
		game.displayResults(game);
	}
	
}
