package game.board;

import game.entities.Bank;
import game.entities.Owner;
import game.entities.Player;

public class Lottery extends Cell{

	private final int lotteryAmount=200;
	public Lottery(String image) {
		super(image);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doAction(Player player, Bank bank,Owner o) {
		// TODO Auto-generated method stub
		if(bank.deductAmount(lotteryAmount))
			player.addAmount(lotteryAmount);
		else System.out.println("Bank is Out of Money . Match Ends");
	}

}
