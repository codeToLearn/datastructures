package game.board;

import game.entities.Bank;
import game.entities.Owner;
import game.entities.Player;
import game.enums.HotelType;

public class Hotel extends Cell{

	private HotelType type;
	public Hotel(String image) {
		super(image);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doAction(Player p, Bank b,Owner o) {
		// TODO Auto-generated method stub
		if(!o.isOwned)
		{
			//if this is not owned
			if(p.deductAmount(HotelType.SILVER.getCost())) {
				o.isOwned=true;
				o.owner=p;
				this.type=HotelType.SILVER;
				b.addAmount(HotelType.SILVER.getCost());
			}
		}else if(o.owner.equals(p)){
			//if it is owned
			double amount;
			switch (this.type) {
			case SILVER:
				amount=HotelType.GOLD.getCost()-HotelType.SILVER.getCost();
				if(p.deductAmount(amount)) {
					this.type=HotelType.GOLD;
					b.addAmount(amount);
				}
				break;
			case GOLD:
				amount=HotelType.PLATINUM.getCost()-HotelType.GOLD.getCost();
				if(p.deductAmount(amount)) {
					this.type=HotelType.PLATINUM;
					b.addAmount(amount);
				}
				break;
			default:
				break;
			}
		}else {
			//if it is owned by somebody else pay the rent
			if(p.deductAmount(this.type.getRent()))
				o.owner.addAmount(this.type.getRent());
			else System.out.println("Player "+p.getName()+" is out of money ...LOSt");
		}
		
	}
	
	@Override
	public double getCost() {
		// TODO Auto-generated method stub
		return this.type.getCost();
	}

}
