package game.board;

import game.entities.Bank;
import game.entities.Owner;
import game.entities.Player;

public abstract class Cell {

	public String image;
	
	public Cell(String image) {
		super();
		this.image = image;
	}

	public abstract void doAction(Player p,Bank b,Owner o);
	
	public double getCost() {
		return 0;
	}
}

