package game.board;

import game.entities.Bank;
import game.entities.Owner;
import game.entities.Player;

public class Jail extends Cell{

	private final int fineAmount=150;
	public Jail(String image) {
		super(image);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doAction(Player player,Bank bank,Owner o) {
		// TODO Auto-generated method stub
		if(player.deductAmount(fineAmount))
			bank.addAmount(fineAmount);
		else System.out.println("Player is Out of Funds .... Player Lost");
	}

	
}
