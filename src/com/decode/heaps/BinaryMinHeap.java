package com.decode.heaps;

import java.util.Arrays;
import java.util.NoSuchElementException;

/*
 * Priority Queue ADT (Abstract Data Type)

The ADT's Fundamental API

isEmpty()
insertWithPriority()
pullHighestPriorityElement()
peek() (which is basically findMax() or findMin()) is O(1) in time complexity and this is crucial.

A heap is one maximally efficient implementation of a priority queue.

We can have:
-) a min heap: min element can be peeked in constant time.
-) or a max heap: max element can be peeked in constant time.

The name of the heap indicates what we can peek in O(1) time.

It does not matter how we implement this as long as we support the expected behaviors of the ADT thus giving our caller what they want from our heap data structure.

A binary heap is a complete binary tree with a total ordering property hence making it a heap with O(1) peek time to the min or max element.

We can implement the heap with actual nodes or we can just use an array and use arithmetic to know who is a parent or left or right child of a specific index.

Insertion into a binary heap:

We insert the item to the "end" of the complete binary tree (bottom right of the tree).

We "bubble up" the item to restore the heap. We keep bubbling up while there is a parent to compare against and that parent is greater than (in the case of a min heap) or less than (in the case of a max heap) the item. In those cases, the item we are bubbling up dominates its parent.

Removal from a binary heap:

We give the caller the item at the top of the heap and place the item at the "end" of the heap at the very "top".

We then "bubble the item down" to restore the heap property.

Min Heap: We keep swapping the value with the smallest child if the smallest child is less than the value we are bubbling down.

Max Heap: We keep swapping the value with the largest child if the largest child is greater than the value we are bubbling down.

In this manner, we restore the heap ordering property.

The critical thing is that we can have O(1) knowledge of the min or max of a set of data, whenever you see a problem dealing with sizes think of a min or max heap.

 */

/*
 * We are going to implement binary min heap
 * Heap is a data structure which is used for getting elements.
 * Priority queue using heap for its implementation.
 * Heap is tree data structure which is complete binary tree and we are using array for heap
 */
public class BinaryMinHeap {

	private int defaultCapacity = 5;
	private int size = 0; // this represents the index
	private int[] heap;

	public BinaryMinHeap() {
		heap = new int[defaultCapacity];
	}

	public BinaryMinHeap(int capacity) {
		defaultCapacity = capacity;
		heap = new int[defaultCapacity];
	}

	public BinaryMinHeap(int[] heapItems) {
		heap = new int[defaultCapacity];
		for (int i = 0; i < heapItems.length; i++) {
			add(heapItems[i]);
		}
	}
	public boolean isEmpty() {
		return size <= 0;
	}

	public int peek() {
		if (size == 0) {
			throw new NoSuchElementException("Heap is empty");
		}
		return heap[0];
	}

	/*
	 * When we are inserting a new element into heap will first check size of heap
	 */
	public void add(int item) {
		// check size
		ensureExtraCapacity();
		// insert at the end of complete binary tree
		heap[size] = item;
		size++;

		// bubble up the element check is it required to bubble up the item
		bubbleUp();
	}

	private void bubbleUp() {
		if (size >= 1) {
			int childIndex = size - 1;

			while (hasParent(childIndex) && getParent(childIndex) > heap[childIndex]) {
				swap(getParentIndex(childIndex), childIndex);
				childIndex = getParentIndex(childIndex);
			}
		}

	}

	public int remove() {
		if (isEmpty()) {
			throw new NoSuchElementException("Heap is empty.");
		}
		// remove first element
		int minItem = heap[0];
		heap[0] = heap[size - 1];
		size--;
		bubbleDown();
		return minItem;
	}

	private void bubbleDown() {

		// check for left
		int index = 0;
		/*
		 * Since binary tree is complete binary tree therefore if no left exist then no
		 * right will be there
		 */
		while (hasleft(index)) {
			int smallerIndex = getLeftIndex(index);
			if (hasRight(index) && getRightChild(index) < getLeftChild(index)) {
				smallerIndex = getRightIndex(index);
			}
			if (heap[smallerIndex] < heap[index]) {
				swap(smallerIndex, index);
				// Move to node we just swap
				index = smallerIndex;
			} else {
				break;
			}
		}
	}

	private boolean hasleft(int index) {
		return getLeftIndex(index) < size;
	}

	private int getLeftIndex(int parentIndex) {
		return 2 * (parentIndex) + 1;
	}

	private int getLeftChild(int parentIndex) {
		return heap[getLeftIndex(parentIndex)];
	}

	private boolean hasRight(int index) {
		return getRightIndex(index) < size;
	}

	private int getRightIndex(int parentIndex) {
		return 2 * (parentIndex) + 2;
	}

	private int getRightChild(int parentIndex) {
		return heap[getRightIndex(parentIndex)];
	}

	// This will swap two numbers
	private void swap(int indexOne, int indexTwo) {
		int temp = heap[indexOne];
		heap[indexOne] = heap[indexTwo];
		heap[indexTwo] = temp;
	}

	private boolean hasParent(int index) {
		return index >= 0 && getParentIndex(index) >= 0;
	}

	/*
	 * This will give parent index
	 */
	private int getParentIndex(int childIndex) {
		return (childIndex - 1) / 2;
	}

	private int getParent(int childIndex) {
		return heap[getParentIndex(childIndex)];
	}

	private void ensureExtraCapacity() {
		if (size == defaultCapacity) {
			heap = Arrays.copyOf(heap, defaultCapacity * 2);
			defaultCapacity *= 2;

		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (int i = 0; i < size; i++) {
			builder.append(heap[i]).append(",");
		}
		builder.append("]");
		return builder.toString();
	}

	public static void main(String[] args) {

		BinaryMinHeap binaryHeap = new BinaryMinHeap();
		binaryHeap.add(10);// insert 10
		binaryHeap.add(4);
		System.out.println(binaryHeap);
		binaryHeap.add(15);
		System.out.println(binaryHeap);
		binaryHeap.remove();
		System.out.println(binaryHeap);
		binaryHeap.add(20);
		System.out.println(binaryHeap);
		binaryHeap.add(30);
		System.out.println(binaryHeap);
		binaryHeap.remove();
		System.out.println(binaryHeap);
		binaryHeap.add(2);
		System.out.println(binaryHeap);
		binaryHeap.add(4);
		System.out.println(binaryHeap);
		binaryHeap.add(-1);
		System.out.println(binaryHeap);
		binaryHeap.add(-3);
		System.out.println(binaryHeap);
	}
}
