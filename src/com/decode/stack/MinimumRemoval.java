package com.decode.stack;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

/**
 * 
 * @author Sachin Jindal Given a string s of '(' , ')' and lowercase English
 *         characters.
 * 
 *         Your task is to remove the minimum number of parentheses ( '(' or
 *         ')', in any positions ) so that the resulting parentheses string is
 *         valid and return any valid string.
 * 
 *         Formally, a parentheses string is valid if and only if:
 * 
 *         It is the empty string, contains only lowercase characters, or It can
 *         be written as AB (A concatenated with B), where A and B are valid
 *         strings, or It can be written as (A), where A is a valid string.
 * 
 * 
 *         Example 1:
 * 
 *         Input: s = "lee(t(c)o)de)" Output: "lee(t(c)o)de" Explanation:
 *         "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted. Example 2:
 * 
 *         Input: s = "a)b(c)d" Output: "ab(c)d" Example 3:
 * 
 *         Input: s = "))((" Output: "" Explanation: An empty string is also
 *         valid. Example 4:
 * 
 *         Input: s = "(a(b(c)d)" Output: "a(b(c)d)"
 */
public class MinimumRemoval {
	public static void main(String[] args) {
		MinimumRemoval minimumRemoval = new MinimumRemoval();
		minimumRemoval.minRemoveToMakeValid("))((");
	}

	public String minRemoveToMakeValid(String s) {
		if (s == null || s.isEmpty()) {
			return s;
		}
		Stack<Integer> intParn = new Stack<>();
		Set<Integer> set = new HashSet();
		char[] charArray = s.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			if (charArray[i] == '(') {
				intParn.push(i);
			} else if (charArray[i] == ')') {
				if (intParn.isEmpty()) {
					set.add(i);
				} else {
					intParn.pop();
				}
			}
		}

		while (!intParn.isEmpty()) {
			set.add(intParn.pop());
		}
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < charArray.length; i++) {
			if (!set.contains(i)) {
				builder.append(s.charAt(i));
			}
		}
		return builder.toString();
	}
}
