package com.decode.graph;

public class SurrondedRegion {

	public void solve(char[][] board) {
		if (board.length == 0 || board[0].length == 0) {
			return;
		}
		int rowL = board.length;
		int colL = board[0].length;
		// first row and last row
		for (int i = 0; i < colL; i++) {
			dfs(board, 0, i);
			dfs(board, rowL - 1, i);
		}
		// first col and last col
		for (int i = 0; i < rowL; i++) {
			dfs(board, i, 0);
			dfs(board, i, colL - 1);
		}

	}

	private void dfs(char[][] board, int row, int col) {
		if (row == board.length || row < 0 || col < 0 || col == board.length || board[row][col] != 'O') {
			return;
		}
		// check for every border case i.e first row and col and last row ,col

		board[row][col] = 'P';
		dfs(board, row + 1, col);
		dfs(board, row - 1, col);
		dfs(board, row, col + 1);
		dfs(board, row, col - 1);
	}
}
