package com.decode.graph;

public class GameOfLife {
	int moves[][] = { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 1 }, { 1, -1 }, { -1, 1 }, { -1, -1 } };

	public static void main(String[] args) {

		GameOfLife gameOfLife = new GameOfLife();
		int[][] board = { { 0, 1, 0 }, { 0, 0, 1 }, { 1, 1, 1 }, { 0, 0, 0 } };
		gameOfLife.gameOfLife(board);
	}

	public void gameOfLife(int[][] board) {
		// idea is very simple
		int m = board.length;
		int n = board[0].length;
		for (int i = 0; i < m; i++) {
			
			for (int j = 0; j < n; j++) {
				int alive = 0;
				for (int k = 0; k < moves.length; k++) {
					if (!valid(i + moves[k][0], j + moves[k][1], board)) {
						if (board[i + moves[k][0]][j + moves[k][1]] == 1
								|| board[i + moves[k][0]][j + moves[k][1]] == -1) {
							alive++;
						}
					}
				}

				if (board[i][j] == 0) {
					if (alive == 3) {
						board[i][j] = -2;
					}
				} else {
					if (alive > 3 || alive < 2) {
						board[i][j] = -1;
					}
				}
			}
		}
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (board[i][j] == -1) {
					board[i][j] = 0;
				} else if (board[i][j] == -2) {
					board[i][j] = 1;
				}
			}
		}
	}

	private boolean valid(int m, int n, int[][] board) {
		return m < 0 || n < 0 || m == board.length || n == board[0].length;
	}
}
