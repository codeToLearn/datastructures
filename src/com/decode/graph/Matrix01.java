package com.decode.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *         Given a matrix consists of 0 and 1, find the distance of the nearest
 *         0 for each cell.
 * 
 *         The distance between two adjacent cells is 1.
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input:<br>
 *         [[0,0,0],<br>
 *         [0,1,0],<br>
 *         [0,0,0]]<br>
 * 
 *         Output:<br>
 *         [[0,0,0],<br>
 *         [0,1,0],<br>
 *         [0,0,0]]<br>
 *         Example 2:
 * 
 *         Input: [[0,0,0],<br>
 *         [0,1,0],<br>
 *         [1,1,1]]<br>
 * 
 *         Output:<br>
 *         [[0,0,0],<br>
 *         [0,1,0],<br>
 *         [1,2,1]]<br>
 */
public class Matrix01 {
	private int[][] shifts = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };// up,down,left,right

	public static void main(String[] args) {
		int[][] matrix = new int[][] { { 0, 0, 0 }, { 0, 1, 0 }, { 1, 1, 1 } };
		Matrix01 matrix01 = new Matrix01();
		matrix01.updateMatrix(matrix);
	}

	/**
	 * 
	 * @param matrix
	 *            The idea is simple make a queue level by level check distance
	 * @return
	 */
	public int[][] updateMatrix(int[][] matrix) {
		int[][] result = new int[matrix.length][matrix[0].length];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				result[i][j] = -1;
			}
		}
		/**
		 * Traverse every point and check level by level
		 */
		
		Queue<Coordinate> queue = new LinkedList<Coordinate>();
		Coordinate coordinate;
		int level = -1;
		boolean found = false;
		HashSet<Coordinate> seen = new HashSet<>();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == 0) {
					result[i][j] = 0;
				} else {
					coordinate = new Coordinate(i, j);
					queue.add(coordinate);
					level = -1;
					found = false;
					while (!queue.isEmpty()) {
						int currentSize = queue.size();
						for (int index = 0; index < currentSize; index++) {
							coordinate = queue.poll();

							seen.add(coordinate);
							if (matrix[coordinate.row][coordinate.col] == 0) {
								queue.clear();
								seen.clear();
								found = true;
								break;
							} else {
								// add its all coordinate for next level
								// Move only in four position - up,down,left,right
								for (int shift = 0; shift < 4; shift++) {
									Coordinate coordinate2 = new Coordinate(coordinate.row + shifts[shift][0],
											coordinate.col + shifts[shift][1]);
									if (!seen.contains(coordinate2) && canTraverse(coordinate2, matrix)) {
										queue.offer(coordinate2);

									}
								}
							}

						}

						level++;
					}
					result[i][j] = found ? level : -1;
				}
			}
		}
		return result;
	}

	class Coordinate {
		int row;
		int col;

		public Coordinate(int row, int col) {
			this.col = col;
			this.row = row;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			// result = prime * result + getOuterType().hashCode();
			result = prime * result + col;
			result = prime * result + row;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Coordinate other = (Coordinate) obj;
			if (col != other.col)
				return false;
			if (row != other.row)
				return false;
			return true;
		}

	}

	private boolean canTraverse(Coordinate grid, int[][] gridArray) {
		return grid.row >= 0 && grid.row < gridArray.length && grid.col >= 0 && grid.col < gridArray[grid.row].length;
	}
}
