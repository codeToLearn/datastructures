package com.decode.graph;

import java.util.ArrayList;
import java.util.List;

public class SpiralMatrix {

	public static void main(String[] args) {
		int[][] matrix = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
		SpiralMatrix matrix2 = new SpiralMatrix();
		matrix2.spiralOrder(matrix);
	}

	public List<Integer> spiralOrder(int[][] matrix) {

		List<Integer> list = new ArrayList<Integer>();
		int r1 = 0;
		int c1 = 0;
		int r2 = matrix.length;
		int c2 = matrix[0].length;
		int total = r2 * c2;
		r2--;
		c2--;

		while (list.size() != total) {
			// left to right
			for (int c = c1; c <= c2; c++) {
				list.add(matrix[r1][c]);
			}

			// up to down
			for (int r = r1 + 1; r <= r2; r++) {
				list.add(matrix[r][c2]);
			}
			// right to left
			if (r1 < r2 && c1 < c2) {
				for (int c = c2 - 1; c >= c1; c--) {
					list.add(matrix[r2][c]);
				}
				// down to up
				for (int r = r2 - 1; r > r1; r--) {
					list.add(matrix[r][c1]);
				}
			}

			r1++;
			c1++;
			r2--;
			c2--;

		}
		return list;
	}
}
