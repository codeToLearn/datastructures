package com.decode.graph;

import java.util.LinkedList;
import java.util.Queue;

public class RottenOranges {
	

	public static void main(String[] args) {
		int[][] grid = { { 2, 1, 1 }, { 0, 1, 1 }, { 1, 0, 1 } };
		RottenOranges rottenOranges = new RottenOranges();
		System.out.println(rottenOranges.orangesRotting(grid));
	}
	int shift[][] = { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 0, 1 } };
	public int orangesRotting(int[][] grid) {
		int timeElasped = 0;
		int freshCount = 0;
		// logic
		// find all the rotten oranges put it into queue
		// find the rotten oranges then process them one by one
		Queue<Grid> queue = new LinkedList<Grid>();
		for (int row = 0; row < grid.length; row++) {
			for (int col = 0; col < grid[0].length; col++) {
				if (grid[row][col] == 1) {
					freshCount++;
				}
				if (grid[row][col] == 2) {
					queue.offer(new Grid(row, col));
				}
			}
		}
		if (freshCount == 0) {
			return timeElasped;
		}
		while (!queue.isEmpty()) {
			timeElasped++;
			int size = queue.size();
			for (int i = 0; i < size; i++) {
				Grid grid2 = queue.poll();
				for (int m = 0; m < shift.length; m++) {
					Grid grid3 = new Grid(grid2.row + shift[m][0], grid2.col + shift[m][1]);
					if (validMove(grid3, grid)) {
						freshCount--;
						if (freshCount == 0) {
							return timeElasped;
						}
						grid[grid3.row][grid3.col] = 2;
						queue.offer(grid3);
					}

				}

			}
		}

		return freshCount == 0 ? timeElasped : -1;
	}

	class Grid {
		int row;
		int col;

		public Grid(int row, int col) {
			this.row = row;
			this.col = col;
		}

	}

	private boolean validMove(Grid g, int[][] grid) {
		return g.row >= 0 && g.col >= 0 && g.row < grid.length && g.col < grid[0].length && grid[g.row][g.col] == 1;
	}
}
