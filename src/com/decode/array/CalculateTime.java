package com.decode.array;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class CalculateTime {
	public static void main(String[] args) {
		String now = "23:05:38";
		String test[] = { "23:05:38", "23:05:02", "23:04:59", "23:04:31", "12:36:07", "08:59:01", "00:00:00" };
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

		for (String test1 : test) {
			try {

				Date date1 = sdf.parse(now);
				Date date2 = sdf.parse(test1);
				long difference = date2.getTime() - date1.getTime();
				if (difference < 0) {
					difference = Math.abs(difference);

				}
				difference = difference / 1000;
				int ss = (int) (difference % 60);
				int mm = (int) ((difference / 60) % 60);
				int hh = (int) ((difference / (60 * 60)) % 24);
				if (ss == 0 && mm == 0 && hh == 0) {
					System.out.println("now");
				} else {
					if (hh != 0) {
						System.out.println((hh > 1 ? hh + " hours" : hh + " hour") + " ago");
					} else if (mm != 0) {
						System.out.println((mm > 1 ? mm + " mintutes" : mm + " minute") + " ago");
					}else {
 						System.out.println((ss > 1 ? ss + " seconds" : ss + " second") + " ago");
					}
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
