package com.decode.array;

public class NextPermutation {
	public static void main(String[] args) {

	}

	public void nextPermutation(int[] nums) {

		// find index of first non increasing number
		int i = nums.length - 2;
		while (i >= 0 && nums[i + 1] <= nums[i]) {
			i--;
		}

		// find just greater element
		if (i >= 0) {
			int j = nums.length - 1;
			while (j >= 0 && nums[j] <= nums[i]) {
				j--;
			}
			// swap two number
			swap(nums, i, j);
		}
		// reverse all the numbers
		reverse(nums, i + 1);

	}

	private void reverse(int[] nums, int start) {
		int i = start, j = nums.length - 1;
		while (i < j) {
			swap(nums, i, j);
			i++;
			j--;
		}
	}

	private void swap(int[] nums, int i, int j) {
		int temp = nums[i];
		nums[i] = nums[j];
		nums[j] = temp;

	}
}