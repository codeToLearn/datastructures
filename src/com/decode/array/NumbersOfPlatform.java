package com.decode.array;

import java.util.TreeMap;

public class NumbersOfPlatform {
	public static void main(String[] args) {
		int arr[] = { 900, 910, 1000, 1100, 1200, 1300 };
		int dep[] = { 910, 920, 940, 1200, 1900, 2000 };
		NumbersOfPlatform numbersOfPlatform = new NumbersOfPlatform();
		numbersOfPlatform.numberOfPlatform(arr, dep);
	}

	public int numberOfPlatform(int[] arival, int[] departure) {

		// suppose arrival array is sorted

		TreeMap<Integer, Integer> treeMap = new TreeMap<>();
		for (int i = 0; i < arival.length; i++) {
			treeMap.put(arival[i], departure[i]);
		}
		int i = 0;
		for (Integer key : treeMap.keySet()) {
			arival[i] = key;
			departure[i] = treeMap.get(key);
			i++;
		}
		int result = 1;

		for (i = 0; i < arival.length; i++) {

			int index = findIndex(arival, departure[i], i, arival.length - 1);
			result = Math.max(result, index - i);
		}
		return result;
	}

	private int findIndex(int[] arrival, int value, int lower, int upper) {

		if (lower <= upper) {
			int mid = lower + (upper - lower) / 2;
			if (value == arrival[mid]) {
				return mid + 1;
			}
			
			if (value < arrival[mid]) {
				if (mid - 1 == lower) {
					return mid - 2;
				}
				
				if ((mid - 1 > lower && value > arrival[mid - 1])) {
					return mid - 1;
				}
				return findIndex(arrival, value, lower, mid - 1);
			} else {
				if (mid + 1 == upper) {
					return mid + 2;
				}
				if ((mid + 1 < upper && value < arrival[mid + 1])) {
					return mid + 1;
				}
				return findIndex(arrival, value, mid + 1, upper);
			}
		}
		return -1;
	}
}
