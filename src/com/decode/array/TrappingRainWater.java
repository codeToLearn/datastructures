package com.decode.array;

public class TrappingRainWater {
	public static void main(String[] args) {
		TrappingRainWater trappingRainWater = new TrappingRainWater();
		trappingRainWater.trap(new int[] { 0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1 });
	}

	public int trap(int[] height) {
		int trap = 0;
		int[] left = new int[height.length]; // maintains left highest
		int[] right = new int[height.length]; // maintains right highest
		int leftSoFar = Integer.MIN_VALUE;
		int rightSOFar = Integer.MIN_VALUE;
		for (int i = 0; i < height.length; i++) {
			leftSoFar = left[i] = Math.max(leftSoFar, height[i]);
			rightSOFar = right[height.length - i - 1] = Math.max(rightSOFar, height[height.length - i - 1]);
		}
		//

		for (int i = 0; i < height.length; i++) {
			trap += Math.min(left[i], right[i]) - height[i];
		}
		return trap;
	}

	// without space
	public int trapWS(int[] height) {
		int trap = 0;
		int left_most = 0;
		int right_most = 0;
		int left = 0;
		int right = height.length - 1;
		while (left < right) {
			if (height[left] < height[right]) {
				if (height[left] >= left_most) {
					left_most = height[left];
				} else {
					trap += left_most - height[left];
				}
				left++;
			} else {
				if (height[right] >= right_most) {
					right_most = height[right];
				} else {
					trap += right_most - height[right];
				}
				right--;
			}
		}
		return trap;
	}
}
