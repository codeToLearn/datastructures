package com.decode.array;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * 
 * @author Sachin Jindal
 *
 *         Implement a basic calculator to evaluate a simple expression string.
 * 
 *         The expression string contains only non-negative integers, +, -, *, /
 *         operators and empty spaces . The integer division should truncate
 *         toward zero.
 * 
 *         Example 1:
 * 
 *         Input: "3+2*2" Output: 7 Example 2:
 * 
 *         Input: " 3/2 " Output: 1 Example 3:
 * 
 *         Input: " 3+5 / 2 " Output: 5
 */
public class BasicCalculator {
	public static void main(String[] args) {
		BasicCalculator basicCalculator = new BasicCalculator();
		System.out.println(basicCalculator.calculate("3/2"));
	}

	public int calculate(String s) {
		if (s == null || s.trim().isEmpty()) {
			return 0;
		}
		s = s.replaceAll(" ", "");

		//
		Stack<Character> operator = new Stack<>();
		Stack<Integer> operand = new Stack<>();
		Stack<Character> temp = new Stack<>();
		char[] chars = s.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (!OPERATOR.isOperator(chars[i])) {
				operand.push(chars[i] - 48);

			} else {
				if (operator.isEmpty()) {
					operator.push(chars[i]);
				} else {
					// check precedence
					if (OPERATOR.greaterPre(chars[i], operator.peek())) {
						operand.push(OPERATOR.map.get(chars[i]).operation(operand.pop(), chars[++i] - 48));

					} else {
						while (!operator.isEmpty() && OPERATOR.greaterPre(operator.peek(), chars[i])) {
							temp.push(operator.pop());
						}
						operator.push(chars[i]);
						while (!temp.isEmpty()) {
							operator.push(temp.pop());
						}
					}

				}
			}

		}

		while (!operator.isEmpty()) {
			int a = operand.pop();
			int b = operand.pop();
			if (operator.peek() == '/') {

				operand.push(OPERATOR.map.get(operator.pop()).operation(b, a));
			} else {
				operand.push(OPERATOR.map.get(operator.pop()).operation(a, b));
			}

		}
		return operand.pop();

	}
}

enum OPERATOR {
	DIVIDE(1, '/') {
		@Override
		public int operation(int a, int b) {
			// TODO Auto-generated method stub
			return a / b;
		}
	},
	MUL(2, '*') {
		@Override
		public int operation(int a, int b) {
			// TODO Auto-generated method stub
			return a * b;
		}
	},
	PLUS(3, '+') {
		@Override
		public int operation(int a, int b) {
			// TODO Auto-generated method stub
			return a + b;
		}
	},
	SUB(4, '-') {
		@Override
		public int operation(int a, int b) {
			// TODO Auto-generated method stub
			return a - b;
		}
	};
	private OPERATOR(int i, char op) {
		this.i = i;
		this.op = op;
	}

	public static Map<Character, OPERATOR> map = new HashMap<Character, OPERATOR>();
	static {
		for (OPERATOR operator : values()) {
			map.put(operator.op, operator);
		}
	}
	int i;
	char op;

	public abstract int operation(int a, int b);

	public static boolean isOperator(char c) {
		return map.containsKey(c);
	}

	public static boolean greaterPre(char a, char b) {
		return map.get(a).i < map.get(b).i;
	}
}
