package com.decode.june.week5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordSearchII {
	private TrieNode root = new TrieNode('#');
	ArrayList<String> result;

	public List<String> findWords(char[][] board, String[] words) {
		result = new ArrayList<>();
		if (words != null && words.length != 0) {
			for (int i = 0; i < words.length; i++) {
				root.insert(words[i]);
			}
			boolean[][] vistited = new boolean[board.length][board[0].length];
			for (int row = 0; row < board.length; row++) {
				for (int col = 0; col < board[row].length; col++) {
					wordMatch(row, col, board, vistited, root);
				}
			}
		}
		return result;
	}

	private int[][] moves = { { 0, 1 }, { 1, 0 }, { -1, 0 }, { 0, -1 } };

	private void wordMatch(int row, int col, char[][] board, boolean[][] vistited, TrieNode root) {
		// base cases
		if (root.endWord) {
			root.endWord = false;
			result.add(root.word);
		}
		if (row < 0 || col < 0 || row == board.length || col == board[0].length
				|| root.adjacents.get(board[row][col]) == null || vistited[row][col]) {
			return;
		}
		vistited[row][col] = true;
		for (int i = 0; i < moves.length; i++) {
			wordMatch(row + moves[i][0], col + moves[i][1], board, vistited, root.adjacents.get(board[row][col]));
		}
		vistited[row][col] = false;
	}

	class TrieNode {
		char character;
		Map<Character, TrieNode> adjacents;
		boolean endWord; // represents where will be word ended
		String word;

		public TrieNode(char character) {
			this.character = character;
			adjacents = new HashMap<Character, TrieNode>();
			endWord = false;
		}

		public void insert(String word) {
			if (word == null || word.isEmpty()) {
				return;
			}
			TrieNode currentNode = root;
			char chars[] = word.toCharArray();

			TrieNode newNode;
			/*
			 * We will traverse node from list and one by one word we will check
			 */
			for (int i = 0; i < chars.length; i++) {

				if (currentNode.adjacents.get(chars[i]) == null) {
					newNode = new TrieNode(chars[i]);
					currentNode.adjacents.put(chars[i], newNode);
				}
				currentNode = currentNode.adjacents.get(chars[i]);

			}
			if (currentNode.character != '#') {
				currentNode.word = word;
				currentNode.endWord = true;
			}
		}
	}

}
