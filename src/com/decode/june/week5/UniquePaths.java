package com.decode.june.week5;

public class UniquePaths {
	public static void main(String[] args) {
		UniquePaths paths = new UniquePaths();
		paths.uniquePaths(3, 2);
	}

	public int uniquePaths(int m, int n) {

		int dp[][] = new int[m][n];
		// last col
		for (int i = 0; i < m; i++) {
			dp[i][n - 1] = 1;

		}
		// last row
		for (int i = 0; i < n; i++) {
			dp[m - 1][i] = 1;

		}
		for (int row = m - 2; row >= 0; row--) {
			for (int col = n - 2; col >= 0; col--) {
				dp[row][col] = dp[row + 1][col] + dp[row][col + 1];
			}
		}
		return dp[0][0];
	}

	public int uniquePaths(int row, int col, int m, int n) {
		if (row == m - 1 && col == n - 1) {
			return 1;
		}
		if (row == m - 1) {
			return uniquePaths(row, col + 1, m, n);
		}
		if (col == n - 1) {
			return uniquePaths(row + 1, col, m, n);
		}
		return uniquePaths(row + 1, col, m, n) + uniquePaths(row, col + 1, m, n);
	}
}
