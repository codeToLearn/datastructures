package com.decode.june.week4;

import com.decode.trees.TreeNode;

public class CompleteBTCountNodes {
	 public int countNodes(TreeNode root) {
	        if(root == null){
	            return 0;
	        }
	        int left = height(root.left,true)+1;
	           int right = height(root.right,false)+1;
	        if(left == right){
	            return (1<<left)-1;
	        }
	        return countNodes(root.left) + countNodes(root.right) +1;
	    }
	    private int height(TreeNode root ,boolean left){
	        
	        if(root ==null){
	            return 0;
	        }
	        return (left ? height(root.left,left) : height(root.right,left))+1;
	    }
}
