package com.decode.june.week4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.TreeSet;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *         Given a list of airline tickets represented by pairs of departure and
 *         arrival airports [from, to], reconstruct the itinerary in order. All
 *         of the tickets belong to a man who departs from JFK. Thus, the
 *         itinerary must begin with JFK.
 * 
 *         Note:
 * 
 *         If there are multiple valid itineraries, you should return the
 *         itinerary that has the smallest lexical order when read as a single
 *         string. For example, the itinerary ["JFK", "LGA"] has a smaller
 *         lexical order than ["JFK", "LGB"]. All airports are represented by
 *         three capital letters (IATA code). You may assume all tickets form at
 *         least one valid itinerary. One must use all the tickets once and only
 *         once. Example 1:
 * 
 *         Input: [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR",
 *         "SFO"]] Output: ["JFK", "MUC", "LHR", "SFO", "SJC"] Example 2:
 * 
 *         Input:
 *         [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]
 *         Output: ["JFK","ATL","JFK","SFO","ATL","SFO"] Explanation: Another
 *         possible reconstruction is ["JFK","SFO","ATL","JFK","ATL","SFO"]. But
 *         it is larger in lexical order.
 * 
 */
public class ReconstructItinary {
	/**
	 * There are four conditions you need to take care of that. 1. Every node should
	 * be processed. 2. Node should be traverse only once. 3. If multiple then
	 * lexical order will be maintained. 4. Begin with JFK
	 */
	public List<String> findItinerary(List<List<String>> tickets) {

		Map<String, PriorityQueue<String>> graph = new HashMap<String, PriorityQueue<String>>();
		for (List<String> from : tickets) {
			if (!graph.containsKey(from.get(0))) {
				graph.put(from.get(0), new PriorityQueue<String>());
			}
			graph.get(from.get(0)).add(from.get(1));
		}
		// Now we need to maintain stack for completing this situation all node should
		// be traversed.

		Stack<String> nodes = new Stack<>();
		nodes.push("JFK");
		List<String> result = new ArrayList<String>();
		while (!nodes.isEmpty()) {
			String source = nodes.peek();

			if (graph.get(source) == null || graph.get(source).isEmpty()) {
				result.add(nodes.pop());

			} else {
				String to = graph.get(source).peek();
				graph.get(source).poll();
				nodes.push(to);
			}
		}

		Collections.reverse(result);
		return result;

	}
}
