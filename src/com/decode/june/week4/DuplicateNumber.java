package com.decode.june.week4;

public class DuplicateNumber {
	public static void main(String[] args) {
		DuplicateNumber duplicateNumber = new DuplicateNumber();
		int num = duplicateNumber.findDuplicate(new int[] { 1, 3, 4, 2, 2 });
		System.out.println(num);
		num = duplicateNumber.findDuplicatHandleZeroAlso(new int[] { 1, 3, 4, 2, 2 });
		System.out.println(num);
	}

	public int findDuplicate(int[] nums) {
		if (nums.length == 0) {
			return 0;
		}
		for (int i = 0; i < nums.length; i++) {

			if (nums[Math.abs(nums[i]) - 1] < 0) {
				return Math.abs(nums[i]);
			}
			nums[Math.abs(nums[i]) - 1] = -nums[Math.abs(nums[i]) - 1];
		}
		return 0;
	}

	public int findDuplicatHandleZeroAlso(int[] nums) {

		if (nums.length == 0) {
			return 0;
		}
		int n = nums.length;
		for (int i = 0; i < n; i++) {
			nums[(nums[i] % n) - 1] = nums[(nums[i] % n) - 1] + n;
		}
		for (int i = 0; i < n; i++) {
			if (nums[i] >= n * 2) {
				return i+1;
			}
		}
		return 0;
	}
}
