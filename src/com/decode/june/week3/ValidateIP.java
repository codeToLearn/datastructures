package com.decode.june.week3;

public class ValidateIP {
	public static void main(String[] args) {
		ValidateIP validateIP = new ValidateIP();
		validateIP.validIPAddress(":::::::");
	}

	public String validIPAddress(String IP) {

		if (IP != null && !IP.isEmpty()) {
			if (IP.contains(".")) {
				String ipv4[] = IP.split("[.]");
				if (countChar(IP, '.') == 3 && ipv4.length == 4 && validateIPV4(ipv4)) {
					return "IPv4";
				}
			} else if (IP.contains(":")) {
				String ipv6[] = IP.split("[:]");
				if (countChar(IP, ':') == 7 && ipv6.length == 8 && validateIPV6(ipv6)) {
					return "IPv6";
				}
			}
		}
		return "Neither";
	}

	private boolean validateIPV4(String IP[]) {
		for (String s : IP) {

			if (s.isEmpty() || s.length() > 3 || (s.charAt(0) == '0' && s.length() != 1)) {
				return false;
			}
			for (char ch : s.toCharArray()) {
				if (!Character.isDigit(ch))
					return false;
			}
			if (Integer.parseInt(s) > 255) {
				return false;
			}

		}
		return true;
	}

	private boolean validateIPV6(String IP[]) {
		String hexdigits = "0123456789abcdefABCDEF";

		for (String s : IP) {
			if (s.isEmpty() || s.length() > 4) {
				return false;
			}
			for (Character ch : s.toCharArray()) {
				if (hexdigits.indexOf(ch) == -1)
					return false;
			}
		}
		return true;
	}

	private int countChar(String s, char c) {
		int count = 0;
		for (char ch : s.toCharArray()) {
			if (ch == c) {
				count++;
			}
		}
		return count;
	}
}
