package com.decode.june.week3;

public class DungeonGame {
	public static void main(String[] args) {

	}

	public int calculateMinimumHP(int[][] dungeon) {
		return 0;
	}

	private int recursiveMinimumHP(int row, int col, int[][] dungeno) {

		// final destination
		if (row == dungeno.length - 1 && col == dungeno[0].length - 1) {
			return dungeno[row][col] > 0 ? 1 : 1 - dungeno[row][col];
		}
		// last row
		if (row == dungeno.length - 1) {
			return Math.max(1, (recursiveMinimumHP(row, col + 1, dungeno) - dungeno[row][col]));
		}
		// last col
		if (col == dungeno[0].length - 1) {
			return Math.max(1, (recursiveMinimumHP(row + 1, col, dungeno) - dungeno[row][col]));
		}

		return Math.max(1, Math.min((recursiveMinimumHP(row, col + 1, dungeno) - dungeno[row][col]),
				(recursiveMinimumHP(row + 1, col, dungeno) - dungeno[row][col])));
	}

	private int recursiveMinimumHPToDP(int[][] dungeno) {

		// final destination
		int dp[][] = new int[dungeno.length][dungeno[0].length];
		int row = dungeno.length - 1;
		int col = dungeno[0].length - 1;
		dp[row][col] = dungeno[row][col] > 0 ? 1 : 1 - dungeno[row][col];
		// last row
		for (col = dungeno[0].length - 2; col >= 0; col--) {
			dp[row][col] = Math.max(1, (dp[row][col + 1] - dungeno[row][col]));
		}
		col = dungeno[0].length - 1;
		// last col
		for (row = dungeno.length - 2; row >= 0; row--) {
			dp[row][col] = Math.max(1, (dp[row + 1][col] - dungeno[row][col]));
		}

		for (row = dungeno.length - 2; row >= 0; row--) {
			for (col = dungeno[0].length - 2; col >= 0; col--) {
				dp[row][col] = Math.max(1,
						Math.min((dp[row][col + 1] - dungeno[row][col]), (dp[row + 1][col] - dungeno[row][col])));
			}
		}

		return dp[0][0];
	}

}
