package com.decode.june.week3;

import java.util.ArrayList;
import java.util.List;

public class PermutationSequence {
	/**
	 * <pre>
	 * 
	 * The general idea is the following (same as other solutions):
	
	We know there are n! possible permutations for n elements.
	Enumerate them one by one
	Most solutions use the previous permutation to generate the next permutation or build it recursively.
	I had the idea to calculate the kth permutation directly from the input. Steps are as follows:
	
	Build a list of all elements in ascending order.
	The length of this list is n (i.e. not the original input size).
	Given k we know what the first element will be in the kth permutation of the current list.
	There are n groups in the lexicographical order of all permutations of the list. Inside a group each permutation's first element is the same. Each group has (n-1)! elements, so an easy k / (n-1)! will give us the index.
	Append the selected element to the result, i.e. the next element in the kth permutation.
	Remove the selected element from the list.
	Now the list has one less elements and we can repeat from Step 2 with k' = k % n!,
	that is the k'th permutation of the reduced list.
	Notice that it doesn't matter what the elements are because the indices are calculated.
	
	Examples for n = 1...4
	elements  k    indices
	[]        -    -       =----- trivial
	
	[1]       0    0       =----- reduces to [] after selecting 1
	
	[1,2]     0    0 0     =----- reduces to [2] after selecting 1
	[2,1]     1    1 0     =----- reduces to [1] after selecting 2
	
	[1,2,3]   0    0 0 0   =\____ reduces to [2,3] after selecting 1
	[1,3,2]   1    0 1 0   =/
	[2,1,3]   2    1 0 0   =\____ reduces to [1,3] after selecting 2
	[2,3,1]   3    1 1 0   =/
	[3,1,2]   4    2 0 0   =\____ reduces to [1,2] after selecting 3
	[3,2,1]   5    2 1 0   =/
	
	[1,2,3,4] 0    0 0 0 0 =\
	[1,2,4,3] 1    0 0 1 0   \
	[1,3,2,4] 2    0 1 0 0    \__ reduces to [2,3,4] after selecting 1
	[1,3,4,2] 3    0 1 1 0    /
	[1,4,2,3] 4    0 2 0 0   /
	[1,4,3,2] 5    0 2 1 0 =/
	[2,1,3,4] 6    1 0 0 0 =\
	[2,1,4,3] 7    1 0 1 0   \
	[2,3,1,4] 8    1 1 0 0    \__ reduces to [1,3,4] after selecting 2
	[2,3,4,1] 9    1 1 1 0    /
	[2,4,1,3] 10   1 2 0 0   /
	[2,4,3,1] 11   1 1 1 0 =/
	[3,1,2,4] 12   2 0 0 0 =\
	[3,1,4,2] 13   2 0 1 0   \
	[3,2,1,4] 14   2 1 0 0    \__ reduces to [1,2,4] after selecting 3
	[3,2,4,1] 15   2 1 1 0    /
	[3,4,1,2] 16   2 2 0 0   /
	[3,4,2,1] 17   2 2 1 0 =/
	[4,1,2,3] 18   3 0 0 0 =\
	[4,1,3,2] 19   3 0 1 0   \
	[4,2,1,3] 20   3 1 0 0    \__ reduces to [1,2,3] after selecting 4
	[4,2,3,1] 21   3 1 1 0    /
	[4,3,1,2] 22   3 2 0 0   /
	[4,3,2,1] 23   3 2 1 0 =/
	 * 
	 * </pre>
	 */
	public static void main(String[] args) {
PermutationSequence permutationSequence = new PermutationSequence();
System.out.println(permutationSequence.getPermutation(3, 3));
	}

	public String getPermutation(int n, int k) {

		StringBuilder builder = new StringBuilder();
		List<Integer> nums = new ArrayList<Integer>(n);

		for (int i = 1; i <= n; i++) {
			nums.add(i);
		}
		int fact = 1;
		for (int i = 2; i < n; i++) {
			fact *= i;
		}
		k=k-1;
		// fix first index
		for (int i = 1; i <n; i++) {
			if(k==0) {
				break;
			}
			builder.append(nums.remove(k / fact));
			k %= fact;
			fact /= n - i;
		}
		for(int i:nums) {
			builder.append(i);
		}
		return builder.toString();
	}
}
