package com.decode.june.week2;

public class IsSubSequence {

	public static void main(String[] args) {
		IsSubSequence isSubSequence = new IsSubSequence();
		isSubSequence.isSubsequence("abc", "ahbgdc");
	}

	public boolean isSubsequence(String s, String t) {
		if (s == null || t == null) {
			return false;
		}
		return isSubsequence(s, t, 0, 0);
	}

	private boolean isSubsequence(String s, String t, int indexT, int indexS) {

		if (indexS >= s.length()) {
			return true;
		}
		if (indexT >= t.length()) {
			return false;
		}
		if (s.charAt(indexS) == t.charAt(indexT)) {
			return isSubsequence(s, t, indexT + 1, indexS + 1);
		}
		return isSubsequence(s, t, indexT + 1, indexS);

	}
}
