package com.decode.june.week2;

public class InsertPosition {
	public static void main(String[] args) {
		int[] nums = { 1, 3,5,6 };
		InsertPosition insertPosition = new InsertPosition();
		System.out.println(insertPosition.searchInsert(nums, 7));
		;
	}

	public int searchInsert(int[] nums, int target) {
		return findIndex(nums, target, 0, nums.length - 1);
	}

	private int findIndex(int[] arrival, int value, int lower, int upper) {
		if (lower <= upper) {
			if (lower == upper) {
				if (value > arrival[lower]) {
					return lower + 1;
				}
				return lower;
			}
			int mid = lower + (upper - lower) / 2;
			if (arrival[mid] == value) {
				return mid;
			}

			if (value > arrival[mid]) {
				if (mid == upper) {
					return mid + 1;
				}
				if (mid + 1 <= upper && value < arrival[mid + 1]) {
					return mid+1;
				}
				return findIndex(arrival, value, mid + 1, upper);
			}
			if (mid - 1 >= lower && value > arrival[mid - 1]) {
				return mid;
			}
			if (mid == lower) {
				return mid;
			}
			return findIndex(arrival, value, lower, mid - 1);

		}

		return -1;
	}
}
