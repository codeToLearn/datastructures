package com.decode.june.week2;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Random;

public class RandomizedSet {
	HashMap<Integer, Integer> valueIndex;
	List<Integer> values;
	Random random;

	/** Initialize your data structure here. */
	public RandomizedSet() {
		valueIndex = new HashMap<>();
		values = new ArrayList<Integer>();
		random = new Random();
	}

	/**
	 * Inserts a value to the set. Returns true if the set did not already contain
	 * the specified element.
	 */
	public boolean insert(int val) {
		if (valueIndex.containsKey(val)) {
			return false;
		}
		values.add(val);
		valueIndex.put(val, values.size() - 1);
		return true;
	}

	/**
	 * Removes a value from the set. Returns true if the set contained the specified
	 * element.
	 */
	public boolean remove(int val) {
		if (!valueIndex.containsKey(val)) {
			return false;
		}
		int index = valueIndex.get(val);
		int lastElement = values.get(values.size() - 1);
		values.set(index, lastElement);
		valueIndex.put(lastElement, index);
		values.remove(values.size() - 1);
		valueIndex.remove(val);
		return true;
	}

	/** Get a random element from the set. */
	public int getRandom() {
		return values.get(random.nextInt(values.size()));
	}
	public static void main(String[] args) {
		RandomizedSet  randomizedSet = new RandomizedSet();
		randomizedSet.insert(1);
		randomizedSet.remove(2);
		randomizedSet.insert(2);
		System.out.println(randomizedSet.getRandom());
		randomizedSet.remove(1);
		randomizedSet.insert(2);
		randomizedSet.getRandom();
	}
}
