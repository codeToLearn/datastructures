package com.decode.june.week2;

public class SortColors {

	public static void main(String[] args) {
		SortColors colors = new SortColors();
		colors.sortColors(new int[] { 2, 0,1 });
	}

	public void sortColors(int[] nums) {

		int zero = 0, one = 0;
		int two = nums.length - 1;
		// set zero
		int index = 0;

		while (index < nums.length && one <= two) {
			if (nums[index] == 0) {

				// swap with zero
				swap(zero, index, nums);
				zero++;
				one++;
				index++;
			} else if (nums[index] == 1) {

				swap(one, index, nums);
				one++;
				index++;
			} else {
				// swap the variable with i
				swap(index, two, nums);
				two--;
			}

		}
	}

	private void swap(int first, int second, int[] nums) {
		if (first == second) {
			return;
		}

		nums[first] = nums[first] + nums[second];
		nums[second] = nums[first] - nums[second];
		nums[first] = nums[first] - nums[second];
	}
}
