package com.decode.june.week1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ReconstructionByHeight {

	public static void main(String[] args) {

	}

	public int[][] reconstructQueue(int[][] people) {

		// sort array using heights in decreasing order if same height then sort using
		// values
		Arrays.sort(people, new Comparator<int[]>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				if (o1[0] != o2[0]) {
					return o2[0] - o1[0];
				}
				return o1[1] - o2[1];
			}

		});
		// k represents number of people in front of that
		List<int[]> heights = new ArrayList<int[]>();
		for (int i = 0; i < people.length; i++) {
			heights.add(people[i][1], people[i]);
		}
		return heights.toArray(people);
	}
}
