package com.decode.june.week1;

import java.util.Arrays;
import java.util.Stack;

public class CoinChange {

	public static void main(String[] args) {
		CoinChange change = new CoinChange();
		change.changeDPOP(5, new int[] { 1, 2, 5 });
		
	}

	public int change(int amount, int[] coins) {

		if (amount == 0 && coins.length == 0)
			return 1;

		return changeRec(amount, 0, coins);
	}

	public int changeRec(int amount, int index, int[] coins) {
		if (amount == 0) {
			return 1;
		}
		if (index < coins.length) {
			if (amount < coins[index]) {
				return changeRec(amount, index + 1, coins);
			}
			return changeRec(amount - coins[index], index, coins) + changeRec(amount, index + 1, coins);
		}
		return 0;

	}

	public int changeDP(int amount, int[] coins) {
		int[][] dp = new int[coins.length + 1][amount + 1];

		//
		// fill 1 for where sum =0;
		dp[coins.length][0] = 1;
		for (int index = coins.length - 1; index >= 0; index--) {
			for (int sum = 0; sum <= amount; sum++) {
				if (sum < coins[index]) {
					dp[index][sum] = dp[index + 1][sum];
				} else {
					dp[index][sum] = dp[index][sum - coins[index]] + dp[index + 1][sum];
				}
			}
		}

		return dp[0][amount];

	}

	public int changeDPOP(int amount, int[] coins) {
		int[] dp = new int[amount + 1];

		//
		// fill 1 for where sum =0;
		dp[0] = 1;
		for (int index = coins.length - 1; index >= 0; index--) {
			for (int sum = 0; sum <= amount; sum++) {
				if (sum >= coins[index]) {
					dp[sum] = dp[sum] + dp[sum - coins[index]];
				}
			}
		}
		return dp[amount];

	}
}
