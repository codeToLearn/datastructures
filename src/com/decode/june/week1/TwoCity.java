package com.decode.june.week1;

import java.util.PriorityQueue;

public class TwoCity {
	public static void main(String[] args) {
		int[][] city = new int[][] { { 10, 20 }, { 30, 200 }, { 400, 50 }, { 30, 20 } };
		TwoCity city2 = new TwoCity();
		city2.twoCitySchedCost(city);
	}

	public int twoCitySchedCost(int[][] costs) {

		class City implements Comparable<City> {
			int a;
			int b;

			public City(int a, int b) {
				this.a = a;
				this.b = b;
			}

			@Override
			public int compareTo(City o) {
				// TODO Auto-generated method stub
				return ((Integer) (o.b - this.b)).compareTo(o.a - this.a);
			}

		}
		PriorityQueue<City> po = new PriorityQueue<>();
		for (int i = 0; i < costs.length; i++) {
			City city = new City(costs[i][0], costs[i][1]);
			po.offer(city);
		}

		int totalCost = 0;
		for (int i = 0; i < costs.length / 2; i++) {
			totalCost += po.poll().a;
		}
		for (int i = costs.length / 2; i < costs.length; i++) {
			totalCost += po.poll().b;
		}
		return totalCost;
	}
}
