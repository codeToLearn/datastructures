package com.decode.june.week1;

import java.util.LinkedList;
import java.util.Queue;

import com.decode.trees.TreeNode;

/**
 * 
 * @author Sachin Jindal
 *
 *         <pre>
 *Invert a binary tree.

Example:

Input:

     4
   /   \
  2     7
 / \   / \
1   3 6   9
Output:

     4
   /   \
  7     2
 / \   / \
9   6 3   1
 * 
 *         </pre>
 */
public class InvertBinaryTree {

	public static void main(String[] args) {
		TreeNode treeNode = TreeNode.stringToTreeNode("[4,2,7,1,3,6,9]");
		InvertBinaryTree binaryTree = new InvertBinaryTree();
		binaryTree.invertTree(treeNode);
	}

	public TreeNode invertTree(TreeNode root) {
		if (root == null) {
			return root;
		}

		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.offer(root);
		while (!queue.isEmpty()) {

			TreeNode node = queue.poll();
			TreeNode temp = node.left;
			node.left = node.right;
			node.right = temp;
			if (node.left != null) {
				queue.offer(node.left);
			}
			if (node.right != null) {
				queue.offer(node.right);
			}

		}
		return root;
	}

	private TreeNode invertTreeRecursive(TreeNode node) {
		if (node == null) {
			return node;
		}
		TreeNode right = invertTreeRecursive(node.right);
		TreeNode left = invertTreeRecursive(node.left);
		node.left = right;
		node.right = left;

		return node;
	}
}
