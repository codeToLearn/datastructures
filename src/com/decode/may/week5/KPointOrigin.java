package com.decode.may.week5;

import java.util.PriorityQueue;

public class KPointOrigin {
	public int[][] kClosest(int[][] points, int K) {

		if (K > points.length) {
			return new int[][] {};
		}
		if (K == points.length) {
			return points;
		}

		class Pair implements Comparable<Pair> {
			int x;
			int y;

			public Pair(int x, int y) {
				this.x = x;
				this.y = y;
			}

			private Integer calculate(Pair pair) {
				return pair.x * pair.x + pair.y * pair.y;
			}

			@Override
			public int compareTo(Pair o) {
				// TODO Auto-generated method stub
				return calculate(this).compareTo(calculate(o));
			}

		}
		int res[][] = new int[K][2];
		PriorityQueue<Pair> pairs = new PriorityQueue<>();
		for (int i = 0; i < points.length; i++) {
			pairs.offer(new Pair(points[i][0], points[i][1]));
		}
		for (int i = 0; i < K; i++) {
			res[i][0] = pairs.peek().x;
			res[i][1] = pairs.peek().y;
			pairs.poll();
		}
		return res;
	}
}
