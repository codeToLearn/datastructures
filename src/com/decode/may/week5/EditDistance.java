package com.decode.may.week5;

/**
 * 
 * @author Sachin Jindal
 *
 *         <pre>
 *Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2.

You have the following 3 operations permitted on a word:

Insert a character
Delete a character
Replace a character
Example 1:

Input: word1 = "horse", word2 = "ros"
Output: 3
Explanation: 
horse -> rorse (replace 'h' with 'r')
rorse -> rose (remove 'r')
rose -> ros (remove 'e')
Example 2:

Input: word1 = "intention", word2 = "execution"
Output: 5
Explanation: 
intention -> inention (remove 't')
inention -> enention (replace 'i' with 'e')
enention -> exention (replace 'n' with 'x')
exention -> exection (replace 'n' with 'c')
exection -> execution (insert 'u')
 *         </pre>
 */
public class EditDistance {

	public static void main(String[] args) {
		EditDistance editDistance = new EditDistance();
		int min = editDistance.minDistance("horse", "ros");
		System.out.println(min);
		min = editDistance.minDistanceDP("intention", "execution");

	}

	public int minDistance(String word1, String word2) {
		return minDistanceRec(0, 0, word1, word2);

	}

	private int minDistanceRec(int i, int j, String word1, String word2) {

		// constraints
		if (i >= word1.length() && j <= word2.length()) {
			// only insert operation
			return word2.length() - j;
		}
		if (j >= word2.length() && i <= word1.length()) {
			// only delete operation
			return word1.length() - i;
		}

		// no operation
		if (word1.charAt(i) == word2.charAt(j)) {
			return minDistanceRec(i + 1, j + 1, word1, word2);
		}
		int insert = minDistanceRec(i, j + 1, word1, word2);
		int replace = minDistanceRec(i + 1, j + 1, word1, word2);
		int delete = minDistanceRec(i + 1, j, word1, word2);
		return 1 + Math.min(insert, Math.min(replace, delete));
	}

	public int minDistanceDP(String word1, String word2) {
		if (word1.length() == 0) {
			return word2.length();
		}
		if (word2.length() == 0) {
			return word1.length();
		}
		int dp[][] = new int[word1.length() + 1][word2.length() + 1];
		for (int i = word1.length(); i >= 0; i--) {
			for (int j = word2.length(); j >= 0; j--) {

				if (i == word1.length()) {
					dp[i][j] = word2.length()-j;
				} else if (j == word2.length()) {
					dp[i][j] = word1.length()-i;
				} else {
					if (word1.charAt(i) == word2.charAt(j)) {
						dp[i][j] = dp[i + 1][j + 1];
					} else {
						dp[i][j] = 1 + Math.min(dp[i + 1][j], Math.min(dp[i + 1][j + 1], dp[i][j + 1]));
					}
				}

			}
		}
		return dp[0][0];

	}
}
