package com.decode.may.week5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindCycleInGraph {

	public boolean canFinish(int numCourses, int[][] prerequisites) {

		Map<Integer, List<Integer>> graph = new HashMap<Integer, List<Integer>>();

		for (int i = 0; i < numCourses; i++) {
			graph.put(i, new ArrayList<>());
		}
		for (int i = 0; i < prerequisites.length; i++) {

			graph.get(prerequisites[i][0]).add(prerequisites[i][1]);
		}
		boolean visited[] = new boolean[numCourses];
		boolean explored[] = new boolean[numCourses];
		for (int i = 0; i < numCourses; i++) {
			if (!visited[i]) {

				if (hasCycle(i, graph, visited, explored)) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean hasCycle(int node, Map<Integer, List<Integer>> graph, boolean[] visited, boolean explored[]) {

		visited[node] = true;
		for (int neighbours : graph.get(node)) {
			if (!visited[neighbours]) {
				if (hasCycle(neighbours, graph, visited, explored)) {
					return true;
				}
			} else {
				if (!explored[neighbours]) {
					// self
					return true;
				}
			}
		}
		explored[node] = true;
		return false;
	}
}
