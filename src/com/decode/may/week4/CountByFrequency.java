package com.decode.may.week4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CountByFrequency {

	public static void main(String[] args) {

	}

	public String frequencySort(String s) {

		StringBuilder stringBuilder = new StringBuilder();
		Map<Character, Integer> countMap = new HashMap<Character, Integer>();
		for (int i = 0; i < s.length(); i++) {
			if (!countMap.containsKey(s.charAt(i))) {
				countMap.put(s.charAt(i), 0);
			}
			countMap.put(s.charAt(i), countMap.get(s.charAt(i)) + 1);
		}
		List<Map.Entry<Character, Integer>> list = new ArrayList<Map.Entry<Character, Integer>>(countMap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<Character, Integer>>() {

			@Override
			public int compare(Map.Entry<Character, Integer> o1, Map.Entry<Character, Integer> o2) {
				// TODO Auto-generated method stub
				return o2.getValue() - o1.getValue();
			}

		});
		for (Map.Entry<Character, Integer> entry : list) {
			for (int i = 0; i < entry.getValue(); i++) {
				stringBuilder.append(entry.getKey());
			}
		}
		return stringBuilder.toString();
	}
}
