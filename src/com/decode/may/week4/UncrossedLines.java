package com.decode.may.week4;

public class UncrossedLines {

	public static void main(String[] args) {
		UncrossedLines lines = new UncrossedLines();
		// [1,3,7,1,7,5]
		// [1,9,2,5,1]
		int count = lines.maxUncrossedLines(new int[] { 3 }, new int[] { 3, 3, 2 });
		System.out.println();
	}

	public int maxUncrossedLines(int[] A, int[] B) {
		return maxUncrossedLinesDP(A, B);
	}

	private int countMax(int i, int j, int A[], int B[]) {
		if (i >= A.length || j >= B.length) {
			return 0;
		}
		if (A[i] == B[j]) {
			return 1 + countMax(i + 1, j + 1, A, B);
		}

		return Math.max(countMax(i + 1, j, A, B), countMax(i, j + 1, A, B));
	}

	public int maxUncrossedLinesDP(int[] A, int[] B) {
		int[][] dp = new int[A.length + 1][B.length + 1];
		// fill last row

		for (int i = A.length - 1; i >= 0; i--) {
			for (int j = B.length - 1; j >= 0; j--) {

				if (A[i] == B[j]) {
					dp[i][j] = 1 + dp[i + 1][j + 1];
				} else {
					dp[i][j] = Math.max(dp[i][j + 1], dp[i + 1][j]);
				}
			}
		}

		return dp[0][0];
	}
}
