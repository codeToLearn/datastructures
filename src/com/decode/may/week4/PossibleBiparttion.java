package com.decode.may.week4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *
 *         Given a set of N people (numbered 1, 2, ..., N), we would like to
 *         split everyone into two groups of any size.
 * 
 *         Each person may dislike some other people, and they should not go
 *         into the same group.
 * 
 *         Formally, if dislikes[i] = [a, b], it means it is not allowed to put
 *         the people numbered a and b into the same group.
 * 
 *         Return true if and only if it is possible to split everyone into two
 *         groups in this way.
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input: N = 4, dislikes = [[1,2],[1,3],[2,4]] Output: true
 *         Explanation: group1 [1,4], group2 [2,3] Example 2:
 * 
 *         Input: N = 3, dislikes = [[1,2],[1,3],[2,3]] Output: false Example 3:
 * 
 *         Input: N = 5, dislikes = [[1,2],[2,3],[3,4],[4,5],[1,5]] Output:
 *         false
 */
public class PossibleBiparttion {
	public static void main(String[] args) {
		PossibleBiparttion biparttion = new PossibleBiparttion();
		biparttion.possibleBipartition(0, new int[][] { { 1, 2 }, { 1, 3 }, { 2, 3 } });
	}

	public boolean possibleBipartition(int N, int[][] dislikes) {
		Node[] nodes = new Node[N + 1];
		for (int i = 1; i <= N; i++)
			nodes[i] = new Node();

		for (int[] edge : dislikes) {
			int from = edge[0];
			int to = edge[1];

			nodes[from].addEdge(nodes[to]);
			nodes[to].addEdge(nodes[from]);
		}

		for (int idx = 1; idx <= N; idx++) {
			if (!nodes[idx].isColored() && !nodes[idx].dfs(Color.BLACK))
				return false;
		}

		return true;
	}

	private static enum Color {
		UNCOLORED, BLACK, WHITE
	};

	private static class Node {
		private Color color;
		private List<Node> neighbours;

		public Node() {
			color = Color.UNCOLORED;
			this.neighbours = new ArrayList<>();
		}

		public boolean isColored() {
			return this.color != Color.UNCOLORED;
		}

		public void addEdge(Node node) {
			this.neighbours.add(node);
		}

		public boolean dfs(Color color) {
			if (isColored()) {
				return this.color == color;
			}

			this.color = color;
			for (Node neighbour : neighbours) {
				if (!neighbour.dfs(color == Color.BLACK ? Color.WHITE : Color.BLACK))
					return false;
			}

			return true;
		}
	}
}
