package com.decode.may.week4;

public class CountingBits {

	public static void main(String[] args) {

	}

	public int[] countBits(int num) {
		int[] result = new int[num + 1];
		int twoPower = 0;
		for (int i = 1; i <= num; i++) {
			if (((i - 1) & i) == 0) {
				result[i] = 1;
				twoPower = i;
			} else {
				result[i] = result[i - twoPower] + 1;
			}
		}
		return result;
	}
}
