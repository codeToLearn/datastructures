package com.decode.may.week4;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *         Given two lists of closed intervals, each list of intervals is
 *         pairwise disjoint and in sorted order.
 * 
 *         Return the intersection of these two interval lists.
 * 
 *         (Formally, a closed interval [a, b] (with a <= b) denotes the set of
 *         real numbers x with a <= x <= b. The intersection of two closed
 *         intervals is a set of real numbers that is either empty, or can be
 *         represented as a closed interval. For example, the intersection of
 *         [1, 3] and [2, 4] is [2, 3].)
 * 
 * 
 * 
 *         Example 1:
 * 
 * 
 * 
 *         Input: A = [[0,2],[5,10],[13,23],[24,25]], B =
 *         [[1,5],[8,12],[15,24],[25,26]] Output:
 *         [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]] Reminder: The inputs and
 *         the desired output are lists of Interval objects, and not arrays or
 *         lists.
 */
public class InternalListIntersection {

	public static void main(String[] args) {
		int[][] A = { { 0, 2 }, { 5, 10 }, { 13, 23 }, { 24, 25 } };
		int[][] B = { { 1, 5 }, { 8,12 }, { 15, 24 }, { 25, 26 } };
		InternalListIntersection internalListIntersection = new InternalListIntersection();
		internalListIntersection.intervalIntersection(A, B);
	}
	

	public int[][] intervalIntersection(int[][] A, int[][] B) {

		int i = 0;
		int j = 0;
		Queue<int[]> queue = new LinkedList<int[]>();
		while (i < A.length && j < B.length) {
			int max = Math.max(A[i][0], B[j][0]);
			int min = Math.min(A[i][1], B[j][1]);
			if (min == A[i][1]) {
				i++;
			} else {
				j++;
			}
			if (max <= min) {
				int result[] = { max, min };
				queue.offer(result);
			}
		}
		if (!queue.isEmpty()) {
			int[][] result = new int[queue.size()][2];
			i = 0;
			while (!queue.isEmpty()) {
				result[i++] = queue.poll();
			}
			return result;
		}

		return new int[][] {};
	}
}
