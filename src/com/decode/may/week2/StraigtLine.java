package com.decode.may.week2;

/**
 * 
 * @author Sachin Jindal<br>
 *         Check If It Is a Straight Line You are given an array coordinates,
 *         coordinates[i] = [x, y], where [x, y] represents the coordinate of a
 *         point. Check if these points make a straight line in the XY plane.
 * 
 * 
 * 
 * 
 * 
 *         Example 1:
 * 
 * 
 * 
 *         Input: coordinates = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]] Output:
 *         true Example 2:
 * 
 * 
 * 
 *         Input: coordinates = [[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]] Output:
 *         false
 * 
 */
public class StraigtLine {

	public static void main(String[] args) {
		StraigtLine straigtLine = new StraigtLine();
		straigtLine.checkStraightLine(new int[][] { { 1, 2 }, { 2, 3 }, { 3, 4 }, { 4, 5 }, { 5, 6 }, { 6, 7 } });
	}

	public boolean checkStraightLine(int[][] coordinates) {
		if (coordinates == null || coordinates.length < 3) {
			return true;
		}
		// logic is slope should be same y2-y1/x2-x1=y3-y2/x3-x2
		// (y2-y1)*(x3-x2) = (y3-y2)*(x2-x1)

		int x1 = coordinates[0][0];
		int x2 = coordinates[1][0];
		int y1 = coordinates[0][1];
		int y2 = coordinates[1][1];

		for (int i = 2; i < coordinates.length; i++) {
			if ((y2 - y1) * (coordinates[i][0] - coordinates[i - 1][0]) != (x2 - x1)
					* (coordinates[i][1] - coordinates[i - 1][1])) {
				return false;
			}
		}
		return true;
	}
}
