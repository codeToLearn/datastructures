package com.decode.may.week2;

import java.util.Stack;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *
 *         Given a non-negative integer num represented as a string, remove k
 *         digits from the number so that the new number is the smallest
 *         possible.
 * 
 *         Note: The length of num is less than 10002 and will be ≥ k. The given
 *         num does not contain any leading zero. Example 1:
 * 
 *         Input: num = "1432219", k = 3 Output: "1219" Explanation: Remove the
 *         three digits 4, 3, and 2 to form the new number 1219 which is the
 *         smallest. Example 2:
 * 
 *         Input: num = "10200", k = 1 Output: "200" Explanation: Remove the
 *         leading 1 and the number is 200. Note that the output must not
 *         contain leading zeroes. Example 3:
 * 
 *         Input: num = "10", k = 2 Output: "0" Explanation: Remove all the
 *         digits from the number and it is left with nothing which is 0.
 */
public class KDigitsRemove {
	public static void main(String[] args) {
		KDigitsRemove digitsRemove = new KDigitsRemove();
		digitsRemove.removeKdigits("1432219", 3);
	}

	/**
	 * I have not come with the solution by himself i need to take help
	 * 
	 * Approach is very simple if next and less than then remove the number
	 * 
	 * @param num
	 * @param k
	 * @return
	 */
	public String removeKdigits(String num, int k) {
		if (num.length() <= k) {
			return "0";
		}

		char chars[] = num.toCharArray();
		Stack<Character> stack = new Stack<>();

		for (int i = 0; i < chars.length; i++) {

			while (k != 0 && !stack.isEmpty() && stack.peek() > chars[i]) {
				k--;
				stack.pop();
			}
			stack.push(chars[i]);
		}
		while (k-- != 0) {
			stack.pop();

		}
		char[] result = new char[stack.size()];
		int j = result.length - 1;

		while (!stack.isEmpty()) {
			result[j--] = stack.pop();
		}

		// remove traling zero
		String ans = new String(result);
		while (ans.length() > 1 && ans.charAt(0) == '0') {
			ans = ans.substring(1);
		}
		return ans;
	}

}
