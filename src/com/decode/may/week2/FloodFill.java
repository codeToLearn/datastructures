package com.decode.may.week2;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class FloodFill {
	public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
		int[][] shifts = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };// up,down,left,right
		Stack<Coordinate> stack = new Stack<>();
		Set<Coordinate> visited = new HashSet<>();
		int preColor = image[sr][sc];
		stack.add(new Coordinate(sr, sc));
		while (!stack.isEmpty()) {
			Coordinate coordinate = stack.pop();
			if (!visited.contains(coordinate)) {
				image[coordinate.row][coordinate.col] = newColor;
				for (int i = 0; i < 4; i++) {
					Coordinate coordinate2 = new Coordinate(coordinate.row + shifts[i][0],
							coordinate.col + shifts[i][1]);
					if (canTraverse(coordinate2, image, preColor)) {
						stack.push(coordinate2);

					}
				}
				visited.add(coordinate);
			}
		}
		return image;
	}

	private boolean canTraverse(Coordinate grid, int[][] gridArray, int color) {
		return grid.row >= 0 && grid.row < gridArray.length && grid.col >= 0 && grid.col < gridArray[grid.row].length
				&& gridArray[grid.row][grid.col] == color;
	}

}

class Coordinate {
	int row;
	int col;

	public Coordinate(int row, int col) {
		this.col = col;
		this.row = row;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		// result = prime * result + getOuterType().hashCode();
		result = prime * result + col;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
}
