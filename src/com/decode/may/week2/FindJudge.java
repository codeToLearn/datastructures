package com.decode.may.week2;

public class FindJudge {
	public int findJudge(int N, int[][] trust) {
		int array[] = new int[N + 1];
		for (int row = 0; row < trust.length; row++) {
			array[trust[row][1] - 1]++;
			array[trust[row][0] - 1]--;
		}

		int index = -1;
		for (int i = 0; i < N; i++) {
			if (array[i] == N - 1) {

				index = i + 1;
			}
		}
		return index;
	}
}
