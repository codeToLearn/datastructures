package com.decode.may.week2;

import com.decode.trees.RightSideView;

/**
 * 
 * @author Sachin Jindal Given a positive integer num, write a function which
 *         returns True if num is a perfect square else False.
 * 
 *         Note: Do not use any built-in library function such as sqrt.
 * 
 *         Example 1:
 * 
 *         Input: 16 Output: true Example 2:
 * 
 *         Input: 14 Output: false
 */
public class PerfectSquare {
	public static void main(String[] args) {

	}

	/**
	 * 
	 * @param num
	 * @return
	 */
	public boolean isPerfectSquare(int num) {
		if (num == 0 || num == 1) {
			return true;
		}
		for (int i = 2; i <= num / 2; i++) {
			if (num / i == i && num % i == 0) {
				return true;
			}
		}
		return false;
	}

	public boolean isPerfectSquareBS(int num) {
		long lower = 1;
		long upper = num;
		long mid = 0;
		while (lower <= upper) {
			mid = lower + (upper - lower) / 2;
			if (mid * mid == num) {
				return true;
			}
			if (mid * mid < num) {
				lower = mid + 1;
			} else {
				upper = mid - 1;
			}
		}
		return false;
	}
}
