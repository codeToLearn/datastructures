package com.decode.may.week1;

import java.util.Arrays;

public class MajorityElement {

	public int majorityElement(int[] nums) {
		Arrays.sort(nums);
		return nums[nums.length / 2];
	}

	// checked
	public int majorityElementON(int[] nums) {
		int vote = nums[0];
		int count = 1;
		int size = nums.length;
		// vote from the second number
		for (int i = 1; i < size; i++) {
			if (count == 0) {
				vote = nums[i];
				count++;
			} else if (vote == nums[i])
				count++;
			else
				count--;
		}
		return vote;
	}
}
