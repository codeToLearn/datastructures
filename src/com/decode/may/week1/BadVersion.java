package com.decode.may.week1;

/**
 * 
 * @author Sachin Jindal
 *
 *         You are a product manager and currently leading a team to develop a
 *         new product. Unfortunately, the latest version of your product fails
 *         the quality check. Since each version is developed based on the
 *         previous version, all the versions after a bad version are also bad.
 * 
 *         Suppose you have n versions [1, 2, ..., n] and you want to find out
 *         the first bad one, which causes all the following ones to be bad.
 * 
 *         You are given an API bool isBadVersion(version) which will return
 *         whether version is bad. Implement a function to find the first bad
 *         version. You should minimize the number of calls to the API.
 * 
 * 
 *         Example:
 * 
 *         Given n = 5, and version = 4 is the first bad version.
 * 
 *         call isBadVersion(3) -> false call isBadVersion(5) -> true call
 *         isBadVersion(4) -> true
 * 
 *         Then 4 is the first bad version.
 */
public class BadVersion {
	/**
	 * 
	 * @param n
	 * @return Since array is sorted we can perform binary search
	 */
	public int firstBadVersion(int n) {
		return 0;
	}

	private boolean isBadVersion(int n) {
		return false;
	}

	/**
	 * 
	 * @param lower
	 * @param upper
	 * @return find first occurrence of true
	 */
	private int binarySeacrh(int lower, int upper) {
		int ans = -1;
		if (lower <= upper) {
			int mid = lower + (upper - lower) / 2;
			if (isBadVersion(mid)) {
				// two possibilities
				if (mid >= lower && isBadVersion(mid - 1)) {
					ans = binarySeacrh(lower, mid - 1);
				} else {
					ans = mid;
				}
			} else {
				ans = binarySeacrh(mid + 1, upper);
			}
		}
		return ans;
	}

}
