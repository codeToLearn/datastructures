package com.decode.may.week1;

import java.util.HashMap;

public class FirstUniqueChar {
	public static void main(String[] args) {

	}

	public int firstUniqChar(String s) {
		if (s == null || s.isEmpty()) {
			return -1;
		}
		HashMap<Character, Integer> map = new HashMap();

		for (Character c : s.toCharArray()) {
			if (map.get(c) == null) {
				map.put(c, 0);
			}
			map.put(c, map.get(c) + 1);

		}
		int i = 0;
		for (Character c : s.toCharArray()) {
			if (map.get(c) == 1) {
				return i;
			}
			i++;
		}
		return -1;
	}

}
