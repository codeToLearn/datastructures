package com.decode.may.week1;

import java.util.HashMap;
import java.util.Map;

public class RansomNote {

	public static void main(String[] args) {

	}

	public boolean canConstruct(String ransomNote, String magazine) {

		if (magazine == null || magazine.isEmpty()) {
			return ransomNote == null || ransomNote.isEmpty();
		}
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		int count = 0;
		for (int i = 0; i < magazine.length(); i++) {
			if (map.get(magazine.charAt(i)) == null) {
				map.put(magazine.charAt(i), 0);
			}
			map.put(magazine.charAt(i), map.get(magazine.charAt(i)) + 1);
		}
		for (int i = 0; i < ransomNote.length(); i++) {
			if (map.get(ransomNote.charAt(i)) == null || map.get(ransomNote.charAt(i)) == 0) {

				return false;
			} else {

				map.put(ransomNote.charAt(i), map.get(ransomNote.charAt(i)) - 1);
				count++;
			}
		}
		return count == ransomNote.length();
	}
}
