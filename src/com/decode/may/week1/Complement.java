package com.decode.may.week1;

import java.util.concurrent.atomic.AtomicInteger;

public class Complement {
	public static void main(String[] args) {
		Complement complement = new Complement();
		complement.findComplement(5);
		
	}

	public int findComplement(int num) {

		if ((num & (num + 1)) == 0) {
			return 0;
		}
		// find next power of two
		int count = 0;
		int temp = num;
		while (num != 0) {
			num >>= 1;
			count += 1;
		}
		int value = 1 << count;

		return value - 1 - temp;
	}

}
