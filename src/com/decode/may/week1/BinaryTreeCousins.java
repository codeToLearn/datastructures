package com.decode.may.week1;

import java.util.LinkedList;
import java.util.Queue;

import com.decode.trees.TreeNode;

/**
 * 
 * @author Sachin Jindal
 *
 *         a binary tree, the root node is at depth 0, and children of each
 *         depth k node are at depth k+1.
 * 
 *         Two nodes of a binary tree are cousins if they have the same depth,
 *         but have different parents.
 * 
 *         We are given the root of a binary tree with unique values, and the
 *         values x and y of two different nodes in the tree.
 * 
 *         Return true if and only if the nodes corresponding to the values x
 *         and y are cousins.
 * 
 * 
 * 
 *         Example 1:
 * 
 * 
 *         Input: root = [1,2,3,4], x = 4, y = 3 Output: false Example 2:
 * 
 * 
 *         Input: root = [1,2,3,null,4,null,5], x = 5, y = 4 Output: true
 *         Example 3:
 * 
 * 
 * 
 *         Input: root = [1,2,3,null,4], x = 2, y = 3 Output: false
 * 
 */
public class BinaryTreeCousins {

	public static void main(String[] args) {
		BinaryTreeCousins binaryTreeCousins = new BinaryTreeCousins();
		System.out.println(binaryTreeCousins.isCousins(TreeNode.stringToTreeNode("[1,2,3,4]"), 4, 3));
		System.out.println(binaryTreeCousins.isCousins(TreeNode.stringToTreeNode("[1,2,3,null,4,null,5]"), 5, 4));
	}

	/**
	 * The idea behind the logic perform level order traversal
	 * 
	 * @param root
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isCousins(TreeNode root, int x, int y) {

		if (root == null || root.val == x || root.val == y) {
			return false;
		}
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.offer(root);
		int size = 0;
		TreeNode node = null;
		int count = 0;
		while (!queue.isEmpty()) {
			size = queue.size();
			count = 0;
			for (int i = 0; i < size; i++) {
				node = queue.poll();
				// any null means depth can never be same
				// check for same parent
				if (node.left != null && node.right != null && (node.left.val == x || node.left.val == y)
						&& (node.right.val == x || node.right.val == y)) {
					return false;
				}
				// check for exist in left value
				if (node.left != null && (node.left.val == x || node.left.val == y)) {
					count++;
				}
				// check for exist in right value
				if (node.right != null && (node.right.val == x || node.right.val == y)) {
					count++;
				}
				if (node.left != null) {
					queue.offer(node.left);
				}
				if (node.right != null) {
					queue.offer(node.right);
				}

				//
			}
			// count ==2 represents both are cousins
			if (count == 2) {
				return true;
			} else if (count > 0) {
				return false;
			}
		}

		return false;
	}
}
