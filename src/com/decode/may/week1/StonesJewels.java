package com.decode.may.week1;


import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Sachin Jindal You're given strings J representing the types of stones
 *         that are jewels, and S representing the stones you have. Each
 *         character in S is a type of stone you have. You want to know how many
 *         of the stones you have are also jewels.
 * 
 *         The letters in J are guaranteed distinct, and all characters in J and
 *         S are letters. Letters are case sensitive, so "a" is considered a
 *         different type of stone from "A".
 * 
 *         Example 1:
 * 
 *         Input: J = "aA", S = "aAAbbbb" Output: 3 Example 2:
 * 
 *         Input: J = "z", S = "ZZ" Output: 0 Note:
 * 
 *         S and J will consist of letters and have length at most 50. The
 *         characters in J are distinct. Show Hint #1
 */
public class StonesJewels {
	Set<Character> set = new HashSet();

	public static void main(String[] args) {
		StonesJewels jewels = new StonesJewels();
		System.out.println(jewels.numJewelsInStones("aA", "A"));
	}

	public int numJewelsInStones(String J, String S) {
		if (S == null || S.isEmpty()) {
			return 0;
		}
		if (J == null || J.isEmpty()) {
			return 0;
		}
		char[] charArray = J.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			set.add(charArray[i]);
		}
		int count = 0;
		charArray = S.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			if (set.contains(charArray[i])) {
				count++;
			}
		}
		return count;
	}
}
