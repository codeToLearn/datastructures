package com.decode.may.week3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Anagrams {

	public static void main(String[] args) {
		Anagrams anagrams = new Anagrams();
		anagrams.findAnagrams("cbaebabacd", "abc");

	}

	public List<Integer> findAnagrams(String s, String p) {

		List<Integer> result = new ArrayList<Integer>();
		Map<Character, Integer> countMap = new HashMap<Character, Integer>();
		for (int i = 0; i < p.length(); i++) {
			if (!countMap.containsKey(p.charAt(i))) {
				countMap.put(p.charAt(i), 0);
			}
			countMap.put(p.charAt(i), countMap.get(p.charAt(i)) + 1);
		}
		int end = 0;
		int start = 0;
		while (end < s.length()) {
			if (countMap.getOrDefault(s.charAt(end), 0) != 0) {
				countMap.put(s.charAt(end), countMap.get(s.charAt(end)) - 1);
				if (end - start + 1 == p.length()) {
					result.add(start);
				}
				end++;

			} else {
				//
				if (start == end) {
					start++;
					end++;
				} else {

					if (countMap.containsKey(s.charAt(start))) {
						countMap.put(s.charAt(start), countMap.get(s.charAt(start)) + 1);

					}
					start++;

				}

			}
		}
		return result;
	}
}
