package com.decode.may.week3;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *         Given a m * n matrix of ones and zeros, return how many square
 *         submatrices have all ones.
 * 
 * 
 * 
 *         Example 1:
 * 
 *         <pre>
Input: matrix =<br>
[
  [0,1,1,1],
  [1,1,1,1],
  [0,1,1,1]
]<br>
Output: 15
Explanation: 
There are 10 squares of side 1.<br>
There are 4 squares of side 2.<br>
There is  1 square of side 3.<br>
Total number of squares = 10 + 4 + 1 = 15.
Example 2:
<br>
Input: matrix = 
[
  [1,0,1],
  [1,1,0],
  [1,1,0]
]
Output: 7
Explanation: 
There are 6 squares of side 1.  
There is 1 square of side 2. 
Total number of squares = 6 + 1 = 7.
 *         </pre>
 */
public class CountOneSquareMatrix {
	public static void main(String[] args) {
		int[][] matrix = new int[3][4];
		matrix[0][0] = 0;
		matrix[0][1] = 1;
		matrix[0][2] = 1;
		matrix[0][3] = 1;
		matrix[1][0] = 1;
		matrix[1][1] = 1;
		matrix[1][2] = 1;
		matrix[1][3] = 1;
		matrix[2][0] = 0;
		matrix[2][1] = 1;
		matrix[2][2] = 1;
		matrix[2][3] = 1;
		CountOneSquareMatrix countOneSquareMatrix = new CountOneSquareMatrix();
		System.out.println(countOneSquareMatrix.countSquares(matrix));
		System.out.println(countOneSquareMatrix.countSquaresDP(matrix));
	}

	public int countSquares(int[][] matrix) {
		int count = 0;
		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[0].length; col++) {
				count += countSquares(matrix, row, col, ' ');
			}
		}
		return count;
	}

	private int countSquares(int[][] matrix, int row, int col, char direction) {
		if (row >= matrix.length || col >= matrix[row].length || matrix[row][col] == 0) {
			return 0;
		}
		switch (direction) {
		case 'l':
			return 1 + countSquares(matrix, row, col + 1, 'l');
		case 'd':
			return 1 + countSquares(matrix, row + 1, col, 'd');
		default:

			int l = countSquares(matrix, row, col + 1, 'l');
			int d = countSquares(matrix, row + 1, col, 'd');
			int di = countSquares(matrix, row + 1, col + 1, ' ');
			return 1 + Math.min(di, Math.min(l, d));
		}

	}

	public int countSquaresDP(int[][] matrix) {
		int count = 0;
		for (int row = matrix.length - 2; row >= 0; row--) {
			for (int col = matrix[row].length - 2; col >= 0; col--) {
				if (matrix[row][col] == 0) {
					continue;
				}
				matrix[row][col] = Math.min(matrix[row][col + 1],
						Math.min(matrix[row + 1][col + 1], matrix[row + 1][col])) + 1;
				count += matrix[row][col];
			}
		}
		for (int col = 0; col < matrix[matrix.length - 1].length; col++) {
			count += matrix[matrix.length - 1][col];
		}
		for (int row = 0; row < matrix.length-1; row++) {
			count += matrix[row][matrix[row].length - 1];
		}
		return count;
	}
}
