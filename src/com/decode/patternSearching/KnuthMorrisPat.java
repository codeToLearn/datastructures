package com.decode.patternSearching;

public class KnuthMorrisPat {
	public static void main(String[] args) {

	}

	private int[] computeLPS(String pat) {

		// Idea is very simple we need to maintain an array which signifies how many
		// character of text and pattern are already matched.
		int lps[] = new int[pat.length()];
		// create here lps
		/**
		 * We will preprocess the pattern string and create an array that indicates the
		 * longest proper prefix which is also suffix at each point in the pattern
		 * string.
		 * 
		 * A proper prefix does not include the original string.
		 * 
		 * For example, prefixes of ABC are , A, AB and ABC. Proper prefixes
		 * are , A and AB.
		 * 
		 * For example, suffixes of "ABC" are, "", "C", "BC", and "ABC". Proper prefixes
		 * are "", "C", and "BC".
		 * 
		 * Why do we care about these??
		 * 
		 * We know all characters behind our mismatch character match.
		 * 
		 * If we can find the length of the longest prefix that matches a suffix to that
		 * point, we can skip len(prefix) comparisons at the beginning.
		 * 
		 * The key reason we care about the prefix to suffix is because we want to
		 * "teleport" back to as early in the string to the point that we still know
		 * that there is a match.
		 * 
		 * Our goal is to minimize going backwards in our search string.
		 * 
		 * 
		 * Complexities:
		 * 
		 * Time: O( len(p) + len(s) )
		 * 
		 * We spend len(p) time to build the prefix-suffix table and we spend len(s)
		 * time for the traversal on average.
		 * 
		 * Space: O( len(p) )
		 * 
		 * Our prefix-suffix table is going to be the length of the pattern string.
		 * 
		 * 
		 */
		int i = 1;
		int j = 0;
		while (i < pat.length()) {
			if (pat.charAt(j) == pat.charAt(i)) {
				j++;
				lps[i] = j;
				i++;
			} else {
				if (j == 0) {
					lps[i] = 0;
					i++;
				} else {
					// backtrack
					j = lps[j - 1];
				}
			}
		}
		return lps;
	}

	int strStr(String haystack, String needle) {
		if (haystack == null || needle == null || haystack.length() < needle.length()) {
			return -1;
		} else if (needle.isEmpty()) {
			return 0;
		}

		int[] lps = computeLPS(needle);
		int i = 0;
		int j = 0;

		while (i < haystack.length()) {
			if (needle.charAt(j) == haystack.charAt(i)) {
				i++;
				j++;
				if (j == needle.length()) {
					return i - j; // match found. Return location of match
				}
			} else {
				if (j == 0) {
					i++;
				} else {
					j = lps[j - 1]; // backtrack j to check previous matching prefix
				}
			}
		}

		return -1; // did not find needle
	}
}
