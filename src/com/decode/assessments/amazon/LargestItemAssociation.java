package com.decode.assessments.amazon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeSet;

public class LargestItemAssociation {

	public List<String> largestItemAssociation(List<PairString> itemAssociation) {
		// write your code here
		if (itemAssociation == null || itemAssociation.isEmpty()) {
			return new ArrayList<String>();
		}
		// make a map here for graph
		Map<String, List<String>> graph = new HashMap<String, List<String>>();
		for (PairString pairString : itemAssociation) {
			if (!graph.containsKey(pairString.first)) {
				graph.put(pairString.first, new ArrayList<>());
			}
			graph.get(pairString.first).add(pairString.second);
			if (!graph.containsKey(pairString.second)) {
				graph.put(pairString.second, new ArrayList<>());
			}
		}
		// graph created now maintain a map which contains all tailing values
		Map<Integer, List<List<String>>> map = new HashMap<>();

		int max = Integer.MIN_VALUE;

		for (String key : graph.keySet()) {
			Queue<String> q = new LinkedList<String>();
			TreeSet<String> set = new TreeSet<>();

			q.offer(key);
			while (!q.isEmpty()) {
				String head = q.poll();
				set.add(head);
				for (String s : graph.get(head)) {
					q.offer(s);
				}
			}

			max = Math.max(max, set.size());
			if (!map.containsKey(max)) {
				map.put(max, new ArrayList<>());
			}
			map.get(max).add(new ArrayList<>(set));
		}

		List<List<String>> maxList = map.get(max);

		Collections.sort(maxList, new Comparator<List<String>>() {

			@Override
			public int compare(List<String> o1, List<String> o2) {
				int result = 0;
				for (int i = 0; i < o1.size() && result == 0; i++) {
					result = o1.get(i).compareTo(o2.get(i));
				}
				return result;

			}
		});
		
		return maxList.get(0);
	}

	public static void main(String[] args) {
		LargestItemAssociation s = new LargestItemAssociation();
		/**
		 * Example 1
		 */
		List<PairString> input = Arrays.asList(new PairString[] { s.new PairString("item1", "item2"),
				s.new PairString("item3", "item4"), s.new PairString("item4", "item5") });
		/**
		 * Testing equal sized arraylist. 1->2->3->7 4->5->6->7
		 */
		List<PairString> input2 = Arrays
				.asList(new PairString[] { s.new PairString("item1", "item2"), s.new PairString("item2", "item3"),
						s.new PairString("item4", "item5"), s.new PairString("item6", "item7"),
						s.new PairString("item5", "item6"), s.new PairString("item3", "item7") });
		List<String> lst = s.largestItemAssociation(input);
		for (String sa : lst)
			System.out.print(" " + sa);
		System.out.println();
		List<String> lst2 = s.largestItemAssociation(input2);
		for (String sa : lst2)
			System.out.print(" " + sa);
		System.out.println();
		/**
		 * Testing duplicates: 1->2->3->7 , 5->6
		 */
		List<PairString> input3 = Arrays
				.asList(new PairString[] { s.new PairString("item1", "item2"), s.new PairString("item1", "item3"),
						s.new PairString("item2", "item7"), s.new PairString("item3", "item7"),
						s.new PairString("item5", "item6"), s.new PairString("item3", "item7") });

		List<String> lst3 = s.largestItemAssociation(input3);
		for (String sa : lst3)
			System.out.print(" " + sa);
	}

	public class PairString {
		String first;
		String second;

		public PairString(String first, String second) {
			this.first = first;
			this.second = second;
		}
	}
}
