package com.decode.assessments.amazon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

/**
 * 
 * @author Sachin Jindal
 *
 *         Given a list of reviews, a list of keywords and an integer k. Find
 *         the most popular k keywords in order of most to least frequently
 *         mentioned.
 * 
 *         The comparison of strings is case-insensitive. Multiple occurances of
 *         a keyword in a review should be considred as a single mention. If
 *         keywords are mentioned an equal number of times in reviews, sort
 *         alphabetically.
 */

/**
 * 
 * @author Sachin Jindal
 * 
 *         <pre>
 *Input:
k = 2
keywords = ["anacell", "cetracular", "betacellular"]
reviews = [
  "Anacell provides the best services in the city",
  "betacellular has awesome services",
  "Best services provided by anacell, everyone should use anacell",
]

Output:
["anacell", "betacellular"]

Explanation:
"anacell" is occuring in 2 different reviews and "betacellular" is only occuring in 1 review.
Example 2:

Input:
k = 2
keywords = ["anacell", "betacellular", "cetracular", "deltacellular", "eurocell"]
reviews = [
  "I love anacell Best services; Best services provided by anacell",
  "betacellular has great services",
  "deltacellular provides much better services than betacellular",
  "cetracular is worse than anacell",
  "Betacellular is better than deltacellular.",
]

Output:
["betacellular", "anacell"]

Explanation:
"betacellular" is occuring in 3 different reviews. "anacell" and "deltacellular" are occuring in 2 reviews, but "anacell" is lexicographically smaller.
 *         </pre>
 */
public class TopKFrequentElements {

	private static List<String> solve(int k, String[] keywords, String[] reviews) {
		List<String> result = new ArrayList<String>();
		Map<String, Integer> map = new HashMap<String, Integer>();
		Set<String> keys = new HashSet<>(Arrays.asList(keywords));
		for (String s : keywords) {
			map.put(s, 0);
		}
		for (String s : reviews) {
			s = s.toLowerCase();
			for (String key : keys) {
				if (s.contains(key)) {
					map.put(key, map.get(key) + 1);
				}
			}
		}

		Queue<String> queue = new PriorityQueue<>(new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				if (map.get(o1) == map.get(o2)) {
					return o1.compareTo(o2);
				}
				return map.get(o2).compareTo(map.get(o1));
			}
		});
		queue.addAll(map.keySet());
		while (!queue.isEmpty() && k-- > 0) {
			result.add(queue.poll());
		}
		return result;

	}

	public static void main(String[] args) {
		int k1 = 2;
		String[] keywords1 = { "anacell", "cetracular", "betacellular" };
		String[] reviews1 = { "Anacell provides the best services in the city", "betacellular has awesome services",
				"Best services provided by anacell, everyone should use anacell", };
		int k2 = 2;
		String[] keywords2 = { "anacell", "betacellular", "cetracular", "deltacellular", "eurocell" };
		String[] reviews2 = { "I love anacell Best services; Best services provided by anacell",
				"betacellular has great services", "deltacellular provides much better services than betacellular",
				"cetracular is worse than anacell", "Betacellular is better than deltacellular.", };
		System.out.println(solve(k1, keywords1, reviews1));
		System.out.println(solve(k2, keywords2, reviews2));
	}

}
