package com.decode.assessments.amazon;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ZombieInAMatrix {
	static int rows = 0;
	static int cols = 0;

	private int minHours(int row, int columns, List<List<Integer>> grid) {
		// todo
		rows = row;
		cols = columns;
		int timeElasped = 0;
		int humans = 0;
		// count here humans
		Queue<Grid> queue = new LinkedList<>();

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (grid.get(i).get(j) == 1) {
					queue.offer(new Grid(i, j));
				} else {
					humans++;
				}
			}
		}
		if (humans == 0) {
			return timeElasped;
		}
		int[][] shifts = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };
		while (!queue.isEmpty()) {
			timeElasped++;
			int size = queue.size();
			for (int i = 0; i < size; i++) {
				Grid grid2 = queue.poll();
				for (int k = 0; k < shifts.length; k++) {
					Grid grid3 = new Grid(grid2.row + shifts[k][0], grid2.col + shifts[k][1]);
					if (grid3.isValid(grid)) {
						humans--;
						if (humans == 0) {
							return timeElasped;
						}
						grid.get(grid3.row).set(grid3.col, 1);
						queue.offer(grid3);
					}
				}

			}
		}
		return humans == 0 ? timeElasped : -1;
	}

	class Grid {
		int row;
		int col;

		public Grid(int row, int col) {
			this.row = row;
			this.col = col;
		}

		public boolean isValid(List<List<Integer>> grid) {
			return row >= 0 && row < rows && col >= 0 && col < cols && grid.get(row).get(col) == 0;
		}
	}

	public static void main(String[] args) {
		List<List<Integer>> list = new ArrayList<List<Integer>>();

		list.add(Arrays.asList(0, 1, 1, 0, 1));
		list.add(Arrays.asList(0, 1, 0, 1, 0));

		list.add(Arrays.asList(0, 0, 0, 0, 1));
		list.add(Arrays.asList(0, 1, 0, 0, 0));
		ZombieInAMatrix aMatrix = new ZombieInAMatrix();
		aMatrix.minHours(4, 5, list);
	}
}
