package com.decode.trie;

import com.decode.trie.TrieImplementation.TrieNode;

/**
 * 
 * @author Sachin Jindal
 *
 *         Design a data structure that supports the following two operations:
 * 
 *         void addWord(word) bool search(word) search(word) can search a
 *         literal word or a regular expression string containing only letters
 *         a-z or .. A . means it can represent any one letter.
 * 
 *         Example:
 * 
 *         addWord("bad") addWord("dad") addWord("mad") search("pad") -> false
 *         search("bad") -> true search(".ad") -> true search("b..") -> true
 * 
 */
public class WordDictionary {
	private final int ALPHABET_SIZE = 26;
	private TrieNode root = new TrieNode('#');

	public static void main(String[] args) {
		WordDictionary wordDictionary = new WordDictionary();
		wordDictionary.addWord("bad");
		wordDictionary.addWord("mad");
		wordDictionary.addWord("dad");
		boolean check = wordDictionary.search("pad");
		System.out.println(check);
		check = wordDictionary.search(".ad");
		System.out.println(check);
		check = wordDictionary.search("b..");

		System.out.println(check);
		check = wordDictionary.search("b..");

		System.out.println(check);
	}

	/** Initialize your data structure here. */
	public WordDictionary() {

	}

	/** Adds a word into the data structure. */
	public void addWord(String word) {

		if (word == null || word.isEmpty()) {
			return;
		}
		TrieNode currentNode = root;
		char chars[] = word.toCharArray();
		int index = -1;
		TrieNode newNode;
		/*
		 * We will traverse node from list and one by one word we will check
		 */
		for (int i = 0; i < chars.length; i++) {
			index = chars[i] - 'a';
			if (currentNode.adjacents[index] == null) {
				newNode = new TrieNode(chars[i]);
				currentNode.adjacents[index] = newNode;

			}
			currentNode = currentNode.adjacents[index];

		}
		if (currentNode.character != '#') {
			currentNode.endWord = true;
		}

	}

	/**
	 * Returns if the word is in the data structure. A word could contain the dot
	 * character '.' to represent any one letter.
	 */
	public boolean search(String word) {
		return search(word, 0, root);
	}

	private boolean search(String word, int index, TrieNode currentNode) {

		if (index == word.length()) {
			return currentNode.endWord;
		}
		char character = word.charAt(index);

		if (character == '.') {

			for (int i = 0; i < ALPHABET_SIZE; i++) {
				if (currentNode.adjacents[i] != null) {
					if (search(word, index + 1, currentNode.adjacents[i])) {

						return true;
					}
				}

			}
		} else {

			int accessIndex = character - 'a';
			return currentNode.adjacents[accessIndex] != null
					&& search(word, index + 1, currentNode.adjacents[accessIndex]);
		}
		return false;
	}

	class TrieNode {
		char character;
		TrieNode[] adjacents;
		boolean endWord; // represents where will be word ended

		public TrieNode(char character) {
			this.character = character;
			adjacents = new TrieNode[ALPHABET_SIZE];
			endWord = false;
		}
	}
}

/**
 * Your WordDictionary object will be instantiated and called as such:
 * WordDictionary obj = new WordDictionary(); obj.addWord(word); boolean param_2
 * = obj.search(word);
 */