package com.decode.trie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordSearch {
	private TrieNode root = new TrieNode('#');

	public List<String> findWords(char[][] board, String[] words) {
		ArrayList<String> result = new ArrayList<>();
		if (words != null && words.length != 0) {

			for (int i = 0; i < words.length; i++) {
				root.insert(words[i]);

			}
		}
		return result;
	}

	public boolean exist(char[][] board, String word) {
		root.insert(word);

		boolean[][] vistited = new boolean[board.length][board[0].length];
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				if (wordMatch(row, col, board, vistited, root)) {
					return true;
				}
			}
		}
		return false;
	}

	private int[][] moves = { { 0, 1 }, { 1, 0 }, { -1, 0 }, { 0, -1 } };

	private boolean wordMatch(int row, int col, char[][] board, boolean[][] vistited, TrieNode root) {
		// base cases

		if (root.endWord) {
			return true;
		}
		if (row < 0 || col < 0 || row == board.length || col == board[0].length
				|| root.adjacents.get(board[row][col]) == null || vistited[row][col]) {
			return false;
		}
		vistited[row][col] = true;
		for (int i = 0; i < moves.length; i++) {
			if (wordMatch(row + moves[i][0], col + moves[i][1], board, vistited, root.adjacents.get(board[row][col]))) {
				return true;
			}
		}
		vistited[row][col] = false;
		return false;

	}

	class TrieNode {
		char character;
		Map<Character, TrieNode> adjacents;
		boolean endWord; // represents where will be word ended

		public TrieNode(char character) {
			this.character = character;
			adjacents = new HashMap<Character, TrieNode>();
			endWord = false;
		}

		public void insert(String word) {
			if (word == null || word.isEmpty()) {
				return;
			}
			TrieNode currentNode = root;
			char chars[] = word.toCharArray();

			TrieNode newNode;
			/*
			 * We will traverse node from list and one by one word we will check
			 */
			for (int i = 0; i < chars.length; i++) {

				if (currentNode.adjacents.get(chars[i]) == null) {
					newNode = new TrieNode(chars[i]);
					currentNode.adjacents.put(chars[i], newNode);
				}
				currentNode = currentNode.adjacents.get(chars[i]);

			}
			if (currentNode.character != '#') {
				currentNode.endWord = true;
			}
		}
	}
}
