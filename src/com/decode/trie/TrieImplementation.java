package com.decode.trie;

/**
 * 
 * @author Sachin Jindal
 *
 *         Implement a trie with insert, search, and startsWith methods.
 * 
 *         Example:
 * 
 *         Trie trie = new Trie();
 * 
 *         trie.insert("apple"); trie.search("apple"); // returns true
 *         trie.search("app"); // returns false trie.startsWith("app"); //
 *         returns true trie.insert("app"); trie.search("app"); // returns true
 *         Note:
 * 
 *         You may assume that all inputs are consist of lowercase letters a-z.
 *         All inputs are guaranteed to be non-empty strings.
 */
/*
 * The trie word comes from retrieval.A trie is a tree-like data structure whose
 * nodes store the letters of an alphabet. By structuring the nodes in a
 * particular way, words and strings can be retrieved from the structure by
 * traversing down a branch path of the tree.
 */

/*
 * Idea of creating a trie we will maintain a inner trie node which will contain
 * link to 26 characters. Initially root node is empty node we will represent it
 * through '#'.
 */
/*
 * we�re representing the nursery rhyme that starts off with something like
 * <b>�Peter Piper picked a peck of pickled peppers�<b>. I won�t try to make you
 * remember the rest of it, mostly because it is confusing and makes my head
 * hurt.
 */
public class TrieImplementation {

	private final int ALPHABET_SIZE = 26;
	private TrieNode root = new TrieNode('#');

	public static void main(String[] args) {
		TrieImplementation implementation = new TrieImplementation();
		// �Peter Piper picked a peck of pickled peppers�
		implementation.insert("peter");
		implementation.insert("piper");
		implementation.insert("picked");
		implementation.insert("a");
		implementation.insert("peck");
		implementation.insert("of");
		implementation.insert("pickled");
		implementation.insert("peppers");
		System.out.println(implementation.search("pecked"));
		System.out.println(implementation.search("of"));
		System.out.println(implementation.search("pickled"));
		System.out.println(implementation.startsWith("pickled"));
		System.out.println(implementation.startsWith("s"));
	}

	/** Inserts a word into the trie. */
	/**
	 * 
	 * @param word
	 *            We will check is this word exist or half completed or not
	 */
	public void insert(String word) {
		if (word == null || word.isEmpty()) {
			return;
		}
		TrieNode currentNode = root;
		char chars[] = word.toCharArray();
		int index = -1;
		TrieNode newNode;
		/*
		 * We will traverse node from list and one by one word we will check
		 */
		for (int i = 0; i < chars.length; i++) {
			index = chars[i] - 'a';
			if (currentNode.adjacents[index] == null) {
				newNode = new TrieNode(chars[i]);
				currentNode.adjacents[index] = newNode;

			}
			currentNode = currentNode.adjacents[index];

		}
		if (currentNode.character != '#') {
			currentNode.endWord = true;
		}
	}

	/** Returns if the word is in the trie. */
	/*
	 * We will traverse through all alphabets
	 */
	public boolean search(String word) {
		if (word == null || word.isEmpty()) {
			return true;
		}
		TrieNode currentNode = root; // set to root
		char chars[] = word.toCharArray();
		int index = -1;

		/*
		 * We will traverse node from list and one by one word we will check
		 */
		for (int i = 0; i < chars.length; i++) {
			index = chars[i] - 'a';
			if (currentNode.adjacents[index] == null) {
				return false;
			}
			currentNode = currentNode.adjacents[index];

		}

		return currentNode.endWord;
	}

	/**
	 * Returns if there is any word in the trie that starts with the given prefix.
	 */
	public boolean startsWith(String word) {
		if (word == null || word.isEmpty()) {
			return true;
		}
		TrieNode currentNode = root; // set to root
		char chars[] = word.toCharArray();
		int index = -1;

		/*
		 * We will traverse node from list and one by one word we will check
		 */
		for (int i = 0; i < chars.length; i++) {
			index = chars[i] - 'a';
			if (currentNode.adjacents[index] == null) {
				return false;
			}
			currentNode = currentNode.adjacents[index];

		}

		return true;
	}

	class TrieNode {
		char character;
		TrieNode[] adjacents;
		boolean endWord; // represents where will be word ended

		public TrieNode(char character) {
			this.character = character;
			adjacents = new TrieNode[ALPHABET_SIZE];
			endWord = false;
		}
	}
}
