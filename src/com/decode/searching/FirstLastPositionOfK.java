package com.decode.searching;

/**
 * 
 * @author Sachin Jindal
 *
 *         Given an array of integers nums sorted in ascending order, find the
 *         starting and ending position of a given target value.
 * 
 *         Your algorithm's runtime complexity must be in the order of O(log n).
 * 
 *         If the target is not found in the array, return [-1, -1].
 * 
 *         Example 1:
 * 
 *         Input: nums = [5,7,7,8,8,10], target = 8 Output: [3,4] Example 2:
 * 
 *         Input: nums = [5,7,7,8,8,10], target = 6 Output: [-1,-1]
 */
public class FirstLastPositionOfK {

	public static void main(String[] args) {
		FirstLastPositionOfK firstLastPositionOfK = new FirstLastPositionOfK();
		System.out.println(firstLastPositionOfK.searchRange(new int[] { 5, 7, 7, 8, 8, 10 }, 6));
	}

	enum SEARCH_TYPE {
		FIRST, LAST
	}

	public int[] searchRange(int[] nums, int target) {
		int[] rangeResult = { -1, -1 };

		int leftBoundIndex = search(nums, target, SEARCH_TYPE.FIRST, 0, nums.length - 1);

		if (leftBoundIndex == -1) {
			return rangeResult;
		}

		/*
		 * If first occurrence is found then add it to the result and search for the
		 * last occurrence and store that result
		 */
		rangeResult[0] = leftBoundIndex;
		rangeResult[1] = search(nums, target, SEARCH_TYPE.LAST, leftBoundIndex, nums.length - 1);

		return rangeResult;
	}

	private int search(int[] nums, int target, SEARCH_TYPE serachType, int lower, int upper) {
		int ans = -1;
		if (lower <= upper) {
			int mid = lower + (upper - lower) / 2;

			if (nums[mid] == target) {
				if (serachType == SEARCH_TYPE.FIRST) {
					if (mid > lower && nums[mid] == nums[mid - 1]) {
						ans = search(nums, target, serachType, lower, mid - 1);
					} else {
						return mid;
					}
				} else {
					if (mid < upper && nums[mid] == nums[mid + 1]) {
						ans = search(nums, target, serachType, mid + 1, upper);
					} else {
						return mid;
					}

				}

			} else if (nums[mid] > target) {
				ans = search(nums, target, serachType, lower, mid - 1);
			} else {
				ans = search(nums, target, serachType, mid + 1, upper);
			}
		}

		return ans;
	}

}
