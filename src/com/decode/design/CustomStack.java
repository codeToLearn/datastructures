package com.decode.design;

import java.util.Stack;

/**
 * 
 * @author Sachin Jindal
 *
 *         Design a stack which supports the following operations.
 * 
 *         Implement the CustomStack class:
 * 
 *         CustomStack(int maxSize) Initializes the object with maxSize which is
 *         the maximum number of elements in the stack or do nothing if the
 *         stack reached the maxSize. void push(int x) Adds x to the top of the
 *         stack if the stack hasn't reached the maxSize. int pop() Pops and
 *         returns the top of stack or -1 if the stack is empty. void inc(int k,
 *         int val) Increments the bottom k elements of the stack by val. If
 *         there are less than k elements in the stack, just increment all the
 *         elements in the stack.
 * 
 * 
 *         Example 1:
 * 
 *         Input
 *         ["CustomStack","push","push","pop","push","push","push","increment","increment","pop","pop","pop","pop"]
 *         [[3],[1],[2],[],[2],[3],[4],[5,100],[2,100],[],[],[],[]] Output
 *         [null,null,null,2,null,null,null,null,null,103,202,201,-1]
 *         Explanation CustomStack customStack = new CustomStack(3); // Stack is
 *         Empty [] customStack.push(1); // stack becomes [1]
 *         customStack.push(2); // stack becomes [1, 2] customStack.pop(); //
 *         return 2 --> Return top of the stack 2, stack becomes [1]
 *         customStack.push(2); // stack becomes [1, 2] customStack.push(3); //
 *         stack becomes [1, 2, 3] customStack.push(4); // stack still [1, 2,
 *         3], Don't add another elements as size is 4 customStack.increment(5,
 *         100); // stack becomes [101, 102, 103] customStack.increment(2, 100);
 *         // stack becomes [201, 202, 103] customStack.pop(); // return 103 -->
 *         Return top of the stack 103, stack becomes [201, 202]
 *         customStack.pop(); // return 202 --> Return top of the stack 102,
 *         stack becomes [201] customStack.pop(); // return 201 --> Return top
 *         of the stack 101, stack becomes [] customStack.pop(); // return -1
 *         --> Stack is empty return -1.
 * 
 * 
 *         Constraints:
 * 
 *         1 <= maxSize <= 1000 1 <= x <= 1000 1 <= k <= 1000 0 <= val <= 100 At
 *         most 1000 calls will be made to each method of increment, push and
 *         pop each separately. Ac
 */
public class CustomStack {

	int maxSize = 0;
	DoublyCustomLinkedList head, tail;
	int size = 0;

	public CustomStack(int maxSize) {
		this.maxSize = maxSize;
		head = new DoublyCustomLinkedList(0);
		tail = new DoublyCustomLinkedList(0);
		head.after = tail;
		tail.before = head;
	}

	public void push(int x) {
		if (size == maxSize) {
			return;
		}
		insertAtLast(new DoublyCustomLinkedList(x));
		size++;
	}

	public int pop() {
		if (isEmpty()) {
			return -1;
		}
		size--;
		return deleteLastNode().data;
	}

	public void increment(int k, int val) {
		DoublyCustomLinkedList currentNode = head.after;
		while (currentNode != tail && k-- > 0) {
			currentNode.data += val;
			currentNode = currentNode.after;
		}
	}

	private boolean isEmpty() {
		return size == 0;
	}

	private void insertAtLast(DoublyCustomLinkedList node) {
		DoublyCustomLinkedList savedBefore = tail.before;

		node.after = tail;
		node.before = savedBefore;
		savedBefore.after = tail.before = node;

	}

	private DoublyCustomLinkedList deleteLastNode() {
		DoublyCustomLinkedList savedBefore = tail.before;
		tail.before = savedBefore.before;
		savedBefore.before.after = tail;

		savedBefore.after = null;
		savedBefore.before = null;
		return savedBefore;
	}

	public static void main(String[] args) {
		// ["CustomStack","push","pop","increment","pop","increment","push","pop","push","increment","increment","increment"]
		// [[2],[34],[],[8,100],[],[9,91],[63],[],[84],[10,93],[6,45],[10,4]]

		CustomStack customStack = new CustomStack(2);
		customStack.push(34);
		System.out.println(customStack.pop());
		customStack.increment(8, 100);
	}
}

class DoublyCustomLinkedList {
	int data;
	DoublyCustomLinkedList after;
	DoublyCustomLinkedList before;

	public DoublyCustomLinkedList(int data) {
		this.data = data;
	}
}
