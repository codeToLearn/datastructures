package com.decode.design;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class MergeKSortedArrays {
	public static void main(String[] args) {

	}

	public List<Integer> mergeSortedArrays(List<List<Integer>> sortedArrays) {
		List<Integer> result = new ArrayList<Integer>();
		/*
		 * Create a list of iterators to keep track of what item we are at in each
		 * array. The list has no values until we add iterators to it
		 */
		List<Iterator<Integer>> iters = new ArrayList<>(sortedArrays.size());
		/*
		 * Populate the iterator list with a respective iterator for each list (we will
		 * use these later to fetch the next items in processing)
		 */
		for (List<Integer> array : sortedArrays) {
			iters.add(array.iterator());
		}

		/*
		 * We create a PriorityQueue. A heap is an implementation of the abstract data
		 * type (ADT) "priority queue". We pass a comparator so it knows how to order
		 * items so that the minimum item is at the top
		 */
		Queue<ArrayEntry> queue = new PriorityQueue<MergeKSortedArrays.ArrayEntry>(new Comparator<ArrayEntry>() {
			@Override
			public int compare(ArrayEntry o1, ArrayEntry o2) {
				// TODO Auto-generated method stub
				return o2.value.compareTo(o1.value);
			}

		});
		/*
		 * Add the FIRST item from EACH list via the iterator we "juiced" out of it
		 * before IF it has an item to add (is not empty).
		 */
		for (int i = 0; i < sortedArrays.size(); i++) {
			if (iters.get(i).hasNext()) {
				queue.add(new ArrayEntry(iters.get(i).next(), i));
			}

		} /*
			 * While the min heap is not empty we: Step 1.) Get the minimum item from the
			 * heap Step 2.) Add the ArrayEntry's value to the result array Step 3.) If the
			 * ejected entry has a next value, add that next item as an annotated ArrayEntry
			 * object And iteration continues... When the min heap is entry we will have
			 * processed all items in every array and have a full sorted result
			 */
		while (!queue.isEmpty()) {
			ArrayEntry evicted = queue.poll();
			result.add(evicted.value);
			if (iters.get(evicted.arrayId).hasNext()) {
				queue.offer(new ArrayEntry(iters.get(evicted.arrayId).next(), evicted.arrayId));
			}
		}
		return result;
	}

	class ArrayEntry {
		Integer value;
		int arrayId;

		public ArrayEntry(Integer value, int arrayId) {
			this.value = value;
			this.arrayId = arrayId;
		}
	}
}
