package com.decode.design;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Sachin Jindal
 *
 *         Design and implement a data structure for Least Recently Used (LRU)
 *         cache. It should support the following operations: get and put.
 * 
 *         get(key) - Get the value (will always be positive) of the key if the
 *         key exists in the cache, otherwise return -1. put(key, value) - Set
 *         or insert the value if the key is not already present. When the cache
 *         reached its capacity, it should invalidate the least recently used
 *         item before inserting a new item.
 * 
 *         The cache is initialized with a positive capacity.
 * 
 *         <b> Follow up: Could you do both operations in O(1) time
 *         complexity?</b>
 * 
 *         Example:
 * 
 *         LRUCache cache = new LRUCache( 2 );
 * 
 *         cache.put(1, 1);<br>
 *         cache.put(2, 2); <br>
 *         cache.get(1); // returns 1 <br>
 *         cache.put(3, 3); // evicts key 2 <br>
 *         cache.get(2); // returns -1 (not found)<br>
 *         cache.put(4, 4); // evicts key 1 <br>
 *         cache.get(1); // returns -1 (not found) <br>
 *         cache.get(3); // returns 3 <br>
 *         cache.get(4); // returns 4
 */

public class LRUCache {

	public static void main(String[] args) {
		LRUCache cache = new LRUCache(2);
		/*
		 * ["LRUCache","put","put","get","put","get","get"]
		 * [[2],[2,1],[1,1],[2],[4,1],[1],[2]]
		 */
		cache.put(2, 1);
		cache.put(1, 1);
		System.out.println(cache.get(2));
		cache.put(4, 1);
		System.out.println(cache.get(1));
		System.out.println(cache.get(2));

	}

	private int size = 0;
	private int capacity = 0;
	/**
	 * Here we are looking for fast removals and fast searching.Therefore if we
	 * think about data structures that Fast searching can be possible with hashmap
	 * and fast removals with doubly linkedList because if we have node after and
	 * before then we can easily remove node.
	 */
	private Map<Integer, LinkedNode> cacheMap = new HashMap<>();

	private LinkedNode head = null;
	private LinkedNode tail = null;

	public LRUCache(int capacity) {
		if (capacity < 0) {
			throw new NullPointerException();
		}
		this.capacity = capacity;
		// Dummy head and tail nodes to avoid empty states
		head = new LinkedNode(-1, -1);
		tail = new LinkedNode(-1, -1);

		// Wire the head and tail together
		head.after = tail;
		tail.before = head;
	}

	/**
	 * 
	 * @param key
	 *            If we get value from hashmap then put that key at the head of
	 *            doubly linked list so that we can manage least recently used key
	 * @return
	 */
	public int get(int key) {
		if (cacheMap.containsKey(key)) {
			LinkedNode node = cacheMap.get(key);
			node = removeNode(node);
			insertAtHead(node);
			cacheMap.put(key, node);
			return node.data;
		}
		return -1;
	}
	public void put(int key, int value) {
		// check exists in map
		if (cacheMap.containsKey(key)) {
			LinkedNode linkedNode = cacheMap.get(key);
			linkedNode.data = value;
			get(key);

		} else {
			if (isFull()) {
				removeLastNode();
			}
			LinkedNode linkedNode = new LinkedNode(key, value);
			insertAtHead(linkedNode);
			cacheMap.put(key, linkedNode);
		}
	}
	private void insertAtHead(LinkedNode linkedNode) {

		linkedNode.before = head;
		linkedNode.after = head.after;

		linkedNode.before.after = linkedNode.after.before = linkedNode;

		size++;
	}

	private LinkedNode removeNode(LinkedNode linkedNode) {

		LinkedNode savedBefore = linkedNode.before;
		LinkedNode savedAfter = linkedNode.after;
		cacheMap.remove(linkedNode.key);
		size--;
		// now maintain the pointers
		savedBefore.after = savedAfter;
		savedAfter.before = savedBefore;
		return linkedNode;
	}

	private void removeLastNode() {

		LinkedNode pre = tail.before;
		if (pre.data == -1) {
			// head
			return;
		}
		removeNode(pre);

	}



	private boolean isFull() {
		return size == capacity;
	}

}

/**
 * 
 * @author Sachin Jindal DOUBLY LINKED LIST
 */
final class LinkedNode {
	int key;
	int data;
	LinkedNode after;
	LinkedNode before;

	public LinkedNode(int key, int data) {
		this.key = key;
		this.data = data;
	}

}
