package com.decode.design;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class MergeKSortedList {
	public ListNode mergeKLists(ListNode[] lists) {
		// Create dummy head listnode
		ListNode dummyListNodeHead = new ListNode(0);
		// create dummy tail node
		ListNode dummyTailNode = dummyListNodeHead;

		// create priority queue
		Queue<ListNode> queue = new PriorityQueue<ListNode>(new Comparator<ListNode>() {

			@Override
			public int compare(ListNode o1, ListNode o2) {
				// TODO Auto-generated method stub
				if (o1.val < o2.val) {
					return -1;
				} else if (o1.val == o2.val) {
					return 0;
				} else {
					return 1;
				}
			}
		});
		// add all heads
		for (ListNode head : lists) {
			if (head != null) {
				queue.add(head);
			}

		}
		while (!queue.isEmpty()) {
			ListNode listNode = queue.poll();
			dummyTailNode.next = listNode;

			dummyTailNode = listNode;
			if (listNode.next != null) {
				queue.add(listNode.next);
			}
		}
		return dummyListNodeHead.next;
	}

	public class ListNode {
		int val;
		ListNode next;

		ListNode() {
		}

		ListNode(int val) {
			this.val = val;
		}

		ListNode(int val, ListNode next) {
			this.val = val;
			this.next = next;
		}
	}
}
