package com.decode.recursion_dp;

/**
 * 
 * @author Sachin Jindal
 *
 *         Given an array of non-negative integers, you are initially positioned
 *         at the first index of the array.
 * 
 *         Each element in the array represents your maximum jump length at that
 *         position.
 * 
 *         Your goal is to reach the last index in the minimum number of jumps.
 * 
 *         Example:
 * 
 *         Input: [2,3,1,1,4] Output: 2 Explanation: The minimum number of jumps
 *         to reach the last index is 2. Jump 1 step from index 0 to 1, then 3
 *         steps to the last index.
 */
public class MinimumNumberOfJumps {

	public static void main(String[] args) {
		MinimumNumberOfJumps minimumNumberOfJumps = new MinimumNumberOfJumps();
		System.out.println(minimumNumberOfJumps.jump(new int[] { 2,1 }));
	}

	public int jump(int[] nums) {

		//return jump(nums, 0);
		return jumpDP(nums);
	}

	/**
	 * CHOICE - Each element in the array represents your maximum jump length at
	 * that position.
	 * 
	 * CONSTRAINT - if any point zero means no path exists or reach at the end of an
	 * array means return 0 no further steps
	 * 
	 * GOALS - NEED TO FIND BETWEEN ALL CHOICES
	 * 
	 * @param nums
	 * @param index
	 *            - represents index of an array
	 * @return Here jump always possible
	 */
	private int jump(int[] nums, int index) {
		// This represents we reach at the end of array

		if (index == nums.length - 1) {
			return 0;
		}
		/**
		 * This represents choices
		 */
		int minSteps = nums.length;
		for (int i = 1; ((index + i) < nums.length) && (i <= nums[index]); i++) {
			// we can not count if zero happens
			if (index + i != nums.length - 1 && nums[i + index] == 0) {
				continue;
			}

			minSteps = Math.min(minSteps, jump(nums, index + i));

		}
		return minSteps == nums.length ? minSteps : minSteps + 1;
	}

	private int jumpDP(int[] nums) {
		int dp[] = new int[nums.length];
		dp[nums.length - 1] = 0;

		for (int index = nums.length - 2; index >= 0; index--) {
			int minSteps = nums.length;
			for (int jump = 1;((index + jump) < nums.length) && (jump <= nums[index]); jump++) {
				if (index + jump != nums.length - 1 && nums[jump + index] == 0) {
					continue;
				}

				minSteps = Math.min(minSteps, dp[index + jump]+1);

			}
			dp[index] = minSteps;
		}
		return dp[0];
	}
}
