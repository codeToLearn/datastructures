package com.decode.recursion_dp;

import java.util.HashMap;
import java.util.Map;

/*
 * A message containing letters from A-Z is being encoded to numbers using the following mapping:

'A' -> 1
'B' -> 2
...
'Z' -> 26
Given a non-empty string containing only digits, determine the total number of ways to decode it.

Example 1:

Input: "12"
Output: 2
Explanation: It could be decoded as "AB" (1 2) or "L" (12).
Example 2:

Input: "226"
Output: 3
Explanation: It could be decoded as "BZ" (2 26), "VF" (22 6), or "BBF" (2 2 6).
 */

public class DecodeWaysRecursiveWithDP {
	public static void main(String[] args) {

		String s = "17";

		System.out.println("TOTAL DECODE WAYS::" + decodeWays(s));
		System.out.println("DECODE WAYS WITH DP::" + decodeWaysDP(s));

	}

	private static int decodeWays(String string) {

		System.out.println(string);
		// if string starts with zero then no possible values for example 0123
		if (isEmpty(string) || string.startsWith("0")) {
			return 0;
		}

		if (string.length() <= 2 && check(string)) {
			// 30
			if (string.endsWith("0")) {
				return 1;
			}
			return string.length();
		}
		// this will decode for left side
		int left = decodeWays(string.substring(1));

		int right = 0;
		// this will decode for right side but we need to check whether substring can
		// further or not e.g 301 -> 30,1 30 can not be taken so right =0 on the other
		// hand if 201 then 20 can be considered
		if (string.length() >= 2 && check(string.substring(0, 2))) {
			right = decodeWays(string.substring(2));
		}
		return left + right;
	}

	/*
	 * The idea behind the code is that i will check for every string of size two.
	 * 123 - 12,23 1- 1 12 -2 23 - 1+2
	 */
	private static int decodeWaysDP(String string) {
		// This will check all case like start with zero or if any string contains 00
		if (isEmpty(string) || string.startsWith("0") || string.contains("00")) {
			return 0;
		}
		
		int table[] = new int[string.length()];
		table[0] = 1;
		String sub = "";
		for (int i = 1; i < string.length(); i++) {
			sub = string.charAt(i - 1) + "" + string.charAt(i);
			if (check(sub)) {
				if (sub.endsWith("0")) {
					table[i] = i == 1 ? 1 : table[i - 2];
				} else {
					table[i] = i == 1 ? 2 : table[i - 1] + table[i - 2];
				}

			} else {
				if ((!sub.endsWith("0") || sub.startsWith("0"))) {
					table[i] = table[i - 1];
				}
			}

		}
		return table[string.length() - 1];
	}

	private static boolean check(String string) {

		if (!isEmpty(string) && string.length() <= 2 && string.charAt(0) > '0') {
			if (string.length() == 1) {
				if (string.charAt(0) <= '9') {
					return true;
				}
			}
			return (string.charAt(0) == '2' && string.charAt(1) <= '6')
					|| (string.charAt(0) == '1' && string.charAt(1) <= '9');
		}
		return false;
	}

	private static boolean isEmpty(String string) {
		if (string == null || string.isEmpty()) {
			return true;
		}
		return false;
	}

}
