package com.decode.recursion_dp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Sachin Jindal
 *
 *
 */
public class CountBitWisePowerOf2 {
	public static void main(String[] args) {

		System.out.println(10 & 3);
	}

	public static long countPairs(List<Integer> arr) {
		long count = 0;
		Map<Integer, Integer> power2Count = new HashMap<Integer, Integer>();
		for (Integer integer : arr) {
			if (isPowerOfTwo(integer)) {

			}
		}
		return count;
	}

	/* Method to check if x is power of 2 */
	static boolean isPowerOfTwo(int x) {
		/*
		 * First x in the below expression is for the case when x is 0
		 */
		return x != 0 && ((x & (x - 1)) == 0);

	}
}
