package com.decode.recursion_dp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordBreak {
	HashMap<String, Boolean> cache = new HashMap<>();

	public static void main(String[] args) {
		WordBreak wordBreak = new WordBreak();
		List<String> words = new ArrayList<>();
		words.add("leet");
		words.add("code");
		wordBreak.wordBreak("leetcode", words);

	}

	public boolean wordBreak(String s, List<String> wordDict) {
		Set<String> set = new HashSet<>(wordDict);

		return wordBreakDP(s, set);
	}

	private boolean wordBreak(String s, Set<String> words) {
		if (cache.get(s) != null) {
			return cache.get(s);
		}

		if (s.isEmpty()) {
			cache.put(s, true);
			return true;
		}
		// lets break the string one by one
		for (int i = 0; i < s.length(); i++) {
			String left = s.substring(0, i + 1);
			String right = s.substring(i + 1);
			if (words.contains(left) && wordBreak(right, words)) {
				cache.put(s, true);
				return true;
			}

		}
		cache.put(s, false);
		return false;
	}

	// copied
	private boolean wordBreakDP(String str, Set<String> dict) {
		boolean[] dp = new boolean[str.length() + 1];
		dp[0] = true;// empty string
		for (int j = 1; j < dp.length; j++) {
			// this part represents character we are taking into consideration
			for (int i = 0; i < j; i++) {
				// the idea behind the logic is very simple but hard to intute
				// for example we have leetscode [leet,leets,code]
				/**
				 * lets break it. <br>
				 * 1.for j=0 we have one possibility check first character & empty string <br>
				 * 
				 * <pre>
				 * 1.1 dp[0] represents empty and 0,1 represents first character
				 * 
				 * 
				 * 2. for j=1 we have two possibility <br>
				 * 2.1 empty && first two char <br>
				 * 2.2 first and second
				 * 
				 * 3. for j=2 ,we have three possibility <br>
				 * 3.1 empty and all three 3 first char <br>
				 * 3.2 first and 1,3 sub <br>
				 * 3.3 first two && last
				 * 
				 * <pre>
				 */
				if (dp[i] && dict.contains(str.substring(i, j))) {
					dp[j] = true;
					break;
				}
			}
		}
		return dp[str.length()];
	}
}
