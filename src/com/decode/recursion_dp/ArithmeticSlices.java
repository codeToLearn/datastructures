package com.decode.recursion_dp;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *         A sequence of number is called arithmetic if it consists of at least
 *         three elements and if the difference between any two consecutive
 *         elements is the same.
 * 
 *         For example, these are arithmetic sequence:
 * 
 *         1, 3, 5, 7, 9 7, 7, 7, 7 3, -1, -5, -9 The following sequence is not
 *         arithmetic.
 * 
 *         1, 1, 2, 5, 7
 * 
 *         A zero-indexed array A consisting of N numbers is given. A slice of
 *         that array is any pair of integers (P, Q) such that 0 <= P < Q < N.
 * 
 *         A slice (P, Q) of array A is called arithmetic if the sequence: A[P],
 *         A[p + 1], ..., A[Q - 1], A[Q] is arithmetic. In particular, this
 *         means that P + 1 < Q.
 * 
 *         The function should return the number of arithmetic slices in the
 *         array A.
 */
public class ArithmeticSlices {
	int count = 0;

	public static void main(String[] args) {
		ArithmeticSlices arithmeticSlices = new ArithmeticSlices();
		System.out.println(arithmeticSlices.numberOfArithmeticSlicesBruteForce(new int[] { 1, 2, 3, 4, 5, 6 }));
		arithmeticSlices.numberOfArithmeticSliceDP(new int[] { 1, 2, 3, 4, 5, 6 });
	}

	public int numberOfArithmeticSlices(int[] A) {
		return 0;
	}

	/**
	 * 
	 * @param A
	 * @return This n^2 approach can we do better??
	 * 
	 */
	private int numberOfArithmeticSlicesBruteForce(int[] A) {
		count = 0;
		int difference = 0;
		int j = 0;
		for (int i = 0; i < A.length - 2; i++) {

			j = i + 2;
			difference = 0;
			if (j < A.length) {
				difference = A[j] - A[j - 1];
				if (difference == A[j - 1] - A[j - 2]) {
					count++;
					j++;
					for (; j < A.length; j++) {
						if (A[j] - A[j - 1] == difference) {
							count++;
						} else {
							break;
						}
					}
				}

			}

		}
		return count;
	}

	/**
	 * 
	 * @param A
	 *            Can we use extra space and make it O(n) If we look previous
	 *            problem 1,2,3,4 we have find 2,3,4 has been calculated twice make
	 *            it once. If array length is larger then same will happen for them
	 *            therefore subproblems are there before using dp lets try to write
	 *            down its recursive approach
	 */
	private void numberOfArithmeticSliceRecursive(int A[]) {
		numberOfArithmeticSliceRecursive(A, A.length - 1);
	}

	private int numberOfArithmeticSliceRecursive(int A[], int index) {
		if (index < 2) {
			return 0;
		}
		int slice = 0;
		if (A[index] - A[index - 1] == A[index - 1] - A[index - 2]) {
			slice = 1 + numberOfArithmeticSliceRecursive(A, index - 1);
			count += slice;
		} else {
			numberOfArithmeticSliceRecursive(A, index - 1);

		}
		// this means that index not be included into any of sub-array
		// means breakage of that array
		return slice;

	}

	private int numberOfArithmeticSliceDP(int A[]) {
		int dp[] = new int[A.length];
		dp[0] = 0;
		dp[1] = 0;
		int sum = 0;
		for (int index = 2; index < A.length; index++) {
			if (A[index] - A[index - 1] == A[index - 1] - A[index - 2]) {
				dp[index] = 1 + dp[index - 1];
				sum += dp[index];
			} else {
				dp[index] = dp[index - 1];
			}
		}
		// dp[n-1] 1+dp[n-2]

		return sum;
	}
}
