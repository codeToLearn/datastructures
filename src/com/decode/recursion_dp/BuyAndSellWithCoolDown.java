package com.decode.recursion_dp;

/*
 * Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times) with the following restrictions:

You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
After you sell your stock, you cannot buy stock on next day. (ie, cooldown 1 day)
Example:

Input: [1,2,3,0,2]
Output: 3 
Explanation: transactions = [buy, sell, cooldown, buy, sell]
 */
public class BuyAndSellWithCoolDown {

	public static void main(String[] args) {
		System.out.println(maxProfitWithCooldown(new int[] { 1, 4, 3, 0, 2 }));
	}

	private static int maxProfitWithCooldown(int[] prices) {

		if (prices.length <= 1) {
			return 0;
		}
		int s0[] = new int[prices.length];//buy
		int s1[] = new int[prices.length];//sell
		int s2[] = new int[prices.length];//rest

		s1[0] = -prices[0];
		s0[0] = 0;
		s2[0] = Integer.MIN_VALUE;
		System.out.println("index = " + 0 + " S0 ::" + s0[0] + ",S1 :: " + s1[0] + ", S2 :: " + s2[0]);
		for (int i = 1; i < prices.length; i++) {
			s0[i] = Math.max(s0[i - 1], s2[i - 1]);// Stay at s0, or rest from s2
			s1[i] = Math.max(s1[i - 1], s0[i - 1] - prices[i]); // Stay at s1, or buy from s0
			s2[i] = s1[i - 1] + prices[i];// Only one way from s1
			System.out.println("index = " + i + " S0 ::" + s0[i] + ",S1 :: " + s1[i] + ", S2 :: " + s2[i]);
		}
		return Math.max(s0[prices.length - 1], s2[prices.length - 1]);
	}
}
