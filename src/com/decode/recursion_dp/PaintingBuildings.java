package com.decode.recursion_dp;

import java.util.Arrays;

/**
 * 
 * @author Sachin Jindal
 *
 *         There are N buildings in a locality.Each building must be paint with
 *         a type of paint.Initially, some buildings are painted with some type
 *         of paint.The building that is painted can not be painted again.You
 *         are given M types of paints, 1 to M inclusive.The specialty of
 *         locality is defined as number of contiguous buildings that are
 *         painted with same type of paint.For example,if six building
 *         apartments are painted from left to right are {1,2,1,1,3,3} , then
 *         the likelihood of the apartment is 4[{1},{2},{1,1}and {3,3}].You are
 *         given an N * M matrix ,where ith and jth row and column represents
 *         building and respective color. denote the cost. You are required to
 *         determine the minimum cost to print all buildings such than specialty
 *         of apartment is exactly K else return -1;
 */
public class PaintingBuildings {
	public static void main(String[] args) {
		PaintingBuildings paintingBuildings = new PaintingBuildings();
		int[][] cost = new int[][] { { 100, 20 }, { 30, 59 }, { 71, 81 }, { 100, 200 } };
		System.out.println(paintingBuildings.minCost(cost, 1, new int[] { 0, 0, 0, 0 }));
	}

	public int minCost(int[][] cost, int K, int[] buildings) {
		for (int i = 0; i < buildings.length; i++) {

			buildings[i]--;

		}
		System.out.println(minCost(cost, K, 0, -1, buildings));
		return minCostDP(cost, K, buildings);
	}

	/**
	 * 
	 * @param cost
	 * @param k
	 * @param building
	 * @param prePaint
	 * @param buildings
	 * @return
	 */
	private int minCost(int[][] cost, int k, int building, int prePaint, int buildings[]) {
		if (k < 0 || (k > 0 && building == buildings.length)) {
			// not possible
			return -1;
		}
		if (k == 0 && building == cost.length) {
			return 0;
		}

		// this constraint indicates that we can not change the color
		int min = Integer.MAX_VALUE;
		int value = -1;
		if (buildings[building] == -1) {

			for (int paint = 0; paint < cost[building].length; paint++) {
				// first building
				if (building == 0) {
					value = minCost(cost, k - 1, building + 1, paint, buildings);

				} else {
					// match with previous
					if (prePaint == paint) {
						// no need to dec k
						value = minCost(cost, k, building + 1, paint, buildings);
					} else {
						value = minCost(cost, k - 1, building + 1, paint, buildings);
					}
				}
				if (value != -1) {
					min = Math.min(min, value + cost[building][paint]);
				}
			}

		} else {
			// first building
			if (building == 0) {
				value = minCost(cost, k - 1, building + 1, buildings[building], buildings);

			} else {
				if (prePaint == buildings[building]) {
					// no need to dec k
					value = minCost(cost, k, building + 1, buildings[building], buildings);
				} else {
					value = minCost(cost, k - 1, building + 1, buildings[building], buildings);
				}
			}
			if (value != -1) {
				return 0;

			}
		}

		return min == Integer.MAX_VALUE ? -1 : min;
	}

	private int minCostDP(int[][] cost, int k, int buildings[]) {
		int[][][] dp = new int[cost.length + 1][cost[0].length][k + 1];

		// STEP 1. Initialize all with -1
		for (int building = 0; building < dp.length; building++) {
			for (int color = 0; color < dp[building].length; color++) {
				for (int sp = 0; sp <= k; sp++) {
					dp[building][color][sp] = -1;
				}
			}
		}

		// STEP 2. LAST ROW SHOULD BE FILLED WHERE N=LENGTH M=1->M AND K=1
		for (int color = 0; color < dp[cost.length - 1].length; color++) {
			dp[dp.length - 1][color][0] = 0;
		}

		// STEP 3. FILL THE DP TABLE
		int min = Integer.MAX_VALUE;
		for (int building = dp.length - 2; building > 0; building--) {

			for (int color = 0; color < dp[cost.length - 1].length; color++) {
				for (int sp = 0; sp <= k; sp++) {
					min = Integer.MAX_VALUE;

					if (sp == 0) {
						if (dp[building + 1][color][sp] != -1) {
							dp[building][color][sp] = cost[building][color] + dp[building + 1][color][sp];
						}
					} else {
						// here we need to find out min from same or different color if possible
						for (int nextColor = 0; nextColor < dp[cost.length - 1].length; nextColor++) {
							if (nextColor == color) {
								// same
								if (dp[building + 1][nextColor][sp] != -1) {
									min = Math.min(dp[building + 1][nextColor][sp], min);
								}
							} else {
								if (dp[building + 1][nextColor][sp - 1] != -1) {
									min = Math.min(dp[building + 1][nextColor][sp - 1], min);
								}

							}
						}
						dp[building][color][sp] = min == Integer.MAX_VALUE ? -1 : cost[building][color] + min;
					}

				}
			}

		}

		//
		// STEP 4. find min in N=0,K=K
		min = Integer.MAX_VALUE;
		// for building first
		for (int color = 0; color < dp[cost.length - 1].length; color++) {
			if (dp[1][color][k - 1] != -1) {
				min = Math.min(min, dp[1][color][k - 1]);
			}

		}
		int value = Integer.MAX_VALUE;
		for (int color = 0; color < dp[cost.length - 1].length; color++) {
			value = Math.min(value, cost[0][color]);
		}
		return min == Integer.MAX_VALUE ? -1 : min + value;
	}
}
