package com.decode.recursion_dp;

import java.util.ArrayList;
import java.util.List;

public class GenerateParenthesis {

	public static void main(String[] args) {
		GenerateParenthesis generateParenthesis = new GenerateParenthesis();
		System.out.println(generateParenthesis.generateParenthesis(2));
	}

	public List<String> generateParenthesis(int n) {
		List<String> result = new ArrayList<String>();
		generateValidParenthesis(1, 1, "", result);

		return result;
	}

	/**
	 * 
	 * @param open
	 * @param close
	 *            The idea of using this method to generate valid parenthesis. Open
	 *            represents open parenthesis and close represents close
	 *            parenthesis. RECURSION IS BASICALLY DEPENDENT UPON THREE FACTORES:
	 *            1. CHOICE 2. CONSTRAINT 3. GOAL. <br>
	 *            In this question we have two choices we can pick ['(',')'].
	 *            Constraint is we can not put close parenthesis until and unless we
	 *            have open parenthesis. <br>
	 *            Our goal is to generate valid parenthesis.
	 */
	private void generateValidParenthesis(int open, int close, String parenthesisString, List<String> result) {

		/*
		 * We have our result when open and close both will be use
		 */
		if (open == 0 && close == 0) {
			result.add(parenthesisString);
			// backtrack plays here
			return;
		}
		/*
		 * At each call we have two choices 1. Insert left parenthesis 2. Insert right
		 * parenthesis
		 * 
		 */

		/*
		 * We can not insert any parenthesis if left parenthesis not exists
		 */
		if (open <= close) {
			if (open > 0) {
				// here we have insert one left
				generateValidParenthesis(open - 1, close, parenthesisString + "(", result);
			}
			if (close > 0) {
				// all left have been inserted so we are inserting now right
				generateValidParenthesis(open, close - 1, parenthesisString + ")", result);
			}
		}

	}
}
