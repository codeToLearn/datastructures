package com.decode.recursion_dp;

import java.util.ArrayList;

import java.util.List;

public class Permutations {

	public static void main(String[] args) {
		Permutations permutations = new Permutations();
		List<String> strings = new ArrayList<String>();
		permutations.permute(strings, "", new ArrayList<>());
		System.out.println(strings);
	}

	private void permute(List<String> strings, String string, List<Integer> visited) {
		if (string.length() == 5) {
			strings.add(string);
			return;
		} // choices
		for (int i = 1; i <= 5; i++) {
			// skip if already exist
			if (visited.contains(i)) {
				continue;
			}
			// 1.) Choose - Add the item to the 'runningChoices'
			visited.add(i);
			// 2.) Explore - Recurse on the choice
			permute(strings, string + i, visited);
			// 3.) Unchoose - We have returned from the recursion, remove the choice we
			// made.
			// The next iteration will try another item in the "slot" we are working on.
			visited.remove(visited.size() - 1);

		}
	}
}
