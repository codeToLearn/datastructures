package com.decode.recursion_dp;

/**
 * 
 * @author Sachin Jindal
 *
 *         In a 2D grid from (0, 0) to (N-1, N-1), every cell contains a 1,
 *         except those cells in the given list mines which are 0. What is the
 *         largest axis-aligned plus sign of 1s contained in the grid? Return
 *         the order of the plus sign. If there is none, return 0.
 * 
 *         An "axis-aligned plus sign of 1s of order k" has some center
 *         grid[x][y] = 1 along with 4 arms of length k-1 going up, down, left,
 *         and right, and made of 1s. This is demonstrated in the diagrams
 *         below. Note that there could be 0s or 1s beyond the arms of the plus
 *         sign, only the relevant area of the plus sign is checked for 1s.
 * 
 *         Examples of Axis-Aligned Plus Signs of Order k:
 * 
 *         <pre>
Order 1:
000
010
000

Order 2:
00000
00100
01110
00100
00000


Order 3:
0000000
0001000
0001000
0111110
0001000
0001000
0000000

Example 1:

Input: N = 5, mines = [[4, 2]]
Output: 2
Explanation:
11111
11111
11111
11111
11011
In the above grid, the largest plus sign can only be order 2.  One of them is marked in bold.
Example 2:

Input: N = 2, mines = []
Output: 1
Explanation:
There is no plus sign of order 2, but there is of order 1.
Example 3:

Input: N = 1, mines = [[0, 0]]
Output: 0
Explanation:
There is no plus sign, so return 0.
 *         </pre>
 */
public class LargestPlusSign {
	public static void main(String[] args) {

		LargestPlusSign largestPlusSign = new LargestPlusSign();
		// System.out.println(largestPlusSign.orderOfLargestPlusSign(5, new int[][] { {
		// 4, 2 } }));
		// System.out.println(largestPlusSign.orderOfLargestPlusSign(2, new int[][]
		// {}));
		// System.out.println(largestPlusSign.orderOfLargestPlusSignDP(5, new int[][] {
		// { 4, 2 } }));
		// System.out.println(largestPlusSign.orderOfLargestPlusSignDP(1, new int[][] {
		// { 0, 0 } }));
		// 5
		// [[1,0],[1,4],[2,4],[3,2],[4,0],[4,3]]
		System.out.println(largestPlusSign.orderOfLargestPlusSignDP(5,
				new int[][] { { 1, 0 }, { 1, 4 }, { 2, 4 }, { 3, 2 }, { 4, 0 }, { 4, 3 } }));

	}

	public int orderOfLargestPlusSign(int N, int[][] mines) {
		if (N == 0) {
			return 0;
		}
		int temp[][] = new int[N][N];
		/**
		 * Initialize by one
		 */
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				temp[i][j] = 1;
			}
		}
		for (int i = 0; i < mines.length; i++) {
			temp[mines[i][0]][mines[i][1]] = 0;
		}
		int max = 0;
		for (int row = 0; row < N; row++) {
			for (int col = 0; col < N; col++) {
				max = Math.max(recursiveAppraoch(row, col, temp, ""), max);
			}
		}
		return max;

	}

	public int orderOfLargestPlusSignDP(int N, int[][] mines) {
		if (N == 0) {
			return 0;
		}
		int temp[][] = new int[N][N];
		/**
		 * Initialize by one
		 */
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				temp[i][j] = 1;
			}
		}
		for (int i = 0; i < mines.length; i++) {
			temp[mines[i][0]][mines[i][1]] = 0;
		}
		return recursiveAppraochTODP(temp);

	}

	private int recursiveAppraoch(int row, int col, int[][] mines, String dir) {
		// System.out.println(dir);
		if (row < 0 || col < 0 || row >= mines.length || col >= mines[row].length || mines[row][col] == 0) {
			return 0;
		}
		/**
		 * Approach is simple we just need to move left,right,up,down find the minimum+1
		 */
		/**
		 * Every 1 asking to its left and right ,up and down am i the center
		 */
		switch (dir) {
		case "l":
			return 1 + recursiveAppraoch(row, col - 1, mines, dir);
		case "r":
			return 1 + recursiveAppraoch(row, col + 1, mines, dir);
		case "d":
			return 1 + recursiveAppraoch(row + 1, col, mines, dir);
		case "u":
			return 1 + recursiveAppraoch(row - 1, col, mines, dir);
		default:
			int left = recursiveAppraoch(row, col - 1, mines, "l");
			int right = recursiveAppraoch(row, col + 1, mines, "r");
			int up = recursiveAppraoch(row - 1, col, mines, "u");
			int down = recursiveAppraoch(row + 1, col, mines, "d");
			return Math.min(Math.min(up, down), Math.min(left, right)) + 1;
		}

	}

	private int recursiveAppraochTODP(int[][] mines) {
		// There are subproblems so we can use dp here
		// dp[i][j] = min(dp[i+1][j],dp[i][j+1],dp[i-1][j],dp[i][j-1]
		/**
		 * Idea is simple calculate number of left of every index and for right compare
		 * with its right
		 */

		int count = 0;
		// count left moves only left direction
		int dp[][] = new int[mines.length][mines.length];
		for (int row = 0; row < mines.length; row++) {
			count = 0;
			for (int col = 0; col < mines[row].length; col++) {
				if (mines[row][col] == 1) {
					count++;
				} else {
					count = 0;
				}
				dp[row][col] = count;
			}
			count = 0;
			// right direction
			for (int col = mines[row].length - 1; col >= 0; col--) {
				if (mines[row][col] == 1) {
					count++;
				} else {
					count = 0;
				}
				dp[row][col] = Math.min(count, dp[row][col]);
			}
		}
		// GO FOR TOP AND BOTTOM
		int max = 0;
		count = 0;
		for (int col = 0; col < mines[0].length; col++) {
			// down
			count = 0;
			for (int row = 0; row < mines.length; row++) {
				if (mines[row][col] == 1) {
					count++;
				} else {
					count = 0;
				}
				dp[row][col] = Math.min(count, dp[row][col]);
			}
			// up
			count = 0;
			for (int row = mines.length - 1; row >= 0; row--) {
				if (mines[row][col] == 1) {
					count++;
				} else {
					count = 0;
				}
				dp[row][col] = Math.min(count, dp[row][col]);
				max = Math.max(max, dp[row][col]);
			}
		}

		return max;
	}
}
