package com.decode.recursion_dp;

public class MinNumberOfIndsertionForMakingPalindrome {
	public int minInsertions(String s) {

		if (s != null && !s.isEmpty()) {
			int n = s.length();
			boolean dp[][] = new boolean[n][n];
			for (int i = 0; i < n; i++) {
				dp[i][i] = true;
			}
			int len = 1;
			int startIndex = -1;
			int endIndex = 0;
			int maxSoFar = 0;
			int start = -1;
			while (len < n) {
				endIndex = n-1;
				startIndex = endIndex - len;
				while (startIndex >= 0) {
					if (s.charAt(startIndex) == s.charAt(endIndex)) {
						if (endIndex - startIndex == 1 || dp[startIndex + 1][endIndex - 1]) {
							dp[startIndex][endIndex] = true;
							if (maxSoFar < endIndex - startIndex + 1) {
								maxSoFar = endIndex - startIndex + 1;
								start = startIndex;
							}
						}
					}
					startIndex--;
					endIndex--;
				}
				len++;
			}
			return n - maxSoFar;
		}
		return 0;
	}
}
