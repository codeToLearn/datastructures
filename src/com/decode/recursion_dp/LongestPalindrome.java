package com.decode.recursion_dp;

/**
 * 
 * @author Sachin Jindal
 *
 *         Given a string s, find the longest palindromic substring in s. You
 *         may assume that the maximum length of s is 1000.
 * 
 *         Example 1:
 * 
 *         Input: "babad" Output: "bab" Note: "aba" is also a valid answer.
 *         Example 2:
 * 
 *         Input: "cbbd" Output: "bb"
 */
public class LongestPalindrome {

	public static void main(String[] args) {
		LongestPalindrome longestPalindrome = new LongestPalindrome();
		System.out.println(longestPalindrome.longestPalindrome("baab"));
	}

	public String longestPalindrome(String s) {

		if (s == null || s.isEmpty() || s.length() == 1) {
			return s;
		}
		/**
		 * The idea behind the logic is i have divided the problem in the form of state
		 * that s represents start and e represents end it means target is to achieve
		 * e-s+1 max.
		 * 
		 * if 0,5 is palindrome it means 1,4 should be palindromic string similarly 2,3
		 * also
		 * 
		 * therefore we need to calculate it by length wise
		 */
		boolean[][] dp = new boolean[s.length()][s.length()];

		// all characters are palindrome to itself

		for (int i = 0; i < dp.length; i++) {
			dp[i][i] = true;
		}
		int startIndex = -1;
		int endIndex = -1;
		int maxsofar = 1;
		int len = 1;
		int start = 0;
		// divide into length

		while (len < s.length()) {
			endIndex = s.length() - 1;
			startIndex = endIndex - len++;
			while (startIndex >= 0) {
				if (s.charAt(startIndex) == s.charAt(endIndex)) {
					if (endIndex - startIndex == 1 || dp[startIndex + 1][endIndex - 1]) {
						dp[startIndex][endIndex] = true;
						if (maxsofar < endIndex - startIndex + 1) {
							maxsofar = endIndex - startIndex + 1;// maintain here maxlength
							start = startIndex;
						}
					}
				}
				startIndex--;
				endIndex--;

			}

		}
		return s.substring(start, start + maxsofar);
	}
}
