package com.decode.recursion_dp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MinSumPathTriangleRecursive {

	private static List<List<Integer>> triangle = new ArrayList<List<Integer>>();
	private static int count = 0;
	private static HashMap<String, Integer> map = new HashMap<>();
	private static int cache[][];

	public static void main(String[] args) {

		List<Integer> row = new ArrayList<>();
		row.add(5);
		// row 1
		triangle.add(row);
		row = new ArrayList<>();
		row.add(3);
		row.add(4);
		// row2
		triangle.add(row);
		row = new ArrayList<>();
		row.add(9);
		row.add(8);
		row.add(1);
		// row 3
		triangle.add(row);
		row = new ArrayList<>();
		row.add(4);
		row.add(5);
		row.add(8);
		row.add(2);
		// row 4
		triangle.add(row);

		cache = new int[triangle.size() + 1][triangle.size() + 1];
		for (int i = 0; i < cache.length; i++) {
			for (int j = 0; j < cache.length; j++) {
				cache[i][j] = Integer.MAX_VALUE;
			}
		}
		System.out.println(msp(0, 0));
		System.out.println("TOTAL_COUNT::" + count);
		System.out.println(map);
	}

	private static int msp(int row, int index) {
		if (map.get("" + row + "_" + index) == null) {
			map.put("" + row + "_" + index, 0);
		}
		map.put("" + row + "_" + index, map.get("" + row + "_" + index) + 1);
		count++;
		System.out.println("CALL FOR ROW ::" + row + "::INDEX" + index);
		if (row == triangle.size() || index == triangle.get(row).size()) {
			return 0;
		}

		int left = cache[row + 1][index] == Integer.MAX_VALUE ? msp(row + 1, index) : cache[row + 1][index];
		cache[row + 1][index] = left;
		int right = cache[row + 1][index + 1] == Integer.MAX_VALUE ? msp(row + 1, index + 1)
				: cache[row + 1][index + 1];
		cache[row + 1][index + 1] = right;
		return (left > right ? right : left) + triangle.get(row).get(index);
	}
}
