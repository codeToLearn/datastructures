package com.decode.recursion_dp;

import java.util.ArrayList;
import java.util.List;

public class MinSumPathTriangleDP {
	private static List<List<Integer>> triangle = new ArrayList<List<Integer>>();

	public static void main(String[] args) {

		List<Integer> row = new ArrayList<>();
		row.add(5);
		// row 1
		triangle.add(row);
		row = new ArrayList<>();
		row.add(3);
		row.add(4);
		// row2
		triangle.add(row);
		row = new ArrayList<>();
		row.add(9);
		row.add(8);
		row.add(1);
		// row 3
		triangle.add(row);
		row = new ArrayList<>();
		row.add(4);
		row.add(5);
		row.add(8);
		row.add(2);
		// row 4
		triangle.add(row);

		int table[][] = new int[triangle.size() + 1][triangle.size() + 1];
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table.length; j++) {
				table[i][j] = 0;
			}
		}
		int rowNum = 0;
		for (List<Integer> rows : triangle) {
			for (int i = 0; i < rows.size(); i++) {
				table[rowNum][i] = rows.get(i);
			}
			rowNum++;
		}
		for (int i = triangle.size() - 2; i >= 0; i--) {
			for (int j = triangle.size() - 2; j >= 0; j--) {
				table[i][j] += Math.min(table[i + 1][j], table[i + 1][j + 1]);
			}
		}
		System.out.println(table[0][0]);
	}

}
