package com.decode.recursion_dp;

/**
 * 
 * @author Sachin Jindal There are a row of n houses, each house can be painted
 *         with one of the three colors: red, blue or green. The cost of
 *         painting each house with a certain color is different. You have to
 *         paint all the houses such that no two adjacent houses have the same
 *         color.
 * 
 *         The cost of painting each house with a certain color is represented
 *         by a n x 3 cost matrix. For example, costs[0][0] is the cost of
 *         painting house 0 with color red; costs[1][2] is the cost of painting
 *         house 1 with color green, and so on... Find the minimum cost to paint
 *         all houses.
 * 
 *         <pre>
 * INPUT : [17,2,17][16,16,5][14,3,19]
 *         		OUTPUT : 10
 *         		EXPLAINATION : Paint house 0 into blue , paint house 1 into green ,paint house 2 into red 2+5+3 =10
 *         </pre>
 */
public class PaintHouse {

	public static void main(String[] args) {
		PaintHouse house = new PaintHouse();
		int cost = house.minCost(new int[][] { { 17, 2, 17 }, { 16, 16, 5 }, { 14, 3, 19 } });

		System.out.println(cost);
		cost = house.minCostDP(new int[][] { { 17, 2, 17 }, { 16, 16, 5 }, { 14, 3, 19 } });
		System.out.println(cost);
	}

	public int minCost(int[][] costs) {
		return minCost(costs, -1, 0);
	}

	/**
	 * 
	 * @param costs
	 * @return
	 * 
	 * 		CHOICES : WE HAVE THREE CHOICE HERE TO CHOOSE RED,BLUE,GREEN COLOR
	 *         CONSTRAINTS : NO ADJACENT BUILDING HAVE SAME COLOR GOAL - MIN_COST
	 *         REQUIRED TO PAINT N BUILDINGS
	 */
	private int minCost(int[][] costs, int preColor, int building) {
		/**
		 * This represents we have reached at last building now its time to accumulate
		 * the final result.
		 */
		if (building == costs.length) {
			return 0;
		}

		int minCost = Integer.MAX_VALUE;
		for (int color = 0; color < costs[building].length; color++) {
			if (preColor == color) {
				continue;
			}
			minCost = Math.min(minCost, costs[building][color] + minCost(costs, color, building + 1));
		}
		return minCost;
	}

	/**
	 * 
	 * @param costs
	 * @return There are subproblems which means we can use dp here
	 * 
	 *         For example - 3 buildings are there now our main concern is find
	 *         subproblems.
	 *
	 */
	private int minCostDP(int[][] costs) {

		int[][] dp = new int[costs.length][costs[0].length];
		// fill the last
		for (int color = 0; color < costs[costs.length - 1].length; color++) {
			dp[costs.length - 1][color] = costs[costs.length - 1][color];
		}
		// fill here dp here
		int cost = Integer.MAX_VALUE;
		if (costs.length >= 2) {
			for (int building = costs.length - 2; building >= 0; building--) {
				for (int color = 0; color < costs[costs.length - 1].length; color++) {
					cost = Integer.MAX_VALUE;
					for (int choice = 0; choice < costs[costs.length - 1].length; choice++) {
						if (choice == color) {
							continue;
						}
						cost = Math.min(cost, dp[building + 1][choice]);
						// min of two

					}
					dp[building][color] = costs[building][color] + cost;
				}
			}
		}

		int minCost = Integer.MAX_VALUE;
		for (int color = 0; color < dp[0].length; color++) {
			minCost = Math.min(minCost, dp[0][color]);
		}
		return minCost;
	}
}
