package com.decode.recursion_dp;

public class PerfectSquares {

	public static void main(String[] args) {
		PerfectSquares perfectSquares = new PerfectSquares();
		perfectSquares.numSquares(12);
	}

	public int numSquares(int n) {
		if (n == 0 || n == 1) {
			return n;
		}
		int dp[] = new int[n + 1];
		dp[1] = 1;
		for (int i = 2; i <= n; i++) {
			int min = Integer.MAX_VALUE;
			for (int j = 1; j * j <= i; j++) {
				min = Math.min(min, 1 + dp[i - (j * j)]);
			}
			dp[i] = min;
		}
		return dp[n];
	}
}
