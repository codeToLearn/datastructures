package com.decode.recursion_dp;

public class PartionEqualSubset {

	public static void main(String[] args) {

		PartionEqualSubset equalSubset = new PartionEqualSubset();
		int[] arr = {1,5,11,5};
		equalSubset.canPartition(arr);
	}

	public boolean canPartition(int[] nums) {

		int sum = 0;

		for (Integer i : nums) {
			sum += i;
		}
		if (sum % 2 != 0) {
			return false;
		}
		return canPartitionDPOpt(nums, sum/2);
	}

	public boolean canPartition(int[] nums, int index, int sum) {

		if (sum == 0) {
			return true;
		}
		if (index < nums.length) {

			if (sum - nums[index] < 0) {
				return canPartition(nums, index + 1, sum);
			}
			return canPartition(nums, index + 1, sum - nums[index]) || canPartition(nums, index + 1, sum);

		}
		return false;
	}

	public boolean canPartitionDP(int[] nums, int total) {

		boolean dp[][] = new boolean[nums.length + 1][total + 1];
		for (int i = nums.length - 1; i >= 0; i--) {
			for (int sum = total; sum >= 0; sum--) {
				if (sum - nums[i] == 0) {
					dp[i][sum] = true;
					continue;
				}
				if (sum - nums[i] < 0) {
					dp[i][sum] = dp[i + 1][sum];
				} else {
					dp[i][sum] = dp[i + 1][sum] || dp[i + 1][sum - nums[i]];
				}
			}

		}
		return dp[0][total];
	}

	public boolean canPartitionDPOpt(int[] nums, int total) {
		boolean[] dp = new boolean[total + 1];
		dp[0] = true;
		for (int i = 0; i < nums.length; i++) {
			for (int sum = total; sum > 0; sum--) {
				if (sum - nums[i] >= 0)
					dp[sum] = dp[sum] | dp[sum - nums[i]];
			}
		}
		return dp[total];
	}
}
