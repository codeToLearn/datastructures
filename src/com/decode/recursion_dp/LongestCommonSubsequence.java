package com.decode.recursion_dp;

/**
 * 
 * @author Sachin Jindal Given two strings text1 and text2, return the length of
 *         their longest common subsequence.
 * 
 *         A subsequence of a string is a new string generated from the original
 *         string with some characters(can be none) deleted without changing the
 *         relative order of the remaining characters. (eg, "ace" is a
 *         subsequence of "abcde" while "aec" is not). A common subsequence of
 *         two strings is a subsequence that is common to both strings.
 * 
 * 
 * 
 *         If there is no common subsequence, return 0.
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input: text1 = "abcde", text2 = "ace" Output: 3 Explanation: The
 *         longest common subsequence is "ace" and its length is 3. Example 2:
 * 
 *         Input: text1 = "abc", text2 = "abc" Output: 3 Explanation: The
 *         longest common subsequence is "abc" and its length is 3. Example 3:
 * 
 *         Input: text1 = "abc", text2 = "def" Output: 0 Explanation: There is
 *         no such common subsequence, so the result is 0.
 * 
 * 
 *         Constraints:
 * 
 *         1 <= text1.length <= 1000 1 <= text2.length <= 1000 The input strings
 *         consist of lowercase English characters only
 */
public class LongestCommonSubsequence {

	public static void main(String[] args) {

		LongestCommonSubsequence longestCommonSubsequence = new LongestCommonSubsequence();
		int l = longestCommonSubsequence.longestCommonSubsequence("abc", "def");
		System.out.println(l);
		int l1 = longestCommonSubsequence.lcsDP("abc", "abc");
		System.out.println(l1);
	}

	public int longestCommonSubsequence(String text1, String text2) {
		if (isEmpty(text1) || isEmpty(text2)) {
			return 0;
		}

		return lcs(text1.length() - 1, text2.length() - 1, text1, text2);
	}

	private int lcs(int index1, int index2, String text1, String text2) {
		if (isEmpty(text1) || isEmpty(text2) || index1 < 0 || index2 < 0) {
			return 0;
		}
		/*
		 * If two character are same abc and ac last character are same therefore
		 * 1+lcs(ab,a)
		 */

		if (text1.charAt(index1) == text2.charAt(index2)) {
			return 1 + lcs(index1 - 1, index2 - 1, text1, text2);
		}
		/*
		 * Now we need to backtrack again and again
		 */
		return Math.max(lcs(index1 - 1, index2, text1, text2), lcs(index1, index2 - 1, text1, text2));

	}

	private boolean isEmpty(String text) {
		return text == null || text.isEmpty();
	}

	/**
	 * 
	 * @param text1
	 * @param text2
	 *            Yes it contains subproblems so we can use top-down approach if we
	 *            check there are two parameters i for text1 and j for text2 if two
	 *            characters are same then dp[i][j] = 1+dp[i-1][j-1] else dp[i][j] =
	 *            Math.max(dp[i-1][j],dp[i][j-1])
	 * 
	 * @return dp[n+1][m+1]
	 */

	private int lcsDP(String text1, String text2) {
		/**
		 * Table size is n+1 and m+1 because there is check for empty string also
		 */
		/*
		 * e.g s1=abc,s2=abc dp[][] = new dp[s1.length+1][s2.length+1] dp[0][0]
		 * represents empty string
		 */
		int[][] dp = new int[text1.length() + 1][text2.length() + 1];
		for (int index1 = 0; index1 < text1.length() + 1; index1++) {

			for (int index2 = 0; index2 < text2.length() + 1; index2++) {
				if (index1 == 0 || index2 == 0) {
					// represents empty string
					dp[index1][index2] = 0;
				} else {
					/*
					 * If Same
					 */
					if (text1.charAt(index1-1) == text2.charAt(index2-1)) {
						dp[index1][index2] = 1 + dp[index1 - 1][index2 - 1];
					} else {
						dp[index1][index2] = Math.max(dp[index1 - 1][index2], dp[index1][index2 - 1]);
					}
				}

			}
		}
		return dp[text1.length()][text2.length()];
	}
}
