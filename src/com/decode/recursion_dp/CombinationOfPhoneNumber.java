package com.decode.recursion_dp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CombinationOfPhoneNumber {
	Map<Character, char[]> map = new HashMap();
	{

		map.put('2', new char[] { 'a', 'b', 'c' });
		map.put('3', new char[] { 'd', 'e', 'f' });
		map.put('4', new char[] { 'g', 'h', 'i' });
		map.put('5', new char[] { 'j', 'k', 'l' });
		map.put('6', new char[] { 'm', 'n', 'o' });

		map.put('7', new char[] { 'p', 'q', 'r', 's' });
		map.put('8', new char[] { 't', 'u', 'v' });
		map.put('9', new char[] { 'w', 'x', 'y', 'z' });
	}

	public static void main(String[] args) {
		CombinationOfPhoneNumber phoneNumber = new CombinationOfPhoneNumber();
		System.out.println(phoneNumber.letterCombinations("722446"));
	}

	public List<String> letterCombinations(String digits) {
		List<String> result = new ArrayList<String>();
		letterCombinations(0, digits.toCharArray(), result, "");
		return result;
	}

	private void letterCombinations(int index, char[] digits, List<String> result, String string) {

		if (string.length() == digits.length) {
			if (string.equals("sachin")) {
				System.out.println();
			}
			result.add(string);
			return;
		}
		for (int i = index; i < digits.length; i++) {
			// fixed first character
			for (int j = 0; j < map.get(digits[i]).length; j++) {
				// iterate over all the characters of fixed character
				//map.get(digits[i]) tell use about character and [j] represents all possible characters and string was the previous string
				letterCombinations(i + 1, digits, result, string + map.get(digits[i])[j]);
				// backtracking and string immutable plays an important role
			}
		}
	}
}
