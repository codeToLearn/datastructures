package com.decode.recursion_dp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Sachin Jindal
 *
 *         You have given a value m and size of array n .Print all unique arrays
 *         can be built using these two parameters.
 */
public class UniqueArraysOfGivenSizeWithGivenValue {
	private final long mod = (long) 1e9 + 7;

	public static void main(String[] args) {
		UniqueArraysOfGivenSizeWithGivenValue arraysOfGivenSizeWithGivenValue = new UniqueArraysOfGivenSizeWithGivenValue();
		// System.out.println(arraysOfGivenSizeWithGivenValue.uniqueArray(2, 15));
		// System.out.println(arraysOfGivenSizeWithGivenValue.uniqueArrayCount(2, 10));
		// System.out.println(arraysOfGivenSizeWithGivenValue.uniqueArrayCountDP(2,
		// 10));
		// System.out.println(arraysOfGivenSizeWithGivenValue.mod);
		// System.out.println(arraysOfGivenSizeWithGivenValue.uniqueArrayHavingComparisonsK(3,
		// 2, 1, -1));
		// System.out.println(arraysOfGivenSizeWithGivenValue.uniqueArrayHavingComparisonsKCount(3,
		// 2, 1, -1));
		System.out.println(arraysOfGivenSizeWithGivenValue.uniqueArrayHavingComparisonsK(5, 3, 3, -1));
		// System.out.println(arraysOfGivenSizeWithGivenValue.uniqueArrayHavingComparisonsKCount(2,
		// 5, 3, -1));
		// arraysOfGivenSizeWithGivenValue.uniqueArrayHavingComparisonsK(5, 3, 2, -1);
		// System.out.println(arraysOfGivenSizeWithGivenValue.uniqueArrayHavingComparisonsK(3,
		// 9, 1, -1));
		// System.out.println(arraysOfGivenSizeWithGivenValue.uniqueArrayHavingComparisonsKCount(5,
		// 3, 2, -1));

	}

	public List<List<Integer>> uniqueArray(int m, int n) {
		/*
		 * The idea is simple we can choose numbers from 1->m therefore we can choose
		 * every number since n becomes 0
		 */
		if (n > 0) {
			List<List<Integer>> result = new ArrayList<>();
			for (int i = 1; i <= m; i++) {
				List<List<Integer>> temp = uniqueArray(m, n - 1);
				if (temp == null) {
					// it means only one element added
					List<Integer> arr = new ArrayList<>();
					arr.add(i);
					result.add(arr);
				} else {
					for (List<Integer> list : temp) {
						list.add(i);
						result.add(list);
					}
				}
			}
			return result;
		}
		return null;

	}

	public int uniqueArrayCount(int m, int n) {

		if (n > 0) {
			int count = 0;
			for (int i = 1; i <= m; i++) {
				int temp = uniqueArrayCount(m, n - 1);
				if (temp == 0) {
					count++;
				} else {
					count += temp;
				}
			}
			return count;
		}

		return 0;
	}

	public int uniqueArrayCountDP(int m, int n) {
		/*
		 * There are subproblems and equation can be written as dp[i] = m*dp[i-1];
		 */
		int[] dp = new int[n];
		dp[0] = m;
		for (int i = 1; i < n; i++) {
			dp[i] = m * dp[i - 1];
		}
		return dp[n - 1];
	}

	public List<List<Integer>> uniqueArrayHavingComparisonsK(int m, int n, int k, int max) {
		/*
		 * In this i have maintained the maxsofar in the list
		 */

		// System.out.println(m + "," + n + "," + k + "," + max);
		List<List<Integer>> result = null;

		if (n == 0 && k == 0) {
			/**
			 * Return empty so we can combine all choose value
			 */
			result = new ArrayList<>();
			List<Integer> arr = new ArrayList<>();

			result.add(arr);
			return result;
		}
		if (n > 0) {
			if (k < 0) {
				// no comparisons are left
				return null;
			}
			result = new ArrayList<>();
			for (int i = 1; i <= m; i++) {
				List<List<Integer>> temp = null;
				if (max < i) {
					// greater than max means comparisons possible
					temp = uniqueArrayHavingComparisonsK(m, n - 1, k - 1, i);
				} else {
					temp = uniqueArrayHavingComparisonsK(m, n - 1, k, max);
				}

				if (temp != null) {
					for (List<Integer> list : temp) {
						//beacuse insert at first point always
						list.add(0, i);
						// merge all results together
					//	Collections.reverse(list);
						result.add(list);

					}
				}
			}
			return result;
		}
		return null;

	}

	public long uniqueArrayHavingComparisonsKCount(int m, int n, int k, int max) {
		/*
		 * In this i have maintained the maxsofar in the list
		 */

		if (n == 0 && k == 0) {
			/**
			 * Return empty so we can combine all choose values One possible result is there
			 */

			return 1;
		}
		if (n > 0) {
			if (k < 0) {
				// no comparisons are left
				// no possible arrays
				return 0;
			}
			long count = 0;
			for (int i = 1; i <= m; i++) {
				long temp = 0;
				if (max < i) {
					// greater than max means comparisons possible
					temp = uniqueArrayHavingComparisonsKCount(m, n - 1, k - 1, i);
				} else {
					temp = uniqueArrayHavingComparisonsKCount(m, n - 1, k, max);
				}

				count += temp;
			}
			return count % mod;
		}
		return 0;

	}

	public long uniqueArrayHavingComparisonsKCountDP(int m, int n, int k, int max) {
		/*
		 * In this i have maintained the maxsofar in the list
		 */

		long[][][] dp = new long[n][n][n];

		/*
		 * if (n == 0 && k == 0) {
		 *//**
			 * Return empty so we can combine all choose values One possible result is there
			 *//*
				 * 
				 * return 1; }
				 */
		for (int i = 0; i < n; i++) {
			dp[i][0][0] = 1;
		}
		if (n > 0) {
			if (k < 0) {
				// no comparisons are left
				// no possible arrays
				return 0;
			}
			long count = 0;
			for (int i = 1; i <= m; i++) {
				long temp = 0;
				if (max < i) {
					// greater than max means comparisons possible
					temp = uniqueArrayHavingComparisonsKCount(m, n - 1, k - 1, i);
				} else {
					temp = uniqueArrayHavingComparisonsKCount(m, n - 1, k, max);
				}

				count += temp;
			}
			return dp[n - 1][n - 1][n - 1] % mod;
		}
		return 0;

	}
}
