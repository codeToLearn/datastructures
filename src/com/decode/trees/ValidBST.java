package com.decode.trees;

public class ValidBST {
	public static void main(String[] args) {
		TreeNode root = new TreeNode(20);
		root.left = new TreeNode(10);
		root.right = new TreeNode(30);
		root.left.left = new TreeNode(5);
		root.left.left.right = new TreeNode(7);
		root.left.right = new TreeNode(15);
		root.right.left = new TreeNode(25);
		root.right.right = new TreeNode(35);
		root.left.right.left = new TreeNode(13);
		root.left.right.right = new TreeNode(18);

		/*
		 * [5,1,4,null,null,3,6]
		 */
		root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.right = new TreeNode(15);
		root.right.left = new TreeNode(12);
		root.right.left.right = new TreeNode(6);
		root.right.left.left = new TreeNode(11);
		root.right.right = new TreeNode(20);

		System.out.println(new ValidBST().validBSTSimpleAppraoch(root)); // n2logn
		System.out.println(new ValidBST().validBSTWithUpperAndLowerBound(root, null, null));
	}

	// this appraoch is n2Logn space is 0(1)
	private boolean validBSTSimpleAppraoch(TreeNode root) {
		if (root == null) {
			return true;
		}
		return isLesser(root.left, root.val) && isGreater(root.right, root.val) && validBSTSimpleAppraoch(root.left)
				&& validBSTSimpleAppraoch(root.right);

	}

	private boolean isLesser(TreeNode root, int value) {
		if (root == null) {
			return true;
		}
		if (root.val < value) {
			return isLesser(root.left, value) && isLesser(root.right, value);
		}
		return false;
	}

	private boolean isGreater(TreeNode root, int value) {
		if (root == null) {
			return true;
		}
		if (root.val >= value) {
			return isGreater(root.left, value) && isGreater(root.right, value);
		}
		return false;
	}

	// this approach is n space is o(2n)
	/*
	 * The idea above could be implemented as a recursion. One compares the node
	 * value with its upper and lower limits if they are available. Then one repeats
	 * the same step recursively for left and right subtrees.
	 */
	private boolean validBSTWithUpperAndLowerBound(TreeNode root, Integer upper, Integer lower) {
		if (root == null) {
			return true;
		}
		System.out.println("lower:"+lower+" , value:"+root.val+" ,upper:"+upper );
		int val = root.val;
		if (lower != null && val <= lower) {
			return false;
		}
		if (upper != null && val >= upper) {
			return false;
		}
		return validBSTWithUpperAndLowerBound(root.left, val, lower)
				&& validBSTWithUpperAndLowerBound(root.right, upper, val);
	}
}
