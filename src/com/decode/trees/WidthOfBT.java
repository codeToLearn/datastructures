package com.decode.trees;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 
 * @author Sachin Jindal
 *
 *         Given a binary tree, write a function to get the maximum width of the
 *         given tree. The width of a tree is the maximum width among all
 *         levels. The binary tree has the same structure as a full binary tree,
 *         but some nodes are null.
 * 
 *         The width of one level is defined as the length between the end-nodes
 *         (the leftmost and right most non-null nodes in the level, where the
 *         null nodes between the end-nodes are also counted into the length
 *         calculation.
 */
public class WidthOfBT {

	public static void main(String[] args) {
	}

	public int widthOfBinaryTree(TreeNode root) {
		return widthOfBTLevelOrderTraversal(root);
	}

	/**
	 * This solution is TLE because we need to traverse null also to restrict null
	 * access i have come up different solution
	 */
	private int widthOfBTLevelOrderTraversal(TreeNode node) {
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.offer(node);
		int max = 0;
		int count = 0;
		// [1,3,2,null,3,null,9]
		int nullCount = 0;
		TreeNode treeNode;
		while (!queue.isEmpty()) {
			int size = queue.size();

			nullCount = 0;
			count = 0;

			for (int i = 0; i < size; i++) {
				treeNode = queue.poll();

				if (treeNode == null) {
					if (count > 0) {
						nullCount++;
						queue.offer(null);
						queue.offer(null);// if no count available it means no need to count null
					}
				} else {

					count += nullCount + 1;
					nullCount = 0;
					/*
					 * If first node and its left null then it need to check its right exists or not
					 */
					if (i == 0 && treeNode.left == null) {
						if (treeNode.right != null) {
							queue.offer(treeNode.right);
						}
					} else if (i == size - 1 && treeNode.right == null) {
						// similarly check its right for last node
						if (treeNode.left != null) {
							queue.offer(treeNode.left);
						}

					} else {

						queue.offer(treeNode.left);

						queue.offer(treeNode.right);

					}

				}

			}
			if (count == 0) {
				queue.clear();
			}
			max = Math.max(max, count);
		}
		return max;
	}

	/**
	 * In this solution i have come with different idea 2*position and 2*position+1
	 * Final solution will be rightmost position - leftmost position
	 */
	private int maxWidthWithLevelOrderTraversal(TreeNode root) {
		
		int maxWidth = 0;
		Queue<Integer> position = new LinkedList<>();
		Queue<TreeNode> levelOrderQueue = new LinkedList<>();

		levelOrderQueue.offer(root);
		position.offer(1);
		int left = 0;
		int right = 0;
		int size = 0;
		int pos;
		TreeNode node;
		while (!levelOrderQueue.isEmpty()) {
			size = levelOrderQueue.size();
			for (int i = 0; i < size; i++) {
				pos = position.poll();
				node = levelOrderQueue.poll();
				if (i == 0) {
					left = pos;
				}
				if (i == size - 1) {
					right = pos;
				}
				if (node.left != null) {
					levelOrderQueue.offer(node.left);
					position.offer(2 * pos);

				}
				if (node.right != null) {
					levelOrderQueue.offer(node.right);
					position.offer(2 * pos + 1);
				}
			}
			maxWidth = Math.max(maxWidth, right - left + 1);

		}
		return maxWidth;
	}

}
