package com.decode.trees;

import java.util.ArrayList;
import java.util.List;

public class KdistanceNode {
	private List<Integer> result = new ArrayList<Integer>();

	public static void main(String[] args) {
		TreeNode node = TreeNode.stringToTreeNode("[0,1,null,3,2]");
		KdistanceNode kdistanceNode = new KdistanceNode();
		kdistanceNode.distanceK(node, new TreeNode(2), 1);

	}

	public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
		findNode(root, target, K);
		return result;
	}

	private int findNode(TreeNode root, TreeNode target, int k) {
		if (root == null) {
			return -1;
		}
		if (root.val == target.val) {
			printDown(root, k);
			return 0;
		}
		int findNode = findNode(root.left, target, k);
		// left subtree
		if (findNode != -1) {
			// backtrack
			if (findNode + 1 == k) {
				// means at root level
				result.add(root.val);
				return -1;
			}
			if (root.right != null) {
				printDown(root.right, k - findNode - 2);
			}
			return findNode + 1;
		}
		// right subtree
		findNode = findNode(root.right, target, k);
		if (findNode != -1) {
			// backtrack
			if (findNode + 1 == k) {
				// means at root level
				result.add(root.val);
				return -1;
			}
			if (root.left != null) {
				printDown(root.left, k - findNode - 2);
			}
			return findNode + 1;
		}
		return -1;
	}

	private void printDown(TreeNode treeNode, int k) {
		if (treeNode == null || k < 0) {
			return;
		}
		if (k == 0) {
			result.add(treeNode.val);
		}
		printDown(treeNode.left, k - 1);
		printDown(treeNode.right, k - 1);
	}
}
