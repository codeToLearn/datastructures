package com.decode.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class RightSideView {

	public static void main(String[] args) {
		RightSideView rightSideView = new RightSideView();
		String string = "[1,2,3,null,5,null,4]";
		TreeNode root = TreeNode.stringToTreeNode(string);
		rightSideView.rightSideView(root);

	}

	public List<Integer> rightSideView(TreeNode root) {
		if (root == null) {
			return new ArrayList<Integer>();
		}
		Queue<TreeNode> queue = new LinkedList<>();
		List<Integer> list = new ArrayList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			int size = queue.size();
			for (int i = 0; i < size; i++) {
				TreeNode poll = queue.poll();
				if (i == size - 1) {
					list.add(poll.val);
				}
				if (poll.left != null) {
					queue.offer(poll.left);

				}
				if (poll.right != null) {
					queue.offer(poll.right);

				}
			}
		}
		return list;

	}
}
