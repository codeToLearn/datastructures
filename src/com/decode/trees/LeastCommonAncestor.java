package com.decode.trees;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *         Given a binary tree, find the lowest common ancestor (LCA) of two
 *         given nodes in the tree.
 * 
 *         According to the definition of LCA on Wikipedia: �The lowest common
 *         ancestor is defined between two nodes p and q as the lowest node in T
 *         that has both p and q as descendants (where we allow a node to be a
 *         descendant of itself).�
 * 
 *         Given the following binary tree: root = [3,5,1,6,2,0,8,null,null,7,4]
 * 
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1 Output: 3
 *         Explanation: The LCA of nodes 5 and 1 is 3. Example 2:
 * 
 *         Input: root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4 Output: 5
 *         Explanation: The LCA of nodes 5 and 4 is 5, since a node can be a
 *         descendant of itself according to the LCA definition.
 */
public class LeastCommonAncestor {
	public static void main(String[] args) {
		TreeNode node = new TreeNode(3);
		node.left = new TreeNode(5);
		node.right = new TreeNode(1);
		node.left.left = new TreeNode(6);
		node.left.right = new TreeNode(2);
		node.right.left = new TreeNode(0);
		node.right.right = new TreeNode(8);
		node.left.right.left = new TreeNode(7);
		node.left.right.right = new TreeNode(4);
		LeastCommonAncestor leastCommonAncestor = new LeastCommonAncestor();
		TreeNode treeNode = leastCommonAncestor.lowestCommonAncestor(node, new TreeNode(8), new TreeNode(7));
		System.out.println(treeNode);

	}

	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		/*
		 * If no value found
		 */
		if (root == null) {
			return root;
		}
		/*
		 * If any value found it means that node is ancestor
		 */
		if (root.val == q.val || root.val == p.val) {
			return root;
		}
		/**
		 * GO deep down left
		 */
		TreeNode left = lowestCommonAncestor(root.left, p, q);
		/**
		 * Go deep down right
		 */
		TreeNode right = lowestCommonAncestor(root.right, p, q);

		if (left != null && right != null) {
			return root;
		}
		return left == null ? right : left;

	}
}
