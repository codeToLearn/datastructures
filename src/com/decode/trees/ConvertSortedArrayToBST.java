package com.decode.trees;

/*
 * 
Given an array where elements are sorted in ascending order, convert it to a height balanced BST.

For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.

Example:

Given the sorted array: [-10,-3,0,5,9],

One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:

      0
     / \
   -3   9
   /   /
 -10  5
 */
public class ConvertSortedArrayToBST {
	public static void main(String[] args) {
		new ConvertSortedArrayToBST().sortedArrayToBST(new int[] { 1,2,3,4,5,6,7 });
	}

	public TreeNode sortedArrayToBST(int[] nums) {
		TreeNode root = null;
		if (nums == null || nums.length == 0) {
			return null;
		}

		if (nums.length == 1) {
			root = new TreeNode(nums[0]);
			return root;
		}
		root = binarySearchTree(nums, 0, nums.length - 1);

		TreeTraversal traversal = new TreeTraversal();
		traversal.preOrderRecursive(root);
		return root;
	}

	private TreeNode binarySearchTree(int[] nums, int lower, int upper) {
		TreeNode root = null;
		if (lower <= upper) {
			int mid = lower + (upper - lower) / 2;
			// root
			root = new TreeNode(nums[mid]);
			root.left = binarySearchTree(nums, lower, mid - 1);
			root.right = binarySearchTree(nums, mid + 1, upper);
			return root;
			// left
			// right
		}
		return null;
	}

}
