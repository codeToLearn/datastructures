package com.decode.trees;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class GrandParentEven {
	public static void main(String[] args) {

	}

	public int sumEvenGrandparent(TreeNode root) {
		
		int sum = 0;
		HashMap<TreeNode, TreeNode> parentNode = new HashMap<>();
		/*
		 * 
		 */
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.offer(root);
		int size = 0;
		TreeNode treeNode;
		while (!queue.isEmpty()) {
			size = queue.size();
			for (int i = 0; i < size; i++) {
				treeNode = queue.poll();

				if (treeNode.left != null) {
					queue.offer(treeNode.left);
					parentNode.put(treeNode.left, treeNode);
					if (parentNode.containsKey(treeNode)) {
						if (parentNode.get(treeNode).val % 2 == 0) {
							sum += treeNode.left.val;
						}
					}
				}
				if (treeNode.right != null) {
					queue.offer(treeNode.right);
					parentNode.put(treeNode.right, treeNode);
					if (parentNode.containsKey(treeNode)) {
						if (parentNode.get(treeNode).val % 2 == 0) {
							sum += treeNode.right.val;
						}
					}
				}
			}
		}
		return sum;
	}
}
