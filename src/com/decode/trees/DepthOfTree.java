package com.decode.trees;

public class DepthOfTree {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(20);
		root.left = new TreeNode(10);
		root.right = new TreeNode(30);
		root.left.left = new TreeNode(5);
		root.left.left.right = new TreeNode(7);
		root.left.right = new TreeNode(15);
		root.right.left = new TreeNode(25);
		root.right.right = new TreeNode(35);
		root.left.right.left = new TreeNode(13);
		root.left.right.right = new TreeNode(18);
		System.out.println(depthOfTree(root));
	}

	private static int depthOfTree(TreeNode root) {
		if (root == null) {
			return 0;
		}
		int left = depthOfTree(root.left);
		int right = depthOfTree(root.right);
		return Math.max(left, right) + 1;
	}
}
