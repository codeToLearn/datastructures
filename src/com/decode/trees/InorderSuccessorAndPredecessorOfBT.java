package com.decode.trees;

public class InorderSuccessorAndPredecessorOfBT {

	static TreeNode predecessor = null;
	static TreeNode successor = null;

	public static void main(String[] args) {
		TreeNode root = new TreeNode(20);
		root.left = new TreeNode(10);
		root.right = new TreeNode(30);
		root.left.left = new TreeNode(5);
		root.left.left.right = new TreeNode(7);
		root.left.right = new TreeNode(15);
		root.right.left = new TreeNode(25);
		root.right.right = new TreeNode(35);
		root.left.right.left = new TreeNode(13);
		root.left.right.right = new TreeNode(18);

		//
		root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);
		inorderTraversalForPredecessor(root);
		inorderTraversalForSuccessor(root);
		inorderTraversalWithSumReplace(root);

	}

	private static void inorderTraversalWithSumReplace(TreeNode root) {
		if (root == null) {
			return;
		}
		inorderTraversalWithSumReplace(root.left);
		System.out.println("Sum Of ::" + root.val + "==" + ((root.predecessor == null ? 0 : root.predecessor.val)
				+ (root.successor == null ? 0 : root.successor.val)));
		inorderTraversalWithSumReplace(root.right);

	}

	public static void inorderTraversalForPredecessor(TreeNode root) {

		if (root == null) {
			return;
		}
		inorderTraversalForPredecessor(root.left);
		root.predecessor = predecessor;
		System.out.println("Pre for ::" + root.val + "->" + (root.predecessor == null ? "0" : root.predecessor.val));
		predecessor = root;
		inorderTraversalForPredecessor(root.right);
	}

	public static void inorderTraversalForSuccessor(TreeNode root) {

		if (root == null) {
			return;
		}
		inorderTraversalForSuccessor(root.right);
		root.successor = successor;
		System.out.println("Suc for ::" + root.val + "->" + (root.successor == null ? "0" : root.successor.val));
		successor = root;
		inorderTraversalForSuccessor(root.left);
	}
}


