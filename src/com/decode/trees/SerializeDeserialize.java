package com.decode.trees;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 
 * @author Sachin Jindal Serialization is the process of converting a data
 *         structure or object into a sequence of bits so that it can be stored
 *         in a file or memory buffer, or transmitted across a network
 *         connection link to be reconstructed later in the same or another
 *         computer environment.
 * 
 *         Design an algorithm to serialize and deserialize a binary tree. There
 *         is no restriction on how your serialization/deserialization algorithm
 *         should work. You just need to ensure that a binary tree can be
 *         serialized to a string and this string can be deserialized to the
 *         original tree structure.
 * 
 *         Example:
 * 
 *         You may serialize the following tree:
 * 
 *         <pre>
 *   1
  / \
 2   3
    / \
   4   5
 *         </pre>
 * 
 *         as "[1,2,3,null,null,4,5]"
 */
public class SerializeDeserialize {

	public static void main(String[] args) {
		SerializeDeserialize serializeDeserialize = new SerializeDeserialize();
		String seString = serializeDeserialize.serialize(TreeNode.stringToTreeNode("[1,2,3,null,null,4,5]"));
		System.out.println(seString);

		System.out.println(serializeDeserialize.deserialize(seString));
	}

	// Encodes a tree to a single string.
	/**
	 * 
	 * @param root
	 * @return We can process preorder fashion traversal
	 */
	public String serialize(TreeNode root) {
		if (root == null) {
			return "X,";
		}
		String left = serialize(root.left);
		String right = serialize(root.right);
		return root.val + "," + left + right;// preorder
		// [1,2,X,X,3,4,X,X,5,X,X]
	}

	// Decodes your encoded data to tree.
	public TreeNode deserialize(String data) {
		LinkedList<String> strings = new LinkedList<>();
		strings.addAll(Arrays.asList(data.split(",")));
		return deserialiseHelper(strings);

	}

	// deserialize in form of preorder
	private TreeNode deserialiseHelper(LinkedList<String> list) {
		if (list.isEmpty()) {
			return null;
		}
		String val = list.poll();
		if (val.equals("X")) {
			return null;
		}
		TreeNode newNode = new TreeNode(Integer.parseInt(val));
		newNode.left = deserialiseHelper(list);
		newNode.right = deserialiseHelper(list);
		return newNode;
	}
}
