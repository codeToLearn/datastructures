package com.decode.trees;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 
 * @author Sachin Jindal
 *
 *         Given a binary tree
 * 
 *         struct Node { int val; Node *left; Node *right; Node *next; }
 *         Populate each next pointer to point to its next right node. If there
 *         is no next right node, the next pointer should be set to NULL.
 * 
 *         Initially, all next pointers are set to NULL.
 * 
 * 
 * 
 *         Follow up:
 * 
 *         You may only use constant extra space. Recursive approach is fine,
 *         you may assume implicit stack space does not count as extra space for
 *         this problem.
 * 
 * 
 *         Example 1:
 * 
 * 
 * 
 *         Input: root = [1,2,3,4,5,null,7] Output: [1,#,2,3,#,4,5,7,#]
 *         Explanation: Given the above binary tree (Figure A), your function
 *         should populate each next pointer to point to its next right node,
 *         just like in Figure B. The serialized output is in level order as
 *         connected by the next pointers, with '#' signifying the end of each
 *         level.
 * 
 * 
 *         Constraints:
 * 
 *         The number of nodes in the given tree is less than 6000. -100 <=
 *         node.val <= 100
 */
public class ConnectRightPointers {

	public static void main(String[] args) {
		Node node = new Node(2);
		node.right = new Node(3);
		node.right.right = new Node(4);
		node.right.right.right = new Node(5);
		node.right.right.right.right = new Node(6);
		ConnectRightPointers connectRightPointers = new ConnectRightPointers();
		connectRightPointers.connectWithConstantSpace(node);

	}

	/**
	 * 
	 * @param root
	 *            The idea is simple maintain queue and perform level order
	 *            traversal
	 * @return
	 */
	public Node connect(Node root) {
		Queue<Node> queue = new LinkedList<Node>();
		int size = 0;
		Node preNode = null;
		Node currNode = null;
		if (root != null) {
			queue.offer(root);
		}

		while (!queue.isEmpty()) {
			size = queue.size();
			preNode = null;

			for (int i = 0; i < size; i++) {
				currNode = queue.poll();
				if (preNode != null) {
					preNode.next = currNode;
				}

				preNode = currNode;
				if (currNode.left != null) {
					queue.offer(currNode.left);
				}
				if (currNode.right != null) {
					queue.offer(currNode.right);
				}
			}

		}
		return root;

	}

	public Node connectWithConstantSpace(Node root) {
		Node head = root; // lower level next
		Node pre = null;
		Node next = null; // upper level next
		while (head != null) {

			if (head.left != null) {
				if (next == null) {
					next = head.left;
				}
				if (pre == null) {
					pre = head.left;
				} else {
					pre.next = head.left;
					pre = pre.next;
				}
			}
			if (head.right != null) {

				if (next == null) {
					next = head.right;
				}
				if (pre == null) {
					pre = head.right;
				} else {
					pre.next = head.right;
					pre = pre.next;
				}
			}
			if (head.next == null) {
				head = next; // move to upper level
				pre = null;
				next = null;
			} else {
				head = head.next;
			}
		}
		return root;
	}
}

// Definition for a Node.
class Node {
	public int val;
	public Node left;
	public Node right;
	public Node next;

	public Node() {
	}

	public Node(int _val) {
		val = _val;
	}

	public Node(int _val, Node _left, Node _right, Node _next) {
		val = _val;
		left = _left;
		right = _right;
		next = _next;
	}
};
