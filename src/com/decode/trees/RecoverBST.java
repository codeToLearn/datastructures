package com.decode.trees;

public class RecoverBST {

	TreeNode first;
	TreeNode second;
	TreeNode previous;

	public static void main(String[] args) {

		String string = "[5,3,9,-2147483648,2]";
		TreeNode root = TreeNode.stringToTreeNode(string);
		RecoverBST recoverBST = new RecoverBST();
		recoverBST.recoverTree(root);
	}

	/**
	 * 
	 * @param root
	 *            The idea is simple we will perform inorder traversal and check
	 *            array is sorted or not
	 * 
	 */
	public void recoverTree(TreeNode root) {
	

		corrrectBST(root);

		int temp = second.val;
		second.val = first.val;
		first.val = temp;

	}

	private void corrrectBST(TreeNode root) {
		if (root == null) {
			return;
		}

		corrrectBST(root.left);
		// perform logic

		if (first == null && (previous !=null && previous.val >= root.val)) {
			first = previous;
		}
		if (first != null && previous.val >= root.val) {
			second = root;
		}
		previous = root;
		corrrectBST(root.right);
	}

}
