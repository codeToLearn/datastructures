package com.decode.trees;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class TreeTraversal {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(20);
		root.left = new TreeNode(10);
		root.right = new TreeNode(30);
		root.left.left = new TreeNode(5);
		root.left.left.right = new TreeNode(7);
		root.left.right = new TreeNode(15);
		root.right.left = new TreeNode(25);
		root.right.right = new TreeNode(35);
		root.left.right.left = new TreeNode(13);
		root.left.right.right = new TreeNode(18);
		System.out.println();
		System.out.println("INORDER RECURSIVE ::");
		TreeTraversal traversal = new TreeTraversal();
		traversal.inorderRecursive(root);

		System.out.println();
		System.out.println("INORDER Iterartive ::");
		traversal.inorderIterative(root);

		System.out.println();
		System.out.println("PREORDER RECURSIVE ::");
		traversal.preOrderRecursive(root);

		System.out.println();
		System.out.println("PREORDER ITERATIVE ::");
		traversal.preOrderIterative(root);

		System.out.println();
		System.out.println("POSTORDER RECURSIVE ::");
		traversal.postOrderRecursive(root);
		System.out.println();
		System.out.println("POSTORDER ITERATIVE ::");
		traversal.postOrderIterative(root);
	}

	public void preOrderRecursive(TreeNode root) {
		if (root == null) {
			return;
		}
		System.out.print(root.val + ",");
		preOrderRecursive(root.left);
		preOrderRecursive(root.right);

	}

	public void preOrderIterative(TreeNode root) {
		if (root == null) {
			return;
		}
		Stack<TreeNode> stack = new Stack<>();
		stack.add(root);
		while (!stack.isEmpty()) {
			root = stack.pop();
			System.out.print(root.val + ",");
			if (root.right != null) {
				stack.push(root.right);
			}
			if (root.left != null) {
				stack.push(root.left);
			}
		}

	}

	public void inorderRecursive(TreeNode root) {

		if (root == null) {
			return;
		}
		inorderRecursive(root.left);
		System.out.print(root.val + ",");
		inorderRecursive(root.right);
	}

	public void inorderIterative(TreeNode root) {
		if (root == null) {
			return;
		}
		Stack<TreeNode> stack = new Stack<>();
		while (!stack.isEmpty() || root != null) {

			if (root != null) {
				stack.push(root);
				root = root.left;
			} else {
				root = stack.pop();
				System.out.print(root.val + ",");
				root = root.right;

			}
		}
	}

	public void postOrderRecursive(TreeNode root) {
		if (root == null) {
			return;
		}
		postOrderRecursive(root.left);
		postOrderRecursive(root.right);
		System.out.print(root.val + ",");

	}

	public void postOrderIterative(TreeNode root) {
		if (root == null) {
			return;
		}
		Stack<TreeNode> stack = new Stack<>();
		Set<TreeNode> visited = new HashSet<>();

		while (!stack.isEmpty() || root != null) {

			if (root == null) {
				root = stack.pop();
				if (root.right != null && !visited.contains(root)) {
					stack.push(root);
					visited.add(root);
					root = root.right;
				} else {
					System.out.print(root.val + ",");
					root = null;
				}

			} else {
				stack.push(root);
				root = root.left;
			}
		}
	}

	public List<List<Integer>> levelOrderTraversal(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) {
			return result;
		}
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		List<Integer> level;
		int count = 0;
		while (!queue.isEmpty()) {
			level = new ArrayList<>();
			count = queue.size();
			result.add(level);
			for (int i = 0; i < count; i++) {
				root = queue.poll();
				if (root.left != null) {
					queue.add(root.left);
				}
				if (root.right != null) {
					queue.add(root.right);
				}
				level.add(root.val);
			}
		}
		return result;
	}
}
