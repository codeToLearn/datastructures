package com.decode.trees;

import java.util.LinkedList;
import java.util.Queue;

public class SymmtericTree {

	public static void main(String[] args) {

	}

	public boolean isSymmetric(TreeNode root) {

		if (root == null) {
			return true;
		}
		if (root.left == null && root.right == null) {
			return true;
		}
		if (root.left == null || root.right == null) {
			return false;
		}
		return isSymmetric(root.left, root.right);
	}

	private boolean isSymmetric(TreeNode node1, TreeNode node2) {
		if (node1 == null && node2 == null) {
			return true;
		}
		// Check both are not null
		if (node1 == null || node2 == null) {
			return false;
		}
		return (node1.val == node2.val) && isSymmetric(node1.left, node2.right) && isSymmetric(node1.right, node2.left);
	}

	private boolean iterativeSymmetric(TreeNode root) {
		if (root == null) {
			return true;
		}
		if (root.left == null && root.right == null) {
			return true;
		}
		if (root.left == null || root.right == null) {
			return false;
		}
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root.left);
		queue.add(root.right);
		TreeNode node1, node2;
		while (!queue.isEmpty()) {
			node1 = queue.poll();
			node2 = queue.poll();
			if (node1 == null && node2 == null) {
				continue;
			}
			// Check both are not null
			if (node1 == null || node2 == null) {
				return false;
			}
			if (node1.val != node2.val) {
				return false;
			}
			queue.add(node1.left);
			queue.add(node2.right);
			queue.add(node1.right);
			queue.add(node2.left);
		}
		return true;
	}
}
