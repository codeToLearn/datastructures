package com.decode.trees;

import java.util.ArrayList;
import java.util.List;

public class UniqueBST {

	public static void main(String[] args) {

		List<TreeNode> result = uniqueBST(1, 2);
		// System.out.println(result.size());
		// System.out.println(countUniqueBst(1, 3));
		System.out.println(countUniqueBSTDP(20));

	}
	/*
	 * Concept of upper and lower like we have used in valid bst we have to choose
	 * root for example we have number =3 it means lower is [1,3] - if we choose 1
	 * at root it means left [1,0] because lower is 1 and value will be 0 and right
	 * is [2,3] - similar concept follow. For every left right will be counted as
	 * well.
	 */

	private static List<TreeNode> uniqueBST(int lower, int upper) {
		// System.out.println(lower + "," + upper);
		if (lower > upper) {
			List<TreeNode> list = new ArrayList<>();
			// System.out.println("null");
			list.add(null);
			return list;
		}

		if (lower == upper) {
			List<TreeNode> list = new ArrayList<>();
			TreeNode root = new TreeNode(lower);
			list.add(root);
			return list;
		}

		List<TreeNode> list = new ArrayList<>();

		for (int index = lower; index <= upper; index++) {
			List<TreeNode> left = uniqueBST(lower, index - 1);
			List<TreeNode> right = uniqueBST(index + 1, upper);
			for (int i = 0; i < left.size(); i++) {
				for (int j = 0; j < right.size(); j++) {
					TreeNode root = new TreeNode(index);
					root.left = left.get(i);
					root.right = right.get(j);
					list.add(root);
				}

			}

		}
		return list;
	}

	private static int countUniqueBst(int lower, int upper) {

		if (lower >= upper) {
			return 1;
		}
		int sum = 0;
		for (int index = lower; index <= upper; index++) {

			int left = countUniqueBst(lower, index - 1);
			int right = countUniqueBst(index + 1, upper);
			sum += left * right;
			System.out.println("left::" + "lower ::" + lower + ",upper ::" + (index - 1) + ",sum:: " + left);
			System.out.println("left::" + "lower ::" + (index + 1) + ",upper ::" + (upper) + ",sum:: " + right);
			System.out.println("root::" + index + ",sum:" + sum);
		}
		return sum;
	}

	// This solution is not mine i have checked from back to back swe
	private static int countUniqueBSTDP(int n) {
		/*
		 * We will answer every subproblem from 0 to n hence the n + 1 to accomodate the
		 * 0 subproblem
		 */
		int[] G = new int[n + 1];

		/*
		 * The answer to the subproblem when n = 0 is 0.
		 */
		G[0] = 1;

		/*
		 * The answer to the subproblem when n = 1 (we can only place a 1 as a value and
		 * have a single node) is 1. A single node can only make 1 unique tree.
		 */
		G[1] = 1;

		/*
		 * We will solve every subproblem from 2 to our target subproblem n
		 */
		for (int i = 2; i <= n; ++i) {
			/*
			 * The answer to the ith subproblem will be the summation of F(i, n) for i = 0
			 * to i = n (we plant every number we have available at the root)
			 * 
			 * Remember that we expressed: F(i, n) = G(i - 1) * G(n - i)
			 * 
			 * The answer to the total unique BST's we can construct with values from 1...n
			 * with i representing what is rooted at the root of the tree is F(i, n).
			 * 
			 * We attain this value by taking the Cartesian Product (fancy word meaning all
			 * possible cross products) between all possible unique left BST's and unique
			 * right BST's.
			 * 
			 * All possible unique left BST's count is G[j - 1] if we plant at i.
			 * 
			 * All possible unique right BST's count is G[i - j] if we plant at i.
			 * 
			 * Taking a product is the same as taking all pairing between the two sets of
			 * possibilites.
			 * 
			 * If still confused, watch the video above, take your time to let it sink in.
			 */
			// here similar concept of lower and upper
			// G[0] - no nodes
			// G[1] - 1 node
			// G[2]
			// G[3] - take one as root,take two as root ,take three as root you can take 2d
			// array also but as we see in our recursive one varaible was fixed
			for (int j = 1; j <= i; ++j) {
				G[i] += G[j - 1] * G[i - j];
			}
			// G[3] = G[3] + G[0]*G[2] -- two nodes at right and zero at left with three as
			// root -- 3
			// G[3] = G[3] + G[1]*G[1] --- two as root
			// G[3] = G[3] + G[2]*G[0] -- one as root
		}
		return G[n];
	}
}
