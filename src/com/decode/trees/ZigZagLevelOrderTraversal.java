package com.decode.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * 
 * @author Sachin Jindal
 *
 *         Given a binary tree, return the zigzag level order traversal of its
 *         nodes' values. (ie, from left to right, then right to left for the
 *         next level and alternate between).
 * 
 *         For example: Given binary tree [3,9,20,null,null,15,7],
 * 
 *         [ [3], [20,9], [15,7] ]
 * 
 */
public class ZigZagLevelOrderTraversal {

	public static void main(String[] args) {
		TreeNode node = new TreeNode(3);
		node.left = new TreeNode(9);
		node.right = new TreeNode(20);
		node.right.left = new TreeNode(15);
		node.right.right = new TreeNode(7);
		ZigZagLevelOrderTraversal levelOrderTraversal = new ZigZagLevelOrderTraversal();
		List<List<Integer>> result = levelOrderTraversal.zigzagLevelOrder(node);
		int[][] array = new int[result.size()][];
		for (int i = 0; i < array.length; i++) {
			array[i] = new int[result.get(i).size()];
		}
		for (int i = 0; i < result.size(); i++) {
			for (int j = 0; j < result.get(i).size(); j++) {
				array[i][j] = result.get(i).get(j);
			}
		}
		System.out.println(array);

	}

	public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		List<Integer> levelResult;
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.offer(root);
		int level = 0;
		Stack<Integer> reverse = new Stack<>();
		int size = 0;
		TreeNode node;
		while (!queue.isEmpty()) {
			size = queue.size();
			levelResult = new ArrayList<>();
			for (int i = 0; i < size; i++) {
				node = queue.poll();
				if (node.left != null) {
					queue.offer(node.left);
				}
				if (node.right != null) {
					queue.offer(node.right);
				}

				if (level % 2 == 0) {
					levelResult.add(node.val);
				} else {
					reverse.push(node.val);
				}

			}
			while (!reverse.isEmpty()) {
				levelResult.add(reverse.pop());
			}
			result.add(levelResult);
			level++;

		}

		return result;
	}
}
