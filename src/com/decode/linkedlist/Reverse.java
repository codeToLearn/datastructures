package com.decode.linkedlist;

public class Reverse {
	public static void main(String[] args) {
		Node head = new Node(1);
		head.next = new Node(2);
		head.next.next = new Node(3);
		head.next.next.next = new Node(4);
		Reverse reverse = new Reverse();
		
		System.out.println(Node.listNodeToString(reverse.reverse(head)));
	}

	public Node reverse(Node head) {

		if (head == null || head.next == null) {
			return head;
		}
		Node rest = reverse(head.next);
		head.next.next = head;
		head.next = null;
		return rest;
	
       
	
       
	}
}
