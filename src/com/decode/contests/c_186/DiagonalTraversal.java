package com.decode.contests.c_186;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

/**
 * 
 * @author Sachin Jindal
 *
 *         5394. Diagonal Traverse II User Accepted:0 User Tried:0 Total
 *         Accepted:0 Total Submissions:0 Difficulty:Medium Given a list of
 *         lists of integers, nums, return all elements of nums in diagonal
 *         order as shown in the below images.
 * 
 * 
 *         Example 1:
 * 
 * 
 * 
 *         Input: nums = [[1,2,3],[4,5,6],[7,8,9]] Output: [1,4,2,7,5,3,8,6,9]
 *         Example 2:
 * 
 * 
 * 
 *         Input: nums = [[1,2,3,4,5],[6,7],[8],[9,10,11],[12,13,14,15,16]]
 *         Output: [1,6,2,8,7,3,9,4,12,10,5,13,11,14,15,16] Example 3:
 * 
 *         Input: nums = [[1,2,3],[4],[5,6,7],[8],[9,10,11]] Output:
 *         [1,4,2,5,3,8,6,9,7,10,11] Example 4:
 * 
 *         Input: nums = [[1,2,3,4,5,6]] Output: [1,2,3,4,5,6]
 * 
 * 
 *         Constraints:
 * 
 *         1 <= nums.length <= 10^5 1 <= nums[i].length <= 10^5 1 <= nums[i][j]
 *         <= 10^9 There at most 10^5 elements in nums.
 * 
 */
public class DiagonalTraversal {
	public static void main(String[] args) {
		DiagonalTraversal diagonalTraversal = new DiagonalTraversal();
		List<List<Integer>> nums = new ArrayList<>();
		// [[20,17,13,14],[12,6],[3,4]]
		List<Integer> row = new ArrayList();
		row.add(20);
		row.add(17);
		row.add(13);
		row.add(14);
		nums.add(row);
		row = new ArrayList<>();
		row.add(12);
		row.add(6);
		// row.add(6);
		nums.add(row);
		row = new ArrayList<>();
		row.add(3);
		row.add(4);
		// row.add(9);
		nums.add(row);
		System.out.println(diagonalTraversal.findDiagonalOrder(nums));
	}

	/**
	 * 
	 * @param nums
	 * @return TIME LIMIT EXCEEDS REASON MAX(ROW)*MAX(COL) NEED TO OPTIMISE SO THAT
	 *         TRAVERSAL WILL BE EQUIVALENT TO NUMBER OF ELEMENTS PRESENT in a 2d
	 *         list.
	 */
	public int[] findDiagonalOrder(List<List<Integer>> nums) {
		List<Integer> result = new ArrayList<Integer>();
		int rowIndex = 0;
		int colIndex = 0;
		int value = 0;
		/**
		 * This will traverse array wise
		 */
		int maxColSize = 0;
		for (int row = 0; row < nums.size(); row++) {
			rowIndex = row;// decrement this
			colIndex = 0; // increment this
			for (int i = 0; i <= row; i++) {
				// check here index exists or not
				if (check(rowIndex, colIndex, nums)) {

					value = nums.get(rowIndex).get(colIndex);
					result.add(value);
				}
				rowIndex--;
				colIndex++;
			}
			maxColSize = Math.max(maxColSize, nums.get(row).size());
		}
		/**
		 * This will traverse last row col wise
		 */

		// max colsize

		int colWiseRow = 0;
		for (int col = 1; col < maxColSize; col++) {
			colWiseRow = nums.size() - 1;
			colIndex = col;
			for (int i = 0; i <= nums.size() - 1; i++) {
				if (check(colWiseRow, colIndex, nums)) {

					value = nums.get(colWiseRow).get(colIndex);
					result.add(value);
				}
				colWiseRow--;
				colIndex++;
				if (colIndex == maxColSize) {
					break;
				}
			}
		}
		int[] array = new int[result.size()];
		for (int i = 0; i < result.size(); i++) {
			array[i] = result.get(i);
		}
		return array;
	}

	private boolean check(int row, int col, List<List<Integer>> nums) {
		return row >= 0 && row < nums.size() && col >= 0 && col < nums.get(row).size();
	}

	public int[] findDiagonalOrderOptimize(List<List<Integer>> nums) {
		Map<Integer, List<Integer>> traversalMap = new LinkedHashMap<>();
		/*
		 * Solution deduce by this logic diagonal sum of index is same i.e. 01 -10 are
		 * diagonal indexes Simple maintain an linkedHashmap store result of diagonally.
		 */
		int row = 0;
		int NO_OF_ELEMENTS = 0;
		for (List<Integer> list : nums) {

			for (int col = 0; col < list.size(); col++) {

				if (!traversalMap.containsKey(row + col)) {
					traversalMap.put(row + col, new ArrayList<>());
				}
				traversalMap.get(row + col).add(0,list.get(col));
				NO_OF_ELEMENTS++;
			}
			row++;
		}
		int[] result = new int[NO_OF_ELEMENTS];
		int i = 0;
		for (Entry<Integer, List<Integer>> entry : traversalMap.entrySet()) {
			for (Integer value : entry.getValue()) {
				result[i++] = value;
			}
		}
		return result;
	}
}
