package com.decode.contests.c_186;

/**
 * 
 * @author Sachin Jindal 5393. Maximum Points You Can Obtain from Cards User
 *         Accepted:0 User Tried:0 Total Accepted:0 Total Submissions:0
 *         Difficulty:Medium There are several cards arranged in a row, and each
 *         card has an associated number of points The points are given in the
 *         integer array cardPoints.
 * 
 *         In one step, you can take one card from the beginning or from the end
 *         of the row. You have to take exactly k cards.
 * 
 *         Your score is the sum of the points of the cards you have taken.
 * 
 *         Given the integer array cardPoints and the integer k, return the
 *         maximum score you can obtain.
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input: cardPoints = [1,2,3,4,5,6,1], k = 3 Output: 12 Explanation:
 *         After the first step, your score will always be 1. However, choosing
 *         the rightmost card first will maximize your total score. The optimal
 *         strategy is to take the three cards on the right, giving a final
 *         score of 1 + 6 + 5 = 12. Example 2:
 * 
 *         Input: cardPoints = [2,2,2], k = 2 Output: 4 Explanation: Regardless
 *         of which two cards you take, your score will always be 4. Example 3:
 * 
 *         Input: cardPoints = [9,7,7,9,7,7,9], k = 7 Output: 55 Explanation:
 *         You have to take all the cards. Your score is the sum of points of
 *         all cards. Example 4:
 * 
 *         Input: cardPoints = [1,1000,1], k = 1 Output: 1 Explanation: You
 *         cannot take the card in the middle. Your best score is 1. Example 5:
 * 
 *         Input: cardPoints = [1,79,80,1,1,1,200,1], k = 3 Output: 202
 * 
 * 
 *         Constraints:
 * 
 *         1 <= cardPoints.length <= 10^5 1 <= cardPoints[i] <= 10^4 1 <= k <=
 *         cardPoints.length
 * 
 */
public class MaxPoints {
	public static void main(String[] args) {
		MaxPoints maxPoints = new MaxPoints();
		System.out.println(maxPoints.maxScore(new int[] {5,2,3,4,2,6,1 }, 3));
	}

	public int maxScore(int[] cardPoints, int k) {
		System.out.println(maxScore(cardPoints, k, 0, cardPoints.length - 1));
		return maxScoreDP(cardPoints, k);
		// return
	}

	private int maxScore(int[] cardPoints, int k, int leftIndex, int rightIndex) {
		// when k ==0 only return the result
		// System.out.println(leftIndex + "," + rightIndex + "::" + k);
		if (k == 0 || leftIndex > rightIndex) {
			return 0;
		}
		int left = 0;
		int right = 0;
		// System.out.println(leftIndex + "," + rightIndex + "::" + k);
		// pick left
		left = cardPoints[leftIndex] + maxScore(cardPoints, k - 1, leftIndex + 1, rightIndex);
		System.out.println("LEFT::" + (leftIndex + 1) + "," + rightIndex + "," + (k - 1) + "::" + left);
		right = cardPoints[rightIndex] + maxScore(cardPoints, k - 1, leftIndex, rightIndex - 1);
		System.out.println("RIGHT::" + (leftIndex) + "," + (rightIndex - 1) + "," + (k - 1) + "::" + right);
		System.out.println("---------------------------");
		return Math.max(left, right);
	}

	private int maxScoreDP(int[] cardPoints, int k) {
		// maintain right prefix array
		int right[] = new int[cardPoints.length];
		right[cardPoints.length - 1] = cardPoints[cardPoints.length - 1];

		for (int j = cardPoints.length - 2; j >= 0; j--) {
			right[j] = right[j + 1] + cardPoints[j];
		}
		/**
		 * The idea is simple maintain right prefix array.
		 * 
		 * We can understand this we the help of an example
		 * 
		 * [11,49,100,20,86,29,72] from recursive equation i have realized every time we
		 * need to get left and right max. for k=4 i introspect if we are at index 3 it
		 * means we have at final choice means k only left 1 it means we are on subarray
		 * 3,6 and k=1 this we give us 72. Now move to index 2 it means we selected 0,1
		 * index now we need to check a[2]+d[3] or a[6]+a[5] max
		 * This will give me an idea i just follow this and get the solution
		 */
		int[] dp = new int[k];
		dp[k - 1] = Math.max(cardPoints[k - 1], cardPoints[cardPoints.length - 1]);

		for (int i = 2; i <= k; i++) {
			// caluculate sum of right

			dp[k - i] = Math.max(cardPoints[k - i] + dp[k - i + 1], right[cardPoints.length - i]);
		}
		return dp[0];
	}

}
