package com.decode.contests.c_191;

public class MaxProductTwoElements {

	public static void main(String[] args) {
		MaxProductTwoElements elements = new MaxProductTwoElements();
		int arr[] = new int[] { 3, 4, 5, 2 };
		elements.maxProduct(arr);
	}

	public int maxProduct(int[] nums) {

		int first = Integer.MIN_VALUE, second = first;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] > first) {

				second = first;
				first = nums[i];

			} else {
				if (nums[i] > second) {
					second = nums[i];
				}
			}
		}
		return (first - 1) * (second - 1);
	}
}
