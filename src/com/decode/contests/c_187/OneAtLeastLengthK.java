package com.decode.contests.c_187;

/**
 * 
 * @author Sachin Jindal
 *
 * 
 *         Given an array nums of 0s and 1s and an integer k, return True if all
 *         1's are at least k places away from each other, otherwise return
 *         False.
 * 
 * 
 * 
 *         Example 1:
 * 
 * 
 * 
 *         Input: nums = [1,0,0,0,1,0,0,1], k = 2 Output: true Explanation: Each
 *         of the 1s are at least 2 places away from each other. Example 2:
 * 
 * 
 * 
 *         Input: nums = [1,0,0,1,0,1], k = 2 Output: false Explanation: The
 *         second 1 and third 1 are only one apart from each other. Example 3:
 * 
 *         Input: nums = [1,1,1,1,1], k = 0 Output: true Example 4:
 * 
 *         Input: nums = [0,1,0,1], k = 1 Output: true
 */
public class OneAtLeastLengthK {
	public boolean kLengthApart(int[] nums, int k) {
		int start = -1;
		int end = -1;

		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 1) {
				if (start == -1) {
					start++;
				} else {
					end = i;
					if (end - start - 1 >= k) {
						start = i;
						end = -1;

						continue;
					} else {
						return false;
					}

				}
			}
		}

		return true;
	}

}
