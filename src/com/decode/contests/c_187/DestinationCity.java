package com.decode.contests.c_187;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *
 *         You are given the array paths, where paths[i] = [cityAi, cityBi]
 *         means there exists a direct path going from cityAi to cityBi. Return
 *         the destination city, that is, the city without any path outgoing to
 *         another city.
 * 
 *         It is guaranteed that the graph of paths forms a line without any
 *         loop, therefore, there will be exactly one destination city.
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input: paths = [["London","New York"],["New
 *         York","Lima"],["Lima","Sao Paulo"]] Output: "Sao Paulo" Explanation:
 *         Starting at "London" city you will reach "Sao Paulo" city which is
 *         the destination city. Your trip consist of: "London" -> "New York" ->
 *         "Lima" -> "Sao Paulo". Example 2:
 * 
 *         Input: paths = [["B","C"],["D","B"],["C","A"]] Output: "A"
 *         Explanation: All possible trips are: "D" -> "B" -> "C" -> "A". "B" ->
 *         "C" -> "A". "C" -> "A". "A". Clearly the destination city is "A".
 *         Example 3:
 * 
 *         Input: paths = [["A","Z"]] Output: "Z"
 * 
 */
public class DestinationCity {
	public static void main(String[] args) {
		DestinationCity city = new DestinationCity();
		List<List<String>> list = new ArrayList<List<String>>();
		List<String> list1 = new ArrayList<>();
		list1.add("B");
		list1.add("C");
		list.add(list1);
		list1 = new ArrayList<>();
		list1.add("D");
		list1.add("B");
		list.add(list1);
		list1 = new ArrayList<>();
		list1.add("C");
		list1.add("A");
		list.add(list1);
		System.out.println(city.destCity(list));
	}

	public String destCity(List<List<String>> paths) {
		if (paths.isEmpty()) {
			return "";
		}
		HashMap<String, String> map = new HashMap();
		for (List<String> list : paths) {
			map.put(list.get(0), list.get(1));
		}
		String des = "";

		String start = paths.get(0).get(0);
		while (map.get(start) != null) {
			start = map.get(start);
		}
		des = start;

		return des;
	}
}
