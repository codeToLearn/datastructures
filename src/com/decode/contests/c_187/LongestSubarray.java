package com.decode.contests.c_187;

import java.util.TreeMap;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *         Longest Continuous Subarray With Absolute Diff Less Than or Equal to
 *         Limit
 * 
 *         Given an array of integers nums and an integer limit, return the size
 *         of the longest continuous subarray such that the absolute difference
 *         between any two elements is less than or equal to limit.
 * 
 *         In case there is no subarray satisfying the given condition return 0.
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input: nums = [8,2,4,7], limit = 4 Output: 2 Explanation: All
 *         subarrays are: [8] with maximum absolute diff |8-8| = 0 <= 4. [8,2]
 *         with maximum absolute diff |8-2| = 6 > 4. [8,2,4] with maximum
 *         absolute diff |8-2| = 6 > 4. [8,2,4,7] with maximum absolute diff
 *         |8-2| = 6 > 4. [2] with maximum absolute diff |2-2| = 0 <= 4. [2,4]
 *         with maximum absolute diff |2-4| = 2 <= 4. [2,4,7] with maximum
 *         absolute diff |2-7| = 5 > 4. [4] with maximum absolute diff |4-4| = 0
 *         <= 4. [4,7] with maximum absolute diff |4-7| = 3 <= 4. [7] with
 *         maximum absolute diff |7-7| = 0 <= 4. Therefore, the size of the
 *         longest subarray is 2. Example 2:
 * 
 *         Input: nums = [10,1,2,4,7,2], limit = 5 Output: 4 Explanation: The
 *         subarray [2,4,7,2] is the longest since the maximum absolute diff is
 *         |2-7| = 5 <= 5. Example 3:
 * 
 *         Input: nums = [4,2,2,2,4,4,2,2], limit = 0 Output: 3
 * 
 */
public class LongestSubarray {
	public static void main(String[] args) {
		/*
		 * [7,40,10,10,40,39,96,21,54,73,33,17,2,72,5,76,28,73,59,22,100,91,80,66,5,49,
		 * 26,45,13,27,74,87,56,76,25,64,14,86,50,38,65,64,3,42,79,52,37,3,21,26,42,73,
		 * 18,44,55,28,35,87] 63
		 */
		LongestSubarray longestSubarray = new LongestSubarray();
		longestSubarray.longestSubarray(new int[] { 7, 40, 10, 10, 40, 39, 96, 21, 54, 73, 33, 17, 2, 72, 5, 76, 28, 73,
				59, 22, 100, 91, 80, 66, 5, 49, 26, 45, 13, 27, 74, 87, 56, 76, 25, 64, 14, 86, 50, 38, 65, 64, 3, 42,
				79, 52, 37, 3, 21, 26, 42, 73, 18, 44, 55, 28, 35, 87 }, 63);

	}

	public int longestSubarray(int[] nums, int limit) {
		if (nums == null || nums.length == 0) {
			return 0;
		}
		int maxLength = 0;
		int lengthSoFar = 0;
		int maxIndex = 0;
		int minIndex = 0;
		int start = 0;
		int max = nums[start];
		int min = nums[start];
		maxLength = lengthSoFar = 1;
		start++;
		TreeMap<Integer, Integer> nextSmallAndBig = new TreeMap<>();

		/**
		 * Logic is find minIndex if max index satisfying the condition it means all
		 * elements are exists between min and max therefore we can increase length
		 * 
		 * If min - max >k it means we need to slide minindex till maxindex
		 */

		/*
		 * [7,40,10,10,40,39,96,21,54,73,33,17,2,72,5,76,28,73,59,22,100,91,80,66,5,49,
		 * 26,45,13,27,74,87,56,76,25,64,14,86,50,38,65,64,3,42,79,52,37,3,21,26,42,73,
		 * 18,44,55,28,35,87] 63
		 */
		for (; start < nums.length; start++) {

			if (max < nums[start]) {
				max = nums[start];
				maxIndex = start;
			}
			if (min > nums[start]) {
				min = nums[start];
				minIndex = start;
			}

			if (max - min <= limit) {
				lengthSoFar++;

			} else {

				lengthSoFar = 0;
				int temp = Math.min(minIndex, maxIndex);
				// sliding
				// we need to follow the same concept just slide max or min according to which
				// one is greater
				while (temp++ != start) {
					if (minIndex > maxIndex) {

						if (lengthSoFar == 0) {
							max = nums[temp];
							maxIndex = temp;
						} else {
							if (max < nums[temp]) {
								max = nums[temp];
								maxIndex = temp;
							}

						}
						if (min - max <= limit) {
							lengthSoFar++;
							maxLength = Math.max(maxLength, lengthSoFar);

						} else {
							lengthSoFar = 0;
						}

					} else {

						if (lengthSoFar == 0) {
							min = nums[temp];
							minIndex = temp;
						} else {
							if (min > nums[temp]) {
								min = nums[temp];
								minIndex = temp;
							}

						}
						if (max - min <= limit) {
							lengthSoFar++;
							maxLength = Math.max(maxLength, lengthSoFar);

						} else {
							lengthSoFar = 0;
						}
					}
				}
			}
			maxLength = Math.max(maxLength, lengthSoFar);
		}
		return maxLength;
	}
}
