package com.decode.contests.c_192;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class KStrongestElement {

	public static void main(String[] args) {
		List<Integer> arr = new ArrayList<Integer>();
		arr.add(2);
		arr.add(1);
		arr.add(5);
		Comparator<Integer> c = new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub

				return o2 - o1;

			}
		};
		Collections.sort(arr, c);
		System.out.println(arr);
	}

	public int[] getStrongest(int[] arr, int k) {

		int res[] = new int[k];
		Arrays.sort(arr);
		int med = arr[(arr.length - 1) / 2];
		PriorityQueue<Integer> queue = new PriorityQueue<>(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {

				if (Math.abs(o1 - med) == Math.abs(o2 - med)) {
					return o2 - o1;
				}

				return ((Integer) Math.abs(o2 - med)).compareTo((Integer) Math.abs(o1 - med));
			}
		});
		for (int i = 0; i < arr.length; i++) {
			queue.offer(arr[i]);
		}
		for (int i = 0; i < k; i++) {
			res[i] = queue.poll();
		}
		return res;
	}
}
