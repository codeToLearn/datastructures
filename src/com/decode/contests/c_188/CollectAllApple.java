package com.decode.contests.c_188;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.decode.trees.TreeNode;

public class CollectAllApple {
	public static void main(String[] args) {
		int n = 7;
		int[][] edges = { { 0, 1 }, { 0, 2 }, { 1, 4 }, { 1, 5 }, { 2, 3 }, { 2, 6 } };
		List<Boolean> hasApple = new ArrayList<>();
		hasApple.add(false);
		hasApple.add(false);
		hasApple.add(true);
		hasApple.add(false);
		hasApple.add(true);
		hasApple.add(true);
		hasApple.add(false);
		CollectAllApple allApple = new CollectAllApple();
		System.out.println(allApple.minTime(n, edges, hasApple));
	}

	public int minTime(int n, int[][] edges, List<Boolean> hasApple) {

		HashMap<Integer, List<Integer>> map = new HashMap<>();
		buildTree(edges, map);
		Set<Integer> visited = new HashSet<>();
		return traverese(0, map, hasApple, visited);
	}

	private int traverese(int node, Map<Integer, List<Integer>> map, List<Boolean> hasApple, Set<Integer> visited) {
		visited.add(node);
		int res = 0;
		if (map.get(node) == null) {
			// means at the end
			if (hasApple.get(node)) {
				res = 2;
			}
			return res;
		}

		for (int child : map.get(node)) {
			if (visited.contains(child)) {
				continue;
			}
			res += traverese(child, map, hasApple, visited);
		}
		if (node != 0 && (res > 0 || hasApple.get(node))) {
			res += 2;
		}
		return res;
	}

	/**
	 * This function build tree
	 * 
	 * @param edges
	 * @param map
	 */
	private void buildTree(int[][] edges, HashMap<Integer, List<Integer>> map) {
		// undirected tree means you can move to both direction
		for (int i = 0; i < edges.length; i++) {
			if (!map.containsKey(edges[i][0])) {
				map.put(edges[i][0], new ArrayList<>());
			}
			if (!map.containsKey(edges[i][1])) {
				map.put(edges[i][1], new ArrayList<>());
			}
			map.get(edges[i][0]).add(edges[i][1]);
			map.get(edges[i][1]).add(edges[i][0]);
		}
	}

}
