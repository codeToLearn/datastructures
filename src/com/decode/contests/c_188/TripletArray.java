package com.decode.contests.c_188;

/**
 * 
 * @author Sachin Jindal Triplets That Can Form Two Arrays of Equal XOR User
 *         Accepted:1430 User Tried:1749 Total Accepted:1434 Total
 *         Submissions:2025 Difficulty:Medium Given an array of integers arr.
 * 
 *         We want to select three indices i, j and k where (0 <= i < j <= k <
 *         arr.length).
 * 
 *         Let's define a and b as follows:
 * 
 *         a = arr[i] ^ arr[i + 1] ^ ... ^ arr[j - 1] b = arr[j] ^ arr[j + 1] ^
 *         ... ^ arr[k] Note that ^ denotes the bitwise-xor operation.
 * 
 *         Return the number of triplets (i, j and k) Where a == b.
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input: arr = [2,3,1,6,7] Output: 4 Explanation: The triplets are
 *         (0,1,2), (0,2,2), (2,3,4) and (2,4,4) Example 2:
 * 
 *         Input: arr = [1,1,1,1,1] Output: 10 Example 3:
 * 
 *         Input: arr = [2,3] Output: 0 Example 4:
 * 
 *         Input: arr = [1,3,5,7,9] Output: 3 Example 5:
 * 
 *         Input: arr = [7,11,12,9,5,2,7,17,22] Output: 8
 */
public class TripletArray {
	public static void main(String[] args) {
		TripletArray tripletArray = new TripletArray();
		tripletArray.countTriplets(new int[] { 1, 1, 1, 1, 1 });

	}

	// O(n3)
	public int countTriplets(int[] arr) {

		int count = 0;
		Integer pre = null;
		Integer sec = null;
		// fix first
		for (int i = 0; i < arr.length - 1; i++) {
			pre = arr[i];
			// a = arr[i]^arr[i+1]........arr[j-1]
			int index1 = i + 1;
			// find cumulative xor of first
			for (int j = i + 1; j < arr.length; j++) {
				while (index1 < j) {
					pre ^= arr[index1++];
				}
				// b =arr[j]^arr[j+1].....arr[k]
				// find cumulative xor of second
				int index2 = j;
				while (index2 < arr.length) {
					if (sec == null) {
						sec = arr[index2];
					} else {
						sec ^= arr[index2];
					}

					if (sec - pre == 0) {
						count++;
					}

					index2++;
				}

				sec = null;
			}
		}

		return count;
	}
//on2
}
