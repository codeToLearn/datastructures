package com.decode.april.week5;

import com.decode.trees.TreeNode;

/**
 * 
 * @author Sachin Jindal
 *
 *         Given a non-empty binary tree, find the maximum path sum.
 * 
 *         For this problem, a path is defined as any sequence of nodes from
 *         some starting node to any node in the tree along the parent-child
 *         connections. The path must contain at least one node and does not
 *         need to go through the root.
 * 
 *         <pre>
Example 1:

Input: [1,2,3]

       1
      / \
     2   3

Output: 6
Example 2:

Input: [-10,9,20,null,null,15,7]

   -10
   / \
  9  20
    /  \
   15   7

Output: 42
 *         </pre>
 */
public class MaxPathSum {
	int max = Integer.MIN_VALUE;

	public static void main(String[] args) {

	}

	public int maxPathSum(TreeNode root) {
		maxPathSumRec(root);
		return max;
	}

	private int maxPathSumRec(TreeNode node) {
		if (node == null) {
			return 0;
		}
		int left = maxPathSumRec(node.left);
		int right = maxPathSumRec(node.right);
		int maxSoFar = Math.max(left, right) + node.val;
		if (node.val > maxSoFar) {
			maxSoFar = node.val;
		}
		max = Math.max(max, maxSoFar);
		if (left + right + node.val > max) {
			max = left + right + node.val;
		}
		return maxSoFar;
	}
}
