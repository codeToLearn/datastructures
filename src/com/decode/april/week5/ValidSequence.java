package com.decode.april.week5;

import com.decode.trees.TreeNode;

/**
 * 
 * @author Sachin Jindal
 *
 *         Given a binary tree where each path going from the root to any leaf
 *         form a valid sequence, check if a given string is a valid sequence in
 *         such binary tree.
 * 
 *         We get the given string from the concatenation of an array of
 *         integers arr and the concatenation of all values of the nodes along a
 *         path results in a sequence in the given binary tree.
 * 
 * 
 * 
 *         Example 1:
 * 
 * 
 * 
 *         Input: root = [0,1,0,0,1,0,null,null,1,0,0], arr = [0,1,0,1] Output:
 *         true Explanation: The path 0 -> 1 -> 0 -> 1 is a valid sequence
 *         (green color in the figure). Other valid sequences are: 0 -> 1 -> 1
 *         -> 0 0 -> 0 -> 0 Example 2:
 * 
 * 
 * 
 *         Input: root = [0,1,0,0,1,0,null,null,1,0,0], arr = [0,0,1] Output:
 *         false Explanation: The path 0 -> 0 -> 1 does not exist, therefore it
 *         is not even a sequence. Example 3:
 * 
 * 
 * 
 *         Input: root = [0,1,0,0,1,0,null,null,1,0,0], arr = [0,1,1] Output:
 *         false Explanation: The path 0 -> 1 -> 1 is a sequence, but it is not
 *         a valid sequence.
 * 
 */
public class ValidSequence {

	public static void main(String[] args) {
		ValidSequence sequence = new ValidSequence();
		TreeNode root = new TreeNode(8);
		root.left = new TreeNode(3);
		root.left.left = new TreeNode(2);
		int arr[] = { 8, 3 };
		System.out.println(sequence.isValidSequence(root, arr));
	}

	public boolean isValidSequence(TreeNode root, int[] arr) {
		// [8,3,null,2,1,5,4]
		// [8]
		if (root == null && arr.length == 0) {
			return true;
		}
		if (root == null) {
			return false;
		}
		return isValidSequence(root, arr, 0);
	}

	/**
	 * 
	 * @param root
	 * @param arr
	 * @param index
	 * @return Leaf node are those node which left and right both null
	 */
	private boolean isValidSequence(TreeNode root, int[] arr, int index) {

		// traverse left
		if (index < arr.length && root.val == arr[index]) {

			if (index == arr.length - 1 && root.left == null && root.right == null) {
				return true;
			}
			boolean left = false, right = false;
			if (root.left != null) {
				left = isValidSequence(root.left, arr, index + 1);
			}
			if (root.right != null) {
				right = isValidSequence(root.right, arr, index + 1);
			}
			return left || right;
		}

		return false;
	}
}
