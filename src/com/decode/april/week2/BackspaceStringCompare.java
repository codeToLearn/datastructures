package com.decode.april.week2;

import java.util.Stack;

/*
 * Given two strings S and T, return if they are equal when both are typed into empty text editors. # means a backspace character.

Example 1:

Input: S = "ab#c", T = "ad#c"
Output: true
Explanation: Both S and T become "ac".
Example 2:

Input: S = "ab##", T = "c#d#"
Output: true
Explanation: Both S and T become "".
Example 3:

Input: S = "a##c", T = "#a#c"
Output: true
Explanation: Both S and T become "c".
Example 4:

Input: S = "a#c", T = "b"
Output: false
Explanation: S becomes "c" while T becomes "b".
 */
public class BackspaceStringCompare {

	public static void main(String[] args) {

		System.out.println(new BackspaceStringCompare().backspaceCompare("ab##", "c#d#"));
	}

	public boolean backspaceCompare(String S, String T) {
		if (S == null && T == null) {
			return true;
		}
		if (S == null || T == null) {
			return false;
		}
		Stack<Character> stack = new Stack<>();
		for (int i = 0; i < S.length(); i++) {
			if (S.charAt(i) == '#') {
				if (!stack.isEmpty()) {
					stack.pop();
				}
			} else {
				stack.push(S.charAt(i));
			}
		}
		S = "";
		while (!stack.isEmpty()) {
			S += stack.pop();
		}
		for (int i = 0; i < T.length(); i++) {
			if (T.charAt(i) == '#') {
				if (!stack.isEmpty()) {
					stack.pop();
				}
			} else {
				stack.push(T.charAt(i));
			}
		}
		T = "";
		while (!stack.isEmpty()) {
			T += stack.pop();
		}
		return S.equals(T);
	}

	/*
	 * I have checked this code from google
	 */
	public boolean backspaceCompareUsingTwoPointers(String S, String T) {
		if (S == null && T == null) {
			return true;
		}
		if (S == null || T == null) {
			return false;
		}
		int i = S.length() - 1;
		int j = T.length() - 1;
		int skipS = 0;
		int skipT = 0;
		while (i >= 0 || j >= 0) {
			// For S
			while (i >= 0) {
				if (S.charAt(i) == '#') {
					skipS++;
					i--;
				} else if (skipS > 0) {
					skipS--;
					i--;
				} else {
					break;
				}
			}
			// FOR T
			while (j >= 0) {
				if (T.charAt(j) == '#') {
					skipT++;
					j--;
				} else if (skipT > 0) {
					skipT--;
					j--;
				} else {
					break;
				}
			}
			if (i >= 0 && j >= 0 && S.charAt(i) != T.charAt(j)) {
				return false;
			}
			if ((i >= 0) != (j >= 0)) {
				return false;
			}
			i--;
			j--;
		}
		return true;
	}

}
