package com.decode.april.week2;

import java.util.Arrays;

import com.decode.heaps.BinaryMaxHeap;

/*
 * We have a collection of stones, each stone has a positive integer weight.

Each turn, we choose the two heaviest stones and smash them together.  Suppose the stones have weights x and y with x <= y.  The result of this smash is:

If x == y, both stones are totally destroyed;
If x != y, the stone of weight x is totally destroyed, and the stone of weight y has new weight y-x.
At the end, there is at most 1 stone left.  Return the weight of this stone (or 0 if there are no stones left.)

 

Example 1:

Input: [2,7,4,1,8,1]
Output: 1
Explanation: 
We combine 7 and 8 to get 1 so the array converts to [2,4,1,1,1] then,
we combine 2 and 4 to get 2 so the array converts to [2,1,1,1] then,
we combine 2 and 1 to get 1 so the array converts to [1,1,1] then,
we combine 1 and 1 to get 0 so the array converts to [1] then that's the value of last stone.
 
 */
public class LastStoneWeight {

	public static void main(String[] args) {
		System.out.println(new LastStoneWeight().lastStoneWeight(new int[] { 2,4,1,1,1 }));
		System.out.println(new LastStoneWeight().lastStoneWeightWithHeap(new int[] {2,4,1,1,1 }));

	}

	/*
	 * In this approach n^-2Logn because every time we need to sort an array. Space
	 * also used by because every time when we strike stones together we need to
	 * sort an array again and again.
	 */
	public int lastStoneWeight(int[] stones) {
		if (stones.length == 1) {
			return stones[0];
		}

		return helper(stones)[0];
	}

	private int[] helper(int[] stones) {
		if (stones.length == 1) {
			return stones;
		}
		Arrays.sort(stones);
		// pick last two numbers
		// return new array
		stones[stones.length - 2] = stones[stones.length - 1] - stones[stones.length - 2];
		int[] arr = new int[stones.length - 1];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = stones[i];
		}
		return helper(arr);
	}

	/*
	 * By using heap we can utilize heap functionality. Because by building max heap
	 * we have top element is max. After removing the top we get second max element.
	 * Update by removing element we need to add difference of both elements
	 */

	public int lastStoneWeightWithHeap(int[] stones) {
		BinaryMaxHeap binaryMaxHeap = new BinaryMaxHeap(stones);
		while (binaryMaxHeap.getSize() > 1) {
			int item = binaryMaxHeap.remove() - binaryMaxHeap.remove();
			binaryMaxHeap.add(item);
		}
		return binaryMaxHeap.remove();
	}
}
