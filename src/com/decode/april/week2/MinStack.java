package com.decode.april.week2;

import java.util.Stack;

/*
 * Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

push(x) -- Push element x onto stack.
pop() -- Removes the element on top of the stack.
top() -- Get the top element.
getMin() -- Retrieve the minimum element in the stack.
 

Example:

MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.getMin();   --> Returns -3.
minStack.pop();
minStack.top();      --> Returns 0.
minStack.getMin();   --> Returns -2.
 */

/*
 * Idea behind the logic is that maintain state at every point
 */
public class MinStack {

	private Stack<Integer> stack;
	private Stack<Integer> stackCacheMin;

	public static void main(String[] args) {
		MinStack minStack = new MinStack();
	//	System.out.println();
		minStack.push(-2);
		minStack.push(0);
		minStack.push(-3);
		minStack.getMin();  // --> Returns -3.
		minStack.pop();
		minStack.top();      //--> Returns 0.
		minStack.getMin();   //--> Returns -2.
	
	}

	/** initialize your data structure here. */
	public MinStack() {
		stack = new Stack<>();
		stackCacheMin = new Stack<>();

	}

	// while pushing update stack caching
	public void push(int x) {

		stack.push(x);
		if (stackCacheMin.isEmpty()) {
			stackCacheMin.push(x);
			return;
		}
		if (stackCacheMin.peek() > x) {
			stackCacheMin.push(x);
		} else {
			stackCacheMin.push(stackCacheMin.peek());
		}
	}

	public void pop() {
		if (!stack.isEmpty()) {
			stack.pop();
			stackCacheMin.pop();
		}
	}

	public int top() {
		if (!stack.isEmpty()) {
			return stack.peek();
		}
		return 0;
	}

	public int getMin() {
		if (!stackCacheMin.isEmpty()) {
			return stackCacheMin.peek();
		}
		return Integer.MIN_VALUE;
	}
}
