package com.decode.april.week2;

import com.decode.trees.TreeNode;

/*
 * Given a binary tree, you need to compute the length of the diameter of the tree. The diameter of a binary tree is the length of the longest path between any two nodes in a tree. This path may or may not pass through the root.

Example:
Given a binary tree
          1
         / \
        2   3
       / \     
      4   5    
Return 3, which is the length of the path [4,2,1,3] or [5,2,1,3].

Note: The length of path between two nodes is represented by the number of edges between them.
 */
public class DaimeterOfTree {

	static int d = 1;

	public static void main(String[] args) {

		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.right = new TreeNode(15);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(20);
		DaimeterOfTree daimeterOfTree = new DaimeterOfTree();
		System.out.println(daimeterOfTree.diameterOfBinaryTree(root)); // O(n) space and O(n^2) //because according to
																		// total height
		// but there is better solution which is O(N)
		daimeterOfBinaryTreeOptimised(root);
		System.out.println(d - 1);
	}

	public int diameterOfBinaryTree(TreeNode treeNode) {
		if (treeNode == null) {
			return 0;
		}
		int leftNumberOfNodes = countMaxNumberOfNodes(treeNode.left);
		int rightNumberOfNodes = countMaxNumberOfNodes(treeNode.right);
		int totalNumberOfNodes = leftNumberOfNodes + rightNumberOfNodes;// represents diameter
		// now calculate by taking left as root
		int leftDiameter = diameterOfBinaryTree(treeNode.left);
		int rightDiameter = diameterOfBinaryTree(treeNode.right);

		return Math.max(Math.max(leftDiameter, rightDiameter), totalNumberOfNodes);
	}

	private int countMaxNumberOfNodes(TreeNode treeNode) {
		if (treeNode == null) {
			return 0;
		}
		int leftNumberOfNodes = countMaxNumberOfNodes(treeNode.left);
		int rightNumberOfNodes = countMaxNumberOfNodes(treeNode.right);
		return Math.max(leftNumberOfNodes, rightNumberOfNodes) + 1;
	}

	// The idea is vey simple as we were calculating again and again
	// Now each node represent the result
	// Intially we will find height and compare with the result
	public static int daimeterOfBinaryTreeOptimised(TreeNode treeNode) {
		if (treeNode == null) {
			return 0;
		}
		int L = daimeterOfBinaryTreeOptimised(treeNode.left);
		int R = daimeterOfBinaryTreeOptimised(treeNode.right);
		d = Math.max(d, L + R + 1);
		return Math.max(L, R) + 1;
	}
}
