package com.decode.april.week2;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 1.

Example 1:
Input: [0,1]
Output: 2
Explanation: [0, 1] is the longest contiguous subarray with equal number of 0 and 1.
Example 2:
Input: [0,1,0]
Output: 2
Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal number of 0 and 1.
Note: The length of the given binary array will not exceed 50,000.
 */
public class ContinguousZeroAndOne {

	public static void main(String[] args) {
		ArrayList<Integer>arr = new ArrayList<>();
		arr.add(1);
		
		ContinguousZeroAndOne andOne = new ContinguousZeroAndOne();
		int result = andOne.findMaxLength(new int[] { 0, 1, 0, 1, 1, 1, 0, 0 });
		System.out.println(result);
	}

	public int findMaxLength(int[] nums) {

		// convert all zero to -1;
		if (nums == null || nums.length == 0) {
			return 0;
		}
		
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 0) {
				nums[i] = -1;
			}
		}
		// problem now converted to subarray with given sum containing both positive and
		// negative number also
		return findSubarrayOfMaxLengthWithGivenSum(0, nums);
	}

	/*
	 * We will use here prefix sum array concept maintain an hashmap with value and
	 * index
	 */
	private int findSubarrayOfMaxLengthWithGivenSum(int k, int[] nums) {
		int length = 0;
		//1,1,-1,-1
		HashMap<Integer, Integer> valueIndexMap = new HashMap<>();
		int currSum = nums[0];
		valueIndexMap.put(currSum, 0);
		for (int i = 1; i < nums.length; i++) {
			currSum += nums[i];
			if (currSum == k) {
				length = Math.max(length, i + 1);
				if (!valueIndexMap.containsKey(currSum)) {
					valueIndexMap.put(currSum, i);
				}
			} else {
				if (valueIndexMap.containsKey(currSum - k)) {
					length = Math.max(length, (i - (valueIndexMap.get(currSum - k) + 1) + 1));
				} else {
					valueIndexMap.put(currSum, i);
				}
			}
		}
		return length;
	}
}
