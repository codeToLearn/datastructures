package com.decode.april.week2;

/*
 * 
  Perform String Shifts
You are given a string s containing lowercase English letters, and a matrix shift, where shift[i] = [direction, amount]:

direction can be 0 (for left shift) or 1 (for right shift). 
amount is the amount by which string s is to be shifted.
A left shift by 1 means remove the first character of s and append it to the end.
Similarly, a right shift by 1 means remove the last character of s and add it to the beginning.
Return the final string after all operations.

 

Example 1:

Input: s = "abc", shift = [[0,1],[1,2]]
Output: "cab"
Explanation: 
[0,1] means shift to left by 1. "abc" -> "bca"
[1,2] means shift to right by 2. "bca" -> "cab"
Example 2:

Input: s = "abcdefg", shift = [[1,1],[1,1],[0,2],[1,3]]
Output: "efgabcd"
Explanation:  
[1,1] means shift to right by 1. "abcdefg" -> "gabcdef"
[1,1] means shift to right by 1. "gabcdef" -> "fgabcde"
[0,2] means shift to left by 2. "fgabcde" -> "abcdefg"
[1,3] means shift to right by 3. "abcdefg" -> "efgabcd"


"mecsk"
[[1,4],[0,5],[0,4],[1,1],[1,5]]
 */
public class ShiftingOperations {
	public static void main(String[] args) {
		ShiftingOperations shiftingOperations = new ShiftingOperations();
		String result = shiftingOperations.stringShift("abcdefg",
				new int[][] {  { 0, 2 } });
		System.out.println(result);
	}

	public String stringShift(String s, int[][] shift) {
		if (shift == null || shift.length == 0) {
			return s;
		}
		int leftSum = 0;
		int rightSum = 0;
		// Logic behind the idea checkout how many will be cancelled
		for (int i = 0; i < shift.length; i++) {
			if (shift[i][0] == 0) {
				leftSum += shift[i][1];
			} else {
				rightSum += shift[i][1];
			}
		}
		int totalShiffting = leftSum - rightSum;
		// -ve represents right shift

		int absouteValue = Math.abs(totalShiffting);
		absouteValue = absouteValue % s.length();
		if (absouteValue == 0) {
			return s;
		}
		if (totalShiffting < 0) {
			totalShiffting = s.length() - absouteValue;
		} else {
			// make it right shift
			totalShiffting = absouteValue;
		}

		char[] charArray = s.toCharArray();
		// right shift means left rotation
		rotateArray(charArray, totalShiffting);
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < charArray.length; i++) {
			result.append(charArray[i]);
		}
		return result.toString();
	}

	private void rotateArray(char[] charArray, int rotation) {

		// reverse first half

		reverse(0, rotation-1, charArray);
		reverse(rotation, charArray.length - 1, charArray);
		reverse(0, charArray.length - 1, charArray);

	}

	private void reverse(int start, int last, char[] chars) {

		while (start < last) {
			swap(start++, last--, chars);
		}
	}

	private void swap(int first, int last, char[] chars) {
		char temp = chars[first];
		chars[first] = chars[last];
		chars[last] = temp;
	}
}
