package com.decode.april.week1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Given an array of strings, group anagrams together.

Example:

Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
Output:
[
  ["ate","eat","tea"],
  ["nat","tan"],
  ["bat"]
]
 */
public class GroupAnagrams {

	public static void main(String[] args) {
		String string[] = { "eat", "tea", "tan", "ate", "nat", "bat" };
		System.out.println(groupAnagrams(string));
	}

	public static List<List<String>> groupAnagrams(String[] strs) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		char[] char1;
		String s;
		for (String string : strs) {
			char1 = string.toCharArray();
			Arrays.sort(char1);
			s = new String(char1);
			if (!map.containsKey(s)) {
				map.put(s, new ArrayList<>());
			}
			map.get(s).add(string);
		}
		List<List<String>> list = new ArrayList<>();
		for (String string : map.keySet()) {
			list.add(map.get(string));
		}
		return list;
	}
}
