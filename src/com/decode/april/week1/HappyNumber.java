package com.decode.april.week1;

import java.util.HashSet;
import java.util.Set;

/*
 * Write an algorithm to determine if a number is "happy".

A happy number is a number defined by the following process: Starting with any positive integer, replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.
 */
public class HappyNumber {
	public static void main(String[] args) {

		System.out.println(isHappy(20));
	}

	public static boolean isHappy(int n) {
		return isHappy(n, new HashSet<>());

	}

	public static boolean isHappy(int n, Set<Integer> set) {
		System.out.println(n);
		if (n == 0) {
			return false;
		}
		if (n == 1) {
			return true;

		}
		if (set.contains(n)) {
			return false;
		}
		set.add(n);
		int num = 0;
		while (n > 0) {
			num += ((n % 10) * (n % 10));
			n = n / 10;
		}
		return isHappy(num, set);

	}
}
