package com.decode.april.week1;

public class SingleNumber {

	public static void main(String[] args) {

		int nums[] = { 1, 2, 1 };
		System.out.println(singleNumber(nums));
	}

	public static int singleNumber(int[] nums) {
		int number = 0;
		for (int i = 0; i < nums.length; i++) {
			number ^= nums[i];
		}
		return number;
	}

}
