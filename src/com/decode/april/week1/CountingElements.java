package com.decode.april.week1;

import java.util.HashMap;

public class CountingElements {
	public static void main(String[] args) {
		 System.out.println(countElements(new int[] { 1, 1, 2 }));
		 System.out.println(countElements(new int[] { 1,1,3,3,5,5,7,7 }));
		System.out.println(countElements(new int[] { 1, 3, 2, 3, 5, 0 }));
	}

	public static int countElements(int[] arr) {
		int count = 0;
		if (arr.length <= 1) {
			return count;
		}

		HashMap<Integer, Integer> countMap = new HashMap<>();
		for (int i = 0; i < arr.length; i++) {

			if (!countMap.containsKey(arr[i])) {
				countMap.put(arr[i], 0);

			}
			countMap.put(arr[i], countMap.get(arr[i]) + 1);

		}
		for (Integer value:countMap.keySet()) {
			if (countMap.containsKey(value+1)) {
				count +=countMap.get(value);
			}
		}
		return count;
	}
}
