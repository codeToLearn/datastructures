package com.decode.april.week1;

public class MaxSumSubArray {

	public static void main(String[] args) {
		int nums[] = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
		System.out.println(maxSubArray(nums));
	}

	public static int maxSubArray(int[] nums) {

		int max = nums[0], sumsofar = nums[0];
		for (int i = 1; i < nums.length; i++) {
			if (nums[i] < sumsofar + nums[i]) {
				sumsofar = sumsofar + nums[i];
			} else {
				sumsofar = nums[i];
			}
			if (sumsofar > max) {
				max = sumsofar;
			}
		}
		return max;

	}
}
