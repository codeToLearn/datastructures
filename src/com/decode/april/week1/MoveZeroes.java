package com.decode.april.week1;
/*
 * Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

Example:

Input: [0,1,0,3,12]
Output: [1,3,12,0,0]
Note:

You must do this in-place without making a copy of the array.
Minimize the total number of operations.
 */

public class MoveZeroes {

	public static void main(String[] args) {
		int nums[] = { 0, 1, 0, 3, 12 };
		moveZeroes(nums);

	}

	public static void moveZeroes(int[] nums) {

		int zeroIndex = -1;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 0) {
				if (zeroIndex == -1) {
					zeroIndex = i;
				}
			} else if (zeroIndex > -1) {
				nums[zeroIndex] = nums[i];
				nums[i] = 0;
				zeroIndex++;
			}
		}

	}
}
