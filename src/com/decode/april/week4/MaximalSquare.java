package com.decode.april.week4;

/**
 * 
 * @author Sachin Jindal
 * 
 *         <p>
 *         Given a 2D binary matrix filled with 0's and 1's, find the largest
 *         square containing only 1's and return its area.
 * 
 *         Example:
 * 
 *         Input:
 * 
 *         <pre>
1 0 1 0 0
1 0 1 1 1
1 1 1 1 1
1 0 0 1 0
 *         </pre>
 * 
 *         Output: 4
 *         </p>
 */
public class MaximalSquare {

	public static void main(String[] args) {
		System.out.println();
		MaximalSquare maximalSquare = new MaximalSquare();
		char[][] matrix = new char[4][5];
		// first row
		matrix[0][0] = '1';
		matrix[0][1] = '1';
		matrix[0][2] = '1';
		matrix[0][3] = '1';
		matrix[0][4] = '1';
		// matrix[0][5] = '1';
		// second row
		matrix[1][0] = '1';
		matrix[1][1] = '1';
		matrix[1][2] = '1';
		matrix[1][3] = '1';
		matrix[1][4] = '1';

		// third row
		matrix[2][0] = '1';
		matrix[2][1] = '1';
		matrix[2][2] = '1';
		matrix[2][3] = '1';
		matrix[2][4] = '1';

		// fourth row
		matrix[3][0] = '1';
		matrix[3][1] = '1';
		matrix[3][2] = '1';
		matrix[3][3] = '1';
		matrix[3][4] = '1';

		System.out.println(maximalSquare.maximalSquare(matrix));
		int rowSize = matrix.length;
		int colSize = matrix[0].length;
		int max = 0;
		for (int row = 0; row < rowSize; row++) {
			for (int col = 0; col < colSize; col++) {
				max = Math.max(max, maximalSquare.findMaxRecursive(row, col, matrix, ""));
			}
		}
		System.out.println(max * max);
		System.out.println(maximalSquare.findMaxDP(matrix));
	}

	/**
	 * 
	 * @param matrix
	 * @return CHECK FOR EVERY POSSIBLE VALUE - (mn)^2
	 */
	public int maximalSquare(char[][] matrix) {
		int rowSize = matrix.length;
		int colSize = matrix[0].length;
		int maxOne = 0;
		int maxOneSoFar = 0;
		int rowIndex = 0;
		int colIndex = 0;
		for (int row = 0; row < rowSize; row++) {
			outer: for (int col = 0; col < colSize; col++) {
				if (matrix[row][col] != '0') {
					// fix here variable
					rowIndex = row;
					colIndex = col;
					// make square

					while (rowIndex < rowSize && colIndex < colSize) {
						maxOneSoFar = 0;
						for (int start = row; start <= rowIndex; start++) {
							for (int end = col; end <= colIndex; end++) {
								if (matrix[start][end] == '0') {
									// because that whole column should not be considered
									continue outer;
								}
								maxOneSoFar++;
							}
						}
						maxOne = Math.max(maxOneSoFar, maxOne);
						if (maxOne == rowSize * colSize) {
							return maxOne;
						}
						rowIndex++;
						colIndex++;
					}
				}
			}

		}
		return maxOne;

	}

	/**
	 * 
	 * @param row
	 * @param col
	 * @param matrix
	 * @return TRY EVERY POSSIBLE DIRECTION (3)^mn
	 */
	private int findMaxRecursive(int row, int col, char[][] matrix, String dir) {

		if (row >= matrix.length || col >= matrix[row].length || matrix[row][col] == '0') {
			return 0;
		}
		// every one asking to other is it possible to add me
		// right
		// if we are moving left then we only move to left
		// if we are moving right then we only move to right
		// if we are at diagonal position what does it means it means this index asking
		// to its left and right is i am the one to make a square
		switch (dir) {
		case "r":
			return 1 + findMaxRecursive(row + 1, col, matrix, dir);

		case "d":
			return 1 + findMaxRecursive(row, col + 1, matrix, dir);

		default:
			int right = findMaxRecursive(row + 1, col, matrix, "r");
			int down = findMaxRecursive(row, col + 1, matrix, "d");
			int diagonal = findMaxRecursive(row + 1, col + 1, matrix, "");
			int min = Math.min(diagonal, Math.min(right, down)) + 1;
			return min;
		}

	}

	/**
	 * 
	 * @param matrix
	 * @return
	 */
	private int findMaxDP(char[][] matrix) {
		/**
		 * In previous recursive appraoch we deduce there was subproblems so we can use
		 * dp here dp[i][j] = Min(dp[i+1][j+1],dp[i+1][j],dp[i][j+1])+1 Minimun
		 * represents which is most suitable so we can get square if rectangle then we
		 * can choose max
		 */
		if (matrix.length == 0) {
			return 0;
		}
		int[][] dp = new int[matrix.length][matrix[0].length];

		// last row and column as it
		int max = 0;
		for (int col = 0; col < matrix[matrix.length - 1].length; col++) {
			dp[matrix.length - 1][col] = matrix[matrix.length - 1][col] - 48;
			if (dp[matrix.length - 1][col] == 1) {
				max = 1;
			}
		}
		for (int row = 0; row < matrix.length; row++) {
			dp[row][matrix[row].length - 1] = matrix[row][matrix[row].length - 1] - 48;
			if (dp[row][matrix[row].length - 1] == 1) {
				max = 1;
			}
		}
		// now we can proceed dp
		for (int row = matrix.length - 2; row >= 0; row--) {
			for (int col = matrix[row].length - 2; col >= 0; col--) {
				if (matrix[row][col] == '0') {
					continue;
				}
				dp[row][col] = Math.min(dp[row][col + 1], Math.min(dp[row + 1][col + 1], dp[row + 1][col])) + 1;
				max = Math.max(max, dp[row][col]);
			}
		}
		return max * max;
	}
}
