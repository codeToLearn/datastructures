package com.decode.april.week4;

/**
 * 
 * @author Sachin Jindal
 *
 *         Determine if you are able to reach the last index.
 * 
 *         Example 1:
 * 
 *         Input: [2,3,1,1,4] Output: true Explanation: Jump 1 step from index 0
 *         to 1, then 3 steps to the last index. Example 2:
 * 
 *         Input: [3,2,1,0,4] Output: false Explanation: You will always arrive
 *         at index 3 no matter what. Its maximum jump length is 0, which makes
 *         it impossible to reach the last index.
 */
public class JumpGame {

	public static void main(String[] args) {
		JumpGame jumpGame = new JumpGame();
		boolean check = jumpGame.canJump(new int[] { 2, 3, 1, 14 });
		System.out.println(check);
		check = jumpGame.canJump(new int[] { 3, 2, 1, 0, 4 });
		System.out.println(check);
		check = jumpGame.canJumpDP(new int[] { 2, 3, 1, 14 });
		System.out.println(check);
		check = jumpGame.canJumpDP(new int[] { 3, 2, 1, 0, 4 });
		System.out.println(check);

	}

	public boolean canJump(int[] nums) {

		return canJump(nums, 0);
	}

	/**
	 * 
	 * @param nums
	 * @param index
	 * @return
	 * 
	 * 		There are subproblems maintain an array
	 * 
	 *         In this problem is if there is zero then we again need to find path
	 *         for next jump
	 */
	private boolean canJump(int[] nums, int index) {

		if (index == nums.length - 1) {

			return true;

		}
		if (checkInBounds(nums, index) && nums[index] != 0) {
			for (int i = 1; i <= nums[index]; i++) {
				if (canJump(nums, index + i)) {
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * In this case we will maintain a boolean array of size of given array. Now in
	 * previous result we have check canJump(i) =
	 * canJump(i+1)||canJump(i+2)||.....canJump(i+num[i]);
	 * 
	 * It means we need to move backwards
	 * 
	 * As we know at last position we are reached so we set it true
	 * 
	 */
	private boolean canJumpDP(int[] nums) {

		boolean jump[] = new boolean[nums.length];
		jump[nums.length - 1] = true;// yes we can reach at last
		for (int index = nums.length - 2; index >= 0; index--) {
			// possibilities are you can jump from index to num[index]+1,....num[index+j]
			for (int j = 1; j <= nums[index]; j++) {
				// initially check length
				if (checkInBounds(nums, index + j)) {
					if (jump[index + j]) {
						jump[index] = true;
						// no need check for other jumps
						break;
					}
				}
			}
		}
		return jump[0];
	}

	private boolean checkInBounds(int[] nums, int index) {
		return index >= 0 && index < nums.length;
	}
}
