package com.decode.april.week4;

import java.util.HashMap;

/**
 * 
 * @author Sachin Jindal You have a queue of integers, you need to retrieve the
 *         first unique integer in the queue.
 * 
 *         Implement the FirstUnique class:
 * 
 *         FirstUnique(int[] nums) Initializes the object with the numbers in
 *         the queue. int showFirstUnique() returns the value of the first
 *         unique integer of the queue, and returns -1 if there is no such
 *         integer. void add(int value) insert value to the queue.
 * 
 * 
 *         Example 1:
 * 
 *         Input:
 *         ["FirstUnique","showFirstUnique","add","showFirstUnique","add","showFirstUnique","add","showFirstUnique"]
 *         [[[2,3,5]],[],[5],[],[2],[],[3],[]] Output:
 *         [null,2,null,2,null,3,null,-1]
 * 
 *         Explanation: FirstUnique firstUnique = new FirstUnique([2,3,5]);
 *         firstUnique.showFirstUnique(); // return 2 firstUnique.add(5); // the
 *         queue is now [2,3,5,5] firstUnique.showFirstUnique(); // return 2
 *         firstUnique.add(2); // the queue is now [2,3,5,5,2]
 *         firstUnique.showFirstUnique(); // return 3 firstUnique.add(3); // the
 *         queue is now [2,3,5,5,2,3] firstUnique.showFirstUnique(); // return
 *         -1
 * 
 *         Example 2:
 * 
 *         Input:
 *         ["FirstUnique","showFirstUnique","add","add","add","add","add","showFirstUnique"]
 *         [[[7,7,7,7,7,7]],[],[7],[3],[3],[7],[17],[]] Output:
 *         [null,-1,null,null,null,null,null,17]
 * 
 *         Explanation: FirstUnique firstUnique = new
 *         FirstUnique([7,7,7,7,7,7]); firstUnique.showFirstUnique(); // return
 *         -1 firstUnique.add(7); // the queue is now [7,7,7,7,7,7,7]
 *         firstUnique.add(3); // the queue is now [7,7,7,7,7,7,7,3]
 *         firstUnique.add(3); // the queue is now [7,7,7,7,7,7,7,3,3]
 *         firstUnique.add(7); // the queue is now [7,7,7,7,7,7,7,3,3,7]
 *         firstUnique.add(17); // the queue is now [7,7,7,7,7,7,7,3,3,7,17]
 *         firstUnique.showFirstUnique(); // return 17
 * 
 *         Example 3:
 * 
 *         Input: ["FirstUnique","showFirstUnique","add","showFirstUnique"]
 *         [[[809]],[],[809],[]] Output: [null,809,null,-1]
 * 
 *         Explanation: FirstUnique firstUnique = new FirstUnique([809]);
 *         firstUnique.showFirstUnique(); // return 809 firstUnique.add(809); //
 *         the queue is now [809,809] firstUnique.showFirstUnique(); // return
 *         -1
 * 
 * 
 * 
 *         Constraints:
 * 
 *         1 <= nums.length <= 10^5 1 <= nums[i] <= 10^8 1 <= value <= 10^8 At
 *         most 50000 calls will be made to showFirstUnique and add.
 * 
 */
/**
 * 
 * @author Sachin Jindal<br>
 *         We need to find first unique number from and given array it means for
 *         searching unique number exists or not we can use hashmap which will
 *         provide use o(1) complexity for searching. e.g. 1,2,3 - 1 is answer
 *         1,2,3,1 - but now the problem is we need to first unique number
 *         because 1 is not more unique number we will remove that number how
 *         could we determine next unique number we can use linkedhashmap - i.e.
 *         doubly linked list
 */
public class FirstUnique {

	DoubleLinkedList head;
	DoubleLinkedList tail;
	HashMap<Integer, DoubleLinkedList> cacheMap = new HashMap<>();

	public FirstUnique(int[] nums) {

		// Dummy head and tail nodes to avoid empty states
		head = new DoubleLinkedList(-1, -1);

		tail = new DoubleLinkedList(-1, -1);
		// wire them together
		head.after = tail;
		tail.before = head;

		// Now we have an array

		for (int i = 0; i < nums.length; i++) {
			add(nums[i]);
		}
	}

	public int showFirstUnique() {
		if (head.after != null) {
			return head.after.key;
		}
		return -1;

	}

	public void add(int value) {
		if (cacheMap.containsKey(value)) {
			// remove from double linked list
			if (cacheMap.get(value) != null) {

				deleteNode(cacheMap.get(value));
				cacheMap.put(value, null);
			}

		} else {
			// push at the end
			DoubleLinkedList doubleLinkedList = new DoubleLinkedList(value, value);
			insertAtLast(doubleLinkedList);
			cacheMap.put(value, doubleLinkedList);
		}
	}

	private void deleteNode(DoubleLinkedList node) {
		DoubleLinkedList savedBefore = node.before;
		DoubleLinkedList savedAfter = node.after;
		// now maintain the pointers
		savedBefore.after = savedAfter;
		savedAfter.before = savedBefore;
	}

	private void insertAtLast(DoubleLinkedList node) {
		DoubleLinkedList savedBefore = tail.before;

		// now maintain the pointers
		node.after = tail;
		node.before = savedBefore;
		savedBefore.after = tail.before = node;

	}

	public static void main(String[] args) {
		FirstUnique firstUnique = new FirstUnique(new int[] { 2, 3, 5 });

		System.out.println(firstUnique.showFirstUnique());

	}
}

class DoubleLinkedList {
	int key;
	int value;
	DoubleLinkedList after;
	DoubleLinkedList before;

	public DoubleLinkedList(int key, int data) {
		this.key = key;
		this.value = data;
	}
}
