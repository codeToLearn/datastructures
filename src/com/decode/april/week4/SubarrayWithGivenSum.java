package com.decode.april.week4;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Sachin Jindal Given an array of integers and an integer k, you need
 *         to find the total number of continuous subarrays whose sum equals to
 *         k.
 * 
 *         Example 1: Input:nums = [1,1,1], k = 2 Output: 2 Note: The length of
 *         the array is in range [1, 20,000]. The range of numbers in the array
 *         is [-1000, 1000] and the range of the integer k is [-1e7, 1e7].
 */
public class SubarrayWithGivenSum {

	public static void main(String[] args) {
		SubarrayWithGivenSum subarrayWithGivenSum = new SubarrayWithGivenSum();
		subarrayWithGivenSum.subarraySum(new int[] { 1, 2, 3 }, 3);
	}

	public int subarraySum(int[] nums, int k) {
		Map<Integer, Integer> valueIndexMap = new HashMap<>();// This will maintain value count sofar
		int currSum = 0;

		int count = 0;
		for (int i = 0; i < nums.length; i++) {

			currSum += nums[i];

			if (currSum == k) {
				// this represent sum is equal so increment count by one
				count++;
			}
			// prefix sum subarray problem
			if (valueIndexMap.containsKey(currSum - k)) {
				// this checks how many count are available for currSum -k
				count += valueIndexMap.get(currSum - k);
			}
			if (!valueIndexMap.containsKey(currSum)) {

				valueIndexMap.put(currSum, 0);
			}

			valueIndexMap.put(currSum, valueIndexMap.get(currSum) + 1);
		}
		return count;

	}
}
