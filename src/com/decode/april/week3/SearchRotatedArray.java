package com.decode.april.week3;

/**
 * 
 * @author Sachin Jindal
 * 
 *         Suppose an array sorted in ascending order is rotated at some pivot
 *         unknown to you beforehand.
 * 
 *         (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).
 * 
 *         You are given a target value to search. If found in the array return
 *         its index, otherwise return -1.
 * 
 *         You may assume no duplicate exists in the array.
 * 
 *         Your algorithm's runtime complexity must be in the order of O(log n).
 * 
 *         Example 1:
 * 
 *         Input: nums = [4,5,6,7,0,1,2], target = 0 Output: 4 Example 2:
 * 
 *         Input: nums = [4,5,6,7,0,1,2], target = 3 Output: -1
 * 
 */

public class SearchRotatedArray {
	public static void main(String[] args) {
		SearchRotatedArray searchRotatedArray = new SearchRotatedArray();
		int result = searchRotatedArray.search(new int[] { 4, 5, 6, 7, 0, 1, 2 }, 0);
		System.out.println(result);
		result = searchRotatedArray.search(new int[] { 4, 5, 6, 7, 0, 1, 2 }, 3);
		System.out.println(result);
	}

	public int search(int[] nums, int target) {
		return binarySearch(0, nums.length - 1, nums, target);
	}

	private int binarySearch(int lower, int upper, int[] nums, int target) {

		int ans = -1;
		if (lower <= upper) {
			int mid = lower + (upper - lower) / 2;
			// root
			if (nums[mid] == target) {
				return mid;
			}

			if (target == nums[lower]) {
				return lower;
			}
			if (target == nums[upper]) {
				return upper;
			}
			if (target > nums[mid]) {

				if (nums[lower] < target && nums[lower] > nums[mid]) {
					ans = binarySearch(lower, mid - 1, nums, target);

				} else {
					ans = binarySearch(mid + 1, upper, nums, target);
				}
			} else {

				if (nums[upper] > target && nums[upper] < nums[mid]) {
					ans = binarySearch(mid + 1, upper, nums, target);

				} else {
					ans = binarySearch(lower, mid - 1, nums, target);
				}

			}

		}
		return ans;
	}
}
