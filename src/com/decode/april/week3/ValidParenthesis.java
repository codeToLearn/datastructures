package com.decode.april.week3;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/*
 * Given a string containing only three types of characters: '(', ')' and '*', write a function to check whether this string is valid. We define the validity of a string by these rules:

Any left parenthesis '(' must have a corresponding right parenthesis ')'.
Any right parenthesis ')' must have a corresponding left parenthesis '('.
Left parenthesis '(' must go before the corresponding right parenthesis ')'.
'*' could be treated as a single right parenthesis ')' or a single left parenthesis '(' or an empty string.
An empty string is also valid.
Example 1:
Input: "()"
Output: True
Example 2:
Input: "(*)"
Output: True
Example 3:
Input: "(*))"
Output: True
Note:
The string size will be in the range [1, 100].

 */
public class ValidParenthesis {
	public static void main(String[] args) {
		ValidParenthesis parenthesis = new ValidParenthesis();
		boolean result = parenthesis.checkValidString("*(()");

		System.out.println(result);

		result = parenthesis.checkValidString("(*)");
		System.out.println(result);
		result = parenthesis.checkValidString("(*))");
		System.out.println(result);

		System.out.println(result);
		result = parenthesis.checkValidString("*())");
		System.out.println(result);

	}

	/**
	 * 
	 * @param s
	 *            This method check string is valid or not We will maintain two
	 *            stack one will maintain id(index) for left and one for star
	 *            Hardest part for me to think how to analyze star come before left
	 *            or not
	 * @return {@link Boolean}
	 */
	public boolean checkValidString(String s) {
		/*
		 * Initially i was thinking we have two stack they will contain the characters
		 * right parenthesis working fine but problem with left parenthesis is i don't
		 * have any idea is * come before left or after with little bit more intuition i
		 * come up with an idea why not store index of values not character itself.
		 */
		Stack<Integer> leftID = new Stack<>();

		Stack<Integer> starID = new Stack<>();
		char[] chars = s.toCharArray();

		if (s != null && !s.isEmpty()) {
			for (int i = 0; i < chars.length; i++) {
				if (chars[i] == ')') {
					if (leftID.isEmpty() && starID.isEmpty()) {
						return false;
					}
					if (leftID.isEmpty()) {
						starID.pop();
					} else {
						leftID.pop();
					}
				} else if (chars[i] == '(') {
					leftID.push(i);
				} else {
					starID.push(i);
				}
			}
		}
		while (!leftID.isEmpty() && !starID.isEmpty()) {
			if (leftID.pop() > starID.pop()) {
				return false;
			}
		}

		return leftID.isEmpty();
	}

}
