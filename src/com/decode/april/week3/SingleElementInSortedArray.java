package com.decode.april.week3;

/**
 * 
 * @author Sachin Jindal
 * 
 *         You are given a sorted array consisting of only integers where every
 *         element appears exactly twice, except for one element which appears
 *         exactly once. Find this single element that appears only once.
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input: [1,1,2,3,3,4,4,8,8] Output: 2 Example 2:
 * 
 *         Input: [3,3,7,7,10,11,11] Output: 10
 * 
 */
public class SingleElementInSortedArray {

	public static void main(String[] args) {
		SingleElementInSortedArray elementInSortedArray = new SingleElementInSortedArray();
		System.out.println(elementInSortedArray.singleNonDuplicate(new int[] { 1, 1, 2 ,2,3,3,4,4,10}));
	}

	/**
	 * The idea behind the logic is <br>
	 * 1.if mid left and right both are different then mid is the answer<br>
	 * 2. if mid left same then check if left elements are even then number should
	 * be in right side else in left side<br>
	 * 3. if mid right same then check if right elements are even then go to left
	 * else move to right
	 * 
	 * @param nums
	 * @return
	 */
	public int singleNonDuplicate(int[] nums) {

		return nums[binarySearch(nums, 0, nums.length - 1)];
	}

	private int binarySearch(int[] nums, int lower, int upper) {

		while (lower <= upper) {
			if (lower == upper) {
				return lower;
			}
			int mid = lower + ((upper - lower) / 2);

			if (mid - 1 >= lower && nums[mid] == nums[mid - 1]) {
				// left are even
				if ((mid - lower - 1) % 2 == 0) {
					// move right
					return binarySearch(nums, mid + 1, upper);
				}
				// move left
				return binarySearch(nums, lower, mid - 2);
			}
			if (mid + 1 <= upper && nums[mid] == nums[mid + 1]) {
				if ((upper - mid - 1) % 2 == 0) {
					// move left
					return binarySearch(nums, lower, mid - 1);
				}
				// move left
				return binarySearch(nums, mid + 2, upper);
			}
			// left and right not exist
			return mid;
		}
		return -1;
	}
}
