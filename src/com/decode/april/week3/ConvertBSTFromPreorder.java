package com.decode.april.week3;

import com.decode.trees.TreeNode;

/**
 * 
 * @author Sachin Jindal
 *
 *         Return the root node of a binary search tree that matches the given
 *         preorder traversal.
 * 
 *         (Recall that a binary search tree is a binary tree where for every
 *         node, any descendant of node.left has a value < node.val, and any
 *         descendant of node.right has a value > node.val. Also recall that a
 *         preorder traversal displays the value of the node first, then
 *         traverses node.left, then traverses node.right.)
 * 
 * 
 * 
 *         Example 1:
 * 
 *         Input: [8,5,1,7,10,12] Output: [8,5,10,1,7,null,12]
 * 
 * 
 * 
 *         Note:
 * 
 *         1 <= preorder.length <= 100 The values of preorder are distinct.
 * 
 */
public class ConvertBSTFromPreorder {

	private int index = 0;

	public static void main(String[] args) {
		ConvertBSTFromPreorder bstFromPreorder = new ConvertBSTFromPreorder();
		bstFromPreorder.bstFromPreorder(new int[] { 8, 5, 1, 7, 10, 12 });
	}

	public TreeNode bstFromPreorder(int[] preorder) {
		// bstFromPreorder(null, null, preorder);
		TreeNode treeNode = bstFromPreorder(Integer.MIN_VALUE, Integer.MAX_VALUE, preorder);
		return treeNode;
	}

	/**
	 * 
	 * @param lower
	 * @param upper
	 * @param preorder
	 *            The idea is to check every node upper and lower limit.
	 * @return {@link TreeNode}
	 */
	private TreeNode bstFromPreorder(int lower, int upper, int[] preorder) {

		if (index >= preorder.length) {
			return null;
		}
		TreeNode treeNode = null;
		/*
		 * // it means we are at root level if ((upper == null && lower == null) ||
		 * (lower == null && preorder[index] < upper) || (upper == null &&
		 * preorder[index] > lower) || (lower != null && upper != null && lower <
		 * preorder[index] && preorder[index] < upper))
		 */ if (lower < preorder[index] && preorder[index] < upper) {
			treeNode = new TreeNode(preorder[index]);
			index++;
			// left
			treeNode.left = bstFromPreorder(lower, treeNode.val, preorder);
			// right
			treeNode.right = bstFromPreorder(treeNode.val, upper, preorder);

		}

		return treeNode;
	}
}
