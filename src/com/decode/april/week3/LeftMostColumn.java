package com.decode.april.week3;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *         (This problem is an interactive problem.)
 * 
 *         A binary matrix means that all elements are 0 or 1. For each
 *         individual row of the matrix, this row is sorted in non-decreasing
 *         order.
 * 
 *         Given a row-sorted binary matrix binaryMatrix, return leftmost column
 *         index(0-indexed) with at least a 1 in it. If such index doesn't
 *         exist, return -1.
 * 
 *         You can't access the Binary Matrix directly. You may only access the
 *         matrix using a BinaryMatrix interface:
 * 
 *         BinaryMatrix.get(x, y) returns the element of the matrix at index (x,
 *         y) (0-indexed). BinaryMatrix.dimensions() returns a list of 2
 *         elements {n, m}, which means the matrix is n * m. Submissions making
 *         more than 1000 calls to BinaryMatrix.get will be judged Wrong Answer.
 *         Also, any solutions that attempt to circumvent the judge will result
 *         in disqualification.
 * 
 *         For custom testing purposes you're given the binary matrix mat as
 *         input in the following four examples. You will not have access the
 *         binary matrix directly.
 * 
 * 
 * 
 *         Example 1:
 * 
 * 
 * 
 *         Input: mat = {{0,0},{1,1}} Output: 0
 *         <p>
 *         Example 2:
 * 
 * 
 * 
 *         Input: mat = {{0,0},{0,1}} Output: 1<br>
 *         Example 3:
 * 
 * 
 * 
 *         Input: mat = {{0,0},{0,0}} Output: -1<br>
 *         Example 4:
 * 
 * 
 * 
 *         Input: mat = {{0,0,0,1},{0,0,1,1},{0,1,1,1}} Output: 1
 * 
 * 
 *         Constraints:
 * 
 *         1 <= mat.length, mat{i}.length <= 100 mat{i}{j} is either 0 or 1.
 *         mat{i} is sorted in a non-decreasing way.
 * 
 */

public class LeftMostColumn {
	public static void main(String[] args) {
		BM bm = new BM();
		LeftMostColumn leftMostColumn = new LeftMostColumn();
		System.out.println(leftMostColumn.leftMostColumnWithOne(bm));
	}

	public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
		List<Integer> matrixSize = binaryMatrix.dimensions();
		int minIndexSofar = -1;
		int index = 0;
		for (int rowNo = 0; rowNo < matrixSize.get(0); rowNo++) {
			index = leftMostColumnWithOneBSFirstOccurance(rowNo, 0, matrixSize.get(1) - 1, binaryMatrix);
			if (index == 0) {
				return 0;
			}
			if (minIndexSofar == -1) {
				minIndexSofar = index;
			}

			if (index != -1 && minIndexSofar > index) {
				minIndexSofar = index;

			}
		}
		return minIndexSofar;
	}

	private int leftMostColumnWithOneBSFirstOccurance(int row, int lower, int upper, BinaryMatrix binaryMatrix) {
		int ans = -1;
		if (lower <= upper) {
			int value = 0;
			int mid = lower + (upper - lower) / 2;
			value = binaryMatrix.get(row, mid);
			ans = mid;
			if (value == 1) {
				if (mid > lower && binaryMatrix.get(row, mid - 1) == 1) {
					ans = leftMostColumnWithOneBSFirstOccurance(row, lower, mid - 1, binaryMatrix);
				}
			} else {
				ans = leftMostColumnWithOneBSFirstOccurance(row, mid + 1, upper, binaryMatrix);
			}
			// root

		}
		return ans;
	}
}

// This is the BinaryMatrix's API interface.
// You should not implement it, or speculate about its implementation
interface BinaryMatrix {
	public int get(int x, int y);

	public List<Integer> dimensions();

}

class BM implements BinaryMatrix {
	int row = 3;
	int col = 4;
	private int[][] matrix = new int[][] { { 0, 0, 1, 1, 1, 1, 1 }, { 0, 1, 1, 1, 1, 1, 1 }, { 0, 0, 0, 1, 1, 1, 1 },
			{ 0, 0, 0, 1, 1, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 1, 1 }, { 0, 0, 0, 1, 1, 1, 1 } };

	public BM() {
		// first
		/*
		 * matrix{0}{0} = 0; matrix{0}{1} = 0; matrix{0}{2} = 0; matrix{0}{3} = 1; //
		 * second
		 * 
		 * matrix{1}{0} = 0; matrix{1}{1} = 0; matrix{1}{2} = 1; matrix{1}{3} = 1; //
		 * IIIrd
		 * 
		 * matrix{2}{0} = 0; matrix{2}{1} = 1; matrix{2}{2} = 1; matrix{2}{3} = 1;
		 */
	}

	public int get(int x, int y) {

		return matrix[x][y];
	}

	public List<Integer> dimensions() {
		List<Integer> list = new ArrayList<Integer>();
		list.add(matrix.length);
		list.add(matrix[0].length);
		return list;
	}
}
