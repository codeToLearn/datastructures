package com.decode.april.week3;

/**
 * 
 * @author Sachin Jindal
 *
 *         Minimum Path Sum Given a m x n grid filled with non-negative numbers,
 *         find a path from top left to bottom right which minimizes the sum of
 *         all numbers along its path.
 * 
 *         Note: You can only move either down or right at any point in time.
 * 
 *         Example:
 * 
 *         Input: [ [1,3,1], [1,5,1], [4,2,1] ] Output: 7 Explanation: Because
 *         the path 1→3→1→1→1 minimizes the sum.
 */
public class MinimumSumPath {
	private int[][] shifts = { { 1, 0 }, { 0, 1 } };// ,down,,right

	public static void main(String[] args) {
		int[][] grid = new int[3][3];
		// first row
		grid[0][0] = 1;
		grid[0][1] = 3;
		grid[0][2] = 1;
		// second row
		grid[1][0] = 1;
		grid[1][1] = 5;
		grid[1][2] = 1;
		// third row
		grid[2][0] = 4;
		grid[2][1] = 2;
		grid[2][2] = 1;
		MinimumSumPath minimumSumPath = new MinimumSumPath();
		System.out.println(minimumSumPath.minPathSum(grid));

	}

	public int minPathSum(int[][] grid) {

		return traversalDP(grid);

	}

	private int traversal(int row, int col, int[][] grid) {

		int down = Integer.MAX_VALUE;
		int newRowDown = row + shifts[0][0];
		int newColDown = col + shifts[0][1];
		if (canTraverse(newRowDown, newColDown, grid)) {
			down = traversal(newRowDown, newColDown, grid);
		}

		int right = Integer.MAX_VALUE;
		int newRowRight = row + shifts[1][0];
		int newColRight = col + shifts[1][1];
		if (canTraverse(newRowRight, newColRight, grid)) {
			right = traversal(newRowRight, newColRight, grid);
		}
		if (!canTraverse(newRowDown, newColDown, grid) && !canTraverse(newRowRight, newColRight, grid)) {
			return grid[row][col];
		}
		return Math.min(down, right) + grid[row][col];
	}

	private boolean canTraverse(int row, int col, int[][] grid) {
		return row >= 0 && row < grid.length && col >= 0 && col < grid[row].length;

	}

	private int traversalDP(int[][] grid) {

		// set first row
		for (int col = 1; col < grid[0].length; col++) {
			grid[0][col] += grid[0][col - 1];
		}
		// set first col
		for (int row = 1; row < grid.length; row++) {
			grid[row][0] += grid[row - 1][0];
		}
		for (int row = 1; row < grid.length; row++) {
			for (int col = 1; col < grid[row].length; col++) {
				grid[row][col]+=Math.min(grid[row-1][col], grid[row][col-1]);
			}
		}
		return grid[grid.length - 1][grid[grid.length - 1].length - 1];
	}

}
