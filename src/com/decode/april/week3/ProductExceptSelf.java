package com.decode.april.week3;

/*
 *   Product of Array Except Self
Given an array nums of n integers where n > 1,  return an array output such that output[i] is equal to the product of all the elements of nums except nums[i].

Example:

Input:  [1,2,3,4]
Output: [24,12,8,6]
Constraint: It's guaranteed that the product of the elements of any prefix or suffix of the array (including the whole array) fits in a 32 bit integer.

Note: Please solve it without division and in O(n).

Follow up:
Could you solve it with constant space complexity? (The output array does not count as extra space for the purpose of space complexity analysis.)
 */
public class ProductExceptSelf {

	public static void main(String[] args) {
		ProductExceptSelf exceptSelf = new ProductExceptSelf();
		exceptSelf.productExceptSelf(new int[] { 1, 2, 3, 4 });
	}

	public int[] productExceptSelf(int[] nums) {
		int[] result = new int[nums.length];
		// logic is maintain prefix product array and suffix product array

		// Initialize result array with one
		for (int i = 0; i < nums.length; i++) {
			result[i] = 1;
		}
		int temp = 1;
		// maintain prefix product result[i] represent product of i-1 and temp represent
		// product of length-1;
		for (int i = 0; i < nums.length; i++) {
			result[i] = temp;
			temp *= nums[i];
		}
		// maintain suffix product 
		//idea is simple multiply i-1 *i+1 we will get product of ith index.
		//It means we need to maintain suffix product in result[i] represents of prefix i-1 and suffix i+1
		
		//temp store result of product upto n-1th index
		//1,1,2,6 --24
		//re-initialize temp with 1 and now temp will represent product of i+1; 
		temp=1;
		for (int i = nums.length-1; i >=0; i--) {
		
			result[i]*=temp;
			temp*=nums[i];
		}
		return result;
	}
}
