package com.decode.april.week3;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;

/*
 * Given a 2d grid map of '1's (land) and '0's (water), count the number of islands. An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.

Example 1:

Input:
11110
11010
11000
00000

Output: 1
Example 2:

Input:
11000
11000
00100
00011

Output: 3

 */
public class NumberOfIslands {

	private Set<Grid> seen = new HashSet<>();// This will be required for checking path is already seen or not

	private int[][] shifts = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };// up,down,left,right

	public static void main(String[] args) {

		char[][] grid = new char[4][5];
		// first row
		grid[0][0] = '1';
		grid[0][1] = '1';
		grid[0][2] = '0';
		grid[0][3] = '0';
		grid[0][4] = '0';
		// second row
		grid[1][0] = '1';
		grid[1][1] = '1';
		grid[1][2] = '0';
		grid[1][3] = '0';
		grid[1][4] = '0';
		// third row
		grid[2][0] = '0';
		grid[2][1] = '0';
		grid[2][2] = '1';
		grid[2][3] = '0';
		grid[2][4] = '0';
		// fourth row
		grid[3][0] = '0';
		grid[3][1] = '0';
		grid[3][2] = '0';
		grid[3][3] = '1';
		grid[3][4] = '1';
		NumberOfIslands numberOfIslands = new NumberOfIslands();
		System.out.println(numberOfIslands.numIslands(grid));
	}

	public int numIslands(char[][] grid) {
		int NO_OF_ISLANDS = 0;
		for (int row = 0; row < grid.length; row++) {
			for (int col = 0; col < grid[row].length; col++) {
				Grid gridClass = new Grid(row, col);
				if (!seen.contains(gridClass) && canTraverse(gridClass, grid)) {
					getNumberOfIslands(gridClass, grid);
					NO_OF_ISLANDS++;
				}
			}
		}
		return NO_OF_ISLANDS;
	}

	/*
	 * This method used for making marking the entries of given graph structure.
	 * 
	 */
	private void getNumberOfIslands(Grid gridClass, char[][] grid) {

		Stack<Grid> stack = new Stack<>();
		stack.push(gridClass);
		while (!stack.isEmpty()) {
			Grid grid2 = stack.pop();
			if (!seen.contains(grid2)) {
				// Move only in four position - up,down,left,right
				for (int i = 0; i < 4; i++) {
					Grid grid3 = new Grid(grid2.row + shifts[i][0], grid2.col + shifts[i][1]);
					if (canTraverse(grid3, grid)) {
						stack.add(grid3);

					}
				}
				// we have traversed this node
				seen.add(grid2);
			}
		}

	}

	private boolean canTraverse(Grid grid, char[][] gridArray) {

		return grid.row >= 0 && grid.row < gridArray.length && grid.col >= 0 && grid.col < gridArray[grid.row].length
				&& gridArray[grid.row][grid.col] == '1';
	}

	class Grid {
		int row;
		int col;

		public Grid(int row, int col) {
			this.row = row;
			this.col = col;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}

			if (o == null || getClass() != o.getClass()) {
				return false;
			}

			Grid that = (Grid) o;
			if (row != that.row || col != that.col) {
				return false;
			}

			return true;
		}

		@Override
		public int hashCode() {
			return Objects.hash(row, col);
		}
	}
}
