package com.decode.july;

public class HammingDistance {
	public int hammingDistance(int x, int y) {

		x = x ^ y;
		// find count set bits
		int count = 0;
		while (x != 0) {
			x = x & (x - 1);
			count++;
		}

		return count;
	}
}
