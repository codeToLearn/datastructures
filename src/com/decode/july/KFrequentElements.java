package com.decode.july;

import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Queue;

public class KFrequentElements {
	public int[] topKFrequent(int[] nums, int k) {
		int result[] = new int[k];
		HashMap<Integer, Integer> map = new HashMap<>();
		for (int num : nums) {
			map.put(num, map.getOrDefault(num, 0) + 1);
		}
		Queue<Integer> queue = new PriorityQueue<Integer>(k, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return map.get(o2).compareTo(map.get(o1));
			}

		});
		for (int key : map.keySet()) {
			queue.offer(key);
		}
		for(int i = 0 ; i<k ; i++) {
			result[i] = queue.poll();
		}
		return result;
	}
}
