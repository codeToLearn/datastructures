package com.decode.july;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

public class CourseScheduleII {
	public static void main(String[] args) {
		int[][] prerequisites = { { 0, 1 } };
		CourseScheduleII courseScheduleII = new CourseScheduleII();
		courseScheduleII.findOrder(2, prerequisites);
	}

	public int[] findOrder(int numCourses, int[][] prerequisites) {
		return dfsTopologicalSort(numCourses, prerequisites);
	}

	private int[] dfsTopologicalSort(int numCopies, int[][] prerequistites) {

		// stores the result
		// This will store result upto base
		Stack<Integer> stack = new Stack<>();
		boolean visited[] = new boolean[numCopies];
		// check all explored vertex
		boolean explored[] = new boolean[numCopies];
		int[] result = new int[numCopies];
		Map<Integer, List<Integer>> graph = new HashMap<Integer, List<Integer>>();

		for (int i = 0; i < numCopies; i++) {
			graph.put(i, new ArrayList<>());
		}
		/**
		 * This graph signifies [1,0] - > 0 should be completed before 1
		 */
		for (int i = 0; i < prerequistites.length; i++) {

			graph.get(prerequistites[i][0]).add(prerequistites[i][1]);
		}
		/**
		 * One by one process all the nodes
		 */
		for (int node = 0; node < numCopies; node++) {
			if (!visited[node]) {
				processNodes(node, graph, visited, explored, stack);
			}
		}
		if (stack.size() != numCopies) {
			return new int[] {};
		}
		while (!stack.isEmpty()) {
			result[--numCopies] = stack.pop();
		}
		return result;
	}

	private void processNodes(int node, Map<Integer, List<Integer>> adj, boolean[] visited, boolean[] explored,
			Stack<Integer> stack) {
		// mark node visited
		visited[node] = true;
		for (int neighbours : adj.get(node)) {
			if (!visited[neighbours]) {
				processNodes(neighbours, adj, visited, explored, stack);
			} else if (!explored[neighbours]) {
				// check here loops exists or not
				return;
			}
		}
		explored[node] = true;
		stack.push(node);
	}

	/**
	 * Approach 2: Using Node Indegree Intuition
	 * 
	 * This approach is much easier to think about intuitively as will be clear from
	 * the following point/fact about topological ordering.
	 * 
	 * The first node in the topological ordering will be the node that doesn't have
	 * any incoming edges. Essentially, any node that has an in-degree of 0 can
	 * start the topologically sorted order. If there are multiple such nodes, their
	 * relative order doesn't matter and they can appear in any order.
	 * 
	 * Our current algorithm is based on this idea. We first process all the
	 * nodes/course with 0 in-degree implying no prerequisite courses required. If
	 * we remove all these courses from the graph, along with their outgoing edges,
	 * we can find out the courses/nodes that should be processed next. These would
	 * again be the nodes with 0 in-degree. We can continuously do this until all
	 * the courses have been accounted for.
	 * 
	 * Algorithm
	 * 
	 * Initialize a queue, Q to keep a track of all the nodes in the graph with 0
	 * in-degree. Iterate over all the edges in the input and create an adjacency
	 * list and also a map of node v/s in-degree. Add all the nodes with 0 in-degree
	 * to Q. The following steps are to be done until the Q becomes empty. Pop a
	 * node from the Q. Let's call this node, N. For all the neighbors of this node,
	 * N, reduce their in-degree by 1. If any of the nodes' in-degree reaches 0, add
	 * it to the Q. Add the node N to the list maintaining topologically sorted
	 * order. Continue from step 4.1.
	 * 
	 */
	private int[] bfsTopologicalSort(int numCopies, int[][] prerequistites) {

		int inDegrees[] = new int[numCopies];
		int result[] = new int[numCopies];
		Map<Integer, List<Integer>> graph = new HashMap<Integer, List<Integer>>();

		for (int i = 0; i < numCopies; i++) {
			graph.put(i, new ArrayList<>());
		}
		/**
		 * This graph signifies [1,0] - > 0 should be completed before 1
		 */
		for (int i = 0; i < prerequistites.length; i++) {
			graph.get(prerequistites[i][0]).add(prerequistites[i][1]);
			inDegrees[prerequistites[i][1]]++;
		}
		// find all indegress
		Queue<Integer> queue = new LinkedList<Integer>();
		for (int i = 0; i < inDegrees.length; i++) {
			if (inDegrees[i] == 0) {
				queue.offer(i);
			}

		}
		int order = 0;
		while (!queue.isEmpty()) {
			int node = queue.poll();
			result[order++] = node;
			for (int num : graph.get(node)) {
				if (--inDegrees[num] == 0) {
					queue.offer(num);
				}
			}
			numCopies--;
		}
		if (numCopies != 0) {
			return new int[] {};
		}
		return result;
	}

}
