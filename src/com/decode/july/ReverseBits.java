package com.decode.july;

import java.util.ArrayList;
import java.util.List;

public class ReverseBits {
	public int reverseBits(int n) {
		int res = 0;
		List<Integer> rev_bits = new ArrayList<Integer>(32);
		int times = 32;
		while (times > 0) {
			int t = n & 1;// to get the last digit
			rev_bits.add(t); // as we get last digit each time, our list is made in reverse order
			n = n >> 1; // remove that last digit
			times--;
		}
		// rebuild number
		for (int i = 0; i < rev_bits.size(); i++) {
			res = res << 1; //set next bit
			res = res | rev_bits.get(i); // get last digit;
			
		}
		return res;
	}
}
