package com.decode.july;

public class UglyNumbers {
	public static void main(String[] args) {
		UglyNumbers numbers = new UglyNumbers();
		int number = numbers.nthUglyNumber(10);
		System.out.println(number);
		number = numbers.nthUglyNumberDP(number);
		System.out.println(number);
	}

	public int nthUglyNumber(int n) {
		if (n <= 1) {
			return n;
		}
		int count = 1;
		int num = 2;
		if (n > 1) {
			while (count != n) {
				if (isUgly(num)) {
					count++;
				}
				if (count == n) {
					return num;
				}
				num++;
			}
		}
		return num;
	}

	private boolean isUgly(int num) {
		num = maxDiv(num, 2);
		num = maxDiv(num, 3);
		num = maxDiv(num, 5);
		return num == 1;
	}

	private int maxDiv(int num, int div) {
		if (num % div == 0) {
			return maxDiv(num / div, div);
		}
		return num;
	}

	public int nthUglyNumberDP(int n) {
		int array[] = new int[n];
		// count how many times 2 comes , 3 comes and 5 comes in factorization
		array[0] = 1;
		int twoCount = 0;
		int threeCount = 0;
		int fiveCount = 0;
		for (int i = 1; i < n; i++) {
			int nextTwo = array[twoCount] * 2;
			int nextThree = array[threeCount] * 3;
			int nextFive = array[fiveCount] * 5;
			int min = Math.min(nextTwo, Math.min(nextThree, nextFive));
			if (min == nextTwo) {
				twoCount++;
			}
			if (min == nextThree) {
				threeCount++;

			}
			if (min == nextFive) {
				fiveCount++;
			}
			array[i] = min;
		}
		return array[n - 1];
	}
}
