package com.decode.july;

public class NumberOfIslands {
	public static void main(String[] args) {
		NumberOfIslands islands= new NumberOfIslands();
		int[][]grid = {{0,1,0,0},{1,1,1,0},{0,1,0,0},{1,1,0,0}};
		islands.islandPerimeter(grid);
	}

	public int islandPerimeter(int[][] grid) {

		int moves[][] = { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 0, 1 } };
		int p = 0;
		for (int row = 0; row < grid.length; row++) {
			int permiter = 0;
			for (int col = 0; col < grid[0].length; col++) {

				if (grid[row][col] == 1) {
					permiter = 4;
					for (int shift = 0; shift < moves.length; shift++) {
						if (isValid(row + moves[shift][0], col + moves[shift][1], grid)) {
							permiter--;
						}
					}
					p += permiter;
				}
				
			}
		}
		return p;
	}

	private boolean isValid(int row, int col, int[][] grid) {
		return row >= 0 && col >= 0 && row < grid.length && col < grid[0].length && grid[row][col] == 1;
	}
}
