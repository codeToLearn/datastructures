package com.decode.july;

import java.util.ArrayList;
import java.util.List;

public class Subsets {
	List<List<Integer>> output = new ArrayList();
	int len;
	int n;

	public List<List<Integer>> subsets(int[] nums) {
		n = nums.length;
		output.add(new ArrayList());
		for (len = 1; len <= n; len++) {

			backtrack(0, new ArrayList(), nums);
		}
		return output;
	}

	private void backtrack(int num, List<Integer> list, int[] nums) {

		if (list.size() == len) {
			// use that combination
			output.add(new ArrayList(list));
			return;
		}
		for (int i = num; i < n; i++) {
			// add i into the current combination
			list.add(nums[i]);
			// use next integers to complete the combination
			backtrack(i + 1, list, nums);
			// backtrack
			list.remove(list.size() - 1);

		}
	}
}
