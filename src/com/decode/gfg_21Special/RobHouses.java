package com.decode.gfg_21Special;

/**
 * 
 * @author Sachin Jindal
 *
 *
 *         In Byteland there is a circular colony of N houses . Each house in
 *         the colony has some money. The value of money can either be positive
 *         or negative. Find the maximum amount of money you can have after
 *         robbing any number of contiguous houses.<br>
 *         Note: Robbing a house having negative money will reduce your money by
 *         that amount. <br>
 *         Input: First line of input contains a single integer T which denotes
 *         the number of test cases. <br>
 *         First line of each test case contains a single integer N which
 *         denotes the total number of houses.<br>
 *         Second line of each test case contains N space separated integers
 *         denoting money in the houses.<br>
 *         Output: For each test case print the maximum money by robbing the
 *         consecutive houses. <br>
 *         User Task: The task is to complete the function maxMoney() which
 *         returns the maximum money.<br>
 *         Constraints: 1 <= T <= 100 1 <= N <= 106 -106 <= Arr[i] <= 106
 *         Example: Input: <br>
 *         3 <br>
 *         7 <br>
 *         8 -8 9 -9 10 -11 12<br>
 *         8<br>
 *         10 -3 -4 7 6 5 -4 -1<br>
 *         8 <br>
 *         -1 40 -14 7 6 5 -4 -1 <br>
 *         Output: <br>
 *         22 <br>
 *         23 <br>
 *         52 <br>
 *         Explanation: <br>
 *         Testcase 1: Starting from last house of the colony, robbed 12 units
 *         and moving in circular fashion, we can rob 8, -8, 9, -9, 10, which
 *         gives maximum robbed money as 22.
 * 
 * 
 */
public class RobHouses {
	public static void main(String[] args) {
		RobHouses houses = new RobHouses();
		houses.maxSubarraySumCircular(new int[] { -1, 40, -14, 7, 6, 5, -4, -1 });
	}

	public int maxSubarraySumCircular(int[] A) {
		/**
		 * The idea of using the Kadane algorithm find first index and last index of a
		 * subarray
		 */
		int max = A[0];
		int maxsofar = A[0];
		int startIndex = 0;
		int endIndex = 0;
		for (int i = 1; i < A.length; i++) {
			maxsofar = maxsofar + A[i];
			if (maxsofar < A[i]) {
				maxsofar = A[i];
				startIndex = i;
			}
			if (maxsofar > max) {
				max = maxsofar;
				endIndex = i;
			}
		}

		return max;
	}
}
