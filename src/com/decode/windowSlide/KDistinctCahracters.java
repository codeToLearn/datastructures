package com.decode.windowSlide;

import java.util.HashSet;
import java.util.Set;

public class KDistinctCahracters {
	public static void main(String[] args) {
		KDistinctCahracters cahracters = new KDistinctCahracters();
		cahracters.getCountOfKDistinctCharacters("aabcd", 3);
	}

	public int getCountOfKDistinctCharacters(String s, int k) {

		int count = 0;
		/**
		 * This problem can be solved using window sliding algorithm.
		 */
		int start = 0;
		int end = 0;
		Set<Character> characters = new HashSet<>();
		while (end < s.length()) {
			if (!characters.contains(s.charAt(end))) {
				if (end - start + 1 == k) {
					characters.remove(s.charAt(start));
					start++;
					count++;
				}
				characters.add(s.charAt(end));
				end++;

			} else {
				characters.remove(s.charAt(start));
				start++;
			}

		}
		return count;
	}
}
