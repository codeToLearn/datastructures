package com.decode.windowSlide;

import java.util.HashSet;

public class LongestNonRepeating {
	public static void main(String[] args) {
		String t = "abcabcbb";
		LongestNonRepeating longestNonRepeating = new LongestNonRepeating();
		longestNonRepeating.lengthOfLongestSubstring(t);
	}

	public int lengthOfLongestSubstring(String s) {
		if (s == null || s.isEmpty()) {
			return 0;
		}
		if (s.length() == 1) {
			return 1;
		}
		int len = 0;
		int start = 0;
		int end = 0;
		HashSet<Character> set = new HashSet<>();
		while (end < s.length()) {
			if (!set.contains(s.charAt(end))) {
				set.add(s.charAt(end));
				end++;
				len = Math.max(len, end - start);
				continue;
			}

			set.remove(s.charAt(start));
			start++;
		}
		return len;
	}
}
