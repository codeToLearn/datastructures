package com.concurrent.BlockingQueue;

import java.util.concurrent.BlockingQueue;

class Producer implements Runnable{
	 BlockingQueue<String> queue;
	 
	 public Producer(BlockingQueue<String> queue) {
		// TODO Auto-generated constructor stub
		 this.queue=queue;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(5000);
			queue.put("First Element");
			Thread.sleep(1000);
			queue.put("Second Element");
			Thread.sleep(1000);
			queue.put("Third Element");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

class Consumer implements Runnable{
 BlockingQueue<String> queue;
	 
	 public Consumer(BlockingQueue<String> queue) {
		// TODO Auto-generated constructor stub
		 this.queue=queue;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			System.out.println(queue.take());
//			System.out.println(queue.poll());
			System.out.println(queue.take());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
public class ArrayBlockingQueue {
	public static void main(String[] args) {
		BlockingQueue<String> queue=new java.util.concurrent.ArrayBlockingQueue<String>(1);
		Thread t1=new Thread(new Producer(queue));
		Thread t2=new Thread(new Consumer(queue));
		t1.start();
		t2.start();
	}


}
