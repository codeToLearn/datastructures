package com.concurrent.BlockingQueue;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class DelayQueueSample implements Delayed{
	private long time;
	
	public DelayQueueSample(long time) {
		// TODO Auto-generated constructor stub
		this.time=System.currentTimeMillis()+time;
	}
	
	@Override
	public int compareTo(Delayed o) {
		// TODO Auto-generated method stub
		if(this.time>((DelayQueueSample)o).time)
			return 1;
		else if(this.time<((DelayQueueSample)o).time)
			return -1;
		return 0;
	}

	@Override
	public long getDelay(TimeUnit unit) {
		long diff=time-System.currentTimeMillis();
		return diff;
	}

}
