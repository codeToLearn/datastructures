package com.concurrent.BlockingQueue;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TransferQueue;

class LinkedTransferQueueProducer implements Runnable {

	protected TransferQueue<String> transferQueue;
	final Random random = new Random();

	public LinkedTransferQueueProducer(TransferQueue<String> queue) {
		this.transferQueue = queue;
	}

	@Override
	public void run() {
		while (true) {
			try {
				String data = UUID.randomUUID().toString();
				System.out.println("Put: " + data);
				transferQueue.transfer(data);
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
 class LinkedTransferQueueConsumer implements Runnable {

	protected TransferQueue<String> transferQueue;

	public LinkedTransferQueueConsumer(TransferQueue<String> queue) {
		this.transferQueue = queue;
	}

	@Override
	public void run() {
		while (true) {
			try {
				String data = transferQueue.take();
				System.out.println(Thread.currentThread().getName()
						+ " take(): " + data);
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}


public class LinkedTransferQueueSample {
	public static void main(String[] args) {
		final TransferQueue<String> transferQueue = new LinkedTransferQueue<String>();

		LinkedTransferQueueProducer queueProducer = new LinkedTransferQueueProducer(
				transferQueue);
		new Thread(queueProducer).start();

		LinkedTransferQueueConsumer queueConsumer1 = new LinkedTransferQueueConsumer(
				transferQueue);
		new Thread(queueConsumer1).start();

		LinkedTransferQueueConsumer queueConsumer2 = new LinkedTransferQueueConsumer(
				transferQueue);
		new Thread(queueConsumer2).start();

	}
}
