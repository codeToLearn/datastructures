package com.concurrent.thread;

public class ProducerConsumer {
	public static Integer count=0;
	public static void main(String[] args) {
		
		ProducerConsumer sol=new ProducerConsumer();
		
		Thread t1=new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true)
				synchronized ("") {
					try {
						Thread.sleep(1000);
						if((count+2)%3!=0)
							"".wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName()+" "+count++);
					"".notify();
				}
			}
		});

		Thread t2=new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true)
				synchronized ("") {

					try {
						Thread.sleep(500);
						if((count+1)%3!=0)
							"".wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName()+" "+count++);
					"".notify();
				}
			}
		});

		Thread t3=new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(true)
				synchronized ("") {
					try {
						System.out.println("Before");
						Thread.sleep(1000);
						System.out.println("After");
						if(count%3!=0)
							"".wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName()+" "+count++);
					"".notify();
				}
			}
		});
		
		
		t3.start();
		t2.start();
		t1.start();
		
	}
}
