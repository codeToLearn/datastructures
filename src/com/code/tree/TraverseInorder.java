package com.code.tree;

import java.util.ArrayList;
import java.util.List;

public class TraverseInorder {
	public List<Integer> inorderTraversal(TreeNode root) {
		List<Integer> inorder=new ArrayList<Integer>();
		inorder(inorder,root);
		return inorder;
	}

	private void inorder(List<Integer> inorder, TreeNode root) {
		// TODO Auto-generated method stub
		if(root==null)
			return ;
		inorder(inorder, root.left);
		inorder.add(root.val);
		inorder(inorder, root.right);
	}
}
