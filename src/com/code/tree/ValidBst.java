package com.code.tree;
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode(int x) { val = x; }
}
public class ValidBst {
	public static void main(String[] args) {
		TreeNode root=new TreeNode(2147483647);
		System.out.println(isValidBST(root));
	}

	public static boolean isValidBST(TreeNode root) {
		if(root==null)
			return true;
		if((root.left!=null && getMaximum(root.left)>=root.val) || (root.right!=null && getMinimum(root.right)<=root.val))
			return false;
		return isValidBST(root.left) && isValidBST(root.right);
	}
	
	public static int getMinimum(TreeNode node) {
		if(node==null)
			return Integer.MAX_VALUE;
		int min=Math.min(getMinimum(node.right), getMinimum(node.left));
		return Math.min(min, node.val);
	}
	
	public static int getMaximum(TreeNode node) {
		if(node==null)
			return Integer.MIN_VALUE;
		int min=Math.max(getMaximum(node.right), getMaximum(node.left));
		return Math.max(min, node.val);
	}
}

