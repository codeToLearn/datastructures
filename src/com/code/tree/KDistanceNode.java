package com.code.tree;

public class KDistanceNode {
	public static void main(String[] args) {
		TreeNode root=new TreeNode(1);
		root.left=new TreeNode(2);
		root.right=new TreeNode(3);
		root.right.left=new TreeNode(4);
		root.right.right=new TreeNode(5);
		
		distanceK(root, root.right, 1, false);
	}

	public static int distanceK(TreeNode root, TreeNode target, int K,boolean found) {
		if(root==null || K<0 )
			return -1;
		if(found)
			K=K-1;
		if(root.val==target.val) {
			found=true;
		}
		if(K==0)
			System.out.println(root.val);
		int left=distanceK(root.left, target, K,found);
		int right=distanceK(root.right, target, K,found);
//		if()
		return K-left;
	}
}
