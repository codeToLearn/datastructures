package com.code.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LevelOrderTraverse {
	public static void main(String[] args) {


	}

	public List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> list=new ArrayList<List<Integer>>();
		Queue<TreeNode> queue=new LinkedList<TreeNode>();
		if(root!=null)
		queue.add(root);
		queue.add(null);
		List<Integer> level=new ArrayList<Integer>();
		while (!queue.isEmpty()) {
			TreeNode node=queue.poll();
			if(node==null) {
				if(level.size()>0)
					list.add(level);
				level=new ArrayList<Integer>();
				if(!queue.isEmpty())
				queue.add(null);
			}else {
				level.add(node.val);
				if(node.left!=null)
					queue.add(node.left);
				if(node.right!=null)
					queue.add(node.right);
			}
		}
		return list;
	}
}
