package com.code.tree;

public class SymmetricTree {
public static void main(String[] args) {
	TreeNode node=new TreeNode(1);
	node.left=new TreeNode(2);
	node.right=new TreeNode(2);
	node.left.left=new TreeNode(3);
//	node.left.right=new TreeNode(4);
	node.right.left=new TreeNode(3);
//	node.right.right=new TreeNode(4);
	System.out.println(isSymmetric(node));
}

public static boolean isSymmetric(TreeNode root) {
	 if(root==null)
         return true;
	return isSymmetric(root.left,root.right);
}

public static boolean isSymmetric(TreeNode left,TreeNode right) {
    if(left==null && right==null)
    	return true;
    if(left==null || right==null)
    	return false;
    if(left.val==right.val) {
    	return isSymmetric(left.left,right.right) && isSymmetric(left.right,right.left);
    }
    return false;
}
}
