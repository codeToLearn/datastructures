package com.code.tree;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class WidthBTree {
	public static void main(String[] args) {
		TreeNode root=new TreeNode(1);
		root.left=new TreeNode(1);
		root.right=new TreeNode(1);;
		root.right.right=new TreeNode(1);;
		root.right.right.left=new TreeNode(1);;
		root.left.left=new TreeNode(1);
		root.left.left.left=new TreeNode(1);
		System.out.println(new WidthBTree().widthOfBinaryTree(root));
	}

	public int widthOfBinaryTree(TreeNode root) {
        return dfs(root, 0, 1, new ArrayList<Integer>(), new ArrayList<Integer>());
    }
    
	//copied solution 
	//it is maintaining two lists maintaining the order of first node and last node at a height
    public int dfs(TreeNode root, int level, int order, List<Integer> start, List<Integer> end){
        if(root == null)return 0;
        if(start.size() == level){
            start.add(order); end.add(order);
        }
        else end.set(level, order);
        int cur = end.get(level) - start.get(level) + 1;
        int left = dfs(root.left, level + 1, 2*order, start, end);
        int right = dfs(root.right, level + 1, 2*order + 1, start, end);
        return Math.max(cur, Math.max(left, right));
    }
}
