package com.code.tree;

public class ArrayToBst {
	public TreeNode sortedArrayToBST(int[] nums) {
		return breakArray(nums, 0,nums.length-1,null);
	}
	
	public TreeNode breakArray(int[] nums,int i,int j,TreeNode root) {
		if(i>j)
			return null;
		int mid=(i+j)/2;
		root=new TreeNode(nums[mid]);
		root.left=breakArray(nums, i, mid-1, root.left);
		root.right=breakArray(nums, mid+1, j, root.right);
		return root;
	}

	public static void main(String[] args) {
		int[] nums=new int[] {0,1,2,3,4,5};
		TreeNode root=new ArrayToBst().sortedArrayToBST(nums);
		System.out.println(root);
	}
}
