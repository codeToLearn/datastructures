package com.code.tree;

import java.util.ArrayList;
import java.util.List;

public class DiagonalTraversal {

	public static void main(String[] args) {
		List<List<Integer>> list=new ArrayList<List<Integer>>();
		TreeNode root=new TreeNode(1);
		root.left=new TreeNode(2);
		root.right=new TreeNode(3);
		root.left.left=new TreeNode(4);
		root.right.left=new TreeNode(5);
		root.right.right=new TreeNode(6);
		if(root==null)
			return;
		List<Integer> l1=new ArrayList<Integer>();
		l1.add(root.val);
		list.add(l1);
		list=new DiagonalTraversal().getDiagonalTraversal(root, list,0);

		//print list
		if(list!=null && list.size()>0) {
			for(List<Integer> l:list) {
				for(Integer i:l) {
					System.out.print(i+" ");
				}
				System.out.println();
			}
		}
	}

	public List<List<Integer>> getDiagonalTraversal(TreeNode root,List<List<Integer>> list,int index){
		if(root.right!=null) {
			list.get(index).add(root.right.val);
			list=getDiagonalTraversal(root.right, list,index);
		}
		if(root.left!=null) {
			List<Integer> l1;
			if(list.size()>index+1)
			{
				l1=list.get(index+1);
				l1.add(root.left.val);
			}
			else
			{
				l1=new ArrayList<Integer>();
				l1.add(root.left.val);
				list.add(l1);
			}
			list=getDiagonalTraversal(root.left, list,index+1);
		}
		return list;
	}
}
