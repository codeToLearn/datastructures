package com.code.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LowestCommonAncestor {
	public static void main(String[] args) {

	}

//	for BTree
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		int left=p.val<q.val?p.val:q.val;
		int right=p.val>q.val?p.val:q.val;
		Queue<TreeNode> queue=new LinkedList<TreeNode>();
		if(root!=null)
		queue.add(root);
		while (!queue.isEmpty()) {
			
		}
		return null;
	}
	
	
	//this will work for BST not BTree
	public TreeNode lowestCommonAncestorBST(TreeNode root, TreeNode p, TreeNode q) {
		int left=p.val<q.val?p.val:q.val;
		int right=p.val>q.val?p.val:q.val;
		Queue<TreeNode> queue=new LinkedList<TreeNode>();
		if(root!=null)
		queue.add(root);
		while (!queue.isEmpty()) {
			TreeNode node=queue.poll();
			if(left==node.val || right==node.val || (left<node.val && right>node.val))
				return node;
			if(node.left!=null)
				queue.add(node.left);
			if(node.right!=null)
				queue.add(node.right);
		}
		return null;
	}
}
