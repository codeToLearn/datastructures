package com.code.tree;

public class AddRowTree {

	public static void main(String[] args) {

	}
	public TreeNode addOneRow(TreeNode root, int v, int d) {
		if(d==1){
			TreeNode node=new TreeNode(v);
			node.left=root;
			return node;
		}
		return addRowRecursive(root,1,v,d);
	}

	public TreeNode addRowRecursive(TreeNode root ,int level, int v , int d){
		if(root==null)
			return root;
		if(level==d-1){
			TreeNode leftN=new TreeNode(v);
			TreeNode rightN=new TreeNode(v);
			leftN.left=root.left;
			rightN.right=root.right;
			root.left=leftN;root.right=rightN;
			return root;
		}
		root.left=addRowRecursive(root.left,level+1,v,d);
		root.right=addRowRecursive(root.right,level+1,v,d);
		return root;
	}
}
