package com.code.tree;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

class Node{
	 int data;
	 Node left;
	 Node right;
}

public class PreOrder {
	public static void preOrder(Node root) {
		Stack<Node> queue=new Stack<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			Node node=queue.pop();
			if(node.right!=null)
				queue.add(node.right);
			if(node.left!=null)
				queue.add(node.left);
			System.out.print(node.data+" ");
			
		}
	}
	public static void main(String[] args) {
		Node root=new Node();
		root.data=1;
		Node n1=new Node();
		n1.data=2;
//		n1.left=new Node();
//		n1.left.data=3;
		n1.right=new Node();
		n1.right.data=5;
		n1.right.left=new Node();
		n1.right.left.data=3;
		n1.right.right=new Node();
		n1.right.right.data=6;
		n1.right.left.right=new Node();
		n1.right.left.right.data=4;
		root.left=n1;
		preOrder(root);
	}
}
