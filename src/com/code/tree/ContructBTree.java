package com.code.tree;

import java.util.*;

public class ContructBTree {
	public static void main(String[] args) {
		System.out.println(new ContructBTree().buildTree(new int[] {3,9,20,15,7}, new int[] {9,3,15,20,7}));
	}
	public TreeNode buildTree(int[] preorder, int[] inorder) {
		Set<Integer> set=new HashSet();
		for(int i=0;i<preorder.length;i++)
			set.add(preorder[i]);
		return getSubTree(set,preorder,inorder);
	}

	public TreeNode getSubTree(Set<Integer> set,int[] preorder,int[] inorder){
		TreeNode root=null;
		if(set.size()==0)
			return root;
		for(int i=0;i<preorder.length;i++)
			if(set.contains(preorder[i])) {
				root=new TreeNode(preorder[i]);
				break;
			}
		Set<Integer> left=new HashSet();
		Set<Integer> right=new HashSet();
		boolean flag=false;
		for(int i=0;i<inorder.length;i++){
			if(!set.contains(inorder[i]))
				continue;
			if(inorder[i]==root.val)
				flag=true;
			else if(flag)
				right.add(inorder[i]);
			else left.add(inorder[i]);
		}
		root.left=getSubTree(left,preorder,inorder);
		root.right=getSubTree(right,preorder,inorder);
		return root;
	}
}
