package com.code.misc;

public class RainWaterTrap {
	public static int trap(int[] height) {
		int left[]=new int[height.length];
		int right[]=new int[height.length];
		int max=0;
		for(int i=1;i<height.length;i++){
			max=Math.max(height[i-1],max);
			left[i]=max;
		}

		max=0;
		for(int i=height.length-2;i>=0;i--){
			max=Math.max(height[i+1],max);
			right[i]=max;
		}

		int volume=0;
		for(int i=0;i<height.length;i++){
			int level=Math.min(left[i],right[i]);
			if(level-height[i]>0)
				volume+=(level-height[i]);
		}
		return volume;
	}
	
	public static void main(String[] args) {
		System.out.println(trap(new int[]{0,1,0,2,1,0,1,3}));
	}
}
