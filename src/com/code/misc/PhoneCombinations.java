package com.code.misc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhoneCombinations {
public static void main(String[] args) {
	List<String> list=new PhoneCombinations().letterCombinations("23");
	for(String s:list) {
		System.out.println(s);
	}
}

public List<String> letterCombinations(String digits) {
    List<String> list=new ArrayList<String>();
    char[] arr=digits.toCharArray();
    if(arr.length==0)
    	return list;
    Map<String, List<String>> keyPadMap=getKeyPadMap();
    addLetterCombinations(list,arr,0,"",keyPadMap);
    return list;
}

private void addLetterCombinations(List<String> list, char[] arr, int i, String string,
		Map<String, List<String>> keyPadMap) {
	// TODO Auto-generated method stub
	if(i>=arr.length) {
		list.add(string);
		return;
	}
	List<String> strList=keyPadMap.get(String.valueOf(arr[i]));
	for(String alpha:strList) {
		addLetterCombinations(list, arr, i+1, string+alpha, keyPadMap);
	}
}

private Map<String, List<String>> getKeyPadMap() {
	Map<String, List<String>> keyPadMap=new HashMap<String, List<String>>();
	List<String> list2=new ArrayList<String>();
	list2.add("a");list2.add("b");list2.add("c");
	keyPadMap.put("2", list2);
	List<String> list3=new ArrayList<String>();
	list3.add("d");list3.add("e");list3.add("f");
	keyPadMap.put("3", list3);
	List<String> list4=new ArrayList<String>();
	list4.add("g");list4.add("h");list4.add("i");
	keyPadMap.put("4", list4);
	List<String> list5=new ArrayList<String>();
	list5.add("j");list5.add("k");list5.add("l");
	keyPadMap.put("5", list5);
	List<String> list6=new ArrayList<String>();
	list6.add("m");list6.add("n");list6.add("o");
	keyPadMap.put("6", list6);
	List<String> list7=new ArrayList<String>();
	list7.add("p");list7.add("q");list7.add("r");list7.add("s");
	keyPadMap.put("7", list7);
	List<String> list8=new ArrayList<String>();
	list8.add("t");list8.add("u");list8.add("v");
	keyPadMap.put("8", list8);
	List<String> list9=new ArrayList<String>();
	list9.add("w");list9.add("x");list9.add("y");list9.add("z");
	keyPadMap.put("9", list9);
	return keyPadMap;
}

}
