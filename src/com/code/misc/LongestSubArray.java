package com.code.misc;

public class LongestSubArray {

	public static void main(String[] args) {
		System.out.println(longestSubArrayN(new int[] {4,2,2,2,4,4,2,2}, 0));
	}
	
	public static int longestSubArray(int[] nums,int limit) {
		 int max=1;
			int n=nums.length;
			for(int i=0;i<n;i++) {
				int high=nums[i],low=nums[i];
				for(int j=i;j<n;j++) {
					if(nums[j]<low) low=nums[j];
					if(nums[j]>high) high=nums[j];
					if(high-low<=limit)
						max=Math.max(max, j-i+1);
	                else break;
				}
			}
			return max;
	}
	
	public static int longestSubArrayN(int[] nums,int limit) {
		int j=0,max=1;
		int high=nums[j],low=nums[j];
		for(int i=1;i<nums.length;i++) {
			if(nums[i]<low) low=nums[i];
			if(nums[i]>high) high=nums[i];
			if(high-low<=limit)
				max=Math.max(max, i-j+1);
            else {
            	j++;
            	high=nums[j];
            	low=nums[j];
            	i=j;
            }
		}
		return max;
	}
}
