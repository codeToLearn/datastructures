package com.code.misc;

public class DistanceOf1s {
	 public boolean kLengthApart(int[] nums, int k) {
	        boolean flag=false;
	        int min=nums.length+1;
	        int count=0;
	        for(int n:nums){
	            if(n==1){
	                if(flag && count<k)
	                    return false;
	                flag=true;
	                count=0;
	            }else count++;
	        }
	        return true;
	    }
}
