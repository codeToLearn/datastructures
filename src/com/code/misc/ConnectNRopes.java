package com.code.misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class ConnectNRopes {
	public static void main(String[] args) {
		System.out.println(getMinCost(new int[] {4,3,2,6}));
	}

	public static int getMinCost(int[] arr) {
		Queue<Integer> queue=new PriorityQueue<Integer>();
		for(int a:arr)
			queue.add(a);
		
		int ans=0;
		while (queue.size()>1) {
			int cost=queue.poll()+queue.poll();
			queue.add(cost);
			ans+=cost;
		}
		
		return ans;
	}
	
	/**
	 * Count of substrings of length K with exactly K distinct characters
	 * @return
	 */
	public static int countSubstrings(String s,int k) {
		char[] arr=s.toCharArray();
		int i=0;
		Map<Character, Integer> countMap=new HashMap<Character, Integer>();
		while(i<k) {
			addInMap(countMap, arr[i]);
			i++;
		}
		
		int count=0;
		if(countMap.size()==k)
			count++;
		
		while (i<arr.length) {
			if(countMap.get(arr[i-k])==1)
				countMap.remove(arr[i-k]);
			else countMap.put(arr[i-k], countMap.get(arr[i-k])-1);
			
			addInMap(countMap, arr[i]);
			if(countMap.size()==k)
				count++;
			i++;
		}
		return count;
	}
	
	private static void addInMap(Map<Character, Integer> countMap,char element) {
		if(countMap.containsKey(element))
			countMap.put(element, countMap.get(element)+1);
		else countMap.put(element, 1);
	}
}
