package com.code.misc;

public class MinHops {
	public static void main(String[] args) throws InterruptedException {
		System.out.println(minSteps(7));
		System.out.println(reachTarget(7));
	}

	public static int minSteps(int target){
		int sum=0;
		int i=1;
		while(sum<target){
			sum+=i;
			i++;
		}

		int diff=sum-target;
		if(diff%2==0){
			return i-1;
		}
		return i-1+(diff*2);
	}
	
	public static int reachTarget(int target) 
    { 
        // Handling negatives by symmetry 
        target = Math.abs(target); 
          
        // Keep moving while sum is smaller 
        // or difference is odd. 
        int sum = 0, step = 0; 
          
        while (sum < target || (sum - target) % 2
                                        != 0) { 
            step++; 
            sum += step; 
        } 
        return step; 
    } 
}
