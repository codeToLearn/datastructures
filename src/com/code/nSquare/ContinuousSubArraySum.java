package com.code.nSquare;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ContinuousSubArraySum {
	public static void main(String[] args) {
		int nums[]=new int[] {1,0};
		System.out.println(new ContinuousSubArraySum().checkSubarraySum(nums, 2));
	}
	
	//final solution of n2
	public boolean checkSubarraySum(int[] nums, int k) {
		for (int i = 0; i < nums.length - 1; i++) {
			if (nums[i] == 0 && nums[i + 1] == 0) return true;
		}
		if(k==0){
			return false;
		}
		int[] temp=new int[nums.length+1];
		temp[0]=0;
		for(int i=1;i<temp.length;i++) {
			temp[i]=nums[i-1]+temp[i-1];
		}
		for(int i=0;i<temp.length-1;i++) {
			for(int j=i+2;j<temp.length;j++) {
				if((temp[j]-temp[i])%k==0)
					return true;
			}
		}
		return false;
	}

	public boolean checkSubarraySum1(int[] nums, int k) {
		if(nums==null || nums.length<2)
			return false;
		for (int i = 0; i < nums.length - 1; i++) {
			if (nums[i] == 0 && nums[i + 1] == 0) return true;
		}
		if(k==0){
			return false;
		}
		return checkSum(nums,k,0,1,nums[0]);
	}

	//recursion code brings tle
	public boolean checkSum(int[] nums, int k,int i,int j,int sum) {
		if(sum%k==0 && i<j-1)
			return true;
		if(j>=nums.length || i>=j)
			return false;
		return checkSum(nums, k, i, j+1, sum+nums[j]) || checkSum(nums, k, i+1, j, sum-nums[i]);
	}
	
	
}
