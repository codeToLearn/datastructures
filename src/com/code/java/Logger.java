package com.code.java;
import java.util.Date;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class Logger {
	private static Map<String,Date> active=new ConcurrentHashMap();
	private static Queue<LogData> inactive=new LinkedBlockingQueue();

	class LogData{
		String processId;
		Date startTime;
		Date endTime;
		//getters and setters
		public String getProcessId() {
			return processId;
		}
		public void setProcessId(String processId) {
			this.processId = processId;
		}
		public Date getStartTime() {
			return startTime;
		}
		public void setStartTime(Date startTime) {
			this.startTime = startTime;
		}
		public Date getEndTime() {
			return endTime;
		}
		public void setEndTime(Date endTime) {
			this.endTime = endTime;
		}
		@Override
		public boolean equals(Object obj) {
			// TODO Auto-generated method stub
			return super.equals(obj);
		}
	}
	
	public void start(String processId) throws InterruptedException{
		final String lock="log"+processId;
		synchronized (lock) {
			if(active.containsKey(processId)) {
				System.out.println("Process is already working and started at "+active.get(processId));
				return;
			}		
			active.put(processId,new Date());
		}
	}

	public void end(String processId){
		String lock="log_"+processId;
		synchronized (lock) {
			if(!active.containsKey(processId))
			{
				System.out.print("No process working for Process Id"+processId);
				return;
			}
			LogData data=new LogData();
			data.setStartTime(active.get(processId));
			data.setProcessId(processId);
			data.setEndTime(new Date());
			active.remove(processId);
			inactive.add(data);
		}
	}

	public void poll(){

		if(inactive.isEmpty()){
			System.out.println("No active processes currently");
		}

		LogData data=inactive.poll();
		System.out.print("Last completed log with process Id"+data.getProcessId()+" and Start Time"+data.getStartTime() +" and End Time "+data.getEndTime());

		if(active.size()>0){
			System.out.print("Active Processes");
			for(String s:active.keySet()){
				System.out.print(s+"|");
			}	
		}
	}

	public static void main(String[] args) throws InterruptedException {
		Logger logger=new Logger();
//		logger.poll();
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					logger.start("1");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					logger.start("1");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
		
		Thread.currentThread().sleep(1000);
		
		for (String s:active.keySet()) {
			System.out.println(s+" - "+active.get(s));
		}
	}
}
