package com.code.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;


class State implements Comparable<State>{
	String stateName;
	int count;

	
	public State(String stateName, int count) {
		super();
		this.stateName = stateName;
		this.count = count;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return stateName+"->"+count;
	}


	@Override
	public int compareTo(State o) {
		// TODO Auto-generated method stub
		if(this.count==o.count)
			return this.stateName.compareTo(o.stateName);
		if(this.count>o.count)
			return -1;
		return 1;
	}
}
public class SortMapOnKeys{

     public static void main1(String []args){
    	 List<State> state=new ArrayList<State>();
    	 state.add(new State("Himachal",5));
    	 state.add(new State("UP",500));
    	 state.add(new State("Haryana",5));
    	 state.add(new State("Rajasthan",12));
    	 state.add(new State("MP",45));
    	 state.add(new State("Assam",1));
    	 System.out.println("before");
    	 for(State s:state) {
    		 System.out.println(s.toString());
    	 }
    	 Collections.sort(state);
    	 System.out.println("After");
    	 for(State s:state) {
    		 System.out.println(s.toString());
    	 }
     }
     
     public static void main(String[] args) {
    	 LinkedHashMap<String, Integer> stateMap=new LinkedHashMap<String, Integer>();
    	 stateMap.put("Himachal",5);
    	 stateMap.put("UP",500);
    	 stateMap.put("Haryana",5);
    	 stateMap.put("Rajasthan",12);
    	 stateMap.put("MP",45);
    	 stateMap.put("Assam",1);
    	 List<Entry<String, Integer>> list=stateMap.entrySet().stream().collect(Collectors.toList());
    	 Collections.sort(list,new Comparator<Entry<String, Integer>>() {
    		 @Override
    		 public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
    			 // TODO Auto-generated method stub
    			 if(o1.getValue()==o2.getValue())
    				 return o1.getKey().compareTo(o2.getKey());
    			 if(o1.getValue()>o2.getValue())
    				 return -1;
    			 return 1;
    		 }});

    	 for(Entry<String, Integer> entry:list) {
    		 System.out.println(entry.getKey()+"->"+entry.getValue());
    	 }
     }
}