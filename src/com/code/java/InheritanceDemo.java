package com.code.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

class A{
	public int a=10;
	protected void display() {
		System.out.println("Hello");
	}
}

class B extends A{
	public int a=100;
}
class C extends B implements Cloneable{
	protected void show() {
		display();
		System.out.println(a);
	}
}

public class InheritanceDemo{


	public static void main(String[] args) throws InterruptedException {
		Integer i = null;
		String[] a= {"12","ab"};
		System.out.println(a);
//		List<String> list=Arrays.asList(a);
		//		list.add("a");

		A aObj=new B();
		System.out.println(((B)aObj).a);

		 List<Integer> list=new ArrayList<Integer>();
		 
		 Runnable r=new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					list.add(1);
//					System.out.println("1");
					list.add(2);	
//					System.out.println("2");
				}
			};
		Thread t=new Thread(r);

		Thread t1=new Thread(r);
		t.start();
		t1.start();
		t.join();
		t1.join();
//		table.put(5, "Khan");
		t1.start();
		System.out.println(list.size());


	}
}
