package com.code.java;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

public class MyStack extends Stack<Integer>{
	Stack<Integer> maxStack=new Stack();
	
	
	@Override
	public Integer push(Integer item) {
		// TODO Auto-generated method stub
		if(!maxStack.isEmpty() && maxStack.peek()<item)
			maxStack.push(item);
		else {
			maxStack.push(maxStack.peek());
		}
		return super.push(item);
	}
	
	@Override
	public synchronized Integer pop() {
		// TODO Auto-generated method stub
		if(!maxStack.isEmpty())
			maxStack.pop();
		return super.pop();
	}
	
	@Override
	public synchronized Integer peek() {
		// TODO Auto-generated method stub
		return super.peek();
	}
	
	public int getMax() {
		if(!maxStack.isEmpty())
			return maxStack.peek();
		return 0;
	}
	
	public static void main(String[] args) {
		List<Integer> nums=new ArrayList<Integer>();
		nums.add(1);
		nums.add(4);
		nums.add(3);
		nums.add(2);
		nums.forEach(a->System.out.print(a));
		System.out.println();
		nums.stream().filter(a->a%2!=0).sorted().forEach(a->System.out.print(a));
	}
}
