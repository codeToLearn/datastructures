package com.code.java;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SynchronizationBug {
	private static Map<String,Date> concurrentMap=new ConcurrentHashMap();
	
	public static void start(String processId) throws InterruptedException{
		final String lock="log"+processId;
		synchronized (processId) {
			if(concurrentMap.containsKey(processId)) {
				System.out.println("Process is already working and started at "+concurrentMap.get(processId));
				return;
			}		
			concurrentMap.put(processId,new Date());
		}
	}
	
	
	public static void main(String[] args) throws InterruptedException {
//		System.out.println(isPrime(10));
		final String pId1="p1";
		final String pId2="p1";
		Thread t1=new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					start(pId1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		Thread t2=new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					start(pId2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		t1.start();
		t2.start();
		t1.join();
		t2.join();
		
		for (String s:concurrentMap.keySet()) {
			System.out.println(s+" - "+concurrentMap.get(s));
		}
	}
	
}
