package com.code.dp;

public class ZeroesAndOnes {
	public static void main(String[] args) {
		System.out.println(findMaxForm(new String[] {"10","0001","111001","1","0"}, 5, 3));
	}
	public static int findMaxForm(String[] strs, int m, int n) {
		int zeroes[]=new int[strs.length] ;
		int ones[]=new int[strs.length] ;
		for (int i = 0; i < ones.length; i++) {
			char[] bin=strs[i].toCharArray();
			int m1=0,n1=0;
			for (int j = 0; j < bin.length; j++) {
				if(bin[j]=='0')
					m1++;
				else n1++;
			}
			zeroes[i]=m1;
			ones[i]=n1;
		}
		return recursiveCount(0, zeroes, m, ones, n);
	}
	
	public static int recursiveCount(int i,int[] zeroes,int m,int[] ones,int n) {
		if(i==ones.length)
			return 0;
		if(m-zeroes[i]<0 || n-ones[i]<0)
			return recursiveCount(i+1, zeroes, m, ones, n);
		return Math.max(1+recursiveCount(i+1, zeroes, m-zeroes[i], ones, n-ones[i]), recursiveCount(i+1, zeroes, m, ones, n));
	}
	
}
