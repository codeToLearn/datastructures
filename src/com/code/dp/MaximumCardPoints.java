package com.code.dp;

public class MaximumCardPoints {
public static void main(String[] args) {
	int arr[]=new int[] {9,7,7,9,7,7,9};
	System.out.println(maxScoreDp(arr, 7));
}

public static int maxScoreDp(int[] cardPoints, int k) {
	int left[]=new int[k];
	left[0]=cardPoints[0];
	for(int i=1;i<k;i++) {
		left[i]=cardPoints[i]+left[i-1];
	}
	
	int right[]=new int[k];
	right[0]=cardPoints[cardPoints.length-1];
	int p=1;
	for(int i=cardPoints.length-2;p<k;i--) {
		right[p]=cardPoints[i]+right[p-1];
		p++;
	}
	int max=Math.max(left[k-1], right[k-1]);
	for(int i=0;i<k-1;i++) {//realised that only diagonal of matrix has maximum sum 
		max=Math.max(max, left[i]+right[k-2-i]);
	}
	return max;
}

public int maxScore(int[] cardPoints, int k) {
    return maximize(cardPoints,0,cardPoints.length-1,0,k);
  }
  
  public int maximize(int[] cardPoints,int start,int end,int sum,int k){
      if(start>end || k==0)
          return sum;
      return Math.max(maximize(cardPoints,start+1,end,sum+cardPoints[start],k-1), maximize(cardPoints,start,end-1,sum+cardPoints[end],k-1));
  }
}
