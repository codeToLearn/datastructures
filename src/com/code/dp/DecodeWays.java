package com.code.dp;

import java.util.ArrayList;
import java.util.List;

public class DecodeWays {
	public static int numDecodings(String s) {
		//if it contains such pattern then cannot be substituted with any alphabet
		if(s.contains("00")||s.startsWith("0") )
			return 0;
		
		char[] c=s.toCharArray();
		int[] arr=new int[c.length+1];
		//initialise array last index with 1
		arr[c.length]=arr[c.length-1]=1;
		
		//special handling for having 0 in the end
		if(s.endsWith("0"))
				arr[c.length-1]=0;
		
		int flag=0;//flag to maintain that a zero has occured
		for(int i=c.length-2;i>=0;i--) {
			if(c[i]=='0') {
				arr[i]=arr[i+1];
				flag=2;
			}else if(flag>0) {
				//in case if 60 is present then its zero can never be substitued 
				if(flag==2 && Integer.parseInt(String.valueOf(c[i]))>2)
					return 0;
				arr[i]=arr[i+1];
				flag--;
			}
			else if((Integer.parseInt(String.valueOf(c[i]))*10)+Integer.parseInt(String.valueOf(c[i+1]))<=26) 
					arr[i]=arr[i+1]+arr[i+2];
			else
				arr[i]=arr[i+1];

		}
		return arr[0];
	}

	public static void main(String[] args) {
		String s="101";
		System.out.println(numDecodings(s));
	}
}
