package com.code.dp;

import java.util.HashSet;
import java.util.Set;

public class LargestPlusSign {
	public static void main(String[] args) {
		int[][] mines=new int[][] {{4,2}};
		System.out.println(new LargestPlusSign().orderOfLargestPlusSignDp(5, mines));
	}

	//referred for this solution
	public int orderOfLargestPlusSignDp(int N, int[][] mines) {
		int max=0;
		int[][] dp=new int[N][N];
		for(int i=0;i<N;i++)
			for(int j=0;j<N;j++)
				dp[i][j]=1;
		
		for(int i=0;i<mines.length;i++) {
			dp[mines[i][0]][mines[i][1]]=0;;
		}
		
		for(int i=0;i<N;i++)
			for(int j=0;j<N;j++) {
				if(dp[i][j]==1) {
				int count=1;
				int dir=1;
				while (i+dir<N && j+dir < N && i-dir>=0 && j-dir>=0) {
					if(dp[i+dir][j]==1 && dp[i][j+dir]==1 && dp[i-dir][j]==1 && dp[i][j-dir]==1) {
						count++;
						dir++;
					}else break;
				}
				max=Math.max(max, count);
				}
			}
		return max;
	}
	
	//48/58 test cases passed . For others it is TLE
	public int orderOfLargestPlusSign1(int N, int[][] mines) {
		int max=0;
		Set<String> set=new HashSet();
		for(int[] mine:mines) {
			set.add(mine[0]+":"+mine[1]);
		}
		for(int i=(int) Math.ceil(((double)N)/2);i>max;i--)
			max=Math.max(max, getMaxForGrid(set,i-1,N-((i-1)*2),N));
		return max;
	}

	private int getMaxForGrid(Set<String> set, int index, int size,int N) {
		// TODO Auto-generated method stub
		int max=0;
		for(int i=index;i<index+size && max<index;i++) 
			for(int j=index;j<index+size && max<index;j++) 
				max=Math.max(max, recursiveMaxSize(set,i,j,N,"*"));
		return max;
	}

	private int recursiveMaxSize(Set<String> set, int i, int j,int N,String direction) {
		// TODO Auto-generated method stub
		if(set.contains(i+":"+j) || i<0 || j<0 || i>N-1 || j> N-1)
			return 0;
		switch (direction) {
		case "right":
			return 1+recursiveMaxSize(set, i, j+1,N,"right");
		case "left":
			return 1+recursiveMaxSize(set, i, j-1,N,"left");
		case "top":
			return 1+recursiveMaxSize(set, i-1, j,N,"top");
		case "down":
			return 1+recursiveMaxSize(set, i+1, j,N,"down");
		}
		return 1+Math.min(recursiveMaxSize(set, i+1, j,N,"down"), 
				Math.min(recursiveMaxSize(set, i, j+1,N,"right"),Math.min(recursiveMaxSize(set, i-1, j,N,"top"), recursiveMaxSize(set, i, j-1,N,"left"))));
	}
}
