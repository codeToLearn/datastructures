package com.code.dp;

import java.util.*;
import java.util.stream.Collectors;

public class WordBreak {
	public static void main(String[] args) {
		List<String> list=new ArrayList<String>();
		list.add("leet");list.add("code");
		System.out.println(new WordBreak().wordBreak("leetcode", list));
		System.out.println(dp("leetcode", list));
	}

	public boolean wordBreak(String s, List<String> wordDict) {
		Set<String> set=wordDict.stream().collect(Collectors.toSet());
		return checkWords(s,0,1,set);
	}
	
	//TLE due to recursion
	private boolean checkWords(String s,int start,int end,Set<String> set){
		if(start==s.length())
			return true;
		if(end>s.length())
			return false;
		if(set.contains(s.substring(start,end)))
			return checkWords(s,start,end+1,set) || checkWords(s,end,end+1,set);
		return checkWords(s,start,end+1,set);
	}
	
	private static boolean dp(String s, List<String> wordDict) {
		Set<String> set=wordDict.stream().collect(Collectors.toSet());
		boolean[][] dp=new boolean[s.length()+1][s.length()+1];
		for(int i=0;i<=s.length();i++) {
			dp[s.length()][i]=true;
		}
		
		for(int i=s.length()-1;i>=0;i--) {
			for(int j=s.length()-1;j>=0 && j>=i;j--) {
				if(set.contains(s.substring(i, j+1)))
					dp[i][j]=dp[i][j+1] || dp[j+1][j+1];
				else dp[i][j]=dp[i][j+1];
			}
		}
		return dp[0][0];
	}
}
