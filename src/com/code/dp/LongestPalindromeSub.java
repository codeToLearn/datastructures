package com.code.dp;

public class LongestPalindromeSub {
	public String longestPalindrome(String s) {
		if("".equals(s))
			return s;
		char[] c=s.toCharArray();
		String palindrome=s.substring(0,1);
		int max=1;//because each single length string is a palindrome
		for(int i=0;i<s.length();i++){
			//palindrome can have either odd length or even length
			int count=1,a=i-1,b=i+1;
			//for odd palindrome
			while(a>=0 && b <s.length() && c[a]==c[b])
			{//this is a kind of dp where it tracks the smaller palindromes and then computes the larger ones
				count+=2;a--;b++;//increment count by two if right and left character are same 
			}
			if(count>max)//everytime we are tracing the max length and the string
			{
				max=count;
				palindrome=s.substring(a+1,b);
			}
			a=i;b=i+1;count=0;
			//for even palindrome
			while(a>=0 && b<s.length() && c[a]==c[b]){
				count+=2;a--;b++;
			}
			if(count>max){
				palindrome=s.substring(a+1,b);
				max=count;
			}
		}
		return palindrome;
	}
}
