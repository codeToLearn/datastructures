package com.code.dp;

public class Knapsack01 {
	public static void main(String[] args) {
		int amount[]=new int[] {10,40,30,50};
		int[] w=new int[] {5,4,6,3};
		System.out.println(maxKnapsack(amount, w, 10));
	}
	
	private static int maxKnapsack(int[] amount,int[] w,int max) { 
		int[][] dp=new int[max+1][w.length+1];
		
		for(int i=max-1;i>=0;i--) {
			for(int j=w.length-1;j>=0;j--) {
				if(max-i-w[j]>=0)
					dp[i][j]=Math.max(amount[j]+dp[max-(max-i-w[j])][j+1], dp[i][j+1]);
				else dp[i][j]=dp[i][j+1];
			}
		}
		return dp[0][0];
	}
}
