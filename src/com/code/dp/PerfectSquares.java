package com.code.dp;

import java.util.ArrayList;
import java.util.List;

public class PerfectSquares {
	public static void main(String[] args) {
		System.out.println(new PerfectSquares().numSquares(10));
	}

	public int numSquares(int n) {
		int num=1,fact=3;
		List<Integer> squares=new ArrayList();
		while(num<=n){
			squares.add(num);
			num+=fact;
			fact+=2;
		}
		return dpMin(n, squares);
		//		return countMinimumSum(n,squares,0);
	}

	private int dpMin(int sum,List<Integer> squares) {
		int[][] dp=new int[sum+1][squares.size()+1];
		for(int i=0;i<sum;i++) {
			dp[i][squares.size()]=100000;
		}

		for(int i=sum-1;i>=0;i--) {
			for(int j=squares.size()-1;j>=0;j--) {
				int diff=sum-i-squares.get(j);
				if(diff<0)
					dp[i][j]=dp[i][j+1];
				else
					dp[i][j]=Math.min(1+Math.min(dp[sum-diff][j+1], dp[sum-diff][j]),dp[i][j+1]);
			}
		}
		return dp[0][0];
	}

	private int countMinimumSum(int sum,List<Integer> squares,int i){
		if(sum==0)
			return 0;
		if(i>=squares.size()|| sum<0)
			return 10000;
		return Math.min(1+Math.min(countMinimumSum(sum-squares.get(i),squares,i+1),
				countMinimumSum(sum-squares.get(i),squares,i))
		,countMinimumSum(sum,squares,i+1));
	}
}
