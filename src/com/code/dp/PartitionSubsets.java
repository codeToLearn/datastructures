package com.code.dp;


public class PartitionSubsets {
	public static void main(String[] args) {
		System.out.println(canPartition(new int[] {1,2,5}));
		System.out.println(partitionRecursive(new int[] {1,2,5},0,8,0));
	}
	
	 private static boolean partitionRecursive(int[] nums,int sumA,int total,int i){
	        if(i==nums.length && sumA==total/2)
	            return true;
	        if(i==nums.length || sumA>total/2)
	            return false;
	        return partitionRecursive(nums,sumA+nums[i],total,i+1) || partitionRecursive(nums,sumA,total,i+1);
	    }


	 //copied
	 public static boolean canPartition(int[] nums) {
		    int sum = 0;
		    
		    for (int num : nums) {
		        sum += num;
		    }
		    
		    if ((sum & 1) == 1) {
		        return false;
		    }
		    sum /= 2;
		    
		    int n = nums.length;
		    boolean[] dp = new boolean[sum+1];
		    dp[0] = true;
		    
		    for (int num : nums) {
		        for (int i = sum; i > 0; i--) {
		            if (i >= num) {
		                dp[i] = dp[i] || dp[i-num];
		            }
		        }
		    }
		    
		    return dp[sum];
		}
}
