package com.code.recursion;

import java.util.ArrayList;
import java.util.List;

public class GenerateParenthesis {
public static void main(String[] args) {
	List<String> list=getParenthesis(3);
	for(String s:list) {
		System.out.println(s);
	}
}

public static List<String> getParenthesis(int n){
	List<String> list=new ArrayList<String>();
	collectParenthesis(list,0,n,"");
	return list;
}

private static void collectParenthesis(List<String> list, int count, int n, String string) {
	// TODO Auto-generated method stub
	if(n==0 && count==0)
		list.add(string);
	else if(count!=0 && n==0)
		collectParenthesis(list, count-1, n, string+")");
	else if(count==0 && n!=0)
		collectParenthesis(list, count+1, n-1, string+"(");
	else {
		collectParenthesis(list, count+1, n-1, string+"(");
		collectParenthesis(list, count-1, n, string+")");
	}
}
}
