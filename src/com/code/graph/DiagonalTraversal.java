package com.code.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class DiagonalTraversal {
	public static void main(String[] args) {
		List<Integer> list1=new ArrayList<Integer>();
		list1.add(1);list1.add(2);list1.add(3);list1.add(4);list1.add(5);
		List<Integer> list2=new ArrayList<Integer>();
		list2.add(6);list2.add(7);
		List<Integer> list3=new ArrayList<Integer>();
		list3.add(8);
		List<Integer> list4=new ArrayList<Integer>();
		list4.add(9);list4.add(10);list4.add(11);
		List<Integer> list5=new ArrayList<Integer>();
		list5.add(12);list5.add(13);list5.add(14);list5.add(15);list5.add(16);
		List<List<Integer>> list=new ArrayList<List<Integer>>();
		list.add(list1);list.add(list2);list.add(list3);
		list.add(list4);list.add(list5);
		for(int a:new DiagonalTraversal().findDiagonalOrder(list))
			System.out.print(a+", ");
	}

//accepted solution taken idea from discussions
	public int[] findDiagonalOrder(List<List<Integer>> list) {
		// TODO Auto-generated method stub
		Map<Integer, List<Integer>> map=new LinkedHashMap<Integer, List<Integer>>();
		for(int i=0;i<list.size();i++)
			for(int j=0;j<list.get(i).size();j++) {
				if(!map.containsKey(i+j))
					map.put(i+j, new ArrayList<Integer>());
				map.get(i+j).add(list.get(i).get(j));
			}
		List<Integer> result=new ArrayList<Integer>();
		List<Integer> temp;
		for(Integer i:map.keySet()) {
			temp=map.get(i);
			for(int k=temp.size()-1;k>=0;k--) {
				result.add(temp.get(k));
			}
		}
		
		int arr[]=new int[result.size()];
		for(int i=0;i<result.size();i++) {
			arr[i]=result.get(i);
		}
		return arr;
	}


	//	this code is working fine but comes to tle because it traverses max*max dimensions
	public int[] findDiagonalOrder2(List<List<Integer>> nums) {
		int startA=0,startB=0;
		List<Integer> result=new ArrayList<Integer>();
		List<Integer> temp;
		int max=-1;
		for(List<Integer> list:nums) {
			max=Math.max(max, list.size());
		}
		while (startB<max ) {
			for(int i=startA,j=startB;i>=0 && j<max;i--,j++) {
				temp=nums.get(i);
				if(temp.size()>j)
					result.add(nums.get(i).get(j));
			}
			if(startA<nums.size()-1)
				startA++;
			else startB++;
		}
		int arr[]=new int[result.size()];
		for(int i=0;i<result.size();i++) {
			arr[i]=result.get(i);
		}
		return arr;
	}

	//	this solution would work if matrix was symmetric but here matrix horizonal count can vary 
	public int[] findDiagonalOrder1(List<List<Integer>> nums) {
		int startA=0,startB=0;
		int[] result=new int[nums.size()*nums.get(0).size()];
		int k=0;
		while (startB<nums.get(0).size() ) {
			for(int i=startA,j=startB;j<nums.size() && i>=0;i--,j++) {
				result[k]=nums.get(i).get(j);
				k++;
			}
			if(startA<nums.size()-1)
				startA++;
			else startB++;
		}
		return result;
	}
}
