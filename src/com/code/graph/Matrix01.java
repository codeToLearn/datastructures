package com.code.graph;
/**
 * Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.

The distance between two adjacent cells is 1.



Example 1:

Input:
[[0,0,0],
 [0,1,0],
 [0,0,0]]

Output:
[[0,0,0],
 [0,1,0],
 [0,0,0]]
 * @author Admin
 *
 */
public class Matrix01 {
	public static void main(String[] args) {
		int[][] matrix=new int[][] 
			{{1,0,1,1,0,0,1,0,0,1},
			{0,1,1,0,1,0,1,0,1,1},
			{0,0,1,0,1,0,0,1,0,0},
			{1,0,1,0,1,1,1,1,1,1},
			{0,1,0,1,1,0,0,0,0,1},
			{0,0,1,0,1,1,1,0,1,0},
			{0,1,0,1,0,1,0,0,1,1},
			{1,0,0,0,1,1,1,1,0,1},
			{1,1,1,1,1,1,1,0,1,0},
			{1,1,1,1,0,1,0,0,1,1}};
		for(int[] arr:updateMatrix(matrix)) {
			for(int a:arr) {
				System.out.print(a+",");
			}
			System.out.println("");
		}
	}

	public static int[][] updateMatrix(int[][] matrix) {
		int[][] visited=new int[matrix.length][matrix[0].length];//maintain visisted
		int max=matrix.length*matrix[0].length;//max path cannot be greater than the product of matrix dimension
		//traverse from top to bottom for maintaining minimum between left and top plus one 
		for(int i=0;i<matrix.length;i++)
			for(int j=0;j<matrix[0].length;j++) {
				if(matrix[i][j]==0)
					visited[i][j]=0;
				else {
					int a=i>0?visited[i-1][j]:max;
					int b=j>0?visited[i][j-1]:max;
					visited[i][j]=1+Math.min(a, b);//min of top and left else put max val
				}
			}
		//at this point we have min of top and left in the visited. Now we traverse from bottom to top 
		for(int i=matrix.length-1;i>=0;i--)
			for(int j=matrix[0].length-1;j>=0;j--)
			{
				if(matrix[i][j]==0)
					visited[i][j]=0;
				else {
					int a=i<matrix.length-1?visited[i+1][j]:max;
					int b=j<matrix[0].length-1?visited[i][j+1]:max;
					visited[i][j]=Math.min(1+Math.min(a, b), visited[i][j]);//maintain min of right , bottom and current index
				}
			}
		return visited;
	}
	//actually we were facing a problem to get the minimum from all four directions and how to stop at a point when the matrix is unvisited
	//Here we organise matrix traversal in two parts 
	//top to bottom for top and left direction
	//bottom to top for bottom and right direction

}
