package com.code.leetcode.may.week1;

import com.decode.searching.FirstLastPositionOfK;

public class BadVersion {
public static void main(String[] args) {
	new BadVersion().firstBadVersion(5);
}

public int firstBadVersion(int n) {
    int i=0,j=n,mid;
    while(i<j){
        mid=i+(j-i)/2;
        if(isBadVersion(mid)){
            j=mid;
        }else{
            i=mid+1;
        }
    }
    return i;
}

private boolean isBadVersion(int mid) {
	// TODO Auto-generated method stub
	return false;
}
}
