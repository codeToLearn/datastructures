package com.code.leetcode.may.week1;

import java.util.*;

public class FirstUniqueChaR {
	public int firstUniqChar(String s) {
        if(s.length()>0){
            Map<Character,Integer> countMap=new LinkedHashMap();
            for(char c:s.toCharArray())
            {
                if(!countMap.containsKey(c))
                    countMap.put(c,1);
                else countMap.put(c,countMap.get(c)+1);
            }
            if(countMap!=null && countMap.size()>0)
                for(Character key:countMap.keySet())
                    if(countMap.get(key)==1)
                return s.indexOf(key.toString());
        }
        return -1;
    }
}
