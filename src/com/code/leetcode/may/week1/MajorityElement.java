package com.code.leetcode.may.week1;

import java.util.HashMap;
import java.util.Map;

public class MajorityElement {
	public static void main(String[] args) {

	}

	public int majorityElement(int[] nums) {
		Map<Integer,Integer> map=new HashMap();
		int n =nums.length/2;
		for(int i=0;i<nums.length;i++){
			if(!map.containsKey(nums[i]))
				map.put(nums[i],1);
			else map.put(nums[i],map.get(nums[i])+1);
			if(map.get(nums[i])>n)
				return nums[i];
		}
		return 0;
	}
}
