package com.code.leetcode.may.week1;

import com.decode.trees.TreeNode;

public class CousinTreeNode {
	public boolean isCousins(TreeNode root, int x, int y) {
        int xheight= search(root,x,y,0);
        if(xheight<0)
            return false;
        int yheight= search(root,y,x,0);
        if(xheight==yheight)
            return true;
        return  false;
    }
    
    public int search(TreeNode root , int x,int y,int height){
        if(root==null)
            return -1;
        if(root.left!=null && root.right!=null)
            if((root.left.val==x && root.right.val==y) || (root.left.val==y && root.right.val==x))
                return -1;
        if(root.val==x)
            return height;
        return Math.max(search(root.left,x,y,height+1),search(root.right,x,y,height+1));
    }
}
