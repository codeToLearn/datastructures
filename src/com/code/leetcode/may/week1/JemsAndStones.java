package com.code.leetcode.may.week1;

import java.util.HashSet;
import java.util.Set;

public class JemsAndStones {
	public static void main(String[] args) {

	}
	public int numJewelsInStones(String J, String S) {
		char[] c =J.toCharArray();
		Set<String> set=new HashSet<String>();
		for(char e:c)
			set.add(e+"");
		int count=0;
		for(char e:S.toCharArray())
			if(set.contains(e+""))
				count++;
		return count;
	}
}
