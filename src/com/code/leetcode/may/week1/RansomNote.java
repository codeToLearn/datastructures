package com.code.leetcode.may.week1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RansomNote {
	public static void main(String[] args) {
System.out.println(new RansomNote().canConstruct("aa", "aab"));
	}

	public boolean canConstruct(String ransomNote, String magazine) {
		Map<Character,Integer> countMap=new HashMap<Character, Integer>();
		for(char c:magazine.toCharArray()) {
			if(!countMap.containsKey(c))
				countMap.put(c, 0);
			countMap.put(c, countMap.get(c)+1);
		}
		for(char c :ransomNote.toCharArray()) {
			if(!countMap.containsKey(c) || countMap.get(c)==0)
				return false;
			countMap.put(c, countMap.get(c)-1);
		}
		return true;
	}
}
