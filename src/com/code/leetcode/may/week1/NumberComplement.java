package com.code.leetcode.may.week1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class NumberComplement {
	public static void main(String[] args) {
System.out.println(toBinary(5));
	}

	private static int toBinary(int num) {
		int ans=0;
		int i=0;
		while (num!=1) {
			ans+=Math.pow(2, i)*(num%2==0?1:0);
			num/=2;i++;
		}
		return ans;
	}
}
