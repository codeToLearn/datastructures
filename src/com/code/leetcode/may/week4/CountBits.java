package com.code.leetcode.may.week4;

public class CountBits {
	public static void main(String[] args) {
		System.out.println(countBits(8));
	}

	public static int[] countBits(int num) {
		int[] arr=new int[num+1];
		arr[0]=0;
		if(num==0)
			return arr;
		arr[1]=1;
		int fact=2,index=0;
		for(int i=2;i<arr.length;i++){
			if(i==fact){
				arr[i]=1;
				index=i;
				fact*=2;
			}else{
				arr[i]=arr[index]+arr[i-index];
			} 
		}
		return arr;
	}
}
