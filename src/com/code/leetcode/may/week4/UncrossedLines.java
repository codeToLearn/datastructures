package com.code.leetcode.may.week4;

public class UncrossedLines {
	/**
	 * 	Input: A = [1,4,2], B = [1,2,4]
		Output: 2
		Explanation: We can draw 2 uncrossed lines as in the diagram.
		We cannot draw 3 uncrossed lines, because the line from A[1]=4 to B[2]=4 will intersect the line from A[2]=2 to B[1]=2.
	 */
	public int maxUncrossedLines(int[] A, int[] B) {
        int[][] dp=new int[A.length+1][B.length+1];
        for(int i=A.length-1;i>=0;i--)
            for(int j=B.length-1;j>=0;j--){
                if(A[i]==B[j])
                    dp[i][j]=Math.max(Math.max(1+dp[i+1][j+1],dp[i+1][j]),dp[i][j+1]);
                else dp[i][j]=Math.max(dp[i+1][j],dp[i][j+1]);
            }
        return dp[0][0];
    }
}
