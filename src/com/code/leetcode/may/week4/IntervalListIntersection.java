package com.code.leetcode.may.week4;

import java.util.ArrayList;
import java.util.List;

public class IntervalListIntersection {
	
	public int[][] intervalIntersection(int[][] A, int[][] B) {
		int i=0,j=0;
		List<List<Integer>> list=new ArrayList<List<Integer>>();
		List<Integer> temp;
		while(i<A.length && j<B.length) {
			int min=Math.max(A[i][0], B[j][0]);
			int max=Math.min(A[i][1], B[j][1]);
			if(min<=max) {
				temp=new ArrayList<Integer>();
				temp.add(min);temp.add(max);
				list.add(temp);
			}
			if(A[i][1]<B[j][1])
				i++;
			else j++;
		}
		int[][] response=new int[list.size()][2];
		i=0;
		for(List<Integer> l:list) {
			response[i][0]=l.get(0);response[i][1]=l.get(1);
			i++;
		}
		return response;
	}
	//this approach not working for one case
	public int[][] intervalIntersection1(int[][] A, int[][] B) {
        int max=Math.max(A[A.length-1][1],B[B.length-1][1]);
        int visit[]=new int[max+1];
        for(int i=0;i<A.length;i++){
            for(int j=A[i][0];j<=A[i][1];j++){
                visit[j]+=1;
            }
        }
        for(int i=0;i<B.length;i++){
            for(int j=B[i][0];j<=B[i][1];j++){
                visit[j]+=1;
            }
        }
        max=visit[0]==2?1:0;
        for(int i=1;i<visit.length;i++){
            if(visit[i]==2 && visit[i-1]<2)
                max++;
        }
        int[][] response=new int[max][2];
        max=0;
        boolean flag=false;
        if(visit[0]==2){
            response[max][0]=0;flag=true;
        }
        for(int i=1;i<visit.length;i++){
            if(flag && visit[i]!=2){
                response[max][1]=i-1;flag=false;max++;
            }else if(!flag && visit[i]==2){
                response[max][0]=i;flag=true;
            }
        }
        return response;
    }
}
