package com.code.leetcode.may.week4;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class SortByFrequency {
	public static void main(String[] args) {
		System.out.println(new SortByFrequency().frequencySort("aaabbddd"));
	}

	public String frequencySort(String s) {
		Map<Character, Integer> countMap=new LinkedHashMap<Character, Integer>();
		for(char c :s.toCharArray()) {
			if(!countMap.containsKey(c))
				countMap.put(c, 0);
			countMap.put(c, countMap.get(c)+1);
		}
		List<Entry<Character, Integer>> entryList=countMap.entrySet().stream().collect(Collectors.toList());
		Collections.sort(entryList,new Comparator<Entry<Character, Integer>>() {
			@Override
			public int compare(Entry<Character, Integer> o1, Entry<Character, Integer> o2) {
				return o2.getValue().compareTo(o1.getValue());
			}
		});
		s=""; 
		for(Entry<Character,Integer> key:entryList) {
			for(int i=0;i<key.getValue();i++)
				s+=key.getKey();
		}
		return s;
	}
}
