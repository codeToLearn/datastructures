package com.code.leetcode.may.week2;

public class FloodFill {
	public static void main(String[] args) {
		int[][] image=new int[][] {{1,1,1},{1,1,0},{1,0,1}};
		image=new FloodFill().floodFill(image, 1, 1, 2);
		System.out.println(image);
	}

	public int[][] floodFill(int[][] image, int sr, int sc,int newColor) {
		return recursiveFill(image, sr, sc, image[sr][sc], newColor);	
	}

	public int[][] recursiveFill(int[][] image, int sr, int sc, int oldColor,int newColor) {
		if(sr<0 || sc<0 || sr>=image.length || sc>=image[0].length || image[sr][sc]==newColor)
			return image;
		if(oldColor!=image[sr][sc] || newColor==image[sr][sc])
			return image;
		image[sr][sc]=newColor;
		image=recursiveFill(image, sr-1, sc, oldColor,newColor);
		image=recursiveFill(image, sr+1, sc, oldColor,newColor);
		image=recursiveFill(image, sr, sc-1, oldColor,newColor);
		image=recursiveFill(image, sr, sc+1, oldColor,newColor);
		return image;	
	}
}
