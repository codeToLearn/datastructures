package com.code.leetcode.may.week2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class TownJudge {
	 public int findJudge(int N, int[][] trust) {
		    int[] arr=new int[N+1];
	        for(int i=0;i<trust.length;i++){
	            arr[trust[i][0]]=-1;
	            if(arr[trust[i][1]]>=0)
	                arr[trust[i][1]]+=1;
	        }
	        int max=0,count=0;
	        for(int i=1;i<arr.length;i++){
	            if(arr[i]<0) count++;
	            else max=Math.max(max,arr[i]);
	        }
	        if(count!=N-1 || max<count)
	            return -1;
	        for(int i=1;i<arr.length;i++){
	            if(arr[i]==max)
	                return i;
	        }
	        return -1;
	    }
}
