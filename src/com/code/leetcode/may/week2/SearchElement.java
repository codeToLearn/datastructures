package com.code.leetcode.may.week2;

import com.decode.april.week3.SingleElementInSortedArray;

/**
 * You are given a sorted array consisting of only integers where every element appears exactly twice, 
 * except for one element which appears exactly once. 
 * Find this single element that appears only once.
   Follow up: Your solution should run in O(log n) time and O(1) space.
 * @author Admin
 */
public class SearchElement {
	public static void main(String[] args) {
		int[] nums=new int[] {1,1,8};
		System.out.println(singleNonDuplicate(nums));
	}
	 public static int singleNonDuplicate(int[] nums) {
		 return getElement(nums,0,nums.length-1);
	 }

	private static int getElement(int[] nums, int i, int j) {
		int mid=(i+j)/2;
		if(mid%2==0) {
			if(mid!=i && nums[mid]==nums[mid-1])
				return getElement(nums, i, mid);
			if(mid==j || nums[mid]!=nums[mid+1])
				return nums[mid];
		}else {
			if(mid!=j && nums[mid]==nums[mid+1])
				return getElement(nums, i, mid);
			if(mid!=j && mid==i)
				return nums[mid+1];
			if(mid==i || nums[mid]!=nums[mid-1])
				return nums[mid];
		}
		return getElement(nums, mid, j); 
	}
}
