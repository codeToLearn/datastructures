package com.code.leetcode.may.week2;

public class PointsStraightLine {
	//slope(m)=y2-y1/x2-x1
	public boolean checkStraightLine(int[][] coordinates) {
		double m=(double)(coordinates[0][1]-coordinates[1][1])/(double)(coordinates[0][0]-coordinates[1][0]);
		for(int i=1;i<coordinates.length;i++){
			if((double)(coordinates[i][1]-coordinates[i-1][1])/(double)(coordinates[i][0]-coordinates[i-1][0])!=m)
				return false;
		}
		return true;
	}
}
