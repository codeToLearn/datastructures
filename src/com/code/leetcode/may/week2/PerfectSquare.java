package com.code.leetcode.may.week2;

import java.util.HashSet;
import java.util.Set;

public class PerfectSquare {
	public static void main(String[] args) {
		System.out.println(new PerfectSquare().isPerfectSquare(11));
	}

	// we can also use binary search
	//1+3+5 always gives perfect square 
    public boolean isPerfectSquare(int num) {
      if (num < 1) return false;
      for (int i = 1; num > 0; i += 2)
        num -= i;
      return num == 0;
    }
	
	//TLE
	public boolean isPerfectSquare1(int num) {
		Set<Integer> set=new HashSet();
		int p=2;
		while(num>=p) {
			while(num%p==0) {
				num/=p;
				if(set.contains(p))
					set.remove(p);
				else set.add(p);
			}
			p++;
		}
		return set.size()==0;
	}
}
