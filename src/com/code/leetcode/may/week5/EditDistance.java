package com.code.leetcode.may.week5;

public class EditDistance {
	 public int minDistance(String word1, String word2) {
	        char[] w1=word1.toCharArray();
	        char[] w2=word2.toCharArray();
	        int[][] dp=new int[w1.length+1][w2.length+1];
	        
	        int k=1;
	        for(int i=w1.length-1;i>=0;i--){
	            dp[i][w2.length]=k;
	            k++;
	        }
	        
	        k=1;
	        for(int i=w2.length-1;i>=0;i--){
	            dp[w1.length][i]=k;
	            k++;
	        }
	        
	        for(int i=w1.length-1;i>=0;i--)
	            for(int j=w2.length-1;j>=0;j--){
	                if(w1[i]==w2[j])
	                    dp[i][j]=dp[i+1][j+1];
	                else dp[i][j]=1+Math.min(dp[i+1][j],Math.min(dp[i+1][j+1],dp[i][j+1]));
	            }
	        
	        return dp[0][0];
	    }
}
