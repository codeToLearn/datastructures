package com.code.leetcode.may.week5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class CourseSchedule {
	public static void main(String[] args) {
		int[][] preReq=new int[][] {{0,1},{1,0}};
		System.out.println(schedule(2, preReq));
	}

	public static boolean schedule(int num,int[][] pre) {
		int[][] grid=new int[num][num];
		for(int[] a:pre) {
			grid[a[1]][a[0]]=1;
		}

		Set<Integer> set;
		for(int i=0;i<num;i++) {
			for(int j=0;j<num;j++) {
				if(grid[i][j]==1) {
					set=new HashSet<Integer>();
					if(!markVisited(set,i,j,num,grid)) return false;
				}
			}
		}
		return true;
	}

	private static boolean markVisited(Set<Integer> set, int i, int j,int num,int[][] grid) {
		// TODO Auto-generated method stub
		if(set.contains(j))
			return false;
		set.add(i);
		grid[i][j]=-1;
		for(int k=0;k<num;k++) {
			if(grid[j][k]==1) {
				 if(!markVisited(set,j,k,num,grid)) return false;
			}
		}
		set.remove(i);
		return true;
	}

	public static boolean canFinish(int numCourses, int[][] prerequisites) {
		ArrayList[] graph = new ArrayList[numCourses];
		int[] degree = new int[numCourses];
		Queue queue = new LinkedList();
		int count=0;

		for(int i=0;i<numCourses;i++)
			graph[i] = new ArrayList();

		for(int i=0; i<prerequisites.length;i++){
			degree[prerequisites[i][1]]++;
			graph[prerequisites[i][0]].add(prerequisites[i][1]);
		}
		for(int i=0; i<degree.length;i++){
			if(degree[i] == 0){
				queue.add(i);
				count++;
			}
		}

		while(queue.size() != 0){
			int course = (int)queue.poll();
			for(int i=0; i<graph[course].size();i++){
				int pointer = (int)graph[course].get(i);
				degree[pointer]--;
				if(degree[pointer] == 0){
					queue.add(pointer);
					count++;
				}
			}
		}
		if(count == numCourses)
			return true;
		else    
			return false;
	}
}
