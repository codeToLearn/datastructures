package com.code.leetcode.may.week5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

public class PointsNearOrigin {
	public static void main(String[] args) {

	}

	public int[][] kClosest(int[][] points, int K) {
        Map<Double, List<Integer>> map=new TreeMap();
		for(int i=0;i<points.length;i++) {
            double dist=Math.sqrt(Math.pow(points[i][0], 2)+Math.pow(points[i][1], 2));
			if(!map.containsKey(dist))
                map.put(dist,new ArrayList());
            map.get(dist).add(i);
		}
		int[][] response=new int[K][2];
		Iterator<Double> itr=map.keySet().iterator();
		for(int i=0;i<K;) {
			List<Integer> indexes=map.get(itr.next());
            for(Integer index:indexes){
			    response[i][0]=points[index][0];
			    response[i][1]=points[index][1];
                i++;
                if(i>=K) break;
            }
		}
		return response;
    }
}
