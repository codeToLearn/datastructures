package com.code.leetcode.june.week2;

import java.util.*;

public class LargestSubset {
	public static void main(String[] args) {
		System.out.println(largestSubset(new int[] {1,3,4,8}));
	}
	//copied dp solution
	public List<Integer> largestDivisibleSubset(int[] nums) {
		int n = nums.length;
		int[] count = new int[n];
		int[] pre = new int[n];
		Arrays.sort(nums);
		int max = 0, index = -1;
		for (int i = 0; i < n; i++) {
			count[i] = 1;
			pre[i] = -1;
			for (int j = i - 1; j >= 0; j--) {
				if (nums[i] % nums[j] == 0) {
					if (1 + count[j] > count[i]) {
						count[i] = count[j] + 1;
						pre[i] = j;
					}
				}
			}
			if (count[i] > max) {
				max = count[i];
				index = i;
			}
		}
		List<Integer> res = new ArrayList<>();
		while (index != -1) {
			res.add(nums[index]);
			index = pre[index];
		}
		return res;
	}


	private static List<Integer> largestSubset(int[] nums) {
		// TODO Auto-generated method stub
		Arrays.sort(nums);
		int max=0;
		List<Integer> result = null;
		for(int i=0;i<nums.length-max;i++) {
			List<Integer> temp=new ArrayList<Integer>();
			temp.add(nums[i]);
			temp=recursiveSolve(nums, i+1, (ArrayList<Integer>) temp);
			if(temp.size()>max) {
				max=temp.size();
				result=temp;
			}
		}
		return result;
	}

	private static List<Integer> recursiveSolve(int[] nums, int i, ArrayList<Integer> arrayList) {
		// TODO Auto-generated method stub
		if(i==nums.length)
			return arrayList;
		if(nums[i]% arrayList.get(arrayList.size()-1)==0)
		{
			List l1= recursiveSolve(nums,i+1,arrayList);
			List<Integer> l2=(List) arrayList.clone();
			l2.add(nums[i]);
			l2= recursiveSolve(nums,i+1,(ArrayList<Integer>) l2);
			return l1.size()>l2.size()?l1:l2;
		}
		return recursiveSolve(nums,i+1,arrayList);
	}


}
