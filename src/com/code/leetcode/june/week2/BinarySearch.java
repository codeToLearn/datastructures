package com.code.leetcode.june.week2;

public class BinarySearch {
	public static void main(String[] args) {
		System.out.println(searchInsert(new int[] {1,3,5,6}, 7));
	}

	public static int searchInsert(int[] nums, int target) {
		int start=0,end=nums.length-1;
		while(start<end){
			int mid=(start+end)/2;
			if(nums[mid]==target)
				return mid;
			if(nums[mid]<target)
				start=mid+1;
			else end=mid;
		}
		if(nums[start]<target)
			return start+1;
		return start;
	}
}
