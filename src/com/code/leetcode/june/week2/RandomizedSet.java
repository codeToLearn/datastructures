package com.code.leetcode.june.week2;

import java.util.*;

public class RandomizedSet {

    Set<Integer> set=new HashSet();
    /** Initialize your data structure here. */
    public RandomizedSet() {

    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        return set.add(val);
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
    	return set.remove(val);
    }
    
    /** Get a random element from the set. */
    public int getRandom() {
        Random random = new Random();
        int randomNumber = random.nextInt(set.size());
        Iterator<Integer> itr=set.iterator();
        for(int i=0;i<randomNumber-1;i++)
        	itr.next();
        return itr.next();
    }
    
    public static void main(String[] args) {
		RandomizedSet set=new RandomizedSet();
		System.out.println(set.insert(1));
		System.out.println(set.remove(2));
		System.out.println(set.insert(2));
		System.out.println(set.getRandom());
		System.out.println(set.remove(1));
		System.out.println(set.insert(2));
		System.out.println(set.getRandom());
	}
}

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */
