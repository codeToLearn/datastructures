package com.code.leetcode.june.week2;

import java.util.Arrays;

public class CheapestFlight {
	public static void main(String[] args) {
		int[][] flights=new int[][] {{0,1,100},{1,2,100},{0,2,500}};
		
		System.out.println(findCheapestPrice1(3, flights, 0, 2, 1));
	}

	public static int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
		int grid[][]=new int[n][n];
		for(int[] f:flights) {
			grid[f[0]][f[1]]=f[2];
		}
		int temp= recursive(grid,src,dst,K);
        return temp==Integer.MAX_VALUE?-1:temp;
	}

	private static int recursive(int[][] grid, int src, int dst, int k) {
		// TODO Auto-generated method stub
		if(src==dst)
			return 0;
		int min=Integer.MAX_VALUE;
		for(int i=0;i<grid.length;i++) {
			if(grid[src][i]!=0 && k>=0) {
				int temp=grid[src][i]+recursive(grid, i, dst, k-1);
				if(temp<min)
					min=temp;
			}
		}
		return min;
	}
	
	//bellman ford copied
	public static int findCheapestPrice1(int n, int[][] flights, int src, int dst, int k) {
        int INF = 0x3F3F3F3F;
        int[] cost = new int[n];
        Arrays.fill(cost, INF);
        cost[src] = 0;
        int ans = cost[dst];
        for(int i = k; i >= 0; i--){ 
            int[] cur = new int[n];
            Arrays.fill(cur, INF);
            for(int[] flight : flights){
                cur[flight[1]] = Math.min(cur[flight[1]], cost[flight[0]] + flight[2]);
            }
            cost = cur;
            ans = Math.min(ans, cost[dst]);
        }
        return ans == INF ? -1 : ans;
    }
}
