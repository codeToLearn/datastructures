package com.code.leetcode.june.week3;

import java.text.NumberFormat;

public class ValidIpAddress {

	public static void main(String[] args) {
		System.out.println(new ValidIpAddress().validIPAddress("2001:0db8:85a3:0:0:8A2E:0370:7334"));
	}
	public String validIPAddress(String IP) {
		if(!IP.startsWith(":") && !IP.endsWith(":") && IP.contains(":")) {
			if(validateIpv6(IP.split(":")))
				return "IPv6";
		}
		else if(!IP.startsWith(".") && !IP.endsWith(".") && IP.contains(".")) {       
			if(validateIpv4(IP.split("[.]")))
				return "IPv4";
		}
		return "Neither";
	}

	private boolean validateIpv6(String[] ip){
		if(ip.length!=8)
			return false;
		for(String i:ip)
		{
			if(i==null || i.length()==0 || i.length()>4)
				return false;
            char[] cArr=i.toCharArray();
			for(char c:cArr)
				if((c>70 && c<97)||c>102 || c<48 )
					return false;
		}
		return true;
	}
	private boolean validateIpv4(String[] ip){
		if(ip.length!=4)
			return false;
        try{
		for(String i:ip)
		{
			int num=Integer.parseInt(i);
			if(num<0 || num>255 || i.length()>String.valueOf(num).length())
				return false;
		}
        }catch(Exception e){
            return false;
        }
		return true;
	}
}
