package com.code.leetcode.june.week3;

import java.util.Arrays;
import java.util.Queue;

public class SurroundedRegions {

	public static void main(String[] args) {
		char[][] board=new char[][] {{'X','X','X','X'},{'X','O','O','X'},{'X','X','O','X'},{'X','O','X','X'}};
		new SurroundedRegions().solve(board);
		for(char[] b:board) {
			for(char c :b) {
				System.out.print(c+", ");}
			System.out.println();}
	}
	public void solve(char[][] board) {
		if(board.length==0)
	          return;
		char grid[][]=new char[board.length][board[0].length];
		for(int i=0;i<board.length;i++) {
            grid[i][board[0].length-1]=board[i][board[0].length-1];
			grid[i][0]=board[i][0];
		}
		for(int i=0;i<board[0].length;i++) {
            grid[board.length-1][i]=board[board.length-1][i];
			grid[0][i]=board[0][i];
        }
		
		for(int i=1;i<board.length-1;i++)
			for(int j=1;j<board[i].length-1;j++) {
				grid[i][j]=board[i][j];
			}
		
		for(int i=1;i<board.length-1;i++)
			for(int j=1;j<board[i].length-1;j++) {
				if(board[i][j]=='O' && grid[i][j-1]!='O' && grid[i-1][j]!='O')
					grid[i][j]='X';
			}
		
		for(int i=board.length-2;i>0;i--)
			for(int j=board[i].length-2;j>0;j--) {
				if(board[i][j]=='O') {
					if(grid[i][j]!='O' && grid[i][j+1]!='O' && grid[i+1][j]!='O') 
						grid[i][j]='X';
					else grid[i][j]='O';
				}
			}
		
		for(int i=1;i<board.length-1;i++)
			for(int j=1;j<board[i].length-1;j++) {
				if(grid[i][j]=='X')
					board[i][j]='X';
			}
	}
}

class A{
	public void fun() {
		System.out.println("A");
	}
}

class B extends A{
	public void fun() {
		System.out.println("B");
	}
	
	public static void main(String[] args) {
		A a=new A();
		B b=(B) a;
		b.fun();
	}
}


