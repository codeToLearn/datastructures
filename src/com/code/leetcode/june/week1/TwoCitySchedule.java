package com.code.leetcode.june.week1;

import java.util.Map;
import java.util.TreeMap;

public class TwoCitySchedule {
	
	public static void main(String[] args) {
		int[][] arr=new int[][] {{259,770},{448,54},{926,667},{184,139},{840,118},{577,469}};
		System.out.println(twoCitySchedCost(arr));
	}
	
	//copied dp
	 public static int twoCitySchedCost(int[][] costs) {
	        int N = costs.length / 2;
	        int[][] dp = new int[N + 1][N + 1];
	        for (int i = 1; i <= N; i++) {
	            dp[i][0] = dp[i - 1][0] + costs[i - 1][0];
	        }
	        for (int j = 1; j <= N; j++) {
	            dp[0][j] = dp[0][j - 1] + costs[j - 1][1];
	        }
	        for (int i = 1; i <= N; i++) {
	            for (int j = 1; j <= N; j++) {
	                dp[i][j] = Math.min(dp[i - 1][j] + costs[i + j - 1][0], dp[i][j - 1] + costs[i + j - 1][1]);
	            }
	        }
	        return dp[N][N];
	    }
	
	
	private static int recursive(int sumA, int sumB, int[][] costs, int i,int a,int b) {
		// TODO Auto-generated method stub
		if(i==costs.length)
			return sumA+sumB;
		if(a==costs.length/2)
			return recursive(sumA, sumB+costs[i][1], costs, i+1, a, b+1);
		if(b==costs.length/2)
			return recursive(sumA+costs[i][0], sumB, costs, i+1, a+1, b);
		return Math.min(recursive(sumA, sumB+costs[i][1], costs, i+1, a, b+1), recursive(sumA+costs[i][0], sumB, costs, i+1, a+1, b));
	}
}
