package com.code.leetcode.june.week1;

public class CoinChange2 {
public static void main(String[] args) {
	System.out.println(change(5, new int[] {1,2,5}));
}

public static int change(int amount, int[] coins) {
    int[][] dp=new int[amount+1][coins.length+1];
    for(int i=0;i<=coins.length;i++)
        dp[amount][i]=1;
    for(int i=amount-1;i>=0;i--)
        for(int j=coins.length-1;j>=0;j--)
        {
            if(amount-i-coins[j]>=0)
                dp[i][j]=dp[amount-(amount-i-coins[j])][j]+dp[i][j+1];
            else dp[i][j]=dp[i][j+1];
        }
    return dp[0][0];
}
}
