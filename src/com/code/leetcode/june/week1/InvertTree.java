package com.code.leetcode.june.week1;

import com.decode.trees.TreeNode;

public class InvertTree {
	public static void main(String[] args) {

	}

	public TreeNode invertTree(TreeNode root) {
		if(root!=null) {
			TreeNode temp=root.left;
			root.left=invertTree(root.right);
			root.right=invertTree(temp);
		}
		return root;
	}
}
