package com.code.leetcode.june.week1;

import java.util.Arrays;
import java.util.Comparator;

public class QueueReconstruct {
	public static void main(String[] args) {
		reconstructQueue(new int[][] {{7,0},{4,4},{7,1},{5,0},{6,1},{5,2}});
	}

	public static int[][] reconstructQueue(int[][] people) {
		Arrays.sort(people,new Comparator<int[]>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				// TODO Auto-generated method stub
				if(o1[0]==o2[0]) return 0;
				if(o1[0]>o2[0])
					return 1; 
				else return -1;
			}
		});

		int[][] result=new int[people.length][2];
        for(int[] re:result)
        {
            re[0]=re[1]=-1;
        }
		for(int i=0;i<people.length;i++) {
			int skip=people[i][1];
			for(int j=0;j<result.length;j++) {
				if(result[j][0]==-1 || result[j][0]>=people[i][0]) {
					skip--;
					if(skip<0) {
						result[j][0]=people[i][0];result[j][1]=people[i][1];
						break;
					}
				}
			}
		}
		return result;
	}
}
