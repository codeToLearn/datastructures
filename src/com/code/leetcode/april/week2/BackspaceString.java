package com.code.leetcode.april.week2;

import java.util.List;
import java.util.Stack;

public class BackspaceString {
	 public static boolean backspaceCompare(String S, String T) {
		 	Stack<Character> stack=new Stack<Character>();
		 	char[] arr=S.toCharArray();
		 	for(int i=0;i<arr.length;i++) {
		 		if(arr[i]=='#') {
		 			if(!stack.isEmpty())
		 			stack.pop();
		 		}else {
		 			stack.push(arr[i]);
		 		}
		 	}
		 	StringBuilder sBuilder=new StringBuilder("");
		 	while (!stack.isEmpty()) {
				sBuilder.append(stack.pop());
			}
		 	
		 	arr=T.toCharArray();
		 	for(int i=0;i<arr.length;i++) {
		 		if(arr[i]=='#') {
		 			if(!stack.isEmpty())
		 			stack.pop();
		 		}else {
		 			stack.push(arr[i]);
		 		}
		 	}
		 	
		 	StringBuilder tBuilder=new StringBuilder("");
		 	while (!stack.isEmpty()) {
				tBuilder.append(stack.pop());
			}
	        return sBuilder.toString().equals(tBuilder.toString());
	    }
	 
	 public static void main(String[] args) {
		System.out.println(backspaceCompare("a##c", "#a#c"));
	}
}
