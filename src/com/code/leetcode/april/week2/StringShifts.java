package com.code.leetcode.april.week2;

public class StringShifts {
public static void main(String[] args) {
	int[][] shift=new int[5][2];
	shift[0][0]=1;
	shift[0][1]=4;
	shift[1][0]=0;
	shift[1][1]=5;
	shift[2][0]=0;
	shift[2][1]=4;
	shift[3][0]=1;
	shift[3][1]=1;
	shift[4][0]=1;
	shift[4][1]=5;
	System.out.println(stringShift("mecsk", shift));
}

public static String stringShift(String s, int[][] shift) {
    int shifts=0;
	for(int i=0;i<shift.length;i++) {
		if(shift[i][0]==0) {
			shifts-=shift[i][1];
		}else {
			shifts+=shift[i][1];
		}
	}
	shifts%=s.length();
	if(shifts>0) {
		return s.substring(s.length()-shifts)+s.substring(0, s.length()-shifts);
	}else if(shifts<0) {
		shifts*=-1;
		return s.substring(shifts)+s.substring(0, shifts);
	}
	return s;
}
}
