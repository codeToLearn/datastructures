package com.code.leetcode.april.week2;

import java.util.HashMap;
import java.util.Map;

public class ContiguousArray {
public static void main(String[] args) {
	int nums[]=new int[] {0,1,0,1,1,0};
	System.out.println(findMaxLength(nums));
}
//trying using n space and n time complexity
public static int findMaxLength(int[] nums) {
  int leftSum[]=new int[nums.length+1];
  leftSum[0]=0;
  for(int i=0;i<nums.length;i++) {
	  leftSum[i+1]=leftSum[i]+(nums[i]==0?-1:1);
  }
  Map<Integer, Integer> sumLocationMap=new HashMap<Integer, Integer>();
  int maxLen=0;
  for(int i=0;i<leftSum.length;i++) {
	  if(sumLocationMap.containsKey(leftSum[i])) {
		  int temp=i-sumLocationMap.get(leftSum[i]);
		  if(temp>maxLen)
			  maxLen=temp;
	  }else {
		  sumLocationMap.put(leftSum[i], i);
	  }
  }
  return maxLen;
}
}
