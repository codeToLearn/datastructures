package com.code.leetcode.april.week2;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode(int x) { val = x; }
}

public class DiameterBTree {
	public Integer max;

	public DiameterBTree() {
		// TODO Auto-generated constructor stub
		max=new Integer(0);
	}
	public int diameterOfBinaryTree(TreeNode root) {
		maxDiam(root);
		return max;
	}

	public int maxDiam(TreeNode root){
		int a=0,b=0,d=0;
		if(root.left==null&&root.right==null)
			return 0;
		if(root.left!=null){
			a=maxDiam(root.left);
			d++;
		}
		if(root.right!=null){
			b=maxDiam(root.right);
			d++;
		}
		if(a+b+d>max)
			max=a+b+d;
		return 1+ (a>b?a:b);
	}

	public static void main(String[] args) {
		TreeNode node=new TreeNode(1);
		node.left=new TreeNode(2);
		node.right=new TreeNode(3);
		node.left.left=new TreeNode(4);
		node.left.right=new TreeNode(5);
		System.out.println(new DiameterBTree().diameterOfBinaryTree(node));
	}
}
