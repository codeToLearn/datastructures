package com.code.leetcode.april.week2;

import java.util.Stack;

public class MinStack {
	private Stack<Integer> stack;
	private Stack<Integer> sortedStack;

	public MinStack() {
		stack=new Stack<Integer>();
		sortedStack=new Stack<Integer>();
	}

	public void push(int x) {
		stack.push(x);
		Stack<Integer> temp=new Stack<Integer>();
		while (!sortedStack.isEmpty() && sortedStack.peek()<x) {
			temp.push(sortedStack.pop());
		}
		sortedStack.push(x);
		while (!temp.isEmpty()) {
			sortedStack.push(temp.pop());
		}
	}

	public void pop() {
		if(stack.isEmpty())
			return;
		int x=stack.pop();
		Stack<Integer> temp=new Stack<Integer>();
		while (!sortedStack.isEmpty() && sortedStack.peek()!=x) {
			temp.push(sortedStack.pop());
		}
		sortedStack.pop();
		while (!temp.isEmpty()) {
			sortedStack.push(temp.pop());
		}
	}

	public int top() {
		if(!stack.isEmpty())
		return stack.peek();
		return 0;
	}

	public int getMin() {
		if(!sortedStack.isEmpty())
		return sortedStack.peek();
		return 0;
	}
	
	public static void main(String[] args) {
		MinStack stack=new MinStack();
		stack.push(-2);
		stack.push(0);
		stack.push(-3);
		System.out.println(stack.getMin());
	}

}
