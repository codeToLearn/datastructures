package com.code.leetcode.april.week2;

import java.util.Stack;

public class LastStoneWeight {
public static void main(String[] args) {
	
}

public int lastStoneWeight(int[] stones) {
    Stack<Integer> stack=new Stack();
    Stack<Integer> temp=new Stack();
    for(int i=0;i<stones.length;i++){
        while(!stack.isEmpty() && stack.peek()>stones[i]){
            temp.push(stack.pop());
        }
        stack.push(stones[i]);
        while(!temp.isEmpty()){
            stack.push(temp.pop());
        }
    }
    while(stack.size()!=1){
        int a=stack.pop()-stack.pop();
          while(!stack.isEmpty() && stack.peek()>a){
            temp.push(stack.pop());
        }
        stack.push(a);
        while(!temp.isEmpty()){
            stack.push(temp.pop());
        }
    }
    return stack.pop();
}
}
