package com.code.leetcode.april.week5;

import com.decode.trees.TreeNode;

public class MaxPathSum {
	public static void main(String[] args) {
		TreeNode root=new TreeNode(9);
		root.right=new TreeNode(-3);
		root.right.left=new TreeNode(-6);
		root.right.right=new TreeNode(2);
		root.right.right.left=new TreeNode(2);
		root.right.right.left.right=new TreeNode(-6);
		root.right.right.left.left=new TreeNode(-6);
		root.right.right.left.left.left=new TreeNode(-6);
		root.left=new TreeNode(6);

		System.out.println(new MaxPathSum().maxPathSum(root));
	}
	int max=0;
	public int maxPathSum(TreeNode root) {
		dfs(root);
		return max;
	}

	private int dfs(TreeNode root) {
		if(root==null)
			return 0;
		int right=dfs(root.right);
		if(right<0) right=0;
		int left=dfs(root.left);
		if(left<0) left=0;
		int val=root.val;
		int temp= val+Math.max(right,left);
		max=Math.max(Math.max(val+right+left,max),temp);
		return temp;
	}
}
