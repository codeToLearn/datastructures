package com.code.leetcode.april.week3;

public class MinPathSum {
	public static void main(String[] args) {

	}

	public static int minPathSum(int[][] grid) {
		if(grid.length==0)
			return 0;
		int[][] temp=new int[grid.length][grid[0].length];
		for(int i=0;i<grid.length;i++) {
			for(int j=0;j<grid[0].length;j++) {
				if(i==0 & j==0)
				temp[i][j]=grid[i][j];
				else if(i==0)
					temp[i][j]=grid[i][j]+temp[i][j-1];
				else if(j==0)
					temp[i][j]=grid[i][j]+temp[i-1][j];
				else temp[i][j]=grid[i][j]+Math.min(temp[i][j-1], temp[i-1][j]);
			}
		}
		return temp[grid.length-1][grid[0].length-1];
	}
}
