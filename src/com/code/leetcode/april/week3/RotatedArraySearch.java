package com.code.leetcode.april.week3;

public class RotatedArraySearch {
public static void main(String[] args) {
	int[] arr=new int[] {1,3,5};
	System.out.println(new RotatedArraySearch().search(arr, 3));
}

public int search(int[] nums, int target) {
    int pivot=getPivot(nums,0,nums.length-1);
    if(pivot==-1)
    	return find(nums, 0, nums.length-1, target);
    if(nums[0]>target) 
    	return find(nums,pivot,nums.length-1,target);
    else
    	return find(nums,0,pivot,target);
}

private int getPivot(int[] nums, int i, int j) {
	// TODO Auto-generated method stub
    if(i>=j)
        return -1;
	int mid=(i+j)/2;
	if(mid<j && nums[mid]>nums[mid+1])
		return mid;
	if(mid>i && nums[mid]<nums[mid-1])
		return mid-1;
	if(nums[mid]<nums[j])
		return getPivot(nums, i, mid-1);
	else return getPivot(nums, mid+1, j);
}

private int find(int[] nums, int i, int j, int target) {
	// TODO Auto-generated method stub[4,0,1,2,3]
	if(i>j)
		return -1;
	int mid=(i+j)/2;
	if(nums[mid]==target)
		return mid;
	if(nums[mid]>target)
		return find(nums, i, mid-1, target);
	else return find(nums, mid+1, j, target);
}
}
