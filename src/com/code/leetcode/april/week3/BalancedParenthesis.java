package com.code.leetcode.april.week3;

import java.util.Stack;

public class BalancedParenthesis {
	public static void main(String[] args) {
		System.out.println(checkValidUsingStack("((*)(*))(*"));
//		System.out.println(checkValidString("(()())()"));
		String s="(((*))(((**)";
//		String s="((()()()))(())";
		System.out.println(checkValidUsingStack(s));
		String s1=")(";
//		String s="((()()()))(())";
		System.out.println(checkValidUsingStack(s1));
	}

	public static boolean checkValidUsingStack(String s) {
		char[] arr=s.toCharArray();
		Stack<Character> stack=new Stack<Character>();
		for(int i=0;i<arr.length;i++) {
			if(arr[i]=='(')
				stack.push(arr[i]);
			else if(arr[i]==')') {
				if(!stack.isEmpty() && stack.peek()=='(')
					stack.pop();
				else if(!stack.isEmpty() && stack.peek()=='*') {
					//pop the stack until u find a opening brace and if u find a closing brace or stack is empty then exit
					int count=0;
					while (!stack.isEmpty() && stack.peek()=='*') {
						stack.pop();
						count++;	
					}
					if(stack.isEmpty() && count>0) {
						count--;
						while (count-->0) {
							stack.push('*');
						}
					}else if(!stack.isEmpty() && stack.peek()=='(') {
						stack.pop();
						while (count-->0) {
							stack.push('*');
						}
					}
					else return false;
				}
				else return false;
			}else if(arr[i]=='*') {
				stack.push('*');
			}
		}
		int count=0;
		while (!stack.isEmpty()) {
			if(count==0 && stack.peek()=='(')
				return false;
			else if(stack.peek()=='(') {
				stack.pop();
				count--;
			}
			else if(stack.peek()=='*') {
				stack.pop();				
				count++;
			}
		}
		return true;
	}
	
	
	public static boolean checkValidString(String s) {
		char[] arr=s.toCharArray();
		int count=0;
		for(int i=0;i<arr.length;i++) {
			if(arr[i]=='(')
				count++;
			else if(arr[i]==')')
				count--;
			if(count<0)
				return false;
		}
		if(count!=0)
			return false;
		return true;
	}
}
