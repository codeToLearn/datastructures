package com.code.leetcode.april.week3;

public class CountIslands {
public static void main(String[] args) {
	
}

public static int numIslands(char[][] grid) {
	int count=0;
    for(int i=0;i<grid.length;i++) {
    	for(int j=0;j<grid[0].length;j++) {
    		if(grid[i][j]=='1') {
    			markVisited(grid,i,j);
    			count++;
    		}
    	}
    }
    return count;
}

private static void markVisited(char[][] grid, int i, int j) {
	// TODO Auto-generated method stub
	if(grid[i][j]=='0')
		return;
	grid[i][j]='0';
	if(i!=0)
		markVisited(grid, i-1, j);
	if(j!=0)
		markVisited(grid, i, j-1);
	if(i!=grid.length-1)
		markVisited(grid, i+1, j);
	if(j!=grid[0].length-1)
		markVisited(grid, i, j+1);
}
}
