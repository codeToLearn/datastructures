package com.code.leetcode.april.week3;

import java.util.ArrayList;
import java.util.List;

class BinaryMatrix {
	int[][] arr=new int[][] {{0,0,0,1,1,1,1,1,1},{0,0,0,0,1,1,1,1,1}};
	 int get(int x, int y) {
		return arr[x][y];

	}
	List<Integer> dimensions() {
		List<Integer> dimension=new ArrayList<Integer>(2);
		dimension.add(arr.length);
		dimension.add(arr[0].length);
		return dimension;
	}
}
public class LeftMostColumn {
	public static void main(String[] args) {
		BinaryMatrix binaryMatrix=new BinaryMatrix();
		System.out.println(leftMostColumnWithOne(binaryMatrix));
	}

	public static int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
		List<Integer> dimension=binaryMatrix.dimensions();
		int index,min=Integer.MAX_VALUE;
		boolean flag=false;
		for(int i=0;i<dimension.get(0);i++) {
			index=binarySearch(binaryMatrix,i,0,dimension.get(1)-1);
			if(index<0)
				continue;
			if(index<min)
				min=index;
		}
		if(min==Integer.MAX_VALUE) return -1;
		return min;
	}

	private static int binarySearch(BinaryMatrix binaryMatrix, int pos,int i, int j) {
		// TODO Auto-generated method stub
		int index=-1;
		if(i<=j) {
			index=j;
			int mid=(i+j)/2;
			int element=binaryMatrix.get(pos, mid);
			if(element==1 && (mid==0||binaryMatrix.get(pos, mid-1)==0))
				index= mid;
			else if(element==1)
				index=binarySearch(binaryMatrix, pos, i, mid-1);
			else index=binarySearch(binaryMatrix, pos, mid+1, j);
		}
		return index;
	}
}

