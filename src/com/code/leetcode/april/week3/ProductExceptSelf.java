package com.code.leetcode.april.week3;

public class ProductExceptSelf {
	public static void main(String[] args) {
		int[] nums=new int[] {1,2,3,4};
		nums=productExceptSelf(nums);
		for(int i=0;i<nums.length;i++){
			System.out.print(nums[i]+",");
		}
	}

	public static int[] productExceptSelf(int[] nums) {
		int[] leftProd=new int[nums.length];
		leftProd[0]=1;
		for(int i=0;i<nums.length-1;i++){
			leftProd[i+1]=leftProd[i]*nums[i];
		}
		int product=nums[nums.length-1];
		for(int i=nums.length-2;i>=0;i--){
			leftProd[i]*=product;
			product=product*nums[i];
		}
		return leftProd;
	}
}
