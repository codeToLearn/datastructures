package com.code.leetcode.april.week1;

import java.util.HashSet;
import java.util.Set;

public class CountingElements {
public static void main(String[] args) {
	int arr[]=new int[] {1,1,2,2};
	System.out.println(countElements(arr));
}

public static int countElements(int[] arr) {
    Set<Integer> set=new HashSet<Integer>();
    for(int a:arr) {
    	set.add(a);
    }
    int count=0;
    for(int a:arr) {
    	if(set.contains(a+1))
    		count++;
    }
    return count;
}
}
