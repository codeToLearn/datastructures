package com.code.leetcode.april.week1;

public class SingleNumber {
public static void main(String[] args) {
	int arr[]=new int[]{1,1,3,2,2};
	System.out.println(singleNumber(arr));
}
public static int singleNumber(int[] nums) {
    int res=0;
    for(int i=0;i<nums.length;i++) {
    	res^=nums[i];
    }
	return res;
    
}
}
