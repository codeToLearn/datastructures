package com.code.leetcode.april.week1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupAnagrams {
	public static void main(String[] args) {
		String[] list=new String[] {"eat","tea","tan","ate","nat","bat"};
		System.out.println(groupAnagrams(list));
	}
	public static List<List<String>> groupAnagrams(String[] strs) {
		Map<String, Integer> hashingMap=getHashKeys();
		Map<String,List<String>> answer=new HashMap<String, List<String>>();
		Long product=1l;
		for(String s:strs) {
			char[] c=s.toCharArray();
			product=1l;
			for(Character ch:c) {
				product*=hashingMap.get(ch.toString());
			}
			if(answer.get(product.toString())==null) {
				answer.put(product.toString(), new ArrayList<String>());
			}
			answer.get(product.toString()).add(s);
		}
		return answer.values().stream().collect(Collectors.toList());
	}
	private static Map<String, Integer> getHashKeys() {
		// TODO Auto-generated method stub
		Map<String,Integer> map=new HashMap<String, Integer>();
		map.put("a", 2);
		map.put("b", 3);map.put("c", 5);map.put("d", 7);
		map.put("e", 11);map.put("f", 13);map.put("g", 17);
		map.put("h", 19);map.put("i", 23);map.put("j", 29);
		map.put("k", 31);
		map.put("l", 37);map.put("m", 41);map.put("n", 43);
		map.put("o", 47);map.put("p", 53);
		map.put("q", 59);map.put("r", 61);
		map.put("s", 67);map.put("t", 71);
		map.put("u", 73);map.put("v", 79);map.put("w", 83);
		map.put("x", 89);map.put("y", 97);map.put("z", 101);
		return map;
	}
}
