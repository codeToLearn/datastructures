package com.code.leetcode.april.week1;

import java.util.HashSet;
import java.util.Set;

public class HappyNumber {
public static void main(String[] args) {
	System.out.println(isHappy(68));
}

public static  boolean isHappy(int n) {
    Set<Integer> set=new HashSet<>();
    while (n!=1 && !set.contains(n)) {
    	set.add(n);
    	n=getSumSquare(n);		
	}
    if(n==1)
	return true;
    else return false;
}

private static int getSumSquare(int n) {
	int num=0;
	while (n>0) {
		num+=Math.pow(n%10, 2);
		n/=10;		
	}
	return num;
}
}
