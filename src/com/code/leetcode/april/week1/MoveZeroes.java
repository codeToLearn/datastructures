package com.code.leetcode.april.week1;

public class MoveZeroes {
public static void main(String[] args) {
	int[] nums=new int[] {0,1,0,3,12};
	moveZeroes(nums);
}

public static void moveZeroes(int[] nums) {
    boolean flag=false;
    int updateIndex=0;
	for(int i=0;i<nums.length;i++) {
    	if(!flag && nums[i]==0) {
    		flag=true;
    		updateIndex=i;
    	}else if(flag){
    		if(nums[i]!=0) {
    			nums[updateIndex]=nums[i];
    			nums[i]=0;
    			updateIndex++;
    		}
    	}
    }
	
	for(int i=0;i<nums.length;i++) {
		System.out.println(nums[i]+",");
	}
}
}
