package com.code.leetcode.april.week1;

public class MaximumSubarray {
public static void main(String[] args) {
	int[] nums=new int[] {1};
	System.out.println(maxSubArray(nums));
}

public static int maxSubArray(int[] nums) {
    int sum=nums[0];
    int maxSum=sum;
    for(int i=1;i<nums.length;i++) {
    	if(sum+nums[i]>nums[i])
    		sum+=nums[i];
    	else sum=nums[i];
    	if(sum>maxSum)
    		maxSum=sum;
    }
    return maxSum;
}
}
