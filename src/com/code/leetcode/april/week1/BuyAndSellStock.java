package com.code.leetcode.april.week1;

public class BuyAndSellStock {
public static void main(String[] args) {
	int[] prices=new int[] {7,1,5,3,6,4};
	System.out.println(maxProfit(prices));
}

public static int maxProfit(int[] prices) {
    
	return buyAndSellDP(prices);
}
public static int buyAndSellDP(int[] prices) {
	int matrix[][] =new int[prices.length+1][2];
	for(int i=prices.length-1;i>=0;i--) {
		matrix[i][0]=Math.max(matrix[i+1][0], -prices[i]+matrix[i+1][1]);
		matrix[i][1]=Math.max(matrix[i+1][1], prices[i]+matrix[i+1][0]);
	}
	return matrix[0][0];
}
public static int buyAndSellRecursive(int[] prices,int index,boolean isPurchased){
	if(index==prices.length)
		return 0;
	int costA=buyAndSellRecursive(prices, index+1, isPurchased);
	int costB=0;
	if(isPurchased) {
		costB=prices[index]+buyAndSellRecursive(prices, index+1, !isPurchased);
	}else {
		costB=-prices[index]+buyAndSellRecursive(prices, index+1, !isPurchased);
	}
	return costA>costB?costA:costB;
}
}
