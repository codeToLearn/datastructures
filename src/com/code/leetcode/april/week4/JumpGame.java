package com.code.leetcode.april.week4;

public class JumpGame {
	public static void main(String[] args) {

	}

	public boolean canJump(int[] nums) {
		boolean[] dp=new boolean[nums.length];
		dp[nums.length-1]=true;
		for(int i=nums.length-2;i>=0;i--)
			for(int j=1;j<=nums[i] && (i+j < nums.length);j++)
				dp[i]=dp[i]||dp[i+j];
		return dp[0];
	}
}
