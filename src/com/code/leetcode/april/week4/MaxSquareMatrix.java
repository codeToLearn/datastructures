package com.code.leetcode.april.week4;

public class MaxSquareMatrix {
	public static void main(String[] args) {

		int[][] grid=new int[][] {{1,0,1,1,1},{0,0,1,1,1},{1,1,1,1,1},{0,0,0,0,1}};
		int max=0;
		for(int i=0;i<grid.length-max;i++)
			for(int j=0;j<grid[0].length;j++)
				max=Math.max(max, recursiveMax(grid,i,j,"diag"));
		System.out.println(max*max);
	}

//	public int maximalSquare(char[][] matrix) {
//        int max=0;
//		for(int i=0;i<matrix.length-max;i++)
//			for(int j=0;j<matrix[0].length;j++)
//				max=Math.max(max, recursiveMax(matrix,i,j,"diag"));
//        return max*max;
//    }
	//this que was asked in paytm interview 
	//it is a self written solution
    private static int recursiveMax(int[][] grid, int i, int j, String direction) {
		// TODO Auto-generated method stub
		if(i>=grid.length || j>=grid[0].length || grid[i][j]==0)
			return 0;
		switch (direction) {
		case "right":
			return 1+recursiveMax(grid, i, j+1, direction);
		case "down":
			return 1+recursiveMax(grid, i+1, j, direction);
		default:
			return 1+Math.min(Math.min(recursiveMax(grid, i, j+1, "right"), recursiveMax(grid, i+1, j, "down")),recursiveMax(grid, i+1, j+1, direction));
		}
	}
    
    private static int dpMax(int[][] grid) {
    	int[][] dp=new int[grid.length+1][grid[0].length+1];
    	int max=0;
    	for(int i=grid.length-1;i>=0;i--)
    		for(int j=grid[0].length-1;j>=0;j--) {
    			if(grid[i][j]==1) {
    				dp[i][j]=1+Math.min(Math.min(dp[i+1][j], dp[i][j+1]), dp[i+1][j+1]);
    				max=Math.max(max, dp[i][i]);
    			}
    		}
    	return max*max;
    }

}
