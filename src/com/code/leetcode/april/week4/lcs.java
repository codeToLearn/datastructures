package com.code.leetcode.april.week4;

public class lcs {
	public static void main(String[] args) {
		
	}
	public int longestCommonSubsequence(String text1, String text2) {
        char[] c1=text1.toCharArray();
        char[] c2=text2.toCharArray();
        int[][] matrix=new int[c1.length+1][c2.length+1];
        for(int i=c1.length-1;i>=0;i--)
            for(int j=c2.length-1;j>=0;j--){
                if(c1[i]==c2[j])
                    matrix[i][j]=1+matrix[i+1][j+1];
                else matrix[i][j]=Math.max(matrix[i+1][j],matrix[i][j+1]);
            }
        return matrix[0][0];
    }
}
