package com.code.leetcode.april.week4;

public class BitwiseAnd {
public static void main(String[] args) {
	int result=2;
	for(int i =2;i<100;i=i+1) {
		result&=i;
		System.out.println(i+": "+result);
	}
	new BitwiseAnd().rangeBitwiseAnd(2147483647,2147483647);
}

public int rangeBitwiseAnd(int m, int n) {
    long result=m;
    for(long i=m;i<n;i++) {
    	result&=(i+1);
    	if(result==0) break;
    }
    return (int)result;
}
}
