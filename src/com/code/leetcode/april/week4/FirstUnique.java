package com.code.leetcode.april.week4;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class FirstUnique {
	
	Set<Integer> uniqueSet=new LinkedHashSet();
	Set<Integer> nonUniqueSet=new HashSet();
	
	public FirstUnique(int[] nums) {
		for(int num:nums) {
			if(!nonUniqueSet.contains(num) && !uniqueSet.contains(num))
				uniqueSet.add(num);
			else if(uniqueSet.contains(num)) {
				uniqueSet.remove(num);
				nonUniqueSet.add(num);
			}
		}
	}

	public int showFirstUnique() {
		if(!uniqueSet.isEmpty())
		{
			return uniqueSet.iterator().next();
		}
		return -1;
	}

	public void add(int num) {
		if(!nonUniqueSet.contains(num) && !uniqueSet.contains(num))
			uniqueSet.add(num);
		else if(uniqueSet.contains(num)) {
			uniqueSet.remove(num);
			nonUniqueSet.add(num);
		}
	}
}
