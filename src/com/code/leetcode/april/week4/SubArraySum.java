package com.code.leetcode.april.week4;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SubArraySum {
public static void main(String[] args) {
	System.out.println(subarraySum(new int[] {0,0,0,0,0,0,0,0,0,0}, 0));
}

public static int subarraySum(int[] nums, int k) {
    int[] arr=new int[nums.length+1];
    arr[0]=0;
    for(int i=1;i<arr.length;i++) {
    	arr[i]=arr[i-1]+nums[i-1];
    }
    Map<Integer,Integer> store=new HashMap<Integer, Integer>();
    store.put(k,1);
    int count=0;
    for(int i=1;i<arr.length;i++) {
    	if(store.containsKey(arr[i])) {
    		count+=store.get(arr[i]);
    	}
    	if(store.containsKey(arr[i]+k)) {
    		store.put(arr[i]+k, store.get(arr[i]+k)+1);
    	}else {
    		store.put(arr[i]+k,1);
    	}
    }
    return count;
}
}
