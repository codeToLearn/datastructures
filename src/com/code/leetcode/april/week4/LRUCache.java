package com.code.leetcode.april.week4;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

public class LRUCache {
	private Map<Integer, Integer> cache;
	private Queue<Integer> queue;
	private int capacity;
	private Map<Integer, Integer> countMap;
	
	public LRUCache(int capacity) {
		this.capacity=capacity;
		cache=new HashMap<Integer, Integer>();
		queue=new LinkedList<>();
		countMap=new HashMap<Integer, Integer>();
	}

	public int get(int key) {
		if(cache.containsKey(key)) {
			queue.add(key);
			countMap.put(key, countMap.get(key)+1);
			return cache.get(key);			
		}
		return -1;
	}

	public void put(int key, int value) {
        if(!cache.containsKey(key) && cache.size()>=capacity) {
			while (!queue.isEmpty()) {
				int a=queue.poll();
				if(countMap.get(a)==1)
				{
					countMap.remove(a);
					cache.remove(a);
					break;
				}else {
					countMap.put(a, countMap.get(a)-1);
				}				
			}
		}
		if(cache.containsKey(key))
			countMap.put(key, countMap.get(key)+1);
		else countMap.put(key, 1);
		cache.put(key, value);
		queue.add(key);
    }


}
